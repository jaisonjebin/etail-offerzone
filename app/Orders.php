<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
    use SoftDeletes;
    protected  $table   =   'orders';

    protected $hidden = array('order_details','razorpay_order_id', 'razorpay_order_details','payment_details','payment_request_details');

    public function getDetails()
    {
        return $this->hasMany('App\OrderDetails', 'order_id','id');
    }
}
