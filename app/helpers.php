<?php
function flash($text='A test Notification',$type='info')
{
    session()->flash('text',$text);
    session()->flash('type',$type);

}

function district($id)
{
    return \DB::table('district')->whereIn('id',$id)->pluck('name')->toArray();
}

function state($id)
{
    return \DB::table('states')->whereIn('id',$id)->pluck('name')->toArray();
}

function country($id)
{
    $country = \DB::table('countries')->select('name')->where('id',$id)->first();
    return $country->name;
}

function city($id)
{
    return \DB::table('cities')->whereIn('id',$id)->pluck('name')->toArray();
}

function user($id)
{
    $user   =   \App\User::find(substr($id,4));
    return $user;
}

/*
|--------------------------------------------------------------------------
| Detect Active Route
|--------------------------------------------------------------------------
|
| Compare given route with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
function isActiveRoute($route, $output = 'active')
{
    if (Route::currentRouteName() == $route)
    {
        return $output;
    }
}
/*
|--------------------------------------------------------------------------
| Detect Active Routes
|--------------------------------------------------------------------------
|
| Compare given routes with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
function areActiveRoutes(Array $routes,$output = 'active open')
{
    foreach ($routes as $route)
    {
        if (Route::currentRouteName() == $route)
        {
            return $output;
        }
    }
}


function getReferalId()
{
    if(Auth::check()){
        if(Auth::user()->type == 'freelancer'){
            return 'DDFL'.Auth::user()->id;
        }
        else{
            return 'DDUR'.Auth::user()->id;
        }

    }
    else{
        return '';
    }

}


function getRating($id,$type)
{
    $total_rating    =   Review::where('product_id',$id)->where('product_type',$type)->avg('rating');
    return $total_rating;
}