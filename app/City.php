<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table    =   'cities';
    protected $fillable =   ['name','district_id','status','state_id'];

    public function getDistrict()
    {
        return $this->belongsTo('App\District', 'district_id','id');
    }
}
