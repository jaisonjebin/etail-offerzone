<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table    =   'wallets';

    public function getWalletTransactions()
    {
        return $this->hasMany('App\WalletTransaction', 'wallet_id', 'id')->orderBy('created_at' ,'desc');
    }


}
