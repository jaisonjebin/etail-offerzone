<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MerchantStore extends Model
{
    use SoftDeletes;
    protected  $table   =   'merchant_store';

    public function getOwner()
    {
        return $this->belongsTo('App\User', 'merchant_id','id');
    }
}

