<?php
namespace App\Scopes;

use App\MerchantDeal;
use App\MerchantStore;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use DB;
class LocationScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    protected $lat = 0,$long=0,$distance=30;

    public function __construct($lat,$long,$distance=1)
    {
        $this->lat = $lat;
        $this->long = $long;
        $this->distance = $distance;
    }

    public function apply(Builder $builder, Model $model)
    {
//        return $builder->where('id',  2);
         $builder->whereRaw('( 3959 * acos( cos( radians('.$this->lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$this->long.') ) + sin( radians('.$this->lat.') ) * sin( radians( latitude ) ) ) )<'.$this->distance);
    }
}