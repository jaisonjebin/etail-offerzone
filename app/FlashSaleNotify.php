<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FlashSaleNotify extends Model
{
    use SoftDeletes;
	protected  $table   =   'flashsalenotify';

    public function getItem()
    {
        return $this->belongsTo('App\FlashSale', 'item_id','id');
    }
}
