<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class LuckyDrawAds extends Model
{
    use SoftDeletes;
    protected  $table   =   'luckydraw_ads';

    public function getLuckyDraw()
    {
        return $this->belongsTo('App\LuckyDraw', 'luckydraw_id','id');
    }
}
