<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use App\Scopes\LocationScope;
use DB;
use Illuminate\Support\Facades\Session;

class MerchantDeal extends Model
{
	/*
		status 0 : freelancer approved
		status 1 : admin approved
		status 2 : mercant added & freelancer approval pending
		status 3 : freelancer rejected
		status 4 : admin rejected
	*/
	
    use SoftDeletes;
    protected  $table   =   'merchant_deal';
    protected $lat = 0,$lng=0,$radius=60;
//    protected $lat = 8.5207295,$long=76.94228729999998,$radius=30;

//    public function scopeLocation($query)
//    {
////        return $query->where('id',  2);
//       return  $query->whereRaw('( 3959 * acos( cos( radians(11.1544661) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(75.8807229) ) + sin( radians(11.1544661) ) * sin( radians( latitude ) ) ) )<1');
//    }

    protected static function boot()
    {
//        $lat = 0;
//        $long=0;
//        $radius=30;
        $lat = Session::get('latitude')?Session::get('latitude'):'11.1565429';
        $lng=Session::get('longitude')?Session::get('longitude'):'75.9487427';
//        dd($lat.' - '.$lng);
        $radius=30;
        parent::boot();

        if(Auth::check() && Auth::user()->type!='user'){

        }else{
            static::addGlobalScope(new LocationScope($lat,$lng,$radius));
        }

    }

    public function getOwner()
    {
        return $this->belongsTo('App\MerchantStore', 'store_id','id');
    }

    public function getStore()
    {
        return $this->belongsTo('App\MerchantStore', 'store_id','id');
    }
	
    public function getMerchant()
    {
        return $this->belongsTo('App\User', 'merchant_id','id');
    }

    public function getCategory()
    {
        return $this->belongsTo('App\Category', 'category_id','id');
    }

    public function isFavourited()
    {
        return $this->hasMany('App\Favorites', 'item_id')
        ->where('users_id', Auth::id())
        ->where('type', 'dealItem');
    }

    public function isPurchased()
    {
        return $this->hasMany('App\OrderDetails', 'product_id')
            ->where('users_id', Auth::id())
            ->where('product_type', 'deal')
            ->where('purchase_complete', '1');
    }

    public function userFavorites()
    {
        return $this->hasMany('App\Favorites', 'item_id')
        ->where('type', 'dealItem');
    }

    public function getReviewsAvg()
    {
        return $this->hasMany('App\Review', 'product_id')
            ->select('rating',DB::raw('avg(rating) as ratingTotal'))
            ->where('product_type', 'deal');
    }

    public function getReviews()
    {
        return $this->hasMany('App\Review', 'product_id')->where('product_type', 'deal');
    }

    public function getReviewsSummary()
    {
        return $this->hasMany('App\Review', 'product_id')->where('product_type', 'deal')->groupBy('rating');
    }
}
