<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Depsimon\Wallet\HasWallet;
class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    use HasWallet;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'otpVerified','otpNum',
    ];
    public function getCity()
    {
        return $this->hasOne('App\City', 'id','city');
    }
    public function getDistrict()
    {
        return $this->hasOne('App\District', 'id','district');
    }

//    public function getReferrer ()
//    {
//        return $this->hasOne('App\User','','');
//    }

}
