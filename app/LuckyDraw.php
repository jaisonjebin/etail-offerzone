<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class LuckyDraw extends Model
{
    use SoftDeletes;
    protected  $table   =   'luckydraw';

    public function getOwner()
    {
        return $this->belongsTo('App\User', 'added_by','id');
    }
}
