<?php

namespace App\Http\ViewComposers;

use App\Cart;
use App\Favorites;
use Illuminate\Contracts\View\View;
use Auth;

class NavComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $cartCount  =   0;
        $favCount   =   0;
        if(Auth::check())
        {
            $cartCount  =   Cart::where('user_id',Auth::user()->id)->where('status',1)->count();
            $favCount  =   Favorites::where('users_id',Auth::user()->id)->count();
        }

        $view->with(['cartCount'=>$cartCount,'favCount'=>$favCount]);
    }
}