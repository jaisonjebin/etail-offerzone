<?php

namespace App\Http\Controllers;

use App\MerchantDeal;
use App\Settings;
use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\WalletController;
class CommissionSplitController extends Controller
{
    public function split($order,$id=null)
    {
        if($id==null&&Auth::check())
        {
            $id=Auth::user()->id;
        }
        $user_referee   =   User::find(substr(Auth::user()->ref_code, 4));



        $order_details   =   json_decode($order->order_details,true);
        if(isset($order_details['deals'] ))
        {
            foreach ($order_details['deals'] as $deal)
            {
            $totalCommissionAmount   =   0;
            $merchant_referee='';
            $freelancer_referee='';

            $deal_db    =   MerchantDeal::find($deal['id']);
            $totalCommissionAmount   =   $deal_db->commission_price*$deal['quantitySale'];
            $merchant   =   User::find($deal['merchant_id']);
            if(isset($merchant->ref_code))
            {
                $merchant_referee   =   User::find(substr($merchant->ref_code, 4));
                $freelancer_referee   =   User::find(substr($merchant_referee->ref_code, 4));
            }

            $commissionSettings =   Settings::where('title','referalPlan')->first();
            $commissionSettings =   json_decode($commissionSettings->data);

            $walletObject   =   new WalletController();
                $data   =   [];
//                    $data['action']  =   'deposit';
                $data['type']  =   'myPurchase';
                $data['description']  =   'Deposited for purchase of '.$deal_db->title;
                $data['user_id']  =   Auth::user()->id;
                $data['user_name']  =  Auth::user()->name;
                $data['order']  =   $deal['id'];
                if(($deal_db->cashbackamount!='')||($deal_db->cashbackamount!=0))
                {
                    $walletObject->deposit(Auth::user()->id,$deal_db->cashbackamount??0,'deposit',$data);
                }

            if($commissionSettings)
            {
                if(($user_referee)&&(isset($commissionSettings->for_referrer)))
                {
                    $commissionAmount   =   0;
                    $commissionAmount   =   (($commissionSettings->for_referrer/100)*$totalCommissionAmount);
                    $totalCommissionAmount  =   $totalCommissionAmount-$commissionAmount;
                    $data   =   [];
//                    $data['action']  =   'deposit';
                    $data['type']  =   'clubPoints';
                    $data['description']  =   'Deposited for purchase of '.$deal_db->title.' by '.Auth::user()->name;
                    $data['user_id']  =   Auth::user()->id;
                    $data['user_name']  =  Auth::user()->name;
                    $data['order']  =   $deal['id'];
                    if(($commissionAmount!='')||($commissionAmount!=0))
                    {
                        $walletObject->deposit($user_referee->id,$commissionAmount??0,'deposit',$data);
                    }

                }
                if(($merchant_referee)&&(isset($commissionSettings->for_freelancer)))
                {
                    $commissionAmount   =   0;
                    $commissionAmount   =   (($commissionSettings->for_freelancer/100)*$totalCommissionAmount);
                    $totalCommissionAmount  =   $totalCommissionAmount-$commissionAmount;
                    $data   =   [];
//                    $data['action']  =   'deposit';
                    $data['type']  =   'freelancerCommission';
                    $data['description']  =   'Deposited for purchase of '.$deal_db->title.' by '.Auth::user()->name;
                    $data['user_id']  =   Auth::user()->id;
                    $data['user_name']  =  Auth::user()->name;
                    $data['order']  =   $deal['id'];
                    if(($commissionAmount!='')||($commissionAmount!=0))
                    {
                        $walletObject->deposit($merchant_referee->id,$commissionAmount??0,'deposit',$data);
                    }

                }
                if(($freelancer_referee)&&(isset($commissionSettings->for_super_freelancer)))
                {
                    $commissionAmount   =   0;
                    $commissionAmount   =   (($commissionSettings->for_super_freelancer/100)*$totalCommissionAmount);
                    $totalCommissionAmount  =   $totalCommissionAmount-$commissionAmount;
                    $data   =   [];
//                    $data['action']  =   'deposit';
                    $data['type']  =   'superfreelancerCommission';
                    $data['description']  =   'Deposited for purchase of '.$deal_db->title.' by '.Auth::user()->name;
                    $data['user_id']  =   Auth::user()->id;
                    $data['user_name']  =  Auth::user()->name;
                    $data['order']  =   $deal['id'];
                    if(($commissionAmount!='')||($commissionAmount!=0))
                    {
                        $walletObject->deposit($freelancer_referee->id,$commissionAmount??0,'deposit',$data);
                    }
                }
                if((Auth::user()->id)&&(isset($commissionSettings->for_purchaser)))
                {
                    $commissionAmount   =   0;
                    $commissionAmount   =   (($commissionSettings->for_purchaser/100)*$totalCommissionAmount);
                    $totalCommissionAmount  =   $totalCommissionAmount-$commissionAmount;
                    $data   =   [];
//                    $data['action']  =   'deposit';
                    $data['type']  =   'superfreelancerCommission';
                    $data['description']  =   'Deposited for purchase of '.$deal_db->title.' by '.Auth::user()->name;
                    $data['user_id']  =   Auth::user()->id;
                    $data['user_name']  =  Auth::user()->name;
                    $data['order']  =   $deal['id'];
                    if(($commissionAmount!='')||($commissionAmount!=0))
                    {
                        $walletObject->deposit(Auth::user()->id,$commissionAmount??0,'deposit',$data);
                    }
                }

                $adminUser  =   User::find(0);
                if(($adminUser)&&(isset($commissionSettings->for_purchaser)))
                {
//                    $commissionAmount   =   0;
//                    $commissionAmount   =   (($commissionSettings->for_purchaser/100)*$totalCommissionAmount);
//                    $totalCommissionAmount  =   $totalCommissionAmount-$commissionAmount;
                    $data   =   [];
//                    $data['action']  =   'deposit';
                    $data['type']  =   'superfreelancerCommission';
                    $data['description']  =   'Deposited for purchase of '.$deal_db->title.' by '.Auth::user()->name;
                    $data['user_id']  =   Auth::user()->id;
                    $data['user_name']  =  Auth::user()->name;
                    $data['order']  =   $deal['id'];
                    if(($totalCommissionAmount!='')||($totalCommissionAmount!=0))
                    {
                        $walletObject->deposit($adminUser->id,$totalCommissionAmount??0,'deposit',$data);
                    }
                }

            }
            else
            {

            }
        }
        }
        return true;

    }
}
