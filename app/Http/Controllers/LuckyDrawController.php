<?php

namespace App\Http\Controllers;

use App\LuckyDraw;
use App\LuckyDrawAds;
use App\LuckyDrawParticipants;
use App\LuckyDrawWinners;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Auth;

class LuckyDrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $luckydraws =   LuckyDraw::with('getOwner')->where('status',1)->get();
        return view('backend.listLuckyDraw',compact('luckydraws'));
    }

    public function advtLuckyDraw_index($id)
    {
        $advts  =   LuckyDrawAds::with('getLuckyDraw')->where('luckydraw_id',$id)->where('status',1)->get();
        return view('backend.advtLuckyDraw',compact(['advts','id']));
    }

    public function getParticipants($id){

        $results = DB::select('select users.name,users.email,users.phone, sum(chances) as points from luckydraw_chances,users where luckydraw_id = ?  and luckydraw_chances.users_id=users.id group by luckydraw_chances.users_id order by points desc', [$id]);
        return view('backend.participants',compact(['results']));
    }

    public function advtLuckyDraw_store(Request $request)
    {
        $rules=array(
            'title'    =>  'required',
            'image'    =>  'required|image',
            'luckydraw_id'    =>  'required',
        );
        $validate= Validator::make($request->all(),$rules);
        if($validate->fails())
        {
            return back()->withInput($request->all())->withErrors($validate);
        }
        $luckydrawads  =   new LuckyDrawAds;
        $luckydrawads->title   =   $request->title;
        if ($request->hasFile('image'))
        {
            $path = $request->file('image')->store('luckydraw/ads','public_uploads');
        }
        $luckydrawads->image   =   $path;
        $luckydrawads->luckydraw_id   =   $request->luckydraw_id;
        $luckydrawads->status   =   1;
        $luckydrawads->save();
        flash('Lucky Draw Adverisment added successfully','success');
        return back();
    }
    public function advtLuckyDraw_delete($id)
    {
        LuckyDrawAds::destroy($id);
        flash('Lucky Draw Advertiment Deleted','success');
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.addLuckyDraw');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=array(
            'title'    =>  'required',
            'banner'    =>  'required|image',
            'start_time'    =>  'required',
            'end_time'    	=>  'required',
            'question'    	=>  'required',
            'option_1'    	=>  'required',
            'option_2'    	=>  'required',
            'option_3'    	=>  'required',
            'option_4'    	=>  'required',
            'answer'    	=>  'required',
        );
        $validate= Validator::make($request->all(),$rules);
        if($validate->fails())
        {
            return back()->withInput($request->all())->withErrors($validate);
        }
        $luckydraw  =   new LuckyDraw;
        $luckydraw->title   =   $request->title;
        if ($request->hasFile('banner'))
        {
            $path = $request->file('banner')->store('luckydraw/banner','public_uploads');
        }
        $luckydraw->banner   =   $path;
        $luckydraw->description   =   $request->description;
        $luckydraw->start_time   =   Carbon::createFromFormat('d F Y - H:i', $request->start_time);
        $luckydraw->end_time   =   Carbon::createFromFormat('d F Y - H:i', $request->end_time);
        $luckydraw->question   =   $request->question;
        $luckydraw->option_1   =   $request->option_1;
        $luckydraw->option_2   =   $request->option_2;
        $luckydraw->option_3   =   $request->option_3;
        $luckydraw->option_4   =   $request->option_4;
        $luckydraw->answer   =   $request->answer;
        $luckydraw->status   =   1;
        $luckydraw->ad_flag   =   isset($request->ad_flag)?$request->ad_flag:0;
        $luckydraw->added_by   =   Auth::user()->id;
        $luckydraw->save();
        flash('Lucky Draw added successfully','success');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $luckyDraw  =   LuckyDraw::find($id);
        if($luckyDraw)
        {
            return view('backend.editLuckyDraw',compact('luckyDraw'));
        }
        else
        {
            flash('Lucky Draw Not Found','warning');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $rules=array(
            'title'    =>  'required',
            'start_time'    =>  'required',
            'end_time'    	=>  'required',
        );
        $validate= Validator::make($request->all(),$rules);
        if($validate->fails())
        {
            return back()->withInput($request->all())->withErrors($validate);
        }

        $luckydraw  =   LuckyDraw::find($request->id);
        if(!$luckydraw)
        {
            flash('Lucky Draw Not Found','warning');
            return back();
        }
        $luckydraw->title   =   $request->title;
        if ($request->hasFile('banner'))
        {
            $path = $request->file('banner')->store('luckydraw/banner','public_uploads');
            $luckydraw->banner   =   $path;
        }
        $luckydraw->description   =   $request->description;
        $luckydraw->start_time   =   Carbon::createFromFormat('d F Y - H:i', $request->start_time);
        $luckydraw->end_time   =   Carbon::createFromFormat('d F Y - H:i', $request->end_time);
        $luckydraw->ad_flag   =   isset($request->ad_flag)?$request->ad_flag:0;
        $luckydraw->added_by   =   Auth::user()->id;
        $luckydraw->save();
        flash('Lucky Draw added successfully','success');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        LuckyDraw::destroy($id);
        flash('Lucky Draw Deleted','success');
        return back();
    }

    public function addWinners()
    {
        $winners    =   LuckyDrawWinners::all();
        return view('backend.addWinners',compact('winners'));
    }
    public function addWinnerspost(Request $request)
    {
        $rules=array(
            'name'    =>  'required',
            'image'    =>  'image',
        );
        $validate= Validator::make($request->all(),$rules);
        if($validate->fails())
        {
            return back()->withInput($request->all())->withErrors($validate);
        }
        $winner =   new LuckyDrawWinners;
        $winner->name   =   $request->name;
        $winner->description    =   $request->description;
        if ($request->hasFile('image'))
        {
            $path = $request->file('image')->store('luckydraw/winner','public_uploads');
            $winner->image_url   =   $path;
        }
        $winner->save();
        flash('Lucky Draw Winner added','success');
        return back();
    }

    public function deleteWinners($id)
    {
        LuckyDrawWinners::destroy($id);
        flash('Lucky Draw Winner Deleted','success');
        return back();
    }
}
