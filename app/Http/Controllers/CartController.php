<?php

namespace App\Http\Controllers;

use App\MerchantDeal;
use App\OrderDetails;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Cart;
use Auth;
class CartController extends Controller
{
    public function add($pid,$type,$quantity=1)
    {
        $product='';
        if($type=='deal')
        {
            $product = MerchantDeal::where('id',$pid)->with('getOwner')->where('start_time', '<=', Carbon::now())->where('status',1)->first();
            $limit = $product->userBuyLimit;
            $orderDetailCount = OrderDetails::
            where('product_id', $product->id)
                ->where('product_type', 'coupon')
                ->where('users_id', Auth::user()->id)
                ->sum('quatity');

            $cartItem = Cart::where('product_id',$pid)->where('user_id',Auth::user()->id)->first();
            if($cartItem)
            {
                if (($orderDetailCount + $quantity + $cartItem->quantity) > $limit) {
                    return response()->json(['msg'=>'Item reached your purchase limit','status'=>'error'],404);
                }
            }
            if (($orderDetailCount + $quantity) <= $limit) {

            }
            else{
                return response()->json(['msg'=>'Item reached your purchase limit','status'=>'error'],404);
            }
        }
        else
        {
            $product='';
        }
        if($product)
        {
            $cartItem = Cart::updateOrCreate(
                ['user_id' => Auth::user()->id,'status' => 1,'product_id'=>$product->id,'type'=>$type],
                ['quantity' => \DB::raw('quantity + '.$quantity),'name'=>$product->title,'price'=>$product->online_sell_price,'meta'=>json_encode(['product_img'=>url('/uploads/'.$product->small_image)])]
            );
            return response()->json(['msg'=>'Item added to cart','status'=>'success'],200);
        }
        else
        {
            return response()->json(['msg'=>'Sorry No Such Item','status'=>'error'],404);
        }

    }

    public function show($id=null)
    {
        if($id==null)
        {
            $id=Auth::user()->id;
        }
        $cartItems   =   Cart::where('user_id',$id)->where('status',1)->get();
        if($cartItems)
        {
            return response()->json(['cartItems'=>$cartItems,'status'=>'success'],200);
        }
        else
        {
            return response()->json(['msg'=>'Sorry item in cart','status'=>'error'],404);
        }
    }

    public function count($id,$quantity)
    {
        $cartItem = Cart::where('id',$id)->where('user_id',Auth::user()->id)->first();

        if($cartItem)
        {
            if($quantity>=1)
            {
                $cartItem->quantity   =   $quantity;
                $cartItem->save();
                return response()->json(['msg'=>'Item added to cart','type'=>'updated','status'=>'success'],200);
            }
            else
            {
                $cartItem->status   =   0;
                $cartItem->save();
                $cartItem->delete();
                return response()->json(['msg'=>'Item removed from cart','type'=>'deleted','status'=>'success'],200);
            }

        }
        else
        {
            return response()->json(['msg'=>'Sorry item in cart','status'=>'error'],404);
        }
    }

    public function destroy($id)
    {
        $cartItem = Cart::where('id',$id)->where('user_id',Auth::user()->id)->first();

        if($cartItem)
        {
            $cartItem->status   =   0;
            $cartItem->save();
            $cartItem->delete();
            return response()->json(['msg'=>'Item removed from cart','status'=>'success'],200);
        }
        else
        {
            return response()->json(['msg'=>'Sorry item not in the cart','status'=>'error'],404);
        }
    }

    public static function getCount($id=null)
    {
        if($id==null)
        {
            $id=Auth::user()->id;
        }
        $count   =   Cart::where('user_id',$id)->where('status',1)->count();
        return $count;
    }

    public function clearCart(){
        if(Cart::where('user_id',Auth::user()->id)->delete()){
            return response()->json(['msg'=>'Cart Cleared','status'=>'success'],200);
        }
        else{
            return response()->json(['msg'=>'Sorry cart could not clear','status'=>'error'],400);
        }

    }

}
