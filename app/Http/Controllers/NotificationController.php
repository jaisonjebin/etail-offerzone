<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\FlashSale;
use App\FlashSaleNotify;
use Carbon\Carbon;

class NotificationController extends Controller
{
    public function FlashSaleNotifyMe(Request $req, $itemId){

        $response = $this->FlashSaleNotifyMeApi($req, $itemId);
        flash($response['msg'],$response['status']);
        return back();
	}

	public function FlashSaleNotifyMeApi(Request $req, $itemId){

        if($req->email=='' && $req->phone==''){
            return (['msg'=>'Phone Number or Email is Required', 'status'=>'error']);
        }
        $flashSale =  FlashSale::find($itemId);
        if($flashSale){
            $flashSaleCheck =  FlashSaleNotify::where('item_id', $itemId)
                ->where('email', $req->email)
                ->where('notificationSent', '0')->count();
            if($flashSaleCheck==0){
                $notif = new FlashSaleNotify();
                $notif->item_id = $itemId;
                $notif->email = $req->email;
                $notif->phone = $req->phone;
                $notif->save();

                return (['msg'=>'You are now subscribed this Flash Sale. You will be notified when sale starts', 'status'=>'success']);

            }
            else{

                return (['msg'=>'You already Subscribed this Flash Sale. You will ne notified when sale starts', 'status'=>'success']);

            }
        }
        else{

            return (['msg'=>'Item not Found', 'status'=>'error']);
        }
    }
	
	public function cronGetFlashSaleNotifications(){
		
        $flashSales =   FlashSale::with('getOwner')->where('start_time', '>=', Carbon::now())->where('start_time', '<=', Carbon::now()->addMinutes(15))->where('status',1)->orderBy('start_time', 'asc')->get();
		
		foreach($flashSales as $flashSale){
			$subscribers = FlashSaleNotify::where('item_id', $flashSale->id)
								->where('notificationSent', '0')->get();
			foreach($subscribers as $subscriber){
				echo $this->sentEMail($subscriber->email,'Sale starts in less than 15 minutes for '.$flashSale->title);
				$subscriber->notificationSent ='1';
				$subscriber->save();
			}
		}
		
		
	}
	
	public static function sentEMail($email, $content){
			return 'Mail sent to '.$email.' content: '.$content;
	}
}
