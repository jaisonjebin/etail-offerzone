<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class DashBoardController extends Controller
{
    public function showDashboard()
    {
        $data['usersCount'] =   User::count();
        return view('backend.dashboard',compact('data'));
    }
}
