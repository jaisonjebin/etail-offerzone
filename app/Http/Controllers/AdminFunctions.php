<?php

namespace App\Http\Controllers;

use App\Wallet;
use App\User;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Category;
class AdminFunctions extends Controller
{
    public function profileEdit($id,$ref_code='dd')
    {
        if(Auth::user()->id==$id||Auth::user()->type=='admin')
        {
            $states  =   DB::table('states')->where('country_id',101)->select('id','name')->get();
            $user   =   User::find($id);
            if($user->type=='merchant')
            {
                $ob =   new MerchantController;
                return $ob->edit($id);
            }
            return view('backend.profile_edit')->with('user',$user)->with('type',$user->type)->with('states',$states);
        }
        else
        {
            flash('You Have No Permission','error');
            return back();
        }
    }

    public function getDistricts($id)
    {
        $districts  =   DB::table('district')->where('state_id',$id)->select('id','name')->get();
        return $districts;
    }

    public function getCities($id,$type='all')
    {
        if($type=='all')
        {
            $cities  =   DB::table('cities')->where('status',1)->where('state_id',$id)->select('id','name')->get();
        }
        else
        {
            $cities  =   DB::table('cities')->where('status',1)->where('district_id',$id)->select('id','name')->get();
        }
        return $cities;
    }

    public function showwallet()
    {
        $id = Auth::user()->id;
        $myPurchase =   [];
        $clubPoints =   [];
        $transactionsHistory    =   [];
        $accountSummary['totalPoints']=0;
        $accountSummary['clubPoints']=0;
        $accountSummary['myPurchasePoints']=0;
        $accountSummary['withdrawalPoints']=0;
        $transactions    =   Wallet::where('user_id',$id)->with('getWalletTransactions')->first();
        if(isset($transactions->getWalletTransactions))
        {
            foreach ($transactions->getWalletTransactions as $transaction)
            {
//            if(($transaction->updated_at->format('F')==date('F')))
//            {
                $accountSummary['totalPoints']=$accountSummary['totalPoints']+($transaction->type=='deposit'?($transaction->amount):0);
                $accountSummary['withdrawalPoints']=$accountSummary['withdrawalPoints']+($transaction->type=='withdraw'?($transaction->amount):0);
                if(json_decode($transaction->meta)) {
                    if (json_decode($transaction->meta)->type == 'clubPoints') {
                        $accountSummary['clubPoints'] = $accountSummary['clubPoints'] + ($transaction->type == 'deposit' ? ($transaction->amount) : (-$transaction->amount));
                    } elseif (json_decode($transaction->meta)->type == 'myPurchase') {
                        $accountSummary['myPurchasePoints'] = $accountSummary['myPurchasePoints'] + ($transaction->type == 'deposit' ? ($transaction->amount) : (-$transaction->amount));
                    } else {

                    }
                }
//            }

                $temp   =   [];
                $temp['id']   =   $transaction->id;
                $temp['date']   =   $transaction->updated_at->format('d M,Y H:i');
                $temp['datetime']   =   $transaction->updated_at;
                $temp['transactionId']   =   $transaction->hash;
                $temp['type']   =   $transaction->type;
                $temp['metaType']   =   json_decode($transaction->meta)->type;
                $temp['details']   =   $transaction->meta;
                $temp['amount']   =   $transaction->amount;
                $transactionsHistory[]  =   $temp;
            }
        }

        return view('backend.your-wallet',compact(['accountSummary','transactionsHistory']));
    }
}
