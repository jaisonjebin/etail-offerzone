<?php

namespace App\Http\Controllers;

use App\Favorites;
use App\FlashSale;
use App\MerchantDeal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{

    public function addToFavorites($type,$id){


        $favoExists = Favorites::where('users_id',Auth::user()->id)
            ->where('type',$type)
            ->where('item_id',$id)
            ->first();

        if($favoExists){
            return response()->json(['status'=>'success','msg'=>'Already in Favorites']);
        }

        $favorites = new Favorites();
        $favorites->type  = $type;
        $favorites->item_id  = $id;
        $favorites->users_id  = Auth::user()->id;
        $favorites->save();

        return response()->json(['status'=>'success','msg'=>'Added to favorites']);



    }

    public function removeFromFavorites($type,$id){

        $favoExists = Favorites::where('users_id',Auth::user()->id)
            ->where('type',$type)
            ->where('item_id',$id)
            ->first();

        if($favoExists){
            $favoExists->delete();
        }

        return response()->json(['status'=>'success','msg'=>'Removed from favorites']);

    }

    public function listFavorites(){

        $favorite = Favorites::where('users_id', Auth::user()->id)->where('type','flashSaleItem')->pluck('item_id');
        $favo['flashSale']=[];
        $favo['dealItem']=[];
        if(count($favorite)){
             $tempFavo = FlashSale::whereIn('id',$favorite)->get();
             foreach($tempFavo as $tempfav){
                 $temp =[];
                 $temp['id'] = $tempfav['id'];
                 $temp['title'] = $tempfav['title'];
                 $temp['small_description'] = $tempfav['small_description'];
                 $temp['offer_price'] = $tempfav['offer_price'];
                 $temp['price'] = $tempfav['price'];
                 $temp['small_image'] = url('uploads/'.$tempfav['small_image']);
                 $favo['flashSale'][] = $temp;
             }
        }

        $favorite = Favorites::where('users_id', Auth::user()->id)->where('type','dealItem')->pluck('item_id');

        if(count($favorite)) {

            $tempDeals = MerchantDeal::whereIn('id', $favorite)->get();
            foreach($tempDeals as $tempDeal){
                $temp =[];
                $temp['id'] = $tempDeal['id'];
                $temp['title'] = $tempDeal['title'];
                $temp['small_description'] = $tempDeal['small_description'];
                $temp['price'] = $tempDeal['online_sell_price'];
                $temp['dealType'] = $tempDeal['dealType'];
                $temp['small_image'] = url('uploads/'.$tempDeal['small_image']);

                $favo['dealItem'][] = $temp;
            }
        }
        $favo['status'] = 'success';
        return response()->json($favo);



    }
}
