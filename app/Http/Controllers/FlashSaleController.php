<?php

namespace App\Http\Controllers;

use App\FlashSale;
use Illuminate\Http\Request;
use Auth;

class FlashSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $flashSales=   FlashSale::with('getOwner')->where('status',1)->get();
        return view('backend.listFlashSale',compact('flashSales'));

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.addFlashSale');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //validation


        $startTime = \DateTime::createFromFormat('j M Y - H:i', $request->start_time);
        $endTime = \DateTime::createFromFormat('j M Y - H:i', $request->end_time);

        $flashsale = new FlashSale;
        $flashsale->start_time = $startTime? $startTime->format('Y-m-d H:i:s') : date('Y-m-d G:i:s' ,strtotime('today')) ;
        $flashsale->end_time = $endTime? $endTime->format('Y-m-d H:i:s') : date('Y-m-d G:i:s' ,strtotime('tomorrow')) ;
        $flashsale->title = $request->title;
        $flashsale->small_description = $request->small_description;
        $flashsale->description = $request->description;
        $flashsale->price = $request->price;
        $flashsale->offer_price = $request->offer_price;
        $flashsale->stock = $request->stock;
        $flashsale->totalstock = $request->stock;
        $flashsale->termsConditions = $request->termsConditions;
        $flashsale->small_image = $request->small_image->store('flashsale/smallimages','public_uploads');
        $sponsersPath=[];
        $bannersPath=[];
        if($request->banners){
            foreach ($request->banners as $img){
                $bannersPath[] = $img->store('flashsale/banners','public_uploads');
            }
        }
        if($request->sponsers){
            foreach ($request->sponsers as $img){
                $sponsersPath[] = $img->store('flashsale/sponsers','public_uploads');
            }
        }

        $flashsale->banners = json_encode($bannersPath);
        $flashsale->sponsers = json_encode($sponsersPath);
        $flashsale->added_by = Auth::user()->id;
        $flashsale->status = 1;
        $flashsale->save();

        flash('Flash Sale added successfully','success');
        return redirect(route('admin.listFlashSale'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $flashSale  =   FlashSale::find($id);
        if($flashSale)
        {
            return view('backend.editFlashSale',compact('flashSale'));
        }
        else
        {
            flash('Flash Sale Not Found','warning');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $flashsale  =   FlashSale::find($request->id);

        $startTime = \DateTime::createFromFormat('j M Y - H:i', $request->start_time);
        $endTime = \DateTime::createFromFormat('j M Y - H:i', $request->end_time);
        $flashsale->start_time = $startTime? $startTime->format('Y-m-d H:i:s') : date('Y-m-d G:i:s' ,strtotime('today')) ;
        $flashsale->end_time = $endTime? $endTime->format('Y-m-d H:i:s') : date('Y-m-d G:i:s' ,strtotime('tomorrow')) ;
        $flashsale->title = $request->title;
        $flashsale->small_description = $request->small_description;
        $flashsale->description = $request->description;
        $flashsale->price = $request->price;
        $flashsale->offer_price = $request->offer_price;
        $flashsale->stock = $request->stock;
        $flashsale->totalstock = $request->stock;
        $flashsale->termsConditions = $request->termsConditions;
        if($request->has('small_image'))
        {
            $flashsale->small_image = $request->small_image->store('flashsale/smallimages','public_uploads');
        }
        $sponsersPath=[];

        if($request->sponsers_1){
            foreach ($request->sponsers_1 as $item)
            {
                if($item!='') {
                    $sponsersPath[] = $item;
                }
            }
        }

        if($request->sponsers)
        {
            foreach ($request->sponsers as $img){
                if($img!='') {
                    $sponsersPath[] = $img->store('flashsale/sponsers', 'public_uploads');
                }
            }
        }

        $bannersPath=[];
        foreach ($request->banners_1 as $item1)
        {
            if($item1!='') {
                $bannersPath[] = $item1;
            }
        }

        if($request->banners)
        {
            foreach ($request->banners as $img){
                if($img!='') {
                    $bannersPath[] = $img->store('flashsale/banners', 'public_uploads');
                }
            }
        }


        $flashsale->banners = json_encode($bannersPath);
        $flashsale->sponsers = json_encode($sponsersPath);
        $flashsale->added_by = Auth::user()->id;
        $flashsale->status = 1;
        $flashsale->save();

        flash('Flash Sale added successfully','success');
        return redirect(route('admin.listFlashSale'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FlashSale::destroy($id);
        flash('Flash Sale Deleted','success');
        return back();
    }
}
