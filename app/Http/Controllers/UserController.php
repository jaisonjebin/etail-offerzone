<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function userRegistration(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'otp' => 'required',
            'password_confirmation' => 'required',
            'password' => 'required|confirmed',
            'phone' => 'required',
            'district' => 'required',
            'city' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->getMessageBag()->first());
        }
        else if(Session::get('otp') != $request->otp){
            return back()->withInput()->withErrors(['otp'=>'Invalid  Otp']);
        }
        elseif(Session::get('mobile')!=$request->phone)
        {
            return back()->withInput()->withErrors(['phone'=>'Phone number does not match with OTP']);
        }
        elseif(User::where('phone',$request->phone)->first())
        {
            return back()->withInput()->withErrors(['phone'=>'Phone Number already resisted']);
        }
        else{
            $user = new User;
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->email = @$request->email;
            $user->district = @$request->district;
            $user->city = @$request->city;
            $user->password = Hash::make($request->password);
            if (session()->exists('referral'))
            {
                $user->ref_code = strtoupper(session()->get('referral'));
            }
            if($user->save()){
                Auth::attempt(['phone'=>$user->phone,'password'=>$request->password]);
            }
            return redirect()->intended('/home');
//            return back();
        }


    }

    public function list()
    {
        $users  =   User::where('type','user')->get();

        return view('backend.listUsers',compact('users'));
    }

    public function status($id)
    {
        $user   =   User::find($id);
        if($user)
        {
            if(Auth::user()->type=='freelancer')
            {
                $user->status   =   $user->status==2?0:($user->status==1?0:0);
            }
            if(Auth::user()->type=='admin')
            {
                $user->status = $user->status == 2 ? 1 : ($user->status == 1 ? 0 : 1);
            }
            $user->save();
            flash('User '.$user->name.'\'s Status Changed','success');
        }
        else
        {
            flash('No Such User','warning');
        }

        return back();
    }

    public function destroy($id)
    {
        $user   =   User::find($id);
        if($user)
        {
            $user->deleted_at   =   Carbon::now();
            $user->save();
            flash('User '.$user->name.' Deleted','success');
        }
        else
        {
            flash('No Such User','warning');
        }
        return back();
    }
}
