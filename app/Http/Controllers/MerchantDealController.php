<?php

namespace App\Http\Controllers;

use App\MerchantDeal;
use App\MerchantStore;
use App\Category;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;

class MerchantDealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if(Auth::user()->type=='freelancer'){
			$store_id = (MerchantStore::where('ref_code','DDFL'.Auth::user()->id)->pluck('id'));
			$merchantDeals = MerchantDeal::whereIn('store_id',$store_id)->get();
		}		
		else if(Auth::user()->type=='admin'){
			$merchantDeals=   MerchantDeal::with('getOwner')->get();
		}	
		else if(Auth::user()->type=='merchant'){

			$store_id = (MerchantStore::where('merchant_id',Auth::user()->id)->pluck('id'));
			$merchantDeals = MerchantDeal::whereIn('store_id',$store_id)->get();
		}
        return view('backend.listMerchantDeal',compact('merchantDeals'));

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$stores = [];
		if(Auth::user()->type =='admin'){
			$stores = MerchantStore::with('getOwner')->get();
		}
		else if(Auth::user()->type =='merchant'){
			$stores = MerchantStore::where('merchant_id',Auth::user()->id)->get();
		}
		else{
			return back();
		}
		$category = Category::where('status','1')->get();
		
        return view('backend.addMerchantDeal',compact('stores','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //validation
		$validator = Validator::make($request->all(), [
			'banners.*' => 'image',
			'sponsers.*' => 'image',
			'small_image' => 'image',
        ]);

        if ($validator->fails()) {
            //return $validator->errors();
            return back()->withInput()->withErrors($validator->errors());
        }
		
		$dealsObj = [];

        $startTime = \DateTime::createFromFormat('j M Y - H:i', $request->start_time);
        $endTime = \DateTime::createFromFormat('j M Y - H:i', $request->end_time);

        $merchantDeal = new MerchantDeal;
		
        $merchantDeal->start_time = $startTime? $startTime->format('Y-m-d H:i:s') : date('Y-m-d G:i:s' ,strtotime('today')) ;
        $merchantDeal->end_time = $endTime? $endTime->format('Y-m-d H:i:s') : date('Y-m-d G:i:s' ,strtotime('tomorrow')) ;
        $merchantDeal->title = $request->title;
        $merchantDeal->category_id = $request->category;
		
        $merchantDeal->small_description = $request->small_description;
        $merchantDeal->description = $request->description;
        $merchantDeal->price = $request->price;
        $merchantDeal->offer_price = $request->offer_price;

        $merchantDeal->stock = $request->stock;
        $merchantDeal->userBuyLimit = $request->userbuylimit;

        $merchantDeal->totalstock = $request->stock;
        $merchantDeal->termsConditions = $request->termsConditions;
        $merchantDeal->small_image = $request->small_image->store('merchantDeal/smallimages','public_uploads');
        $sponsersPath=[];
        $bannersPath=[];
        if($request->banners){
            foreach ($request->banners as $img){
                $bannersPath[] = $img->store('merchantDeal/banners','public_uploads');
            }
        }
        if($request->sponsers){
            foreach ($request->sponsers as $img){
                $sponsersPath[] = $img->store('merchantDeal/sponsers','public_uploads');
            }
        }

        $merchantDeal->banners = json_encode($bannersPath);
        $merchantDeal->sponsers = json_encode($sponsersPath);
        $merchantDeal->added_by = Auth::user()->id;		
        $merchantDeal->status = 2;

        if(Auth::user()->type == 'freelancer' || Auth::user()->type == 'admin'|| Auth::user()->type == 'merchant') {

            if ($request->offetType == 'voucher') {
                $merchantDeal->online_sell_price = $request->voucher_price ?? 0;
                $merchantDeal->dealType = 'voucher';
            }
            elseif ($request->offetType == 'coupon') {
                // voucher_price
                $merchantDeal->online_sell_price = $request->offer_price ?? 0;
                $merchantDeal->cashbackamount = $request->cashbackamount;
                $merchantDeal->dealType = 'coupon';
            }
            else {
                $merchantDeal->online_sell_price = $request->offer_price ?? 0;
                $merchantDeal->dealType = 'offer';
            }

            $merchantDeal->commission_price =  $request->commission_price??0;

        }


		if(Auth::user()->type=='admin'){
			$merchantDeal->status = 1;
		}
		
		$flag = 0;
		
		$merchantStoreList = MerchantStore::whereIn('id',$request->store)->get();
		foreach($merchantStoreList as $merchantStore){
			if($merchantStore)
			{
                $loc    =   json_decode($merchantStore->location);
                $merchantDeal->latitude = $loc->latitude;
                $merchantDeal->longitude = $loc->longitude;

				if(Auth::user()->type == 'admin'){
					$merchantDeal->merchant_id = $merchantStore->merchant_id;
					$merchantDeal->store_id = $merchantStore->id;
					$dealsObj[] = $merchantDeal->replicate();
				}
				else if (Auth::user()->type == 'merchant'){
					if($merchantStore->merchant_id == Auth::user()->id){
						
						$merchantDeal->merchant_id = $merchantStore->merchant_id;
						$merchantDeal->store_id = $merchantStore->id;
						$dealsObj[] = $merchantDeal->replicate();				
					}	
					else{
						$flag++;
						flash('Not Allowed :P','error');
					}
				}
				else{
					$flag++;
					flash('Not Allowed','error');
				}
				
			}
			else{
				$flag++;
				flash('Store Not found. May be deleted','error');
			}
		}
		
        
		if($flag != 0){
			return back();
		}
		foreach($dealsObj as $deal){
			$deal->save();
			flash('Deal added successfully','success');
		}	
        return redirect(route(Auth::user()->type.'.listMerchantDeal'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
		$category = Category::where('status','1')->get();
        $merchantDeal  =   MerchantDeal::find($id);
        if($merchantDeal)
        {
			if(Auth::user()->type=='freelancer'){
				$store_data = MerchantStore::where('id',$merchantDeal->store_id)->where('ref_code','DDFL'.Auth::user()->id)->first();
				if(!$store_data){
					flash('You dont have the permission','success');
					return back();
				}
			}		
			else if(Auth::user()->type=='merchant'){
				if(Auth::user()->id != $merchantDeal->merchant_id){
					flash('You dont have the permission','success');
					return back();
				}
			}
		
            return view('backend.editMerchantDeal',compact('merchantDeal','category'));
        }
        else
        {
            flash('Deal Not Found','warning');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $merchantDeal  =   MerchantDeal::find($request->id);
		
        $startTime = \DateTime::createFromFormat('j M Y - H:i', $request->start_time);
        $endTime = \DateTime::createFromFormat('j M Y - H:i', $request->end_time);
        $merchantDeal->start_time = $startTime? $startTime->format('Y-m-d H:i:s') : date('Y-m-d G:i:s' ,strtotime('today')) ;
        $merchantDeal->end_time = $endTime? $endTime->format('Y-m-d H:i:s') : date('Y-m-d G:i:s' ,strtotime('tomorrow')) ;
        $merchantDeal->title = $request->title;		
        $merchantDeal->category_id = $request->category;
        $merchantDeal->small_description = $request->small_description;
        $merchantDeal->description = $request->description;
        $merchantDeal->price = $request->price??0;
        $merchantDeal->offer_price = $request->offer_price??0;
        $merchantDeal->userBuyLimit = $request->userbuylimit??0;
        $merchantDeal->stock = $request->stock;
        $merchantDeal->totalstock = $request->stock;
        $merchantDeal->termsConditions = $request->termsConditions;

        if(Auth::user()->type == 'freelancer' || Auth::user()->type == 'admin'|| Auth::user()->type == 'merchant') {
            if ($request->offetType == 'voucher') {
                $merchantDeal->online_sell_price = $request->voucher_price;
                $merchantDeal->dealType = 'voucher';
            }
            elseif ($request->offetType == 'coupon') {
                $merchantDeal->online_sell_price = $request->offer_price;
                $merchantDeal->cashbackamount = $request->cashbackamount;
                $merchantDeal->dealType = 'coupon';
            }else {
                $merchantDeal->online_sell_price = $request->offer_price;
                $merchantDeal->dealType = 'offer';
            }

            $merchantDeal->commission_price =  $request->commission_price??0;
        }


        if($request->has('small_image'))
        {
            $merchantDeal->small_image = $request->small_image->store('merchantDeal/smallimages','public_uploads');
        }
        $sponsersPath=[];
        if($request->sponsers_1)
        {
            foreach ($request->sponsers_1 as $item)
            {
                if($item!='') {
                    $sponsersPath[] = $item;
                }
            }
        }

        if($request->sponsers)
        {
            foreach ($request->sponsers as $img){
                if($img!='') {
                    $sponsersPath[] = $img->store('merchantDeal/sponsers', 'public_uploads');
                }
            }
        }

        $bannersPath=[];
        if($request->banners_1){
            foreach ($request->banners_1 as $item1)
            {
                if($item1!='') {
                    $bannersPath[] = $item1;
                }
            }
        }

        if($request->banners)
        {
            foreach ($request->banners as $img){
                if($img!='') {
                    $bannersPath[] = $img->store('merchantDeal/banners', 'public_uploads');
                }
            }
        }


        $merchantDeal->banners = json_encode($bannersPath);
        $merchantDeal->sponsers = json_encode($sponsersPath);
        $merchantDeal->added_by = Auth::user()->id;
		$merchantDeal->status = 2;
		if(isset($request->status)){
			if(Auth::user()->type == 'freelancer'){
				if(in_array($request->status,['0','2','3'])){
					$merchantDeal->status = $request->status;
				}
				else{
					flash('You dont have the permission to change that.','error');
					return back();
				}
			}
			if(Auth::user()->type == 'admin'){
				$merchantDeal->status = $request->status;
			}
		}
        
        $merchantDeal->save();

        flash('Deal updated successfully','success');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MerchantDeal::destroy($id);
        flash('Deal Deleted','success');
        return back();
    }


    public function addToTopPicks($id,$status){
        if(Auth::user()->type=='admin'){
            $merchantDeal  =   MerchantDeal::find($id);
            if($merchantDeal){
                $merchantDeal->toppicks = $status;
                $merchantDeal->save();
                return response()->json(['status'=>true,'value'=>$status]);
            }
            else{
                return response()->json(['status'=>false]);
            }
        }
        else{
            return response()->json(['status'=>false]);
        }
    }

    public function addToDealoftheDay($id,$status){
        if(Auth::user()->type=='admin'){
            $merchantDeal  =   MerchantDeal::find($id);
            if($merchantDeal){
                $merchantDeal->dealoftheday = $status;
                $merchantDeal->save();
                return response()->json(['status'=>true,'value'=>$status]);
            }
            else{
                return response()->json(['status'=>false]);
            }
        }
        else{
            return response()->json(['status'=>false]);
        }
    }

}
