<?php

namespace App\Http\Controllers;

use App\MerchantDeal;
use App\Review;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\FlashSale;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id,$type)
    {
        $reviews    =   Review::where('product_id',$id)->where('product_type',$type)->get();
        return $reviews;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'product_type' => 'required',
            'rating' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails())
        {
            return back()->withInput()->withErrors($validator->getMessageBag()->first());
        }
        else
        {
            if($request->product_type=='flashSale')
            {
                $product    =   FlashSale::find($request->product_id);
            }
            elseif($request->product_type=='deal')
            {
                $product    =   MerchantDeal::find($request->product_id);
            }
            else
            {
                return back()->withInput()->withErrors(['rating'=>'Product not found']);
            }
            if((!$product)||(!$product->isPurchased()))
            {
                return back()->withInput()->withErrors(['rating'=>'Product not found']);
            }
            $review =   Review::where('user_id',Auth::user()->id)->where('product_id',$request->product_id)->where('product_type',$request->product_type)->first();
            if(!$review)
            {
                $review =   new Review;
            }
            $review->user_id    =   Auth::user()->id;
            $review->product_id    =   $request->product_id;
            $review->product_type    =   $request->product_type;
            $review->rating    =   $request->rating;
            $review->description    =   $request->description;
            $review->save();
            $product->avg_rating    =   number_format($product->getReviewsAvg()->first()->ratingTotal,1);
            $product->save();
            flash('Thanks for review.','success');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
