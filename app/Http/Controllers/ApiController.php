<?php

namespace App\Http\Controllers;

use App\Category;
use App\DeliveryAddress;
use App\FlashSale;
use App\LuckyDraw;
use App\LuckyDrawAds;
use App\LuckyDrawParticipants;
use App\MerchantDeal;
use App\OrderDetails;
use App\Orders;
use App\Review;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\LuckydrawChances;
use App\Settings;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Razorpay\Api\Api;


class ApiController extends Controller
{
    function registerUser(Request $request){

         $validator = Validator::make($request->all(), [
             'password' => 'required',
             'name' => 'required|min:3',
             'phone' => 'required',
         ]);

        if ($validator->fails()) {
            return $this->returnError($validator->getMessageBag()->first());
        }
        else{

            $user = User::where('phone',$request->phone)->first();
            if($user){
                return $this->returnError('Phone number already exists');
            }
            $newUser = new User();
            $newUser->name = $request->name;
            $newUser->email = @$request->email;
            $newUser->phone = $request->phone;
            $newUser->password = Hash::make($request->password);
            $newUser->_token = sha1(md5(uniqid().uniqid()));
            $newUser->otpVerified =False;
            $newUser->otpNum = rand(10000,99999);
            $newUser->save();

            $newUser->otpStatus = SmsController::sent_sms($request->phone,'OTP for DealDaa is '.$newUser->otpNum);

            return $this->returnSuccess(['userData'=>$newUser]);
        }

    }

    function resentOtp(Request $request){
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->returnError($validator->getMessageBag()->first());
        }
        else{
            $user = User::where('phone',$request->phone)->first();
            if($user){
                $user->otpVerified =False;
                $user->otpNum = rand(10000,99999);
                $user->save();
                $user->otpStatus = SmsController::sent_sms($request->phone,'OTP for DealDaa is '.$user->otpNum);
                return $this->returnSuccess(['UserExist'=>true, 'msg'=> 'OTP '.$user->otpStatus.' to given phone number']);
            }
            else{
                return $this->returnSuccess(['UserExist'=>false, 'msg'=> 'OTP not sent to given phone number']);
            }

        }
    }

    function otpVerify(Request $request){
        //dd($request->header('Authorization'));

//        file_put_contents('vivek.txt',json_encode($request->all()));
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->returnError($validator->getMessageBag()->first());
        }
        else{
            $user = User::where('phone',$request->phone)->first();
            if($user->otpNum == $request->otp){
                $user->otpVerified = 1;
                $user->_token = sha1(md5(uniqid().uniqid()));
                $user->save();
                return $this->returnSuccess(['userData'=>$user]);
            }
            else{
                return $this->returnError('Invalid OTP');
            }
        }

    }

    function loginUser(Request $request){
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->returnError($validator->getMessageBag()->first());
        }
        else{
            if (Auth::attempt(['phone' => $request->phone, 'password' => $request->password])) {
                if(Auth::user()->otpVerified){
                   if(Auth::user()->status == 1){
                       Auth::user()->_token = sha1(md5(uniqid().uniqid()));
                       Auth::user()->save();
                       return $this->returnSuccess(['userData'=>Auth::user()]);
                   }
                   else{
                       return $this->returnError('User not Enabled');
                   }
                }
                else{
                    return $this->returnError('Mobile number not Verified');
                }
            }
            else{
                return $this->returnError('Authentication Failed');
            }

        }
    }

    function home(Request $request){
//        Auth::loginUsingId(1, TRUE); //@todo:remove this

        $response['sliders'] = $this->getMobileSliders();
        $response['luckyDraw'] = $this->getLuckyDraw();
        $response['flashSales'] = $this->getFlashSale();
        $response['deals'] = $this->getDeals();
        $response['coupons'] = $this->getCoupons();
        $response['dealsOfTheDay'] = $this->getDealsoftheDay();
        $response['topPicks'] = $this->getTopPics();
        $response['categories'] = $this->getCategories();
        $response['cartCount'] = CartController::getCount();

        return $this->returnSuccess($response);

    }

    function getMobileSliders(){
        $slider = [];
        $deals = MerchantDeal::with('getOwner')->where('mobileSlider',  '1')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->inRandomOrder()->get();

        foreach ($deals as $deal){
            $sliderTemp['id'] = $deal->id;
            $sliderTemp['image'] = url('uploads/'.$deal->small_image);
            $sliderTemp['type'] = 'deal';
            $slider[] = $sliderTemp;
        }

        $flashSales = FlashSale::with('getOwner')->where('mobileSlider',  '1')->where('start_time', '<=', Carbon::now())->where('status',1)->where('stock','>',0)->orderBy('start_time', 'asc')->get();

        foreach ($flashSales as $flashSale){
            $sliderTemp['id'] = $flashSale->id;
            $sliderTemp['image'] = url('uploads/'.$flashSale->small_image);
            $sliderTemp['type'] = 'flashSale';
            $slider[] = $sliderTemp;
        }
        return ($slider);

    }

    function getLuckyDraw(){

        $luckyDraw = LuckyDraw::where('start_time','<=',Carbon::now())->where('end_time','>=',Carbon::now())->first();
        if($luckyDraw){
            $luckyDrawPart = LuckyDrawParticipants
                ::where('users_id',Auth::user()->id)
                ->where('luckydraw_id',$luckyDraw->id)
                ->orderBy('created_at', 'desc')->first();
            if($luckyDrawPart){
                $luckyDraw->participated = true;
            }
            else{
                $luckyDraw->participated = false;
            }
            $luckyDraw->banner = url('/uploads/'.$luckyDraw->banner);
            return $luckyDraw;
        }
        else{
            return null;
        }
    }

    function getFlashSale(){

        $flashSalesOrg = FlashSale::with('getOwner')->where('status',1)->where('stock','>',0)->orderBy('start_time', 'asc')->limit(8)->get();
        $flashSales = [];
        foreach ($flashSalesOrg as $flash){
            $temp = [];
            $temp['id'] = $flash->id;
            $temp['title'] = $flash->title;
            $temp['small_description'] = $flash->small_description;
            $temp['description'] = $flash->description;
            $temp['price'] = $flash->price;
            $temp['offer_price'] = $flash->offer_price;
            $temp['totalstock'] = $flash->totalstock;
            $temp['sold'] = $flash->sold;
            $temp['start_time'] = $flash->start_time;
            $temp['end_time'] = $flash->end_time;
            $temp['status'] = $flash->status;

            $temp['small_image'] = url('uploads/'.$flash->small_image);
            $flashSales[] = $temp;
        }

        return $flashSales;
    }

    function getFlashSaleSingle($flashSaleId){

        $flashSalesOrg = FlashSale::where('id',$flashSaleId)->with('getOwner')->where('start_time', '<=', Carbon::now())->where('status',1)->where('stock','>',0)->get();
        $flashSales = [];
        foreach ($flashSalesOrg as $flash){
            $temp = [];
            $temp['id'] = $flash->id;
            $temp['title'] = $flash->title;
            $temp['small_description'] = $flash->small_description;
            $temp['description'] = $flash->description;
            $temp['termsConditions'] = $flash->termsConditions;
            $temp['price'] = $flash->price;
            $temp['offer_price'] = $flash->offer_price;
            $temp['totalstock'] = $flash->totalstock;
            $temp['sold'] = $flash->sold;
            $temp['start_time'] = $flash->start_time;
            $temp['end_time'] = $flash->end_time;
            $temp['status'] = $flash->status;
            $temp['small_image'] = url('uploads/'.$flash->small_image);
            $temp['sponsers']   =   [];
            if($flash->sponsers) {
                foreach (json_decode($flash->sponsers) as $sponsers) {
                    $temp['sponsers'][] = url('uploads/' . $sponsers);
                }
            }
            $flashSales[] = $temp;
        }

        return response()->json(['flashSales'=>$flashSales,'type'=>'success'],200);
    }

    function getDeals(){

        $dealsOrg = MerchantDeal::with('getOwner')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->inRandomOrder()->limit(8)->get();
        $deals = $this->formatDeals($dealsOrg);

        return $deals;
    }

    function getDeal($dealId){

        $dealsOrg = MerchantDeal::where('id',$dealId)->with('getStore')->with('getCategory')->with('getOwner')->where('start_time', '<=', Carbon::now())->where('status',1)->get();
        $deals  =   [];
        foreach ($dealsOrg as $deal) {
            $temp = [];
            $temp['id'] = $deal->id;
            $temp['category_id'] = $deal->category_id;
            $temp['category_text'] = $deal->getCategory->name;
            $temp['title'] = $deal->title;
            $temp['small_description'] = $deal->small_description;
            $temp['description'] = $deal->description;
            $temp['dealType'] = $deal->dealType;
            $temp['price'] = $deal->price;
            $temp['offer_price'] = $deal->offer_price;
            $temp['online_sell_price'] = $deal->online_sell_price;
            $temp['totalstock'] = $deal->totalstock;
            $temp['sold'] = $deal->sold;
            $temp['start_time'] = $deal->start_time;
            $temp['end_time'] = $deal->end_time;
            $temp['status'] = $deal->status;
            $temp['dealoftheday'] = $deal->dealoftheday;
            $temp['toppicks'] = $deal->toppicks;
            $temp['termsConditions'] = $deal->termsConditions;


            $temp['store_id'] = $deal->getStore->id;
            $temp['store_name'] = $deal->getStore->name;
            $temp['store_descrption'] = $deal->getStore->descrption;
            $temp['store_firm_phone'] = $deal->getStore->firm_phone;
            $temp['store_firm_email'] = $deal->getStore->firm_email;
            $temp['store_address'] = $deal->getStore->address;
            $temp['store_city'] = $deal->getStore->city;
            $temp['store_city'] = $deal->getStore->city;
            $temp['store_district'] = $deal->getStore->district;
            $temp['store_state'] = $deal->getStore->state;
            $temp['store_pincode'] = $deal->getStore->pincode;
            $temp['store_location']['latitude'] =   '';
            $temp['store_location']['longitude'] =   '';
            if($deal->getStore->location)
            {
                $temp['store_location']['latitude'] =    json_decode($deal->getStore->location,true)?json_decode($deal->getStore->location,true)['latitude']:'';
                $temp['store_location']['latitude'] =   (json_decode($deal->getStore->location,true))&&(json_decode($deal->getStore->location,true)['longitude']!=null)?json_decode($deal->getStore->location,true)['longitude']:'';
            }

            $temp['store_gstin'] = $deal->getStore->gstin;
            $temp['store_firm_images']  =   [];
            if(json_decode($deal->getStore->firm_images))
            {
                foreach (json_decode($deal->getStore->firm_images) as $firm_images)
                {
                    $temp['store_firm_images'][]    =   url('uploads/' . $firm_images);
                }
            }
            $temp['small_image'] = url('uploads/' . $deal->small_image);
            $deals[] = $temp;
        }
        return response()->json(['deals'=>$deals,'type'=>'success'],200);
        return $deals;
    }

    function getCoupons(){

        $dealsOrg = MerchantDeal::where('dealType','coupon')->with('getOwner')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->inRandomOrder()->limit(8)->get();
        $deals = $this->formatDeals($dealsOrg);

        return $deals;
    }

    function getTopPics(){
        $topPics = MerchantDeal::with('getOwner')->where('toppicks',  '1')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->inRandomOrder()->limit(3)->get();
        return $this->formatDeals($topPics);;
    }

    function getDealsoftheDay(){
        $dealoftheday = MerchantDeal::with('getOwner')->where('dealoftheday',  '1')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->get();
        return $this->formatDeals($dealoftheday);;
    }

    function getCategories(){
        $category = Category::where('status','1')->get();
        $categoryRespose = [];

        foreach ($category as $cat){
            $temp = [];
            $temp['id'] = $cat->id;
            $temp['name'] = $cat->name;
            $temp['image'] = $cat->image?url('uploads/' . $cat->image):'';
            $categoryRespose[] = $temp;
        }
        return $categoryRespose;
    }

    public function listDealsCategory($categoryId, $pageno)
    {

        $noOfItemsinPage = 9;

        if ($categoryId=='top-picks'){
            $deals = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('toppicks',  '1')->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->limit($noOfItemsinPage)->skip($noOfItemsinPage*($pageno-1))->get();
        }
        elseif ($categoryId=='deal-of-the-day'){
            $deals = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('dealoftheday',  '1')->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->limit($noOfItemsinPage)->skip($noOfItemsinPage*($pageno-1))->get();

        }
        elseif($categoryId!=0){
            $deals = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('category_id',$categoryId)->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->limit($noOfItemsinPage)->skip($noOfItemsinPage*($pageno-1))->get();
        }
        else{
            $deals = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->limit($noOfItemsinPage)->skip($noOfItemsinPage*($pageno-1))->get();
        }

        if(count($deals)==0){
            return response()->json(['pageno'=>$pageno,'deals'=>[]],200);
//            return 'genisys';
        }
        else
        {
            $deals1 =   [];
            $deals1 = $this->formatDeals($deals);
            return response()->json(['pageno'=>$pageno,'deals'=>$deals1],200);
        }

    }

    function participateLuckydraw($luckydrawId){

        $luckyDraw = LuckyDraw::where('id',$luckydrawId)->first();
        if($luckyDraw){
            $luckyDrawPart = new LuckyDrawParticipants();
            $luckyDrawPart->luckydraw_id = $luckydrawId;
            $luckyDrawPart->users_id = Auth::user()->id;
            $luckyDrawPart->save();

            $settings = Settings::where('title','luckydraw')->first();
            $points = 1;
            if($settings){
                $sData = json_decode($settings->data);
                $points =  isset($sData->participate_point)?$sData->participate_point:'';
            }

            LuckydrawChances::updateOrCreate(
                ['luckydraw_id' => $luckydrawId,
                'users_id' => Auth::user()->id,
                'type' => 'luckydraw_participate'], ['chances' => $points]
            );
            if($luckyDraw->ad_flag == 0){
                return $this->returnSuccess(['msg'=>'Successfully Participated']);
            }
            else{
                return $this->getLuckyDrawAds($luckydrawId);
            }
        }
        else{
            return $this->returnError('Could not participate');
        }

    }

    function viewAdsLuckyDraw($luckydrawId){
        $luckyDraw = LuckyDraw::where('id',$luckydrawId)->first();
        if($luckyDraw){
            $settings = Settings::where('title','luckydraw')->first();
            $points = 1;
            if($settings){
                $sData = json_decode($settings->data);
                $points =  $sData->adview_point;
            }

                $luckyChances = new LuckydrawChances;
                $luckyChances->luckydraw_id =  $luckydrawId;
                $luckyChances->users_id = Auth::user()->id;
                $luckyChances->type = 'ads_watch';
                $luckyChances->chances = $points;
                $luckyChances->save();

            return $this->returnSuccess(['msg'=> 'Ads Point registered']);

        }
        else{
            return $this->returnError('Could not participate');
        }
    }

    function getLuckyDrawAds($luckydrawId){

        $luckyDrawAds = LuckyDrawAds::where('luckydraw_id',$luckydrawId)->get();
//        dd($luckyDrawAds);
        if($luckyDrawAds->isEmpty()){
            return $this->returnError('No Lucky Draw Ads Active');

        }
        else{
            return $this->returnSuccess(['msg'=>'Successfully Participated','luckyDrawAds'=>$luckyDrawAds]);
        }
    }

    function formatDeals($dealsOrg){
        $deals = [];
        foreach ($dealsOrg as $deal) {
            $temp = [];
            $temp['id'] = $deal->id;
            $temp['category_id'] = $deal->category_id;
            $temp['title'] = $deal->title;
            $temp['small_description'] = $deal->small_description;
            $temp['description'] = $deal->description;
            $temp['category_text'] = $deal->getCategory->name;
            $temp['price'] = $deal->price;
            $temp['online_sell_price'] = $deal->online_sell_price;
            $temp['offer_price'] = $deal->offer_price;
            $temp['totalstock'] = $deal->totalstock;
            $temp['cashbackamount'] = $deal->cashbackamount??0;
            $temp['sold'] = $deal->sold;
            $temp['start_time'] = $deal->start_time;
            $temp['end_time'] = $deal->end_time;
            $temp['star_rating'] = $deal->avg_rating=='' ? "0" : $deal->avg_rating;
            $temp['status'] = $deal->status;
            $temp['dealoftheday'] = $deal->dealoftheday;
            $temp['toppicks'] = $deal->toppicks;
            $temp['location'] = ($deal->getOwner->city!=''?implode(city([$deal->getOwner->city]),','):'').($deal->getOwner->district!=''?','.implode(district([$deal->getOwner->district]),','):'');
            $temp['store_name'] = $deal->getOwner->	name?$deal->getOwner->name:'';

            $temp['small_image'] = url('uploads/' . $deal->small_image);
            $deals[] = $temp;
        }
        return $deals;
    }

    function formatOrder($order){

        $format['id'] = $order->id;
        $format['order_date'] = $order->order_date;
        $format['orderid'] =  'ODR'.str_pad($order->id, 5, '0', STR_PAD_LEFT);
        $format['ordertotal'] = $order->order_total;
        $format['address_id'] = $order->address_id;
        $format['razorpay_order_id'] = $order->razorpay_order_id;

        $format['paymentStatusMsg'] = 'Pending';
        if($order->order_status){
            $format['paymentStatusMsg'] = 'Purchase Complete';
        }
        else{
            if($order->expiry >= Carbon::now()) {
                if ($order->payment_status == 1){
                    $format['paymentStatusMsg'] = 'Pending';
                }
                else{
                    $format['paymentStatusMsg'] = 'Pending Payment';
                }
            }
            else{
                    $format['paymentStatusMsg'] = 'Expired';
            }
         }

        $dAddress = json_decode($order->address_details);
        $format['address_details']['fullName']    =   $dAddress->fullName;
        $format['address_details']['landmark']    =   $dAddress->landmark;
        $format['address_details']['mobileNumber']    =   $dAddress->mobileNumber;
        $format['address_details']['pincode']    =   $dAddress->pincode;
        $format['address_details']['addr1']    =   $dAddress->addr1;
        $format['address_details']['addr2']    =   $dAddress->addr2;
        $format['address_details']['city']    =   $dAddress->city;
        $format['address_details']['state']    =   $dAddress->state;

        return $format;

    }

    function formatOrderDetails($order){

        $format['order_id'] = $order->id;
        $format['ordertotal'] = $order->order_total;
        $format['address_id'] = $order->address_id;
        $format['razorpay_order_id'] = $order->razorpay_order_id;

        $orderDeatils = $order->getDetails()->get();

        foreach ($orderDeatils as $oDetail){
            $temp = [];
            if($oDetail->product_type == 'flashsale'){
                $flashsale = $oDetail->getFlashSale()->first();
                if($flashsale){
                    $temp['id'] = $flashsale->id;
                    $temp['title'] = $flashsale->title;
                    $temp['small_description'] = $flashsale->small_description;
                    $temp['offer_price'] = $flashsale->offer_price;
                    $temp['avg_rating'] = $flashsale->avg_rating;
                    $temp['image'] = url('uploads/'.$flashsale->small_image);
                    $temp['quantity'] = $oDetail->quatity;
                    $temp['price'] = $oDetail->price;
                    $temp['purchase_complete'] = $oDetail->purchase_complete;
                    $temp['type'] = 'flashsale';
                }

                $format['orderDetails'][] = $temp;
            }
            else{
                $deal = $oDetail->getDeal()->first();
                $temp['id'] = $deal->id;
                $temp['title'] = $deal->title;
                $temp['small_description'] = $deal->small_description;
                $temp['offer_price'] = $deal->offer_price;
                $temp['avg_rating'] = $deal->avg_rating;
                $temp['image'] = url('uploads/'.$deal->small_image);
                $temp['quantity'] = $oDetail->quatity;
                $temp['price'] = $oDetail->price;
                $temp['purchase_complete'] = $oDetail->purchase_complete;
                $temp['type'] = $deal->dealType;
                if($deal->dealType == 'coupon'){
                    $temp['cashbackamount'] = $deal->cashbackamount??0;
                    $temp['coupon_code'] = '';
                    $temp['coupon_expiry'] = '';
                    if($deal->dealType == 'coupon'){
                        if($order->order_status == 1){
                            if($order->payment_status == 0){
                                if($oDetail->expiry < Carbon::now() && $oDetail->coupon_used!=1){
                                    $temp['coupon_code'] ='COUPON EXPIRED';
                                }
                                else{
                                    $temp['coupon_code'] = $oDetail->coupon_code;
                                    $temp['coupon_expiry'] = $oDetail->expiry ?? '';

                                }
                            }
                            else{
                                $temp['coupon_code'] =$oDetail->coupon_code;
                            }
                        }
                    }
                }

                $format['orderDetails'][] = $temp;
            }
        }

        return ($format);

    }

    function addDeliveryAddress(Request $request){

        $rules=array(
            'fullName'    =>  'required',
            'mobile'    =>  'required',
            'pincode'    =>  'required',
            'addr1'    =>  'required',
            'addr2'    =>  'required',
            'city'    =>  'required',
        );
        $validate= Validator::make($request->all(),$rules);
        if($validate->fails())
        {
            return $this->returnError($validate->errors());
        }
        if($request->id > 0){
            $deliveryAddress = DeliveryAddress::where('users_id',Auth::user()->id)->where('id', $request->id)->first();

            if(!$deliveryAddress){
                return $this->returnError('No delivery Address Found');
            }
        }
        else{
            $deliveryAddress = new DeliveryAddress;
        }
        $deliveryAddress->fullName =$request->fullName;
        $deliveryAddress->users_id =Auth::user()->id;
        $deliveryAddress->mobileNumber =$request->mobile;
        $deliveryAddress->pincode =$request->pincode;
        $deliveryAddress->addr1 =$request->addr1;
        $deliveryAddress->addr2 =$request->addr2??'';
        $deliveryAddress->city =$request->city??'';
        $deliveryAddress->state =$request->state??'';
        $deliveryAddress->landmark =$request->landmark??'';
        $deliveryAddress->save();

        return $this->returnSuccess(['msg'=> 'Delivery Address Saved','data'=>$deliveryAddress]);

    }

    function deleteDeliveryAddress(Request $request, $id){

        $deliveryAddress = DeliveryAddress::where('users_id',Auth::user()->id)
            ->where('id',$id)->first();

        if($deliveryAddress){
            $deliveryAddress->delete();
            return $this->returnSuccess(['msg'=> 'Delivery address deleted']);
        }
        else{
            return $this->returnError('Delivery address couldnot be deleted');
        }

    }

    function listDeliveryAddress(){
        $dAddress = DeliveryAddress::where('users_id', Auth::user()->id)->get();
        $res['data'] = $dAddress;
        $res['status'] = 'success';
        return response()->json($res);
    }

    function returnError($msg){
        $res['msg'] = $msg;
        $res['status'] = 'error';
        return response()->json($res,200);
    }

    function returnSuccess($data){
        $data['msg']    =   '';
        $data['status'] = 'success';
        return response()->json($data,200);
    }

    function orderHistory()
    {

        $orderHistory   =   Orders::where('users_id',Auth::user()->id)->get();
        $orderData  =   [];
        foreach ($orderHistory as $order)
        {
            $orderData[]    =   $this->formatOrder($order);
        }
        return response()->json(['orderData'=>$orderData,'type'=>'success'],200);
    }

    public function createOrder($addressId)
    {

        $dAddress = DeliveryAddress::where('users_id', Auth::user()->id)->where('id', $addressId)->first();
        if(!$dAddress){
            return response()->json(['msg'=>'Please Choose Valid Delivery Address','status'=>'error'],200);
        }

        $cartOb         =   new CartController();
        $cart   =   $cartOb->show()->getOriginalContent()['cartItems'];
        $orderDetailObjs = [];
        $dealsSent = [];
        $orderTotal = 0;
        foreach ($cart as $item) {
            if($item->type == 'deal'){
                if ($item->quantity > 0){
                    $deal = MerchantDeal::where('id', $item->product_id)->where('start_time', '<=', Carbon::now())->where('status', 1)->first();

                    if($deal){
                        $deal->quantitySale = $item->quantity;
                        $deal->termsConditions = 'removed';
                        $dealsSent[] = $deal;

                        $orderDetail = new OrderDetails();
                        $orderDetail->product_id = $deal->id;
                        $orderDetail->product_type = $item->type;
                        $orderDetail->order_id = 0;
                        $orderDetail->merchant_id = $deal->merchant_id;
                        $orderDetail->store_id = $deal->store_id;
                        $orderDetail->delivery_status = 0;
                        $orderDetail->users_id = Auth::user()->id;
                        $orderDetail->quatity = $item->quantity;
                        $orderDetail->price = $deal->online_sell_price;
                        if($deal->dealType == 'coupon'){
                            $orderDetail->product_type = 'coupon';
                            if($deal->online_sell_price == 0){
                                $orderDetail->purchase_complete = 1;
                                $orderDetail->expiry =  Carbon::now()->addDays(7);
                            }
                        }
                        $orderTotal = $orderTotal + ((float)$deal->online_sell_price * (float)$item->quantity);
                        $orderDetailObjs[] = $orderDetail;
                    }
                }
            }
        }

        $order_details['deals'] = $dealsSent;
        $order = new Orders();
        $order->users_id = Auth::user()->id;
        $order->address_id = $dAddress->id;
        $order->address_details = json_encode($dAddress);
        $order->order_date = date('Y-m-d H:i:s');
        $order->order_status = 0; // pending Order
        $order->expiry = Carbon::now()->addMinutes(15);
        $order->order_total = $orderTotal; // order Total
        $order->shipped_date = ''; // not Shipped Yet
        $order->shipping_details = '';
        $order->payment_status = 0; // pending Payment
        $order->payment_details = '';
        $order->payment_request_details = ''; //should taken from payment gateway
        $order->order_details = json_encode($order_details);
        $order->save();

        foreach ($orderDetailObjs as $orderDet){
            $orderDet->order_id = $order->id;
            $orderDet->coupon_code = $order->id. strtoupper(uniqid()).Auth::user()->id;
            $orderDet->save();
        }
        if($orderTotal == 0){
            $order->order_status = 1;
        }
        else{
            $api = new Api(config('razorpay.key_id'), config('razorpay.key_secret'));
            $razorOrder  = $api->order->create(array('receipt' => $order->id, 'amount' => ($orderTotal*100), 'currency' => 'INR')); // Creates order

            $order->razorpay_order_id = $razorOrder->id;
            $order->razorpay_order_details = json_encode((array)$razorOrder);
        }
        $order->save();

        $cartOb->clearCart();



        return response()->json(['razorpay_order_id',$order->razorpay_order_id,'status'=>'success'],200);

    }

    public function viewOrder($id){

        $order = Orders::with('getDetails')->where('users_id',Auth::user()->id)->where('id',$id)->first();
        if(!$order)
        {
            return response()->json(['msg'=>'Order could not found','status'=>'error'],200);
        }
        else{
            $orderFormated = $this->formatOrderDetails($order);
        }

        return response()->json(['order'=>$orderFormated,'status'=>'success'],200);
    }

    public function saveOrderFinish(Request $request, $id){


        $order = Orders::where('users_id',Auth::user()->id)->where('id',$id)->first();
        if(!$order)
        {
            return response()->json(['msg'=>'Order couldnot found','status'=>'error'],200);
        }

        $order->payment_request_details = json_encode($request->all());
        $order->save();

        $ob =   new CommissionSplitController();
        $ob->split($order);
        $api = new Api(config('razorpay.key_id'), config('razorpay.key_secret'));
        try {
            $payment = $api->payment->fetch($request->razorpay_payment_id)->capture(array('amount' => ($order->order_total * 100))); // Captures a payment
        }
        catch (\Exception $e){
            return response()->json(['msg'=>$e->getMessage(),'status'=>'error'],200);

        }

        if($payment->status == 'captured'){
            $order->payment_status = 1;
            $order->order_status = 1;
            OrderDetails::where('order_id',$order->id)->where('users_id',Auth::user()->id)->update(['purchase_complete' => 1]);
        }
        else{
            $order->payment_status = 0;
            $order->order_status = 0;
        }
        $order->payment_details = json_encode((array)$payment);
        $order->save();

        //save after everything
        if($payment->status == 'captured') {
            $orderDetailItems = OrderDetails::where('order_id', $order->id)->where('users_id', Auth::user()->id)->get();
            foreach ($orderDetailItems as $item) {
                if($item->product_type == 'deal' || $item->product_type == 'coupon'){
                    MerchantDeal::find($item->product_id)->increment('sold',$item->quatity);
                    MerchantDeal::find($item->product_id)->decrement('stock',$item->quatity);
                }
            }
        }
        return response()->json(['msg'=>'Payment successfully','status'=>'error'],200);
    }

    public function flashsaleOrder($id,$addressId){

        $dAddress = DeliveryAddress::where('users_id', Auth::user()->id)->where('id', $addressId)->first();
        if(!$dAddress){
            return response()->json(['msg'=>'Please Choose Valid Delivery Addressd','status'=>'error'],200);
        }

        $flashSale =   FlashSale::with('getOwner')->with('isPurchased')->where('status',1)->where('id', $id)->first();

        if($flashSale) {
            if ($flashSale->end_time >= Carbon::now()) {
                if ($flashSale->start_time <= Carbon::now()) {
                    //currently sale on
                } else {
                    // sale yet to start
                    return response()->json(['msg'=>'Flash sale yet to Start','status'=>'error'],200);
                }
            } else {
                //sale passed
            }
        }
        else{
            return response()->json(['msg'=>'Flash sale not found','status'=>'error'],200);
        }

        //check Stock
        if( ($flashSale->totalstock - $flashSale->sold) < 1 ){
            return response()->json(['msg'=>'Sorry Sold Out','status'=>'error'],200);
        }

        // check already purchased
        if(count($flashSale->isPurchased)){
            //already purchased

        }

        $flashSale->quantitySale  = 1;
        $flashSale->termsConditions = 'removed';
        $flashSale->description = 'removed';

        $order_details['flashSale'] = $flashSale;
        $order = new Orders();
        $order->users_id = Auth::user()->id;
        $order->address_id = $dAddress->id;
        $order->address_details = json_encode($dAddress);
        $order->order_date = date('Y-m-d H:i:s');
        $order->order_status = 0; // pending Order
        $order->shipped_date = ''; // not Shipped Yet
        $order->expiry = Carbon::now()->addMinutes(15);
        $order->order_total = $flashSale->offer_price; // order Total
        $order->shipping_details = '';
        $order->payment_status = 0; // pending Payment
        $order->payment_details = '';
        $order->payment_request_details = ''; //should taken from payment gateway
        $order->order_details = json_encode($order_details);
        $order->save();

        $orderDetail = new OrderDetails();
        $orderDetail->product_id = $flashSale->id;
        $orderDetail->product_type = 'flashsale';
        $orderDetail->order_id = $order->id;
        $orderDetail->users_id = Auth::user()->id;
        $orderDetail->quatity = 1;
        $orderDetail->price = $flashSale->offer_price;
        $orderDetail->save();

        $api = new Api(config('razorpay.key_id'), config('razorpay.key_secret'));
        $razorOrder  = $api->order->create(array('receipt' => $order->id, 'amount' => ($order->order_total*100), 'currency' => 'INR')); // Creates order

        $order->razorpay_order_id = $razorOrder->id;
        $order->razorpay_order_details = json_encode((array)$razorOrder);
        $order->save();

        return response()->json(['razorpay_order_id'=>$order->razorpay_order_id,'order_id'=>$order->id,'status'=>'success'],200);

    }

    public function getReview($id,$type)
    {
        $ob =   new ReviewController;
        $data   =   $ob->index($id,$type);
        $reviews    =   [];
        if($data->isEmpty())
        {
            return response()->json(['msg'=>'No reviews','status'=>'error'],200);
        }
        else
        {
            foreach ($data as $item)
            {
                $temp   =   [];
                $temp['owner']   =   $item->getOwner->name;
                $temp['owner_id']   =   $item->getOwner->id;
                $temp['rating']   =   $item->rating;
                $temp['description']   =   $item->description;
                $temp['date']   =   $item->updated_at->format('F d, Y');
                $temp['isOwner']   =   ($item->getOwner->id==Auth::user()->id)?TRUE:FALSE;
                $temp['avatar']   =   url('/avatar.png');
                $reviews[]   =   $temp;
            }
            return response()->json(['msg'=>'','reviews'=>$reviews,'status'=>'success'],200);
        }
    }

    public function writeReview(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'product_type' => 'required',
            'rating' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->returnError($validator->getMessageBag()->first());
        } else {
            if ($request->product_type == 'flashSale') {
                $product = FlashSale::find($request->product_id);
            } elseif ($request->product_type == 'deal') {
                $product = MerchantDeal::find($request->product_id);
            } else {
                return $this->returnError('Product not found');
            }
            if ((!$product) || (!$product->isPurchased())) {
                return $this->returnError('Product not found');
            }
            $review = Review::where('user_id', Auth::user()->id)->where('product_id', $request->product_id)->where('product_type', $request->product_type)->first();
            if (!$review) {
                $review = new Review;
            }
            $review->user_id = Auth::user()->id;
            $review->product_id = $request->product_id;
            $review->product_type = $request->product_type;
            $review->rating = $request->rating;
            $review->description = $request->description;
            $review->save();
            $product->avg_rating = number_format($product->getReviewsAvg()->first()->ratingTotal, 1);
            $product->save();
            $temp   =   [];
            $temp['owner']   =   Auth::user()->name;
            $temp['owner_id']   =   Auth::user()->id;
            $temp['rating']   =   $review->rating;
            $temp['description']   =   $review->description;
            $temp['date']   =   $review->updated_at->format('F d, Y');
            $temp['isOwner']   =   TRUE;
            $temp['avatar']   =   url('/avatar.png');

//            $data   =  Review::find($review->id);
            return response()->json(['status'=>'success','msg'=>'Thanks for review','review'=>$temp],200);
//            return $this->returnSuccess('Thanks for review');
        }
    }

}
