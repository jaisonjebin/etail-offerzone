<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request)
    {
        if(is_numeric($request->get('email'))){
            return ['phone'=>$request->get('email'),'password'=>$request->get('password')];
        }
        return $request->only($this->username(), 'password');
    }

    public function showLoginForm()
    {
        return view('site.signin');
    }

    public function login(Request $request)
    {
        $rules=array(
            'email'    	=>  'required',
            'password'    	=>  'required'
        );
        $validate= Validator::make($request->all(),$rules);
        if($validate->fails())
        {
            return back()->withInput($request->all())->withErrors($validate);
        }
        $remember   =   $request->remember?true:false;

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password,'status'=>1], $remember))
        {
            return redirect()->intended('/home');
        }
        elseif(Auth::attempt(['phone' => $request->email, 'password' => $request->password,'status'=>1], $remember))
        {
            return redirect()->intended('/home');
        }
        else
        {
            return back()->withInput($request->all())->withErrors(['email'=>'Invalid Credential']);
        }

    }


}
