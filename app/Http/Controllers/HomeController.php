<?php

namespace App\Http\Controllers;

use App\Category;
use App\Favorites;
use App\LuckyDrawWinners;
use App\Mail\ContactFormMail;
use App\MerchantDeal;
use App\OrderDetails;
use App\Orders;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Integer;
use Razorpay\Api\Api;
use Razorpay\Api\Order;
use Session;
use App\FlashSale;
use App\DeliveryAddress;
use App\Settings;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Mail;
use Hash;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function changeCurrentLocation(Request $request){

        $locationText = "Unnamed Location";
        $return['locationText'] =  $locationText;

        if($request->locationText!=''){
            $locationText = $request->locationText;
        }
        if($request->latitude && $request->longitude){
            if(Auth::check()){
                Auth::user()->latitude = $request->latitude;
                Auth::user()->longitude = $request->longitude;
                Auth::user()->locationText = $locationText;
                Auth::user()->save();
            }
            $return['latitude'] =  $request->latitude;
            $return['longitude'] =  $request->longitude;
            $return['locationText'] =  $locationText;
        }
        else{
            if(Auth::check()){
                $return['latitude'] =  Auth::user()->latitude;
                $return['longitude'] =  Auth::user()->longitude;
                $return['locationText'] =  Auth::user()->locationText;
            }
            else{
                $return['locationText'] = "Etailoffer, Kondotty, Kerala 673638";
                $return['latitude'] =  '11.1565429';
                $return['longitude'] = '75.9487427';
            }
        }

        Session::put('latitude',$return['latitude']);
        Session::put('longitude',$return['longitude']);
        Session::put('locationText',$return['locationText']);

        return $return;

    }

    public function index()
    {
        $settings = Settings::where('title','homeSliders')->first();
        $winners = LuckyDrawWinners::all();
		$sliders = [];
		if($settings){	
			$sliders = json_decode($settings->data);
		}
		$categories = Category::withCount('getDeal')->limit(10)->get();
		
        $flashSales = FlashSale::with('getOwner')->where('start_time', '<=', Carbon::now())->where('status',1)->where('stock','>',0)->orderBy('start_time', 'asc')->limit(3)->get();
        $flashSalesYetToStart = [];
        if(count($flashSales) < 3){
            $flashSalesYetToStart = FlashSale::with('getOwner')->where('start_time', '>', Carbon::now())->where('status',1)->where('stock','>',0)->orderBy('start_time', 'asc')->limit((3-count($flashSales)))->get();
        }
        $deals = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('start_time', '<=', Carbon::now())->where('dealType', '!=','coupon')->where('status',1)->orderBy('start_time', 'asc')->limit(6)->get();
        $coupons = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('start_time', '<=', Carbon::now())->where('dealType', 'coupon')->where('status',1)->orderBy('start_time', 'asc')->limit(10)->get();
        $topPics = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('toppicks',  '1')->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->inRandomOrder()->limit(3)->get();
//        return view('front.home',compact('flashSales','sliders','deals','topPics'));
//        dd($deals);
        return view('site.home',compact('winners','coupons','flashSales','sliders','deals','topPics','categories','flashSalesYetToStart'));
    }

    public function contact()
    {
//        dd(env('DB_PORT'));
//        dd(DB::connection());
        return view('front.contact');
    }

    public function myProfile(){

//        $dAddress = DeliveryAddress::where('users_id', Auth::user()->id)->get();

//        $favorite = Favorites::where('users_id', Auth::user()->id)->where('type','flashSaleItem')->pluck('item_id');
//        $favo['flashSale']=[];
//        $favo['dealItem']=[];
//        if(count($favorite)){
//            $favo['flashSale'] = FlashSale::whereIn('id',$favorite)->get();
//        }
//
//        $favorite = Favorites::where('users_id', Auth::user()->id)->where('type','dealItem')->pluck('item_id');
//        if(count($favorite)) {
//            $favo['dealItem'] = MerchantDeal::whereIn('id', $favorite)->get();
//        }
        $cartOb         =   new CartController();
        $cart   =   $cartOb->show()->getOriginalContent()['cartItems'];
//        $myreferralcode =   ((Auth::user()->type=='freelancer')?('DDFL'.Auth::user()->id):((Auth::user()->type=='merchant')?('DDMR'.Auth::user()->id):('DDUR'.Auth::user()->id)));
//        $myteam   =   User::where('ref_code',$myreferralcode)->get();
        return view('site.profile',compact('dAddress','favo','cart','myteam'));
//        return view('front.profile',compact('dAddress','favo','cart'));

    }

    public function profileEdit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'district' => 'required',
            'city' => 'required',
            'email' => 'email',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->getMessageBag()->first());
        }
        else
        {
            $user   =   User::find(Auth::user()->id);
            if($user)
            {
                $user->name =   $request->name;
                $user->email =   $request->email;
                $user->city =   $request->city;
                $user->district =   $request->district;
                $user->save();
                flash('Profile updated','success');
                return back();
            }
            else
            {
                flash('Profile not found','warning');
                return back();
            }
        }
    }

    public function myPurchase()
    {

        $orderDetails = OrderDetails::with('getOrder')
            ->with('getDeal')
            ->with('getFlashSale')
            ->where('users_id',Auth::user()->id)
            ->orderBy('created_at','desc')
            ->get();

        return view('site.mypurchase',compact('orderDetails'));
    }

    public function myTeam()
    {
        $myreferralcode =   ((Auth::user()->type=='freelancer')?('DDFL'.Auth::user()->id):((Auth::user()->type=='merchant')?('DDMR'.Auth::user()->id):('DDUR'.Auth::user()->id)));
        $myteam   =   User::where('ref_code',['DDUR'.Auth::user()->id,'DDMR'.Auth::user()->id,'DDFL'.Auth::user()->id])->get();
        return view('site.myteam',compact('myteam'));
    }

    public function myNotifications()
    {
        return view('site.myNotifications');
    }

    public function myRewards()
    {
        return view('site.myRewards');
    }

    public function myAddress()
    {
        $dAddress = DeliveryAddress::where('users_id', Auth::user()->id)->get();
        return view('site.myAddress',compact('dAddress'));
    }

    public function mySettings()
    {
        return view('site.mySettings');
    }

    public function myFavorites()
    {
        $favorite = Favorites::where('users_id', Auth::user()->id)->where('type','flashSaleItem')->pluck('item_id');
        $favo['flashSale']=[];
        $favo['dealItem']=[];
        if(count($favorite))
        {
            $favo['flashSale'] = FlashSale::whereIn('id',$favorite)->get();
        }

        $favorite = Favorites::where('users_id', Auth::user()->id)->where('type','dealItem')->pluck('item_id');
        if(count($favorite))
        {
            $favo['dealItem'] = MerchantDeal::whereIn('id', $favorite)->get();
        }
        return view('site.myFavorites',compact('favo'));
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password_confirmation' => 'required',
            'password' => 'required|confirmed',
            'current_password' => 'required',

        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->getMessageBag()->first());
        }
        else
        {
            $user   =   User::where('id',Auth::user()->id)->first();
            if($user&&(Hash::check($request->current_password, $user->password)))
            {
                $user->password = Hash::make($request->password);
                $user->save();
                flash('Password changed','success');
                return back();
            }
            else
            {
                return back()->withInput()->withErrors(['password'=>'User not found']);
            }

        }
    }

    public function allCategories()
    {
        $categories = Category::withCount('getDeal')->get();
        return view('site.categories',compact('categories'));
    }

    public function nearBy()
    {
        $deals = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->get();
        return view('site.nearBy',compact('deals'));
    }

    public function index1($page)
    {
        return view($page);
    }

    public function flashSaleItem($id){

        $flashSale =   FlashSale::with('getReviewsAvg')->with('getOwner')->with('isFavourited')->with('isPurchased')->with('userFavorites')->where('status',1)->where('id', $id)->first();
        $flashSales = FlashSale::with('getOwner')->where('id', '!=', $id)->where('end_time', '>=', Carbon::now())->where('status',1)->where('stock','>',0)->orderBy('start_time', 'asc')->limit(5)->get();
        $topPics = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('toppicks',  '1')->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->inRandomOrder()->limit(4)->get();


        if($flashSale){
            if($flashSale->end_time >= Carbon::now()){
                if($flashSale->start_time <= Carbon::now()){
                    //currently sale on
                    $flashSale->saleStart = true;
                    $flashSale->yettoStart = false;
                    $flashSale->saleEnd = false;
                }
                else{
                    // sale yet to start
                    $flashSale->saleStart = false;
                    $flashSale->yettoStart = true;
                }
            }
            else{
                //sale passed : changed to sale on
                $flashSale->saleStart = true;
                $flashSale->yettoStart = false;
                $flashSale->saleEnd = false;
            }

            $review['isPurchased'] = $flashSale->isPurchased()->count();
            $review['getReviewsSummary'] = $flashSale->getReviewsSummary()->get();
            $review['avg'] = number_format($flashSale->getReviewsAvg()->first()->ratingTotal,1);
            $review[1]  =   0;
            $review[2]  =   0;
            $review[3]  =   0;
            $review[4]  =   0;
            $review[5]  =   0;
            $review['totalCount']   =   0;
            foreach ($flashSale->getReviewsSummary()->get() as $item)
            {
                $review[$item->rating] = $item->countTotal;
                $review['totalCount'] = $review['totalCount']+$item->countTotal;
            }
//            dd($review);

            return view('site.flashsaleSingle',compact('flashSale','flashSales','topPics','review'));
        }
        else{
            flash('Sorry Flash not Found','error');
            return redirect(route('home'));

        }
    }

    public function dealItem($id){

        $singleDeal =   MerchantDeal::with('getOwner')->where('start_time', '<=', Carbon::now())->where('status',1)->where('id', $id)->first();
        $topPics = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('toppicks',  '1')->where('id', '!=',$id)->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->inRandomOrder()->limit(4)->get();


        if($singleDeal){
            $similarDeals =  MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('category_id',$singleDeal->category_id)->where('id', '!=',$id)->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->inRandomOrder()->limit(3)->get();

            if($singleDeal->end_time >= Carbon::now()){
                if($singleDeal->start_time <= Carbon::now()){
                    //currently sale on
                    $singleDeal->saleStart = true;
                    $singleDeal->yettoStart = false;
                    $singleDeal->saleEnd = false;
                }
                else{
                    // sale yet to start
                    $singleDeal->saleStart = false;
                    $singleDeal->yettoStart = true;
                }
            }
            else{
                //sale passed : changed to sale on
                $singleDeal->saleStart = true;
                $singleDeal->yettoStart = false;
                $singleDeal->saleEnd = false;
            }
            $review['isPurchased'] = $singleDeal->isPurchased()->count();
            $review['getReviewsSummary'] = $singleDeal->getReviewsSummary()->get();
            $review['avg'] = number_format($singleDeal->getReviewsAvg()->first()->ratingTotal,1);
            $review[1]  =   0;
            $review[2]  =   0;
            $review[3]  =   0;
            $review[4]  =   0;
            $review[5]  =   0;
            $review['totalCount']   =   0;
            foreach ($singleDeal->getReviewsSummary()->get() as $item)
            {
                $review[$item->rating] = $item->countTotal;
                $review['totalCount'] = $review['totalCount']+$item->countTotal;
            }
//            dd($review);


            return view('site.dealSingle',compact('singleDeal', 'topPics','review','similarDeals'));
//            return view('front.dealSingle',compact('singleDeal','flashSales','deals'));
        }
        else{
            flash('Sorry Deal not Found','error');
            return redirect(route('home'));

        }
        //return view('front.home',compact('flashSale'));
    }

    public function listDeals(){
        $topPics = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('toppicks',  '1')->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->limit(3)->get();
        $dealoftheday = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('dealoftheday',  '1')->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->limit(3)->get();
        $listTitle = "Hot Deals";
        $categoryId = 0;

        return view('site.dealList',compact('dealoftheday','topPics','listTitle','categoryId'));
    }

    public function listDealsCategory($categoryId){

        if ($categoryId=='top-picks'){
            $listTitle = "Top Picks";
        }
        elseif ($categoryId=='deal-of-the-day'){
            $listTitle = "Deals of the Day";
        }
        elseif($categoryId!=0){
            $cat = Category::where('id',$categoryId)->first();
            if($cat){
                $listTitle = 'Deals on '.$cat->name;
            }
            else{
                $listTitle = "Deals";
            }
        }
        else{
            return back();
        }

        $topPics = [];
        $dealoftheday = [];

        return view('site.dealList',compact('dealoftheday','topPics','listTitle', 'categoryId'));

    }

    public function loadDealsAjax($categoryId, $pageno)
    {

        $noOfItemsinPage = 9;

        if ($categoryId=='top-picks'){
            $deals = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('toppicks',  '1')->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->limit($noOfItemsinPage)->skip($noOfItemsinPage*($pageno-1))->get();
        }
        elseif ($categoryId=='deal-of-the-day'){
            $deals = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('dealoftheday',  '1')->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->limit($noOfItemsinPage)->skip($noOfItemsinPage*($pageno-1))->get();

        }
        elseif($categoryId!=0){
            $deals = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('category_id',$categoryId)->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->limit($noOfItemsinPage)->skip($noOfItemsinPage*($pageno-1))->get();
        }
        else{
            $deals = MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('dealType', '!=','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->limit($noOfItemsinPage)->skip($noOfItemsinPage*($pageno-1))->get();
        }

        if(count($deals)==0){
            return 'genisys';
        }
        else{
            return view('site.includes.dealAjaxTemplate',compact('deals'));
        }

    }

    public function listCoupons()
    {
        return view('site.couponList');
    }

    public function couponItem($id)
    {
        return view('site.couponSingle');
    }

    public function flashsaleAddtoCart(Request $req){

        $flashSale =   FlashSale::with('getOwner')->where('status',1)->where('id', $req->itemId)->first();
        if($flashSale){
            if($flashSale->stock > 0){
                if($flashSale->start_time <= Carbon::now()){
                    //currently sale on
                    $flashSale->saleStart = true;
                    $flashSale->yettoStart = false;
                    $flashSale->saleEnd = false;

                    return redirect()->route('flashsaleItemcheckout',$flashSale->id);
                }
                else{
                    flash('Sorry Out of Stock','error');
                    return back();
                }
            }
            else{
                flash('Sorry Out of Stock','error');
                return back();
            }
        }
        else{
            flash('Sorry Flash not Found','error');
            return back();
        }
    }

    public function flashsaleItemcheckout($id){

        $flashSale =   FlashSale::with('getOwner')->where('status',1)->where('id', $id)->first();

        if($flashSale){
            if($flashSale->end_time >= Carbon::now()){
                if($flashSale->start_time <= Carbon::now()){
                    //currently sale on
                    $flashSale->saleStart = true;
                    $flashSale->yettoStart = false;
                    $flashSale->saleEnd = false;
                }
                else{
                    // sale yet to start
                    $flashSale->saleStart = false;
                    $flashSale->yettoStart = true;
                }
            }
            else{
                //sale passed
                $flashSale->saleStart = false;
                $flashSale->yettoStart = false;
                $flashSale->saleEnd = true;
            }
            $dAddress = DeliveryAddress::where('users_id', Auth::user()->id)->get();

            $paymentSummary = [];
            $paymentSummary['total'] = $flashSale->offer_price;
            $paymentSummary['quantity'] = 1;

            return view('site.cartCheckOut',compact('flashSale','dAddress','paymentSummary'));
        }
        else{
            flash('Sorry Flash not Found','error');
            return redirect(route('home'));

        }
    }

    public function listFlashSale(){

        $flashSales = FlashSale::with('getOwner')->where('end_time', '>=', Carbon::now())->where('status',1)->where('stock','>',0)->orderBy('start_time', 'asc')->limit(9)->get();
        return view('site.flashsaleList',compact('flashSales'));

    }

    public function loadFlashSaleAjax($pageno){

        $noOfItemsinPage = 9;
        $flashSales = FlashSale::with('getOwner')->where('end_time', '>=', Carbon::now())->where('status',1)->where('stock','>',0)->orderBy('start_time', 'asc')->limit($noOfItemsinPage)->skip($noOfItemsinPage*($pageno-1))->get();


        if(count($flashSales)==0){
            return 'genisys';
        }
        else{
            return view('site.includes.flashsaleAjaxTemplate',compact('flashSales'));
        }

    }

    public function loadCouponsAjax($pageno){

        $noOfItemsinPage = 9;
        $coupons =  MerchantDeal::with('getOwner')->with('isFavourited')->with('userFavorites')->where('dealType','coupon')->where('start_time', '<=', Carbon::now())->where('status',1)->orderBy('start_time', 'asc')->limit($noOfItemsinPage)->skip($noOfItemsinPage*($pageno-1))->get();



        if(count($coupons)==0){
            return 'genisys';
        }
        else{
            return view('site.includes.couponAjaxTemplate',compact('coupons'));
        }

    }

    public function cart(){

        $cartOb         =   new CartController();
        $cart   =   $cartOb->show()->getOriginalContent()['cartItems'];

        return view('site.cart',compact('cart'));

    }

    public function checkOut(){

        $dAddress = DeliveryAddress::where('users_id', Auth::user()->id)->get();


        $cartOb         =   new CartController();
        $cart   =   $cartOb->show()->getOriginalContent()['cartItems'];

        $dealsSent = [];

        $paymentSummary = [];
        $paymentSummary['total'] = 0;
        $paymentSummary['quantity'] = 0;
        foreach ($cart as $item) {
            if($item->type == 'deal'){

                $product = MerchantDeal::where('id',$item->product_id)->with('getOwner')->where('start_time', '<=', Carbon::now())->where('status',1)->first();
                $limit = $product->userBuyLimit;
                $orderDetailCount = OrderDetails::
                where('product_id', $product->id)
                    ->where('product_type', 'coupon')
                    ->where('users_id', Auth::user()->id)
                    ->sum('quatity');
                if (($orderDetailCount + $item->quantity) > $limit) {
                    flash('Sorry, '.$item->name.' has reached your buying limit.','error');
                    return back();
                }

                $paymentSummary['total'] = $paymentSummary['total'] + ($item->price * $item->quantity);
                $paymentSummary['quantity'] = $paymentSummary['quantity'] + $item->quantity;
                if ($item->quantity > 0){
                    $deal = MerchantDeal::where('id', $item->product_id)->where('start_time', '<=', Carbon::now())->where('status', 1)->first();
                    if($deal){
                        $deal->quantitySale = $item->quantity;
                        $dealsSent[] = $deal;
                    }
                }
            }

        }

        return view('site.cartCheckOut',compact('dAddress','dealsSent','paymentSummary'));


    }

    public function createOrder($addressId){

        $dAddress = DeliveryAddress::where('users_id', Auth::user()->id)->where('id', $addressId)->first();
        if(!$dAddress){
            flash('Please Choose Valid Delivery Address','error');
            return back();
        }

        $cartOb         =   new CartController();
        $cart   =   $cartOb->show()->getOriginalContent()['cartItems'];
        $orderDetailObjs = [];
        $dealsSent = [];
        $orderTotal = 0;
        foreach ($cart as $item) {
            if($item->type == 'deal'){
                if ($item->quantity > 0){

                    $deal = MerchantDeal::where('id', $item->product_id)->where('start_time', '<=', Carbon::now())->where('status', 1)->first();
                    if($deal) {
                        $limit = $deal->userBuyLimit;
                        $orderDetailCount = OrderDetails::
                        where('product_id', $deal->id)
                            ->where('product_type', 'coupon')
                            ->where('users_id', Auth::user()->id)
                            ->sum('quatity');
                        if (($orderDetailCount + $item->quantity) <= $limit) {
                            $deal->quantitySale = $item->quantity;
                            $deal->termsConditions = 'removed';
                            $dealsSent[] = $deal;

                            $orderDetail = new OrderDetails();
                            $orderDetail->product_id = $deal->id;
                            $orderDetail->product_type = $item->type;
                            $orderDetail->order_id = 0;
                            $orderDetail->merchant_id = $deal->merchant_id;
                            $orderDetail->store_id = $deal->store_id;
                            $orderDetail->delivery_status = 0;
                            $orderDetail->users_id = Auth::user()->id;
                            $orderDetail->quatity = $item->quantity;
                            $orderDetail->price = $deal->online_sell_price;
                            if ($deal->dealType == 'coupon') {
                                $orderDetail->product_type = 'coupon';
                                if ($deal->online_sell_price == 0) {
                                    $orderDetail->purchase_complete = 1;
                                    $orderDetail->expiry = Carbon::now()->addDays(7);
                                }
                            }
                            $orderTotal = $orderTotal + ((float)$deal->online_sell_price * (float)$item->quantity);
                            $orderDetailObjs[] = $orderDetail;
                        }
                    }
                }
            }
        }

        $order_details['deals'] = $dealsSent;
        $order = new Orders();
        $order->users_id = Auth::user()->id;
        $order->address_id = $dAddress->id;
        $order->address_details = json_encode($dAddress);
        $order->order_date = date('Y-m-d H:i:s');
        $order->order_status = 0; // pending Order
        $order->expiry = Carbon::now()->addMinutes(15);
        $order->order_total = $orderTotal; // order Total
        $order->shipped_date = ''; // not Shipped Yet
        $order->shipping_details = '';
        $order->payment_status = 0; // pending Payment
        $order->payment_details = '';
        $order->payment_request_details = ''; //should taken from payment gateway
        $order->order_details = json_encode($order_details);
        $order->save();

        foreach ($orderDetailObjs as $orderDet){
            $orderDet->order_id = $order->id;
            $orderDet->coupon_code = $order->id. strtoupper(uniqid()).Auth::user()->id;
            $orderDet->save();
        }
        if($orderTotal == 0){
            $order->order_status = 1;
        }
        else{
            $api = new Api(config('razorpay.key_id'), config('razorpay.key_secret'));
            $razorOrder  = $api->order->create(array('receipt' => $order->id, 'amount' => ($orderTotal*100), 'currency' => 'INR')); // Creates order

            $order->razorpay_order_id = $razorOrder->id;
            $order->razorpay_order_details = json_encode((array)$razorOrder);
        }
        $order->save();

        $cartOb->clearCart();



        return redirect()->route('user.viewOrder',$order->id);

    }

    public function viewOrder($id){

        $order = Orders::with('getDetails')->where('users_id',Auth::user()->id)->where('id',$id)->first();
        if(!$order){
            flash('Order couldnot found','error');
            return back();
        }

        return view('site.acceptPaymentfromCart',compact('order'));
//        return view('front.acceptPaymentfromCart',compact('order'));
    }

    public function saveOrderFinish(Request $request, $id){


        $order = Orders::where('users_id',Auth::user()->id)->where('id',$id)->first();
        if(!$order){
            flash('Order couldnot found','error');
            return back();
        }

        $order->payment_request_details = json_encode($request->all());
        $order->save();


        $api = new Api(config('razorpay.key_id'), config('razorpay.key_secret'));
        try {
            $payment = $api->payment->fetch($request->razorpay_payment_id)->capture(array('amount' => ($order->order_total * 100))); // Captures a payment
        }
        catch (\Exception $e){
            flash($e->getMessage(),'error');
            return back();
        }

        if($payment->status == 'captured'){
            $order->payment_status = 1;
            $order->order_status = 1;
            OrderDetails::where('order_id',$order->id)->where('users_id',Auth::user()->id)->update(['purchase_complete' => 1]);

            $ob =   new CommissionSplitController();
            $ob->split($order);
        }
        else{
            $order->payment_status = 0;
            $order->order_status = 0;
        }
        $order->payment_details = json_encode((array)$payment);
        $order->save();

        //save after everything
        if($payment->status == 'captured') {
            $orderDetailItems = OrderDetails::where('order_id', $order->id)->where('users_id', Auth::user()->id)->get();
            foreach ($orderDetailItems as $item) {
                if($item->product_type == 'deal' || $item->product_type == 'coupon'){
                    MerchantDeal::find($item->product_id)->increment('sold',$item->quatity);
                    MerchantDeal::find($item->product_id)->decrement('stock',$item->quatity);
                }
            }
        }

        return view('site.acceptPaymentfromCart',compact('order'));
    }

    public function flashsaleOrder($id,$addressId){

        $dAddress = DeliveryAddress::where('users_id', Auth::user()->id)->where('id', $addressId)->first();
        if(!$dAddress){
            flash('Please Choose Valid Delivery Address','error');
            return back();
        }

        $flashSale =   FlashSale::with('getOwner')->with('isPurchased')->where('status',1)->where('id', $id)->first();

        if($flashSale) {
            if ($flashSale->end_time >= Carbon::now()) {
                if ($flashSale->start_time <= Carbon::now()) {
                    //currently sale on
                } else {
                    // sale yet to start
                    flash('Flash sale yet to Start','error');
                    return back();

                }
            } else {
                //sale passed
            }
        }
        else{
            flash('Flash sale not found','error');
            return back();
        }

        //check Stock
        if( ($flashSale->totalstock - $flashSale->sold) < 1 ){
            flash('Sorry Sold Out','error');
            return back();
        }

        // check already purchased
        if(count($flashSale->isPurchased)){
            //already purchased

        }

        $flashSale->quantitySale  = 1;
        $flashSale->termsConditions = 'removed';
        $flashSale->description = 'removed';

        $order_details['flashSale'] = $flashSale;
        $order = new Orders();
        $order->users_id = Auth::user()->id;
        $order->address_id = $dAddress->id;
        $order->address_details = json_encode($dAddress);
        $order->order_date = date('Y-m-d H:i:s');
        $order->order_status = 0; // pending Order
        $order->shipped_date = ''; // not Shipped Yet
        $order->expiry = Carbon::now()->addMinutes(15);
        $order->order_total = $flashSale->offer_price; // order Total
        $order->shipping_details = '';
        $order->payment_status = 0; // pending Payment
        $order->payment_details = '';
        $order->payment_request_details = ''; //should taken from payment gateway
        $order->order_details = json_encode($order_details);
        $order->save();

        $orderDetail = new OrderDetails();
        $orderDetail->product_id = $flashSale->id;
        $orderDetail->product_type = 'flashsale';
        $orderDetail->order_id = $order->id;
        $orderDetail->users_id = Auth::user()->id;
        $orderDetail->quatity = 1;
        $orderDetail->price = $flashSale->offer_price;
        $orderDetail->save();

        $api = new Api(config('razorpay.key_id'), config('razorpay.key_secret'));
        $razorOrder  = $api->order->create(array('receipt' => $order->id, 'amount' => ($order->order_total*100), 'currency' => 'INR')); // Creates order

        $order->razorpay_order_id = $razorOrder->id;
        $order->razorpay_order_details = json_encode((array)$razorOrder);
        $order->save();

        return redirect()->route('user.viewOrder',$order->id);

    }

    public function generateCoupon($id){

        $singleDeal =   MerchantDeal::with('getOwner')->where('start_time', '<=', Carbon::now())->where('status',1)->where('id', $id)->first();
        if($singleDeal){
            if($singleDeal->stock>0){
                $limit = $singleDeal->userBuyLimit;
                $orderDetail = OrderDetails::
                where('product_id',$singleDeal->id)
                ->where('product_type','coupon')
                ->where('users_id',Auth::user()->id)
                ->sum('quatity');
                if( $orderDetail < $limit ){

                    $deal = $singleDeal;
                    $singleDeal->stock = $singleDeal->stock-1;
                    $singleDeal->sold = $singleDeal->sold+1;
                    $singleDeal->save();

                    $deal->quantitySale = 1;
                    $deal->termsConditions = 'removed';

                    $orderDetail = new OrderDetails();
                    $orderDetail->product_id = $deal->id;
                    $orderDetail->product_type = 'coupon';
                    $orderDetail->order_id = 0;
                    $orderDetail->merchant_id = $deal->merchant_id;
                    $orderDetail->store_id = $deal->store_id;
                    $orderDetail->delivery_status = 0;
                    $orderDetail->users_id = Auth::user()->id;
                    $orderDetail->quatity = 1;
                    $orderDetail->price = $deal->online_sell_price;
                    $orderDetail->purchase_complete = 1;
                    $orderDetail->expiry =  Carbon::now()->addDays(7);
                    $orderTotal = 0;
                    $orderDetail->save();

                    $order_details['deals'][] = $deal;

                    $order = new Orders();
                    $order->users_id = Auth::user()->id;
                    $order->address_id = 0;
                    $order->address_details = json_encode(array('name'=>'Pick up at Store'));
                    $order->order_date = date('Y-m-d H:i:s');
                    $order->order_status = 0; // pending Order
                    $order->expiry = Carbon::now()->addMinutes(15);
                    $order->order_total = $deal->online_sell_price; // order Total
                    $order->shipped_date = ''; // not Shipped Yet
                    $order->shipping_details = '';
                    $order->payment_status = 0; // pending Payment
                    $order->payment_details = '';
                    $order->payment_request_details = ''; //should taken from payment gateway
                    $order->order_details = json_encode($order_details);
                    $order->order_status = 1;
                    $order->save();

                    $orderDetail->order_id = $order->id;
                    $orderDetail->coupon_code = $order->id. strtoupper(uniqid()).Auth::user()->id;
                    $orderDetail->save();
                    return redirect()->route('user.viewOrder', $order->id);

            }
                else{
                    flash('You can purchase maximum of '.$limit.' items ','error');
                    return back();
                }
            }
            else{
                flash('Sorry Deal Out of Stock','error');
                return back();
            }
        }
        else{
            flash('Sorry Deal not Found','error');
            return back();
        }
    }

    public function addDeliveryAddress(Request $request){

        $rules=array(
            'fullName'    =>  'required',
            'mobile'    =>  'required',
            'pincode'    =>  'required',
            'addr1'    =>  'required',
            'city'    =>  'required',
        );
        $validate= Validator::make($request->all(),$rules);
        if($validate->fails())
        {
            return back()->withInput($request->all())->withErrors($validate);
        }
        if($request->id > 0){
            $deliveryAddress = DeliveryAddress::where('users_id',Auth::user()->id)->where('id', $request->id)->first();

            if(!$deliveryAddress){
                return back();
            }
        }
        else{
            $deliveryAddress = new DeliveryAddress;
        }
        $deliveryAddress->fullName =$request->fullName;
        $deliveryAddress->users_id =Auth::user()->id;
        $deliveryAddress->mobileNumber =$request->mobile;
        $deliveryAddress->pincode =$request->pincode;
        $deliveryAddress->addr1 =$request->addr1;
        $deliveryAddress->addr2 =$request->addr2;
        $deliveryAddress->city =$request->city;
        $deliveryAddress->state =$request->state;
        $deliveryAddress->landmark =$request->landmark;
        $deliveryAddress->save();

        return back();

    }

    public function deleteDeliveryAddress(Request $request, $id){

        $deliveryAddress = DeliveryAddress::where('users_id',Auth::user()->id)
        ->where('id',$id)->first();

        if($deliveryAddress){
            $deliveryAddress->delete();
            flash('Delivery address deleted','success');
        }
        else{
            flash('Delivery address couldnot be deleted','error');

        }


        return back();

    }

    public function sentOtp(Request $request)
    {
        if((User::where('phone',$request->phone)->first()))
        {
            return 'User already exists';
        }

        Session::put('otp', rand(1000,9999));
        Session::put('mobile', $request->phone);
        return SmsController::sent_sms($request->phone,'OTP for DealDaa is '.Session::get('otp'));
        return 'Otp Sent';
    }

    public function sentOtp1(Request $request)
    {
        if(!(User::where('phone',$request->phone)->first()))
        {
            return 'User not found';
        }
        Session::put('otp', rand(1000,9999));
        Session::put('mobile', $request->phone);
        return SmsController::sent_sms($request->phone,'OTP for DealDaa is '.Session::get('otp'));
        return 'Otp Sent';
    }

    public function otpVerification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json('Invalid  Otp',200);
        }
        else if(Session::get('otp') != $request->otp){
            return response()->json('Invalid  Otp',200);
        }
        else{
            return response()->json('Otp Verified',200);
        }
    }

    public function passwordReset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password_confirmation' => 'required',
            'password' => 'required|confirmed',

        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->getMessageBag()->first());
        }
        else
        {
            $user   =   User::where('phone',Session::get('mobile'))->first();
            if($user)
            {
                $user->password = Hash::make($request->password);
                $user->save();
                if($user->type=='merchant')
                {
                    return redirect()->route('mertchant.signin');
                }
                elseif($user->type=='freelancer')
                {
                    return redirect()->route('freelancer.signin');
                }
                elseif($user->type=='admin')
                {
                    return redirect()->route('admin.login');
                }
                else
                {
                    return redirect()->route('signin');
                }

            }
            else
            {
                return back()->withInput()->withErrors(['password'=>'User not found']);
            }

        }
    }
    
    public function getAppLink(Request $request)
    {
        $rules=array(
            'number'    =>  'required|numeric',
        );
        $validate= Validator::make($request->all(),$rules);
        if($validate->fails())
        {
            return response()->json(['msg'=>$validate->errors()], 400);
        }
        $applink    =   'https://play.google.com/store/apps/details?id=zeekoi.com.instanttrolls';
        $res='';
        try
        {
            $res    =   SmsController::sent_sms($request->mobile,'Dealdaa,The best app with offers.Follow link to download app'.$applink);
        }
        catch(Exception $e)
        {
            
        }
        if($res=='sent')
        {
            return response()->json(['msg'=>'App Link Send to '.$request->number], 200);
        }
        else
        {
           return response()->json(['msg'=>'Invalid Number.Please enter valid number'], 400); 
        }
    
    }

    public function contactForm(Request $request)
    {
        $rules=array(
            'name'    =>  'required',
            'email'    =>  'required|email',
            'message'    =>  'required',
        );
        $validate= Validator::make($request->all(),$rules);
        if($validate->fails())
        {
            return response()->json(['msg'=>$validate->errors()], 400);
        }

        try
        {
            Mail::to('jebin@zeekoi.com')->send(new ContactFormMail($request));
            return response()->json(['msg'=>'Mail Send Successful','type'=>'success'], 200);
        }
        catch(Exception $e)
        {
            return response()->json(['msg'=>['error'=>'Something Went Wrong']], 400);
        }
    }

    public function getData()
    {
        DB::enableQueryLog();
        $data   =   MerchantDeal::get();
//        dd(DB::getQueryLog());
        dd($data);
    }
}
