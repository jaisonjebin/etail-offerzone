<?php
namespace App\Http\Controllers;
use App\MerchantStore;
use App\OrderDetails;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use App\Category;
use Illuminate\Support\Facades\Validator;
use Hash;
use Auth;
class MerchantController extends Controller {
    public function index()
    {
        if(Auth::user()->type =='freelancer')
        {
            $users = User::where('type','merchant')->where('ref_code','DDFL'.Auth::user()->id)->get();
        }
        else
        {
            $users = User::where('type','merchant')->get();
        }
//        return $users;
        return view('backend.listMerchant',compact('users'));
    }
    public function create($ref_id=null)
    {
        if (session()->exists('referral'))
        {
            $ref_id = strtoupper(session()->get('referral'));
        }
        else
        {
            if(Auth::check())
            {
                $ref_id =   (Auth::user()->type=='freelancer')?'DDFL'.Auth::user()->id:'';
            }
        }
        $states = DB::table('states')->where('country_id',101)->select('id','name')->get();
        $categories = Category::all();
        return view('backend.addMerchant',compact(['states','ref_id','categories']));
    }
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'email|sometimes',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'pincode' => 'required',
            'state' => 'required',
            'city' => 'required',
            'firm_name' => 'required',
            'category' => 'required', // 'contact_person' => 'required', // 'firm_phone' => 'required',
            'password' => 'required|confirmed',
            'tnc' => 'required',
        ]);
        if ($validator->fails())
        {
            return back()->withErrors($validator->errors())->withInput($request->all());
        }
        elseif(User::where('phone',$request->phone)->first())
        {
            return back()->withInput()->withErrors(['phone'=>'Phone Number already resisted']);
        }
        else {
            $user = new User;
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->password = $request->password ? Hash::make($request->password) : Hash::make('123456');
            $user->phone1 = json_encode($request->phone1);
            $user->email = $request->email;
            $user->address = $request->address;
            $user->pincode = $request->pincode;
            $user->state = $request->state;
            $user->district = $request->district;
            $user->city = $request->city;
            $user->Payee_Name = $request->account_name;
            $user->Account_No = $request->account_no;
            $user->PAN_Card_No = $request->account_pan;
            $user->IFSC = $request->account_ifsc;
            $user->Company_Name = $request->firm_name;
            $user->GSTIN = $request->gstin;
            $user->Business_Description = $request->descrption;
            $user->password = $request->password ? Hash::make($request->password) : Hash::make('123456');
            $user->type = 'merchant';
            if ($request->ref_code != '') {
                $freelancer = User::where('id', substr($request->ref_code, 4))->where('type', 'freelancer')->first();
                if ($freelancer) {
                    $user->ref_code = strtoupper($request->ref_code);
                    $user->status = 0;
                } else {
                    $user->status = 2;
                    flash('Invalid Referal Code', 'error');
                }
            }
            if ($request->hasFile('avatar')) {
                $path = $request->avatar->store('avatars', 'public_uploads');
                $user->avatar = $path;
            }
            $user->save();
            $store = new MerchantStore;
            $store->merchant_id = $user->id;
            $store->name = $request->firm_name;
            $store->category = $request->category;
            $store->gstin = $request->gstin;
            $store->descrption = $request->descrption;
            $store->contact_person = $request->name;
            $store->designation = $request->designation;
            $store->firm_phone = $request->firm_phone;
            $store->firm_email = $request->firm_email;
            $store->address = $request->address;
            $store->pincode = $request->pincode;
            $store->state = $request->state;
            $store->district = $request->district;
            $store->city = $request->city;
            $loc = [];
            $loc['latitude'] = $request->latitude;
            $loc['longitude'] = $request->longitude;
            $store->location = json_encode($loc);
            if ($request->ref_code != '') {
                if ($freelancer) {
                    $store->ref_code = strtoupper($request->ref_code);
                    $store->status = 0;
                } else {
                    $store->status = 2;
                    flash('Invalid Referal Code', 'error');
                }
            }
            $store->account_name = $request->account_name;
            $store->account_no = $request->account_no;
            $store->account_bank = $request->account_bank;
            $store->account_bank_branch = $request->account_bank_branch;
            $store->account_pan = $request->account_pan;
            $store->account_ifsc = $request->account_ifsc;
            if ($request->hasFile('address_proof')) {
                $path = $request->address_proof->store('merchant/address_proof', 'public_uploads');
                $store->address_proof = $path;
            }
            if ($request->hasFile('firm_images')) {
                $path = [];
                foreach ($request->firm_images as $img) {
                    $path[] = $img->store('merchant/store', 'public_uploads');
                }
                $store->firm_images = json_encode($path);
            }
            $store->save();
        }

		flash('Merchant Saved','success');
//		return redirect()->route('merchant.showDashboard');

		return back();
        //$ob    =    new LoginController();

        //return $ob->backendLoginPost($request);
//        return redirect()->route('merchant.showDashboard');

}
    public function edit($id)

    {
        $states = DB::table('states')->where('country_id',101)->select('id','name')->get();
        $user = User::find($id);
        $store = MerchantStore::where('merchant_id',$user->id)->first();
        $categories = Category::all();
        $freelancers = User::with('getCity')->where('type','freelancer')->where('status',1)->get();
        return 
        view('backend.profile_edit')->with('user',$user)->with('store',$store)->with('type',$user->type)->with('states',$states)->with('categories',$categories)->with('freelancers',$freelancers);
    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'email',
            'address' => 'required',
            'pincode' => 'required',
            'state' => 'required',
            'city' => 'required',
            'firm_name' => 'required',
            'category' => 'required',
            'firm_phone' => 'required',
        ]);
        if ($validator->fails())
        {
            return back()->withErrors($validator->errors())->withInput($request->all());
        }
        $user = User::find($request->user_id);
        $store = MerchantStore::where('merchant_id',$request->user_id)->first();
        if((!$user)&&(!$store))
        {
            flash('Mearchant Not Found','warning');
            return back()->withInput($request->all());
        }
        $user->name = $request->name;
        $user->phone = $request->phone;
		$user->password = $request->password?Hash::make($request->password):Hash::make('123456');
        $user->phone1 = json_encode($request->phone1);
        $user->email = $request->mail;
        $user->address = $request->address;
        $user->pincode = $request->pincode;
        $user->state = $request->state;
        $user->district = $request->district;
        $user->city = $request->city;
        $user->Payee_Name = $request->account_name;
        $user->Account_No = $request->account_no;
        $user->PAN_Card_No = $request->account_pan;
        $user->IFSC = $request->account_ifsc;
        $user->Company_Name = $request->firm_name;
        $user->GSTIN = $request->gstin;
        $user->Business_Description = $request->descrption;
        $user->type='merchant';
        if ($request->hasFile('avatar'))
        {
            $path = $request->avatar->store('avatars', 'public_uploads');
            $user->avatar = $path;
        }
        $user->save();
        if(Auth::user()->type=='admin')
        {
            $store->ref_code = strtoupper($request->ref_code);
        }
        $store->merchant_id = $user->id;
        $store->name = $request->firm_name;
        $store->category = $request->category;
        $store->gstin = $request->gstin;
        $store->descrption = $request->descrption;
        $store->contact_person = $request->contact_person;
        $store->designation = $request->designation;
        $store->firm_phone = $request->firm_phone;
        $store->firm_email = $request->firm_email;
        $store->address = $request->address;
        $store->pincode = $request->pincode;
        $store->state = $request->state;
        $store->district = $request->district;
        $store->city = $request->city;
        $loc = [];
        $loc['latitude'] = $request->latitude;
        $loc['longitude'] = $request->longitude;
        $store->location = json_encode($loc);
        $store->account_name = $request->account_name;
        $store->account_no = $request->account_no;
        $store->account_bank = $request->account_bank;
        $store->account_bank_branch = $request->account_bank_branch;
        $store->account_pan = $request->account_pan;
        $store->account_ifsc = $request->account_ifsc;
        if ($request->hasFile('address_proof'))
        {
            $path = $request->address_proof->store('merchant/address_proof', 'public_uploads');
            $store->address_proof = $path;
        }
        $sponsersPath=[];

        if($request->sponsers_1){
            foreach ($request->sponsers_1 as $item)
            {
                if($item!='') {
                    $sponsersPath[] = $item;
                }
            }
        }
        if ($request->hasFile('firm_images'))
        {
            foreach ($request->firm_images as $img)
            {
                $sponsersPath[] = $img->store('merchant/store', 'public_uploads');
            }
            $store->firm_images =json_encode($sponsersPath);
        }
        $store->save();
        flash('Merchant Profile Updated','success');
        return redirect()->route(Auth::user()->type.'.showDashboard');
    }

    public function listOrder(Request $request)
    {
        $from   =   ($request->from!='')?$request->from:Carbon::now()->subDays(30);
        $to =   ($request->to!='')?$request->to:Carbon::now();
        if(Auth::user()->type=='admin')
        {
            $orderDetails = OrderDetails::with('getOrder')->with('getStore')
                ->with('getDeal')
                ->where('product_type','!=','flashsale')
                ->whereBetween('updated_at', [$from, $to])
                ->orderBy('updated_at','desc')
                ->get();
        }
        else
        {
            $orderDetails = OrderDetails::with('getOrder')->with('getStore')
                ->with('getDeal')
                ->where('product_type','!=','flashsale')
                ->where('merchant_id',Auth::user()->id)
                ->whereBetween('updated_at', [$from, $to])
                ->orderBy('updated_at','desc')
                ->get();
        }

        return view('backend.listOrders',compact('orderDetails'));
    }

    public function viewOrder($id)
    {

        $order = Orders::with('getDetails')->where('users_id',Auth::user()->id)->where('id',$id)->first();
        if(!$order)
        {
            flash('Order couldnot found','error');
            return back();
        }

        return view('site.viewOrder',compact('order'));
//        return view('front.acceptPaymentfromCart',compact('order'));
    }
}
