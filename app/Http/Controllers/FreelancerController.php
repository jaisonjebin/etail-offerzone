<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DB;
use PragmaRX\Countries\Package\Countries;
class FreelancerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users  =   User::where('type','freelancer')->get();
        return view('backend.listFreelancer',compact('users'));
    }

    public function indexSuperFreelancer()
    {
        $users  =   User::where('type','freelancer')->where('isSuperFreelancer',1)->get();
        return view('backend.listFreelancer',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $countries  =   DB::table('countries')->pluck('name','id')->toArray();
        $states  =   DB::table('states')->select('id','name')->get();

        return view('backend.addFreelancer')->with('states',$states);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            //return $validator->errors();
            return back()->withInput()->withErrors($validator->errors());
        }
        elseif(User::where('phone',$request->phone)->first())
        {
            return back()->withInput()->withErrors(['phone'=>'Phone Number already resisted']);
        }
        else
        {
            $user = new User;
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->password = $request->password?Hash::make($request->password):Hash::make('123456');
            $user->phone1 = json_encode($request->phone1);
            $user->email = $request->email;
            $user->address = $request->address;
            $user->pincode = $request->pincode;
            $user->state = $request->state;
            $user->district = $request->district;
            $user->city = $request->city;
            $user->education = $request->education;
            $user->gender = $request->gender;
            $user->Payee_Name = $request->account_name;
            $user->Account_No = $request->account_no;
            $user->bank = $request->account_bank;
            $user->branch = $request->account_bank_branch;
            $user->PAN_Card_No = $request->account_pan;
            $user->IFSC = $request->account_ifsc;
            $user->languages = $request->languages;
            if( $request->dob)
            {
                $dob = \DateTime::createFromFormat('d/m/Y', $request->dob);
                $user->dob = $dob? $dob->format('Y-m-d H:i:s') : date('Y-m-d G:i:s' ,strtotime('today')) ;
            }
            if ($request->hasFile('avatar'))
            {
                $path = $request->avatar->store('avatars', 'public_uploads');
                $user->avatar = $path;
            }
            if ($request->hasFile('address_proof'))
            {
                $path       =   [];
                foreach ($request->address_proof as $img)
                {
                    $path[] = $img->store('freelancer/address_proof', 'public_uploads');
                }
                $user->address_proof = json_encode($path);
            }
            $user->password = $request->password?Hash::make($request->password):'123456';
            $user->type='freelancer';
            $user->save();
            flash('Freelancer Saved','success');
            return back();
        }
    }

    public function store1(Request $request)
    {
        //return $request->all();
        $validator = Validator::make($request->all(), [
            'Company_Name' => 'required',
            'Vendor_Name' => 'required',
            'ref_code'=>'required',
            'Payee_Name'=>'required',
            'BusinessDescription'=>'required',
            'password' => 'required|confirmed',
            'phone' => 'required',

        ]);

        if ($validator->fails()) {
            //return $validator->errors();
            return back()->withInput()->withErrors($validator->errors());
        }
        elseif(User::where('phone',$request->phone)->first())
        {
            return back()->withInput()->withErrors(['phone'=>'Phone Number already resisted']);
        }
        else
        {
            $user = new User;
            $user->name = $request->Vendor_Name;
            $user->phone = $request->phone;
            $user->email = $request->email;

            $user->Company_Name = $request->Company_Name;
            $user->ref_code = $request->ref_code;
            $user->Payee_Name = $request->Payee_Name;
            $user->PAN_Card_No = $request->PAN_Card_No;
            $user->GSTIN = $request->GSTIN;
            $user->Account_No = $request->Account;
            $user->IFSC = $request->IFSC;
            $user->Business_Description = $request->BusinessDescription;
            $user->password = Hash::make($request->password);
            $user->type='merchant';
            $user->save();
            flash('Merchant Saved','success');
            return back();
        }
    }

    public function index1()
    {
        if(Auth::user()->type =='freelancer') {
            $users  =   User::where('type','merchant')->where('ref_code','DDFL'.Auth::user()->id)->get();

        }
        else{
            $users  =   User::where('type','merchant')->get();

        }
        return view('backend.listMerchant',compact('users'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
//            return $validator->errors();
            return back()->withInput()->withErrors($validator->errors());
        }
        else
        {
            if(Auth::user()->id==$request->id||Auth::user()->type=='admin')
            {
                $user = User::find($request->id);
                if($request->superFreelancerRefID)
                {
                    $superFreelancer   =   User::where('type','freelancer')->where('status',1)->find(substr($request->superFreelancerRefID,4));
                    if(($superFreelancer))
                    {
                        $superFreelancer->isSuperFreelancer =   1;
                        $superFreelancer->save();
                        $user->superFreelancerRefID = $request->superFreelancerRefID;
                    }
                    else
                    {
                        return back()->withInput()->withErrors(['superFreelancerRefID'=>'No Such Freelancer']);
                    }
                }
                else
                {
                    $user->superFreelancerRefID = 0;
                }
                $user->name = $request->name;
//                $user->phone = $request->phone;
                $user->email = $request->email;
                $user->address = $request->address;
                $user->state = $request->state;
                $user->district = $request->district;
                $user->city = $request->city;

                if($request->dob!='')
                {
                    $dob = \DateTime::createFromFormat('d/m/Y', $request->dob);
                    $user->dob = $dob? $dob->format('Y-m-d H:i:s') : date('Y-m-d G:i:s' ,strtotime('today')) ;
                }
                if ($request->hasFile('avatar'))
                {
                    $path = $request->avatar->store('avatars', 'public_uploads');
                    $user->avatar = $path;
                }
                if ($request->hasFile('address_proof'))
                {
                    $path       =   [];
                    foreach ($request->address_proof as $img)
                    {
                        $path[] = $img->store('freelancer/address_proof', 'public_uploads');
                    }

                    $user->address_proof = json_encode($path);
                }
                if($request->password!='') {
                    $user->password = $request->password ? Hash::make($request->password) : Hash::make('123456');
                    $user->password_plain = $request->password ? $request->password : '123456';
                }
                $user->type='freelancer';
                $user->save();
                flash('Freelancer Profile Updated','success');
                return back();
            }
            else
            {
                flash('You Have No Permission','error');
                return back();
            }

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
//        return $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|unique:users,phone,'.$request->id,
        ]);

        if ($validator->fails()) {
            //return $validator->errors();
            return back()->withInput()->withErrors($validator->errors());
        }
        else
        {
            if(Auth::user()->id==$request->id||Auth::user()->type=='admin')
            {
                $user = User::find($request->id);
                $user->name = $request->name;
                if( $request->password!='')
                    $user->password =Hash::make($request->password);
                $user->phone = $request->phone;
                $user->phone1 = json_encode($request->phone1);
                $user->email = $request->email;
                $user->address = $request->address;
                $user->pincode = $request->pincode;
                $user->state = $request->state;
                $user->district = $request->district;
                $user->city = $request->city;
                $user->education = $request->education;
                $user->gender = $request->gender;
                $user->Payee_Name = $request->account_name;
                $user->Account_No = $request->account_no;
                $user->bank = $request->account_bank;
                $user->branch = $request->account_bank_branch;
                $user->PAN_Card_No = $request->account_pan;
                $user->IFSC = $request->account_ifsc;
                $user->languages = $request->languages;
                if($request->dob!='')
                {
                    $dob = \DateTime::createFromFormat('d/m/Y', $request->dob);
                    $user->dob = $dob? $dob->format('Y-m-d H:i:s') : date('Y-m-d G:i:s' ,strtotime('today')) ;
                }
                if ($request->hasFile('address_proof'))
                {
                    $path       =   [];
                    foreach ($request->address_proof as $img)
                    {
                        $path[] = $img->store('freelancer/address_proof', 'public_uploads');
                    }
                    $user->address_proof = json_encode($path);
                }
                if ($request->hasFile('avatar'))
                {
                    $path = $request->avatar->store('avatars', 'public_uploads');
                    $user->avatar = $path;
                }
                if($request->password!='') {
                    $user->password = $request->password ? Hash::make($request->password) : Hash::make('123456');
                    $user->password_plain = $request->password ? $request->password : '123456';
                }
                $user->type='admin';
                $user->save();
                flash('Admin Profile Updated','success');
                return back();
            }
            else
            {
                flash('You Have No Permission','error');
                return back();
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function covertFreelancer($id)
    {
        $user   =   User::find($id);
//        return $user;
        if(!$user)
        {
            flash('User Not Found','warning');
            return back();
        }
        else
        {
            $states  =   DB::table('states')->where('country_id',101)->select('id','name')->get();
            return view('backend.profile_edit')->with('user',$user)->with('type','freelancer')->with('states',$states);
//            $ob =   new AdminFunctions();
//            return $ob->profileEdit($user->id);
        }
    }
}
