<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function backendLogin()
    {
        return view('auth.login');
    }

    public function backendLoginPost(Request $request)
    {
        $rules=array(
             'email'    	=>  'required',
             'password'    	=>  'required'
        );
        $validate= Validator::make($request->all(),$rules);
        if($validate->fails())
        {
            return back()->withInput($request->all())->withErrors($validate);
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password,'status'=>1], $request->remember))
        {
            return redirect()->route(Auth::user()->type.'.showDashboard');
        }
        elseif(Auth::attempt(['phone' => $request->email, 'password' => $request->password,'status'=>1], $request->remember))
        {
            return redirect()->route(Auth::user()->type.'.showDashboard');
        }
        else
        {
            return back()->withInput($request->all())->withErrors(['email'=>'Invalid Credentials']);
//             return back()->withInput($request->all());
        }
    }
}
