<?php

namespace App\Http\Controllers;

use DB;
use App\Category;
use App\City;
use Illuminate\Http\Request;
use App\Settings;
use Auth;
use App\User;
use Illuminate\Support\Facades\Validator;

class AdminSettingsController extends Controller
{
    public function saveLuckyDrawSettings(Request $request){

        $settings = Settings::updateOrCreate(
            ['title' => 'luckydraw'], ['data' => json_encode($request->except('_token'))]
        );
        return back();
    }

    public function saveReferalSettings(Request $request)
    {
        $settings = Settings::updateOrCreate(
            ['title' => 'referalPlan'], ['data' => json_encode($request->except('_token'))]
        );
        return back();
    }

    public function show(){
        $settings['luckdraw'] = Settings::where('title','luckydraw')->first();
        $settings['referalPlan'] = Settings::where('title','referalPlan')->first();

        $districts  =   DB::table('district')->where('state_id',19)->select('id','name')->get();

        return view('backend.settings',compact('settings'));
    }
	
    public function frontSliders(){
		
        $settings = Settings::where('title','homeSliders')->first();
		$sliders = [];
		if($settings){
			$sliders = json_decode($settings->data);
		}
        return view('backend.addSlidersFront',compact('sliders'));
    }
	
    public function frontSlidersSave(Request $request){

		$bannersPath = [];
		if($request->sliderOld){
			foreach($request->sliderOld as $key=>$blockSliders){
				foreach($blockSliders as $nkey => $value){
                    $temp = [];
                    $temp['image'] = $value;
                    $temp['link'] = $request->sliderOldLink[$key.'Link'][$nkey];
                    $bannersPath[$key][] = $temp;

				}
			}
		}
		
		if($request->slider){
			foreach($request->slider as $key=>$blockSliders){
				foreach($blockSliders as $nkey =>$value){
				    $temp = [];
				    $temp['image'] = $value->store('home/sliders','public_uploads');
				    $temp['link'] = $request->sliderLink[$key.'Link'][$nkey];
					$bannersPath[$key][] = $temp;
				}
			}
		}

		$settings = Settings::updateOrCreate(
            ['title' => 'homeSliders'], ['data' => json_encode($bannersPath)]
        );
		
        return back();
    }

	public function addCategory()
    {
        $categories = Category::all();
//        return $categories;
        return view('backend.addCategory',compact('categories'));
    }

    public function saveCategory(Request $request)
    {
//        return $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'image',
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors()],401);
        }
        $image='';
        if($request->hasFile('image'))
        {
            $image = $request->image->store('category_images', 'public_uploads');
        }
        $category = Category::updateOrCreate(
            ['name' => $request->name],
            ['name' => $request->name, 'cssIconClass' => $request->cssClass, 'status'=>1, 'added_by'=>Auth::user()->id, 'image'=>$image]
        );
       return $this->addCategory();
    }

    public function statusCategory($id)
    {
        $category   =   Category::find($id);
        if($category)
        {
            $category->status   =   !($category->status);
            $category->save();
            flash('Category Status Changed','success');
            return back();
        }
        else
        {
            flash('Category Not Found','warning');
            return back();
        }
    }

    public function deleteCategory($id)
    {
        $category   =   Category::find($id);
        if($category)
        {
            $category->delete();
            flash('Category Deleted','success');
            return back();
        }
        else
        {
            flash('Category Not Found','warning');
            return back();
        }
    }

    public function showUser($id=null)
    {
        if((Auth::guest())&&((Auth::user()->type!='admin')||(Auth::user()->type!='freelancer')||(Auth::user()->type!='merchant')))
        {
            return back();
        }
        if($id==null)
        {
            $id=Auth::user()->id;
        }

        $user   =   User::find($id);
        if((Auth::user()->type=='admin')||(Auth::user()->id==$user->id))
        {
            return view('backend.viewProfile',compact('user'));
        }
        else
        {
            flash('Unauthorized','error');
            return back();
        }

    }



    public function addCity($id=11)
    {
        $cities  =   City::with('getDistrict')->where('district_id',$id)->get();
        $districts  =   \DB::table('district')->where('state_id',19)->select('id','name')->get();
        return view('backend.addCity')->with('districts',$districts)->with('cities',$cities)->with('id',$id);
    }

    public function saveCity(Request $request)
    {
//        return $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'district' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors()],401);
        }

        $city = new City;

        $city->name = $request->name;
        $city->state_id=19;
        $city->district_id=$request->district;
        $city->status=1;
        $city->timestamps = false;
        $city->save();
        flash('City Saved','success');
        return $this->addCity();
    }

    public function statusCity($id)
    {
        $city   =   City::find($id);
        if($city)
        {
            $city->status   =   !($city->status);
            $city->timestamps = false;
            $city->save();
            flash('City Status Changed','success');
            return back();
        }
        else
        {
            flash('City Not Found','warning');
            return back();
        }
    }

    public function deleteCity($id)
    {
        $category   =   Category::find($id);
        if($category)
        {
            $category->delete();
            flash('City Deleted','success');
            return back();
        }
        else
        {
            flash('City Not Found','warning');
            return back();
        }
    }

}
