<?php

namespace App\Http\Controllers;

use App\User;
use App\Wallet;
use Illuminate\Http\Request;
use Auth;
class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deposit($id,$commissionAmount,$type,$data)
    {
        $user = User::find($id);
        $user->deposit($commissionAmount, $type, $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $id=Auth::user()->id;

//        $user = User::find($id);
//        $user->deposit(100, 'deposit', ['type' => 'myPurchase','description' => 'Deposit of 100 credits for your purchase']);
//        $user->deposit(70, 'deposit', ['type' => 'myPurchase','description' => 'Deposit of 100 credits for your purchase']);
//        $user->deposit(10, 'deposit', ['type' => 'clubPoints', 'user_id'=>'1', 'user_name'=>'Vivek U','description' => 'Deposit of 100 credits for your purchase']);
//        $user->deposit(80, 'deposit', ['type' => 'clubPoints', 'user_id'=>'1', 'user_name'=>'Vivek U','description' => 'Deposit of 100 credits for your purchase']);
//        $user->withdraw(10, 'withdraw', ['type' => 'purchase',  'product_id'=>'251', 'product_details'=>$json,'description' => 'Purchase of Item #1234']);
        $myPurchase =   [];
        $clubPoints =   [];
        $transactionsHistory    =   [];
        $accountSummary['totalPoints']=0;
        $accountSummary['clubPoints']=0;
        $accountSummary['myPurchasePoints']=0;
        $accountSummary['withdrawalPoints']=0;
        $transactions    =   Wallet::where('user_id',$id)->with('getWalletTransactions')->first();
        if(isset($transactions->getWalletTransactions))
        {
            foreach ($transactions->getWalletTransactions as $transaction)
            {
                $temp   =   [];
//            if(($transaction->updated_at->format('F')==date('F')))
//            {
                $accountSummary['totalPoints']=$accountSummary['totalPoints']+($transaction->type=='deposit'?($transaction->amount):0);
                $accountSummary['withdrawalPoints']=$accountSummary['withdrawalPoints']+($transaction->type=='withdraw'?($transaction->amount):0);
                if(isset(json_decode($transaction->meta)->type))
                {
                    if ((json_decode($transaction->meta)->type == 'superfreelancerCommission')||(json_decode($transaction->meta)->type == 'freelancerCommission')||(json_decode($transaction->meta)->type == 'clubPoints'))
                    {
                        $temp['metaType']   =   isset(json_decode($transaction->meta)->type)?json_decode($transaction->meta)->type:'NA';
                        $accountSummary['clubPoints'] = $accountSummary['clubPoints'] + ($transaction->type == 'deposit' ? ($transaction->amount) : (-$transaction->amount));
                    }
                    elseif (json_decode($transaction->meta)->type == 'myPurchase')
                    {
                        $temp['metaType']   =   isset(json_decode($transaction->meta)->type)?json_decode($transaction->meta)->type:'NA';
                        $accountSummary['myPurchasePoints'] = $accountSummary['myPurchasePoints'] + ($transaction->type == 'deposit' ? ($transaction->amount) : (-$transaction->amount));
                    }
                    else
                    {

                    }
                }
//            }

                $temp['date']   =   $transaction->updated_at->format('d M Y H:i');
                $temp['datetime']   =   $transaction->updated_at;
                $temp['transactionId']   =   $transaction->hash;
                $temp['type']   =   $transaction->type;

                $temp['details']   =   $transaction->meta??'';
                $temp['amount']   =   $transaction->amount;
                $transactionsHistory[]  =   $temp;
            }
        }

//        return dd($transactionsHistory);
        return view('site.myWallet',compact(['accountSummary','transactionsHistory']));
//        return view('front.wallet',compact(['accountSummary','transactionsHistory']));

    }

    public function myWalletTransaction()
    {
        $id=Auth::user()->id;

//        $user = User::find($id);
//        $user->deposit(100, 'deposit', ['type' => 'myPurchase','description' => 'Deposit of 100 credits for your purchase']);
//        $user->deposit(70, 'deposit', ['type' => 'myPurchase','description' => 'Deposit of 100 credits for your purchase']);
//        $user->deposit(10, 'deposit', ['type' => 'clubPoints', 'user_id'=>'1', 'user_name'=>'Vivek U','description' => 'Deposit of 100 credits for your purchase']);
//        $user->deposit(80, 'deposit', ['type' => 'clubPoints', 'user_id'=>'1', 'user_name'=>'Vivek U','description' => 'Deposit of 100 credits for your purchase']);
//        $user->withdraw(10, 'withdraw', ['type' => 'purchase',  'product_id'=>'251', 'product_details'=>$json,'description' => 'Purchase of Item #1234']);
        $myPurchase =   [];
        $clubPoints =   [];
        $transactionsHistory    =   [];
        $accountSummary['totalPoints']=0;
        $accountSummary['clubPoints']=0;
        $accountSummary['myPurchasePoints']=0;
        $accountSummary['withdrawalPoints']=0;
        $transactions    =   Wallet::where('user_id',$id)->with('getWalletTransactions')->first();
        if(isset($transactions->getWalletTransactions))
        {
            foreach ($transactions->getWalletTransactions as $transaction)
            {
                $temp   =   [];
//            if(($transaction->updated_at->format('F')==date('F')))
//            {
                $accountSummary['totalPoints']=$accountSummary['totalPoints']+($transaction->type=='deposit'?($transaction->amount):0);
                $accountSummary['withdrawalPoints']=$accountSummary['withdrawalPoints']+($transaction->type=='withdraw'?($transaction->amount):0);
                if(isset(json_decode($transaction->meta)->type))
                {
                    if ((json_decode($transaction->meta)->type == 'superfreelancerCommission')||(json_decode($transaction->meta)->type == 'freelancerCommission')||(json_decode($transaction->meta)->type == 'clubPoints'))
                    {
                        $temp['metaType']   =   isset(json_decode($transaction->meta)->type)?json_decode($transaction->meta)->type:'NA';
                        $accountSummary['clubPoints'] = $accountSummary['clubPoints'] + ($transaction->type == 'deposit' ? ($transaction->amount) : (-$transaction->amount));
                    }
                    elseif (json_decode($transaction->meta)->type == 'myPurchase')
                    {
                        $temp['metaType']   =   isset(json_decode($transaction->meta)->type)?json_decode($transaction->meta)->type:'NA';
                        $accountSummary['myPurchasePoints'] = $accountSummary['myPurchasePoints'] + ($transaction->type == 'deposit' ? ($transaction->amount) : (-$transaction->amount));
                    }
                    else
                    {

                    }
                }
//            }

                $temp['date']   =   $transaction->updated_at->format('d M Y H:i');
                $temp['datetime']   =   $transaction->updated_at;
                $temp['transactionId']   =   $transaction->hash;
                $temp['type']   =   $transaction->type;

                $temp['details']   =   $transaction->meta??'';
                $temp['amount']   =   $transaction->amount;
                $transactionsHistory[]  =   $temp;
            }
        }

        return view('site.myWalletTransaction',compact(['accountSummary','transactionsHistory']));
    }

    public function myWalletWithdrawals()
    {
        $id=Auth::user()->id;

//        $user = User::find($id);
//        $user->deposit(100, 'deposit', ['type' => 'myPurchase','description' => 'Deposit of 100 credits for your purchase']);
//        $user->deposit(70, 'deposit', ['type' => 'myPurchase','description' => 'Deposit of 100 credits for your purchase']);
//        $user->deposit(10, 'deposit', ['type' => 'clubPoints', 'user_id'=>'1', 'user_name'=>'Vivek U','description' => 'Deposit of 100 credits for your purchase']);
//        $user->deposit(80, 'deposit', ['type' => 'clubPoints', 'user_id'=>'1', 'user_name'=>'Vivek U','description' => 'Deposit of 100 credits for your purchase']);
//        $user->withdraw(10, 'withdraw', ['type' => 'purchase',  'product_id'=>'251', 'product_details'=>$json,'description' => 'Purchase of Item #1234']);
        $myPurchase =   [];
        $clubPoints =   [];
        $transactionsHistory    =   [];
        $accountSummary['totalPoints']=0;
        $accountSummary['clubPoints']=0;
        $accountSummary['myPurchasePoints']=0;
        $accountSummary['withdrawalPoints']=0;
        $transactions    =   Wallet::where('user_id',$id)->with('getWalletTransactions')->first();
        if(isset($transactions->getWalletTransactions))
        {
            foreach ($transactions->getWalletTransactions as $transaction)
            {
                $temp   =   [];
                if(($transaction->type == 'withdraw'))
                {
                    $accountSummary['withdrawalPoints'] = $accountSummary['withdrawalPoints'] + ($transaction->type == 'withdraw' ? ($transaction->amount) : 0);
                    $temp['date'] = $transaction->updated_at->format('d M Y H:i');
                    $temp['datetime'] = $transaction->updated_at;
                    $temp['transactionId'] = $transaction->hash;
                    $temp['type'] = $transaction->type;
                    $temp['details'] = $transaction->meta ?? '';
                    $temp['amount'] = $transaction->amount;
                    $transactionsHistory[] = $temp;
                }
            }
        }
        return view('site.myWalletWithdrawals',compact(['accountSummary','transactionsHistory']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }





    public function showApi($id=null)
    {
        if($id==null&&Auth::check())
        {
            $id=Auth::user()->id;
        }
//        $user = User::find($id);
//        $user->deposit(100, 'deposit', ['type' => 'myPurchase','description' => 'Deposit of 100 credits for your purchase']);
//        $user->deposit(70, 'deposit', ['type' => 'myPurchase','description' => 'Deposit of 100 credits for your purchase']);
//        $user->deposit(10, 'deposit', ['type' => 'clubPoints', 'user_id'=>'1', 'user_name'=>'Vivek U','description' => 'Deposit of 100 credits for your purchase']);
//        $user->deposit(80, 'deposit', ['type' => 'clubPoints', 'user_id'=>'1', 'user_name'=>'Vivek U','description' => 'Deposit of 100 credits for your purchase']);
//        $user->withdraw(10, 'withdraw', ['type' => 'purchase',  'product_id'=>'251', 'product_details'=>$json,'description' => 'Purchase of Item #1234']);
        $myPurchase =   [];
        $clubPoints =   [];
        $transactionsHistory    =   [];
        $accountSummary['totalPoints']=0;
        $accountSummary['clubPoints']=0;
        $accountSummary['myPurchasePoints']=0;
        $accountSummary['withdrawalPoints']=0;
        $transactions    =   Wallet::where('user_id',$id)->with('getWalletTransactions')->first();
        if(($transactions))
        {
            foreach ($transactions->getWalletTransactions as $transaction)
            {
//            if(($transaction->updated_at->format('F')==date('F')))
//            {
                $accountSummary['totalPoints']=$accountSummary['totalPoints']+($transaction->type=='deposit'?($transaction->amount):0);
                $accountSummary['withdrawalPoints']=$accountSummary['withdrawalPoints']+($transaction->type=='withdraw'?($transaction->amount):0);
                if(isset(json_decode($transaction->meta)->type)) {
                    if (json_decode($transaction->meta)->type == 'clubPoints') {
                        $accountSummary['clubPoints'] = $accountSummary['clubPoints'] + ($transaction->type == 'deposit' ? ($transaction->amount) : (-$transaction->amount));
                    } elseif (json_decode($transaction->meta)->type == 'myPurchase') {
                        $accountSummary['myPurchasePoints'] = $accountSummary['myPurchasePoints'] + ($transaction->type == 'deposit' ? ($transaction->amount) : (-$transaction->amount));
                    } else {

                    }
                }
//            }

                $temp   =   [];
                $temp['date']   =   $transaction->updated_at->format('d M,Y H:i');
                $temp['datetime']   =   $transaction->updated_at;
                $temp['transactionId']   =   $transaction->hash;
                $temp['type']   =   $transaction->type;
                $temp['metaType']   =   isset(json_decode($transaction->meta)->type)?json_decode($transaction->meta)->type:'NA';
                $temp['details']   =   $transaction->meta??'';
                $temp['amount']   =   $transaction->amount;
                $transactionsHistory[]  =   $temp;
            }
        }

        return response()->json(['status'=>'success','accountSummary'=>$accountSummary,'transactionsHistory'=>$transactionsHistory]);

    }
}
