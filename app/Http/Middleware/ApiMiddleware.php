<?php

namespace App\Http\Middleware;

use App\Http\Controllers\HomeController;
use App\User;
use Closure;
use Auth;
use Illuminate\Support\Facades\Request;

class ApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where('_token',$request->header('Authorization'))->first();
        if ($user) {
            if($user->otpVerified){
                if($user->status == 1){
                    Auth::login($user);
                    $request    =   new \Illuminate\Http\Request;
                    $ob =   new HomeController();
                    $ob->changeCurrentLocation($request);
                    return $next($request);
                }
                else{
                    return $this->returnError('User not Enabled');
                }
            }
            else{
                return $this->returnError('Mobile number not Verified');
            }
        }
        else{
            return $this->returnError('Authentication Failed');
        }

    }

    function returnError($msg){
        $res['msg'] = $msg;
        $res['status'] = 'error';
        return response()->json($res,401);
    }
}
