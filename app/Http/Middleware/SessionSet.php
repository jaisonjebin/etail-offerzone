<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class SessionSet
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset($_GET['ref']))
        {
            Session::forget('referral');
            Session::put('referral', $_GET['ref']);
            Session::save();
        }
        return $next($request);
    }
}
