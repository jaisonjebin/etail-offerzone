<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FlashSale extends Model
{

    use SoftDeletes;
    protected  $table   =   'flashsale';
    protected $with =   ['getReviewsAvg'];

    public function getOwner()
    {
        return $this->belongsTo('App\User', 'added_by','id');
    }

    public function isFavourited()
    {
        return $this->hasMany('App\Favorites', 'item_id')
            ->where('users_id', Auth::id())
            ->where('type', 'flashsaleItem');
    }

    public function isPurchased()
    {
        return $this->hasMany('App\OrderDetails', 'product_id')
            ->where('users_id', Auth::id())
            ->where('product_type', 'flashsale')
            ->where('purchase_complete', '1');
    }

    public function userFavorites()
    {
        return $this->hasMany('App\Favorites', 'item_id')
            ->where('type', 'flashsaleItem');
    }

    public function getReviewsAvg()
    {
        return $this->hasMany('App\Review', 'product_id')
            ->select('rating',DB::raw('avg(rating) as ratingTotal'))
            ->where('product_type', 'flashSale');
    }

    public function getReviews()
    {
        return $this->hasMany('App\Review', 'product_id')->where('product_type', 'flashSale');
    }

    public function getReviewsSummary()
    {
        return $this->hasMany('App\Review', 'product_id')
            ->select('rating',DB::raw('count(*) as countTotal'))
            ->where('product_type', 'flashSale')
            ->groupBy('rating');
    }
}
