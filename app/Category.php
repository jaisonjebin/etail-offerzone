<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Category extends Model
{
    use SoftDeletes;
    protected $table    =   'category';
    protected $fillable =   ['name','added_by','status','image','cssIconClass'];

    public function getUser()
    {
        return $this->belongsTo('App\User', 'added_by');
    }

    public function getDeal()
    {
        return $this->hasMany('App\MerchantDeal', 'category_id')->where('dealType','!=','coupon');
    }
}
