<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetails extends Model
{
    use SoftDeletes;
    protected  $table   =   'order_details';
    protected  $with = ['getDeal','getFlashSale'];

    public function getOrder()
    {
        return $this->belongsTo('App\Orders', 'order_id','id');
    }
    public function getDeal()
    {
        return $this->belongsTo('App\MerchantDeal', 'product_id','id');
    }

    public function getFlashSale()
    {
        return $this->belongsTo('App\FlashSale', 'product_id','id');
    }

    public function getUser()
    {
        return $this->belongsTo('App\User', 'users_id','id');
    }

    public function getStore()
    {
        return $this->belongsTo('App\MerchantStore', 'store_id','id');
    }
}
