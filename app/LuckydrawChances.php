<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LuckydrawChances extends Model
{
    protected $table='luckydraw_chances';
    protected $fillable = ['luckydraw_id','users_id','type','chances'];
}
