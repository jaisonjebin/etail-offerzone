<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LuckyDrawParticipants extends Model
{
    protected  $table   =   'luckydraw_participants';

    public function getUser()
    {
        return $this->belongsTo('App\User', 'user_id','id');
    }
}
