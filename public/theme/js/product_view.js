"use strict";

function pluralize($n, $forms) {
  return $n % 10 == 1 && $n % 100 != 11
    ? $forms[0]
    : ( $n % 10 >= 2 && $n % 10 <= 4 && ( $n % 100 < 10 || $n % 100 >= 20 )
       ? $forms[1]
       : $forms[2]
      );
}

//year, month, day, hour, min, sec
var xDate = new Date(2017, 12, 1, 2, 29, 59, 999);
// time zone correction
var offset = (-60 * 3 * 60 * 1000) - (xDate.getTimezoneOffset() * 60 * 1000);

var getTimeLeft = function () {
  var delta = xDate - (new Date()) + offset;
  if (delta < 0) {
    return {d: 0, h: 0, m: 0, s: 0};
  }
  var timeLeft = new Date(delta);
  var time = {
    d: timeLeft.getUTCDate() - 1,
    h: timeLeft.getUTCHours(),
    m: timeLeft.getUTCMinutes(),
    s: timeLeft.getUTCSeconds()
  };
  return time;
};

$('.acounter').each(function () {
  var $counter = $(this),
      $d = $('.days', $counter),
      $h = $('.hours', $counter),
      $m = $('.minutes', $counter),
      $s = $('.seconds', $counter),
  rgPlural = {
    days   : $d.data('plur').split(','),
    hours  : $h.data('plur').split(','),
    minutes: $m.data('plur').split(','),
    seconds: $s.data('plur').split(',')
  };

  setInterval(function () {
    var time = getTimeLeft();



    $('.ainset', $d).html(time.d+'<span>'+pluralize(time.d,rgPlural.days)+'</span>') ;
    $('.ainset', $h).html(time.h+'<span>'+pluralize(time.h,rgPlural.hours)+'</span>');
    $('.ainset', $m).html(time.m+'<span>'+pluralize(time.m,rgPlural.minutes)+'</span>');
    $('.ainset', $s).html(time.s+'<span>'+pluralize(time.s,rgPlural.seconds)+'</span>');

    $d.attr('data-progress', time.d);
    $h.attr('data-progress', time.h);
    $m.attr('data-progress', time.m);
    $s.attr('data-progress', time.s);
  }, 200);
});