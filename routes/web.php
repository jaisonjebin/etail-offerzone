<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('getData','HomeController@getData');



//Front End before login
Route::middleware(['sessionset'])->group(function ()
{

    Route::post('setCurrentLocation', 'HomeController@changeCurrentLocation')->name('changeCurrentLocation');
    Route::get('/', 'HomeController@index')->name('user.showDashboard');
    Route::get('home', 'HomeController@index')->name('home');
    Route::get('all-categories', 'HomeController@allCategories')->name('allCategories');
    Route::get('near-by', 'HomeController@nearBy')->name('nearBy');
    Route::view('signup', 'site.signup')->name('signup');
    Route::post('user/register', 'UserController@userRegistration')->name('user.register');
    Route::view('signin', 'site.signin')->name('signin');
    Route::view('mertchant-signin', 'site.mertchant_signin')->name('mertchant.signin');
    Route::view('freelancer-signin', 'site.freelancer_signin')->name('freelancer.signin');
    Route::view('contact', 'site.contact')->name('contact');
    Route::view('about', 'site.about')->name('about');
    Route::view('terms-and-conditions', 'site.termsandconditions')->name('termsandconditions');
    Route::view('faq', 'site.faq')->name('faq');
    Route::view('privacy-policy', 'site.privacy_policy')->name('privacy_policy');
    Route::post('contact/form', 'HomeController@contactForm')->name('contact.form');
    Route::post('otpVerification', 'HomeController@sentOtp')->name('sentOTP');
    Route::post('sentOtp1', 'HomeController@sentOtp1')->name('sentOtp1');
    Route::post('otpVerification/stepTwo', 'HomeController@otpVerification')->name('otpVerification');
    Route::view('passwordReset/view', 'site.passwordReset')->name('passwordReset.view');
    Route::post('passwordReset', 'HomeController@passwordReset')->name('passwordReset');
    Route::get('getDistricts/{id}', 'AdminFunctions@getDistricts')->name('getDistricts');
    Route::get('getCities/{id}/{type?}', 'AdminFunctions@getCities')->name('getCities');

    Route::get('flashSale/list', 'HomeController@listFlashSale')->name('flashsaleItemList');
    Route::get('flashSale/{id}', 'HomeController@flashSaleItem')->name('flashsaleItem');
    Route::post('/flashSale/notify/{itemid}', 'NotificationController@FlashSaleNotifyMe')->name('flashSale.notifyme');
    Route::post('/flashSale/listLoad/{pageno}', 'HomeController@loadFlashSaleAjax')->name('loadFlashSaleAjax');
    Route::get('/flashSale/{id}', 'HomeController@flashSaleItem')->name('flashsaleItem');


    Route::get('/deal/list', 'HomeController@listDeals')->name('dealFrontist');
    Route::get('/deal/list/{categoryId}', 'HomeController@listDealsCategory')->name('deallistDealsCategory');
    Route::post('/deal/listLoad/{categoryId}/{pageno}', 'HomeController@loadDealsAjax')->name('deallistDealsAjax');
    Route::get('/deal/offer/{id}', 'HomeController@dealItem')->name('dealItem');

    Route::get('coupon/list', 'HomeController@listCoupons')->name('couponFrontist');
    Route::get('coupon/{id}', 'HomeController@dealItem')->name('couponItem');
    Route::post('coupon/listLoad/{pageno}', 'HomeController@loadCouponsAjax')->name('loadCouponsAjax');



    Route::get('/store/list', 'HomeController@listDeals')->name('storeFrontist');

    Route::group(['middleware' => 'auth'], function()
    {
        Route::get('my-profile', 'HomeController@myProfile')->name('user.myProfile');
        Route::get('my-purchase', 'HomeController@myPurchase')->name('user.myPurchase');
        Route::get('my-team', 'HomeController@myTeam')->name('user.myTeam');
        Route::get('my-notifications', 'HomeController@myNotifications')->name('user.myNotifications');
        Route::get('my-Rewards', 'HomeController@myRewards')->name('user.myRewards');
        Route::get('my-Wish-List', 'HomeController@myFavorites')->name('user.myFavorites');
        Route::get('my-Wallet', 'WalletController@show')->name('user.myWallet');
        Route::get('wallet/my-transaction', 'WalletController@myWalletTransaction')->name('user.myWalletTransaction');
        Route::get('wallet/my-withdrawals', 'WalletController@myWalletWithdrawals')->name('user.myWalletWithdrawals');
        Route::get('my-Address', 'HomeController@myAddress')->name('user.myAddress');
        Route::get('my-Settings', 'HomeController@mySettings')->name('user.mySettings');
        Route::post('changePassword', 'HomeController@changePassword')->name('user.changePassword');
        Route::post('my-profile/edit', 'HomeController@profileEdit')->name('user.profileEdit');

        Route::get('/favorites/{type}/{id}', 'FavoriteController@addToFavorites')->name('user.addtoFavorite');
        Route::get('/favorites/remove/{type}/{id}', 'FavoriteController@removeFromFavorites')->name('user.removeFavorite');

        Route::get('flashSale/{id}/checkout', 'HomeController@flashsaleItemcheckout')->name('flashsaleItemcheckout');
        Route::get('flashSale/{id}/checkout/{addressId}', 'HomeController@flashsaleOrder')->name('flashsaleOrder');
        Route::post('flashSale/addtoCart', 'HomeController@flashsaleAddtoCart')->name('user.addtoCart');

        Route::post('delivery-address/add', 'HomeController@addDeliveryAddress')->name('user.addDeliveryAddess');
        Route::get('delivery-address/delete/{id}', 'HomeController@deleteDeliveryAddress')->name('user.deleteDeliveryAddress');

        Route::get('add/cart/{pid}/{type}/{quantity?}', 'CartController@add')->name('user.addCart');
        Route::get('count/cart/{id}/{quantity}', 'CartController@count')->name('user.countCart');
        Route::get('show/cart', 'CartController@show')->name('user.showCart');
        Route::get('remove/cart/{id}', 'CartController@destroy')->name('user.removeCart');
        Route::get('cart', 'HomeController@cart')->name('cart');
        Route::get('cart/checkOut', 'HomeController@checkOut')->name('user.checkOut');
        Route::get('/cart/checkOut/{addressId}', 'HomeController@createOrder')->name('user.checkOutAddress');
        Route::get('/order/{Id}', 'HomeController@viewOrder')->name('user.viewOrder');
        Route::post('/order/{Id}', 'HomeController@saveOrderFinish')->name('user.viewOrder');

        Route::post('write-review', 'ReviewController@store')->name('user.writeReview');


        Route::get('/generateCoupon/{id}', 'HomeController@generateCoupon')->name('user.generateCoupon');

    });








    Route::post('getAppLink', 'HomeController@getAppLink')->name('getAppLink');

    //Route::get('/{page}', 'HomeController@index1')->name('soon');


    Route::get('/cron/sentNotification', 'NotificationController@cronGetFlashSaleNotifications');



    //Front End before login
    //after login
    Route::group(['middleware' => 'auth'], function()
    {

        Route::get('profileEdit/{id}', 'AdminFunctions@profileEdit')->name('profileEdit');
        Route::post('profileEditSave/freelancer', 'FreelancerController@edit')->name('freelancer.profileEdit');
        Route::post('profileEditSave/admin', 'FreelancerController@update')->name('admin.profileEdit');
        Route::post('profileEditSave/merchant', 'MerchantController@update')->name('merchant.updateMerchant');

        Route::get('makeorder',function (){
        });


    });


    Auth::routes();
    Route::get('admin/login', 'LoginController@backendLogin')->name('admin.login');
    Route::post('admin/login', 'LoginController@backendLoginPost')->name('admin.loginPost');
    Route::post('/saveMerchant', 'MerchantController@store')->name('saveMerchant');
    Route::get('/merchant/register', 'MerchantController@create')->name('addMerchant');


    // Merchant Routes
    Route::post('merchant/login', 'LoginController@backendLoginPost')->name('merchant.login');
    Route::group(['prefix' => 'merchant',  'middleware' => ['auth','auth.merchant']], function()
    {
        Route::get('/', 'Freelancer\DashboardController@index')->name('merchant.showDashboard');
        //deal
        Route::get('/addMerchantDeal', 'MerchantDealController@create')->name('merchant.addMerchantDeal');
        Route::post('/addMerchantDeal', 'MerchantDealController@store')->name('merchant.saveMerchantDeal');
        Route::get('/listMerchantDeal', 'MerchantDealController@index')->name('merchant.listMerchantDeal');
        Route::get('/merchantDeal/edit/{id}', 'MerchantDealController@edit')->name('merchant.merchantDealEdit');

        Route::get('/merchantDeal/delete/{id}', 'MerchantDealController@destroy')->name('merchant.merchantDealDelete');

        Route::post('/merchantDeal/update', 'MerchantDealController@update')->name('merchant.updatemerchantDeal');

//        Route::view('/viewOrder/{id}', 'backend.viewOrder')->name('admin.viewOrder');
//        Route::get('/viewOrder/{id}', 'MerchantController@viewOrder')->name('admin.viewOrder');
        Route::get('/listOrder', 'MerchantController@listOrder')->name('merchant.listOrder');
        Route::post('/listOrderPost', 'MerchantController@listOrder')->name('merchant.listOrderPost');

    });

//    Freelancer Routes
    Route::get('freelancer/login', 'LoginController@backendLogin')->name('freelancer.login');
    Route::group(['prefix' => 'freelancer',  'middleware' => ['auth','auth.freelancer']], function()
    {

        Route::get('/listMerchant', 'MerchantController@index')->name('freelancer.listMerchant');
        Route::get('/', 'Freelancer\DashboardController@index')->name('freelancer.showDashboard');
        Route::get('/listMerchantDeal', 'MerchantDealController@index')->name('freelancer.listMerchantDeal');

        Route::get('/merchantDeal/edit/{id}', 'MerchantDealController@edit')->name('freelancer.merchantDealEdit');
        Route::post('/merchantDeal/update', 'MerchantDealController@update')->name('freelancer.updatemerchantDeal');
    });

    //Admin Routes
    Route::group(['prefix' => 'admin',  'middleware' => ['auth.admin']], function()
    {
        Route::view('/viewOrder/{id}', 'backend.viewOrder')->name('admin.viewOrder');
//        Route::get('/viewOrder/{id}', 'MerchantController@viewOrder')->name('admin.viewOrder');
        Route::get('/listOrder', 'MerchantController@listOrder')->name('admin.listOrder');
        Route::post('/listOrderPost', 'MerchantController@listOrder')->name('admin.listOrderPost');
        Route::get('/listMerchant', 'MerchantController@index')->name('admin.listMerchant');
        Route::get('/', 'DashBoardController@showDashboard')->name('admin.showDashboard');
        Route::get('/dashboard', 'DashBoardController@showDashboard')->name('admin.showDashboard');

//        LuckyDraw
        Route::get('/addLuckyDraw', 'LuckyDrawController@create')->name('admin.addLuckyDraw');
        Route::post('/addLuckyDraw', 'LuckyDrawController@store')->name('admin.addLuckyDrawPost');
        Route::get('/listLuckyDraw', 'LuckyDrawController@index')->name('admin.listLuckyDraw');
        Route::get('/listLuckyDraw/{id}/ads', 'LuckyDrawController@advtLuckyDraw_index')->name('admin.advtLuckyDraw');
        Route::get('/listLuckyDraw/{id}/participants', 'LuckyDrawController@getParticipants')->name('admin.participants');
        Route::post('/listLuckyDraw/ads', 'LuckyDrawController@advtLuckyDraw_store')->name('admin.advtLuckyDrawSave');
        Route::get('/listLuckyDraw/ads/delete/{id}', 'LuckyDrawController@advtLuckyDraw_delete')->name('admin.advtLuckyDrawDelete');
        Route::get('/listLuckyDraw/delete/{id}', 'LuckyDrawController@destroy')->name('admin.LuckyDrawDelete');
        Route::get('/LuckyDraw/edit/{id}', 'LuckyDrawController@edit')->name('admin.LuckyDrawEdit');
        Route::post('/updateLuckyDraw', 'LuckyDrawController@update')->name('admin.updateLuckyDraw');
        Route::get('LuckyDraw/addWinners', 'LuckyDrawController@addWinners')->name('admin.addWinners');
        Route::post('LuckyDraw/addWinnerspost', 'LuckyDrawController@addWinnerspost')->name('admin.addWinnerspost');
        Route::get('LuckyDraw/deleteWinners/{id}', 'LuckyDrawController@deleteWinners')->name('admin.deleteWinners');

        Route::get('/settings', 'AdminSettingsController@show')->name('admin.settings');
        Route::get('/addCategory', 'AdminSettingsController@addCategory')->name('admin.addCategory');
        Route::post('/addCategory', 'AdminSettingsController@saveCategory')->name('admin.saveCategory');
        Route::get('/category/status/{id}', 'AdminSettingsController@statusCategory')->name('admin.CategoryStatus');
        Route::get('/category/delete/{id}', 'AdminSettingsController@deleteCategory')->name('admin.CategoryDelete');
        Route::post('/saveLuckyDrawSettings', 'AdminSettingsController@saveLuckyDrawSettings')->name('admin.saveLuckyDrawSettings');
        Route::post('/saveReferalSettings', 'AdminSettingsController@saveReferalSettings')->name('admin.saveReferalSettings');
        Route::get('/frontSilders', 'AdminSettingsController@frontSliders')->name('admin.frontsliders');
        Route::post('/frontSilders', 'AdminSettingsController@frontSlidersSave')->name('admin.frontsliders');

        Route::get('/addCity/{id?}', 'AdminSettingsController@addCity')->name('admin.addCity');
        Route::post('/addCity', 'AdminSettingsController@saveCity')->name('admin.saveCity');
        Route::get('/city/status/{id}', 'AdminSettingsController@statusCity')->name('admin.CityStatus');
        Route::get('/city/delete/{id}', 'AdminSettingsController@deleteCity')->name('admin.CityDelete');

    //Deal

        Route::get('/addMerchantDeal', 'MerchantDealController@create')->name('admin.addMerchantDeal');
        Route::post('/addMerchantDeal', 'MerchantDealController@store')->name('admin.saveMerchantDeal');
        Route::get('/listMerchantDeal', 'MerchantDealController@index')->name('admin.listMerchantDeal');
        Route::get('/merchantDeal/edit/{id}', 'MerchantDealController@edit')->name('admin.merchantDealEdit');
        Route::get('/merchantDeal/delete/{id}', 'MerchantDealController@destroy')->name('admin.merchantDealDelete');
        Route::post('/merchantDeal/update', 'MerchantDealController@update')->name('admin.updatemerchantDeal');
        Route::get('/merchantDeal/addToTopPicks/{id}/{status}', 'MerchantDealController@addToTopPicks')->name('admin.addToTopPicks');
        Route::get('/merchantDeal/addToDealoftheDay/{id}/{status}', 'MerchantDealController@addToDealoftheDay')->name('admin.addToDealoftheDay');



    //FlashSale
        Route::get('/flashSale', 'FlashSaleController@index')->name('admin.listFlashSale');
        Route::get('/addFlashSale', 'FlashSaleController@create')->name('admin.addFlashSale');
        Route::post('/addFlashSale', 'FlashSaleController@store')->name('admin.saveFlashSale');
        Route::get('/flashSale/delete/{id}', 'FlashSaleController@destroy')->name('admin.FlashSaleDelete');
        Route::get('/flashSale/edit/{id}', 'FlashSaleController@edit')->name('admin.FlashSaleEdit');
        Route::post('/flashSale/update', 'FlashSaleController@update')->name('admin.updateFlashSale');


        Route::view('/blank', 'backend.blank');


        Route::get('/users/status/{id}', 'UserController@status')->name('admin.UserStatus');
        Route::get('/users/delete/{id}', 'UserController@destroy')->name('admin.UserDelete');
        Route::get('/listUsers', 'UserController@list')->name('admin.listUsers');

        Route::get('/addFreelancer', 'FreelancerController@create')->name('admin.addFreelancer');
        Route::post('/saveFreelancer', 'FreelancerController@store')->name('admin.saveFreelancer');
        Route::get('/deleteFreelancer/{id}', 'FreelancerController@destroy')->name('admin.deleteFreelancer');
        Route::get('/listFreelancer', 'FreelancerController@index')->name('admin.listFreelancer');
        Route::get('/listSuperFreelancer', 'FreelancerController@indexSuperFreelancer')->name('admin.listSuperFreelancer');


        Route::get('/covert/freelancer/{id}', 'FreelancerController@covertFreelancer')->name('admin.covertToFreelancer');

    });


    Route::get('/profile/view/{id?}', 'AdminSettingsController@showUser')->name('viewProfile');
    Route::get('your-wallet', 'AdminFunctions@showwallet')->name('admin.wallet');






//    Only For Devs
    Route::get('test/sms', function ()
    {
//        $ob =   new \App\Http\Controllers\SmsController();
//        return dd($ob->sent_sms('9496330076','OTP 1425'));
        return 'Sorry Only For Devs';
    });
});