<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/register', 'ApiController@registerUser');
Route::post('/login', 'ApiController@loginUser');
Route::post('/verifyOTP', 'ApiController@otpVerify');
Route::post('/resentOTP', 'ApiController@resentOtp');

Route::group(['middleware' => 'api.vv'], function () {

    Route::get('/home', 'ApiController@home');
    Route::get('/getLuckyDraw', 'ApiController@getLuckyDraw');
    Route::get('/getFlashSale', 'ApiController@getFlashSale');
    Route::get('/getFlashSaleSingle/{flashSaleId}', 'ApiController@getFlashSaleSingle');
    Route::get('/getLuckyDrawAds/{luckydrawId}', 'ApiController@getLuckyDrawAds');
    Route::get('/participateLuckydraw/{luckydrawId}', 'ApiController@participateLuckydraw');
    Route::get('/viewAdsLuckyDraw/{luckydrawId}', 'ApiController@viewAdsLuckyDraw');
    Route::get('/getDeal/{dealId}', 'ApiController@getDeal');
    Route::get('/getDeals', 'ApiController@getDeals');
    Route::get('/listDealsCategory/{categoryId}/{pageno}', 'ApiController@listDealsCategory');
    Route::get('/getCoupons', 'ApiController@getCoupons');
    Route::post('/flashSale/notify/{itemid}', 'NotificationController@FlashSaleNotifyMeApi');



//    Cart

    Route::get('add/cart/{pid}/{type}/{quantity?}', 'CartController@add');
    Route::get('show/cart', 'CartController@show');
    Route::get('destroy/cart/{id}', 'CartController@destroy');
    Route::get('count/cart/{id}/{quantity}', 'CartController@count');


    Route::get('getReview/{id}/{type}', 'ApiController@getReview');
    Route::post('writeReview', 'ApiController@writeReview');

    Route::get('show/wallet', 'WalletController@showApi');

    //address
    Route::post('/delivery-address/add', 'ApiController@addDeliveryAddress');
    Route::get('/delivery-address/delete/{id}', 'ApiController@deleteDeliveryAddress');
    Route::get('/delivery-address/list', 'ApiController@listDeliveryAddress');

    //favorites
    Route::get('/favorites/{type}/{id}', 'FavoriteController@addToFavorites');
    Route::get('/favorites/remove/{type}/{id}', 'FavoriteController@removeFromFavorites');
    Route::get('/favorites/list', 'FavoriteController@listFavorites');
    Route::post('changeCurrentLocation', 'HomeController@changeCurrentLocation');

//    Route::post('changeCurrentLocation', 'HomeController@changeCurrentLocation');

    //Orders
//    Route::get('cart/checkOut', 'ApiController@checkOut')->name('user.checkOut');
    Route::get('/cart/checkOut/{addressId}', 'ApiController@createOrder');
    Route::get('/order/{Id}', 'ApiController@viewOrder');
    Route::post('/order/{Id}', 'ApiController@saveOrderFinish');
    Route::get('/orderHistory', 'ApiController@orderHistory');

    Route::get('flashSale/{id}/checkout/{addressId}', 'ApiController@flashsaleOrder')->name('flashsaleOrder');


});


//Route::get('/home2', 'ApiController@home');



