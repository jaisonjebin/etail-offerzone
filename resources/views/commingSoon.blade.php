<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/commingSoon/style.css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <title>Dealdaa</title>
</head>
<body">

<div class="container-fluid p-0">


    <div class="col-lg-12 login text-center p-0">

        <div class=" col-lg-12 text-center" style="padding-top:50px; margin-bottom:30px;">
            <img src="/commingSoon/images/logo.png" class="img-fluid ">
        </div>

        <!--<a href="#" class="mr-5">Freelancer Register</a> <a href="#">Merchent Register</a>-->


        <div class="col-lg-12 text-center">
            <img src="/commingSoon/images/boost.png" class="img-fluid">

        </div>


        <h5  style="color:#fff;font-weight:400; margin-top:30px; ">" We are coming in... "</h5>

        <div class="container">





            <div id="timer"></div>
        </div>



        <div class="col-lg-12 pl-0 pr-0 bottum_link">
            <a href="{{route('soon','aboutus')}}">About Us</a>
            <a href="{{route('soon','terms')}}">Terms & Conditions</a>
            <a href="{{route('soon','privacy')}}">Privacy Policy</a>
            <a href="{{route('soon','return')}}">Refunds/Cancellation</a>
            <a href="{{route('soon','price')}}">Pricing</a>
            <a href="{{route('soon','contact')}}">Contact Us</a>

        </div>


    </div>



</div>


<script>
    const year = new Date().getFullYear();
    const fourthOfJuly = new Date(2018, 6, 23).getTime();

    // countdown
    timer = setInterval(function() {

        // get today's date
        const today = new Date().getTime();

        // get the difference
        const diff = fourthOfJuly - today;

        // math
        days = Math.floor(diff / (1000 * 60 * 60 * 24));
        hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
        seconds = Math.floor((diff % (1000 * 60)) / 1000);

        // display
        document.getElementById("timer").innerHTML =
            "<div class=\"days\"> \
          <div class=\"numbers\">" + days + "</div>days</div> \
<div class=\"hours\"> \
  <div class=\"numbers\">" + hours + "</div>hours</div> \
<div class=\"minutes\"> \
  <div class=\"numbers\">" + minutes + "</div>minutes</div> \
<div class=\"seconds\"> \
  <div class=\"numbers\">" + seconds + "</div>seconds</div> \
</div>";

    }, 1000);

</script>




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>