<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li><li class="sidebar-search-wrapper">
    </li>

    <li class="nav-item start {{isActiveRoute(['admin.showDashboard'])}}">
        <a href="{{route('admin.showDashboard')}}" class="nav-link">
            <i class="icon-home"></i>
            <span class="title">Dashboard</span>
            <span class="selected"></span>
        </a>
    </li>

    <li class="nav-item  {{areActiveRoutes(['admin.addLuckyDraw','admin.listLuckyDraw','admin.addWinners'])}}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">Lucky Draw</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item  {{isActiveRoute('admin.addLuckyDraw')}}">
                <a href="{{route('admin.addLuckyDraw')}}" class="nav-link ">
                    <span class="title">Add Lucky Draw</span>
                </a>
            </li>
            <li class="nav-item  {{isActiveRoute('admin.listLuckyDraw')}}">
                <a href="{{route('admin.listLuckyDraw')}}" class="nav-link ">
                    <span class="title">List Lucky Draw</span>
                </a>
            </li>
            <li class="nav-item  {{isActiveRoute('admin.addWinners')}}">
                <a href="{{route('admin.addWinners')}}" class="nav-link ">
                    <span class="title">Lucky Draw Winners</span>
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item  {{areActiveRoutes(['admin.addMerchantDeal','admin.listMerchantDeal'])}}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">Deals and Coupons</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item  {{isActiveRoute('admin.addMerchantDeal')}}">
                <a href="{{route('admin.addMerchantDeal')}}" class="nav-link ">
                    <span class="title">Add Deals</span>
                </a>
            </li>
            <li class="nav-item  {{isActiveRoute('admin.listMerchantDeal')}}">
                <a href="{{route('admin.listMerchantDeal')}}" class="nav-link ">
                    <span class="title">List Deal</span>
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item  {{areActiveRoutes(['admin.addFlashSale','admin.listFlashSale'])}}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-energy"></i>
            <span class="title">Flash Sale</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item  {{isActiveRoute('admin.addFlashSale')}}">
                <a href="{{route('admin.addFlashSale')}}" class="nav-link ">
                    <span class="title">Add Flash Sale</span>
                </a>
            </li>
            <li class="nav-item  {{isActiveRoute('admin.listFlashSale')}}">
                <a href="{{route('admin.listFlashSale')}}" class="nav-link ">
                    <span class="title">List Flash Sale</span>
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item  {{areActiveRoutes(['admin.addFlashSale','admin.listFlashSale'])}}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-energy"></i>
            <span class="title">Reports</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item  {{isActiveRoute('admin.addFlashSale')}}">
                <a href="#" class="nav-link ">
                    <span class="title">Coming Soon</span>
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item  {{areActiveRoutes(['admin.listUsers'])}}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-users"></i>
            <span class="title">Users</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item  {{isActiveRoute('admin.listUsers')}}">
                <a href="{{route('admin.listUsers')}}" class="nav-link ">
                    <span class="title">Users List</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="nav-item  {{areActiveRoutes(['admin.listFreelancer','admin.addFreelancer','admin.listSuperFreelancer'])}}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-badge"></i>
            <span class="title">Freelancers</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item  {{isActiveRoute('admin.listSuperFreelancer')}}">
                <a href="{{route('admin.listSuperFreelancer')}}" class="nav-link ">
                    <span class="title">Super Freelancer List</span>
                </a>
            </li>
            <li class="nav-item  {{isActiveRoute('admin.listFreelancer')}}">
                <a href="{{route('admin.listFreelancer')}}" class="nav-link ">
                    <span class="title">Freelancer List</span>
                </a>
            </li>
            <li class="nav-item  {{isActiveRoute('admin.addFreelancer')}}">
                <a href="{{route('admin.addFreelancer')}}" class="nav-link ">
                    <span class="title">Add Freelancer</span>
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item  {{areActiveRoutes(['admin.listMerchant','admin.listMerchant'])}}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-briefcase"></i>
            <span class="title">Merchants</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item  {{isActiveRoute('admin.listMerchant')}}">
                <a href="{{route('admin.listMerchant')}}" class="nav-link ">
                    <span class="title">Merchant List</span>
                </a>
            </li>
            <li class="nav-item  {{isActiveRoute('addMerchant')}}">
                <a href="{{route('addMerchant')}}" class="nav-link ">
                    <span class="title">Add Merchant</span>
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item start {{isActiveRoute('admin.wallet')}}">
        <a href="{{route('admin.wallet')}}" class="nav-link">
            <i class="icon-wallet"></i>
            <span class="title">Wallet</span>
            <span class="selected"></span>
        </a>
    </li>
    <li class="nav-item  {{areActiveRoutes(['admin.listOrder'])}}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">Orders</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item  {{isActiveRoute('admin.listOrder')}}">
                <a href="{{route('admin.listOrder')}}" class="nav-link ">
                    <span class="title">List Order</span>
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item start {{isActiveRoute('admin.addCategory')}}">
        <a href="{{route('admin.addCategory')}}" class="nav-link">
            <i class="icon-layers"></i>
            <span class="title">Category</span>
            <span class="selected"></span>
        </a>
    </li>
    <li class="nav-item start {{isActiveRoute('admin.addCity')}}">
        <a href="{{route('admin.addCity')}}" class="nav-link">
            <i class="icon-layers"></i>
            <span class="title">City</span>
            <span class="selected"></span>
        </a>
    </li>

    <li class="nav-item start {{isActiveRoute('admin.frontsliders')}}">
        <a href="{{route('admin.frontsliders')}}" class="nav-link">
            <i class="icon-frame"></i>
            <span class="title">Slider Settings</span>
            <span class="selected"></span>
        </a>
    </li>

    <li class="nav-item start {{isActiveRoute(['admin.settings'])}}">
        <a href="{{route('admin.settings')}}" class="nav-link">
            <i class="icon-settings"></i>
            <span class="title">Settings</span>
            <span class="selected"></span>
        </a>
    </li>

	

</ul>
