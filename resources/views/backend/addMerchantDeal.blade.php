@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<link href="/back/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<link href="/back/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />

<link href="/back/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="/back/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<style>
.select2-container, .select2-drop, .select2-search, .select2-search input {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 100%;
}
.select2-container .select2-choice {
    height: 34px;
}
.select2-container .select2-choice .select2-arrow {
    display: none;
}
</style>
@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-summernote/summernote.js" type="text/javascript"></script><script src="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script>
        $(".summernote").summernote({height:250});
        $(".form_datetime").datetimepicker({
            autoclose: !0,
            isRTL: false,
            format: "dd MM yyyy - hh:ii",
            fontAwesome: !0,
            pickerPosition: "bottom-right",
            startDate: new Date(),
            todayBtn:true
        });
        $('.addFileBlock').on('click',function(){
            $(this).closest('li').prev().clone().insertBefore($(this).closest('li'));
            $(this).closest('li').prev().find('input').val('');
        })

        $('[name="offetType"]').on('change',function () {
            console.log($(this).val());
            if($(this).val() =='voucher'){
                $('.voucherPriceBlock').find('.control-label').html('Voucher Price');
                $('.couponPriceBlock').slideUp();
                $('.voucherPriceBlock').slideDown();

            }
            else if($(this).val() =='coupon'){
                $('.voucherPriceBlock').find('.control-label').html('Coupon Price');

                // $('.voucherPriceBlock').slideDown();
                 $('.voucherPriceBlock').slideUp();
                $('.couponPriceBlock').slideDown();
            }
            else {
                $('.voucherPriceBlock').slideUp();
                $('.couponPriceBlock').slideUp();
            }
        });
    </script>
	
        <script src="/back/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script>
			$(".select2, .select2-multiple").select2({
				placeholder:'Select Options',
				width: null
			});
		</script>
		
    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="">Merchant Deal</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Add Deal</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

            <form class="form-horizontal" method="post" action="{{route(Auth::user()->type.'.saveMerchantDeal')}}" enctype="multipart/form-data">
                @csrf

                <div class="row"  style="margin-top:20px;">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group1">
                                <button type="submit"  class="btn green button-submit pull-right"> Save
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
					<div class="col-md-6">
						<div class="portlet light bordered" style="margin-top:20px;">
						<div class="portlet-body">
							<h4>Deal General Details</h4>
							<div class="form-group">
								<label class="control-label col-md-4">Product Title
								</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="title" value=""  required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Short Description
								</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="small_description" value="">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Price / MRP
								</label>
								<div class="col-md-8">
									<input type="number" class="form-control" name="price" value="">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Offer Price
								</label>
								<div class="col-md-8">
									<input type="number" class="form-control" name="offer_price" value="" >
								</div>
							</div>



                                <div class="form-group">
                                    <label class="col-md-4 control-label">Deal Type</label>
                                    <div class="col-md-8">
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="offetType" value="offer" > Offer
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="offetType" value="voucher" > Voucher
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="offetType" value="coupon" > Coupon
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group voucherPriceBlock" style="display: none" >
                                    <label class="control-label col-md-4">Voucher Price
                                    </label>
                                    <div class="col-md-8">
                                        <input type="number" class="form-control" name="voucher_price">
                                    </div>
                                </div>
                                <div class="form-group couponPriceBlock" style="display: none" >
                                    <label class="control-label col-md-4">Cash Back Amount
                                    </label>
                                    <div class="col-md-8">
                                        <input type="number" class="form-control" name="cashbackamount">
                                    </div>
                                </div>

                            @if(Auth::user()->type == 'freelancer' || Auth::user()->type == 'admin')

                                <div class="form-group" >
                                    <label class="control-label col-md-4">Commission Amount
                                    </label>
                                    <div class="col-md-8">
                                        <input type="number" class="form-control" name="commission_price">
                                    </div>
                                </div>

                            @endif


							<div class="form-group">
								<label class="control-label col-md-4">Category
								</label>
								<div class="col-md-8">
									<select type="number" class="form-control select2" name="category" required >
										@foreach($category as $cat)
										<option value="{{$cat->id}}">{{$cat->name}}</option>
										@endforeach
									<select>
								</div>
							</div>
							
							

							<div class="clear-fix clearfix"></div>

						</div>
					</div>
					</div>
					<div class="col-md-6">
						<div class="portlet light bordered" style="margin-top:20px;">
						<div class="portlet-body">
							<h4>Deal Publish Deatils</h4>
							<div class="form-group">
								<label class="control-label col-md-4">Store
								</label>
								<div class="col-md-8">

									<select class="form-control select2-multiple" name="store[]" multiple required>
										@foreach($stores as $store)
                                            @if($store->getOwner)
                                                @if($store->getOwner->status == 1)
										            <option value="{{$store->id}}">{{$store->name}}</option>
                                                @endif
                                            @endif
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">Start Time
								</label>
								<div class="col-md-8">
									<input type="text" class="form-control form_datetime" name="start_time" value="{{date('d-M-Y')}}" required readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">End Time
								</label>
								<div class="col-md-8">
									<input type="text" class="form-control form_datetime" name="end_time" value="{{date('d-M-Y',strtotime('tomorrow'))}}" required readonly>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4">Stock
								</label>
								<div class="col-md-8">
									<input placeholder="Use -1 for Unlimited Stock" type="number" class="form-control " name="stock" value="" required>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4">User Buy Limit
								</label>
								<div class="col-md-8">
									<input placeholder="Use -1 for Unlimited Buy Limit" type="number" class="form-control " name="userbuylimit" value="" required>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4">List Image (2:3)
								</label>
								<div class="col-md-8">
									<input type="file" class="form-control " name="small_image" value="" required>
								</div>
							</div>

							<div class="clear-fix clearfix"></div>

						</div>
					</div>
					</div>
				</div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Deal Description</h4>
                                    <div class="form-group">
                                        <textarea name="description" class="summernote" style="display: none;"></textarea>
                                    </div>
                                    <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Terms & Condtions</h4>
                                    <div class="form-group">
                                        <textarea name="termsConditions" class="summernote" style="display: none;"></textarea>
                                    </div>
                                    <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Deal Banners (13 : 4)</h4>
                                <div class="form-group">
                                    <ol class="list-group">
                                        <li class="list-group-item"><input type="file" name="banners[]" required></li>
                                        <li class="list-group-item"><input type="file" name="banners[]"></li>
                                        <li class="list-group-item"><a class=" btn btn-primary btn-block addFileBlock"><i class="fa fa-plus"></i> Add New</a></li>
                                    </ol>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Sponser/Other Images (If any)</h4>
                                <div class="form-group">
                                    <ol class="list-group">
                                        <li class="list-group-item"><input type="file" name="sponsers[]"></li>
                                        <li class="list-group-item"><input type="file" name="sponsers[]"></li>
                                        <li class="list-group-item"><a class=" btn btn-primary btn-block addFileBlock"><i class="fa fa-plus"></i> Add New</a></li>
                                    </ol>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                        <div class="form-group1">
                            <button type="submit"  class="btn green button-submit pull-right"> Save
                                <i class="fa fa-check"></i>
                            </button>
                        </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

