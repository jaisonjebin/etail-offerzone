@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <script>
        $('#listLuckyDrawTable').dataTable({
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                info: "Showing _START_ to _END_ of _TOTAL_ records",
                infoEmpty: "No records found",
                infoFiltered: "(filtered1 from _MAX_ total records)",
                lengthMenu: "Show _MENU_",
                search: "Search:",
                zeroRecords: "No matching records found",
                paginate: {previous: "Prev", next: "Next", last: "Last", first: "First"}
            },
            bStateSave: !0,
            lengthMenu: [[5, 15, 20, -1], [5, 15, 20, "All"]],
            pageLength: 5,
            pagingType: "bootstrap_full_number",
            columnDefs: [{orderable: !1, targets: [0]}, {searchable: !1, targets: [0]}, {className: "dt-left"}],
            order: [[4, "desc"]]
        });

        $('.delete_confirmation').click(function(e) {
            e.preventDefault(); // Prevent the href from redirecting directly
            var linkURL = $(this).attr("href");
            warnBeforeRedirect(linkURL);
        });

        function warnBeforeRedirect(linkURL) {

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    );
                    window.location=linkURL;

                }
            })
        }
    </script>
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>List Lucky Draw Winners</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

            <div class="portlet light bordered">
                <div class="portlet-body">
                    <h4>Add Lucky Draw Winners</h4>
                    <div style="padding-bottom: 20px;">
                        <form class="form-inline l-saveLuckyDrawFormWinner" action="{{route("admin.addWinnerspost")}}" role="form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="col-lg-4">
                            <label class="control-label col-md-4">Name</label>
                            <input type="text" class="form-control col-md-4" id="InputCity" placeholder="Enter Name" name="name" required>
                            <label class="control-label col-md-4 padding-tb-10"><span class="label label-danger label-sm">Image Ration 1:1</span></label>
                            <input type="file" class="form-control col-md-4" accept="image/*" required name="image" >
                        </div>
                        <div class="col-lg-6">
                            <label class="control-label col-md-4">Description</label>
                            <textarea name="description" rows="4" cols="40"></textarea>
                        </div>
                        <div class="col-lg-2">
                        <input type="submit" class="btn btn-primary l-saveLuckyDrawWinner" value="Save"/>
                        </div>
                    </form>
                    </div>
                </div>
                <hr>

                <div class="portlet-body" style="padding-top: 20px;">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="listCityTable">
                        <thead>
                        <tr>
                            <th>
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                    <span></span>
                                </label>
                            </th>
                            <th> Name </th>
                            <th> Description </th>
                            <th> Actions </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($winners as $winner)
                            <tr class="odd gradeX">
                                <td>
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" value="{{$winner->id}}" />
                                        <span></span>
                                    </label>
                                </td>
                                <td> <img src="/uploads/{{$winner->image_url}}" width="100" height="100"/> </td>
                                <td> {{$winner->name}} </td>
                                <td> {{$winner->description}} </td>
                                <td>
                                    <a href="{{route('admin.deleteWinners',$winner->id)}}" data-text="Yes, Delete!" class="delete_confirmation btn btn-warning">
                                        <i class="fa fa-trash"></i></a>
                                    {{--<a href="{{route('admin.CityDelete',$city->id)}}" class="delete_confirmation btn btn-danger" data-text="Yes, delete it!">--}}
                                    {{--<i class="fa fa-trash"></i> Delete</a>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

