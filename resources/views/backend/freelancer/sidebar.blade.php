<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li>
    <li class="nav-item start {{isActiveRoute(['freelancer.showDashboard'])}}">
        <a href="{{route('freelancer.showDashboard')}}" class="nav-link">
            <i class="icon-home"></i>
            <span class="title">Dashboard</span>
            <span class="selected"></span>
        </a>
    </li>


    <li class="nav-item  {{areActiveRoutes(['freelancer.listMerchant','freelancer.listMerchant'])}}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-users"></i>
            <span class="title">Merchant</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item  {{isActiveRoute('freelancer.listMerchant')}}">
                <a href="{{route('freelancer.listMerchant')}}" class="nav-link ">
                    <span class="title">Merchant List</span>
                </a>
            </li>
            <li class="nav-item ">
                <a href="{{route('addMerchant')}}" class="nav-link " target="_blank">
                    <span class="title">Add Merchant</span>
                </a>
            </li>
        </ul>
    </li>
	
	  <li class="nav-item  {{areActiveRoutes(['freelancer.addMerchantDeal'])}}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">Merchant Deals</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item  {{isActiveRoute('freelancer.listMerchantDeal')}}">
                <a href="{{route('freelancer.listMerchantDeal')}}" class="nav-link ">
                    <span class="title">List Deal</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="nav-item start {{isActiveRoute('admin.wallet')}}">
        <a href="{{route('admin.wallet')}}" class="nav-link">
            <i class="icon-wallet"></i>
            <span class="title">Wallet</span>
            <span class="selected"></span>
        </a>
    </li>

</ul>
