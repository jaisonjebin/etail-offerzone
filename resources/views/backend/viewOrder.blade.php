@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <script>


    </script>
@endpush
@section('pagebody')

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="">Order</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>#Order NUUMBER</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

            <div class="portlet light bordered margin-top-30">
                <div class="portlet-body">
                    <div class="col-lg-6">
                        <h4 >Product Details</h4>
                        <hr>
                        <div class="col-lg-9">
                            <table class="table table-responsive table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <td>Attribute</td>
                                        <td>Value</td>
                                    </tr>
                                </thead>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Image </td>
                                        <td><img src="/uploads/merchantDeal/smallimages/0HxsoWlgC0ReRCD8bvkcTYF5DOojllM8lKaDEEli.jpeg" class="img-responsive" style=" max-height: 72px;width: auto;"></td>
                                    </tr>
                                    <tr>
                                        <td>Name </td>
                                        <td>Product Name</td>
                                    </tr>
                                    <tr>
                                        <td>Details </td>
                                        <td>Lorem Ipsm Lorem Ipsm Lorem Ipsm Lorem Ipsm</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-lg-6" style="    border-left: 1px solid #e4e3e3">
                        <h4 >Purchaser Details</h4>
                        <hr>

                        <table class="table table-responsive table-condensed table-hover">
                            </thead>
                            <tbody>
                            <tr>
                                <td>Name </td>
                                <td>Jebin</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>

                </div>
                <hr>

            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop