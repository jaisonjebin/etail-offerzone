@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <script>
        $('#listLuckyDrawTable').dataTable({
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                info: "Showing _START_ to _END_ of _TOTAL_ records",
                infoEmpty: "No records found",
                infoFiltered: "(filtered1 from _MAX_ total records)",
                lengthMenu: "Show _MENU_",
                search: "Search:",
                zeroRecords: "No matching records found",
                paginate: {previous: "Prev", next: "Next", last: "Last", first: "First"}
            },
            bStateSave: !0,
            lengthMenu: [[5, 15, 20, -1], [5, 15, 20, "All"]],
            pageLength: 5,
            pagingType: "bootstrap_full_number",
            columnDefs: [{orderable: !1, targets: [0]}, {searchable: !1, targets: [0]}, {className: "dt-left"}],
            order: [[4, "desc"]]
        });





        $('.delete_confirmation').click(function(e) {
            e.preventDefault(); // Prevent the href from redirecting directly
            var linkURL = $(this).attr("href");
            warnBeforeRedirect(linkURL);
        });

        function warnBeforeRedirect(linkURL) {

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    );
                    window.location=linkURL;

                }
            })
        }
    </script>
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Settings</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->
            <div class="col-md-6">
                <div class="portlet light bordered" style="margin-top:20px;">
                <div class="portlet-body">
                    <h4>Lucky Draw Settings</h4>
                     <form class="form-horizontal" method="post" action="{{route('admin.saveLuckyDrawSettings')}}">
                         @csrf
                         @php
                            $lD = $settings['luckdraw']? json_decode($settings['luckdraw']['data']):[];
                         @endphp
                         <div class="form-group">
                             <label class="control-label col-md-6">Points for Participating
                             </label>
                             <div class="col-md-6">
                                 <input type="number" class="form-control" name="participate_point" value="{{@$lD->participate_point}}">
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-md-6">Points for Ads Views
                             </label>
                             <div class="col-md-6">
                                 <input type="number" class="form-control" name="adview_point" value="{{@$lD->adview_point}}">
                             </div>
                         </div>
                         <div class="form-group">
                             <label class="control-label col-md-6">Points for Sharing
                             </label>
                             <div class="col-md-6">
                                 <input type="number" class="form-control" name="sharing_point" value="{{@$lD->sharing_point}}">
                             </div>
                         </div>

                         <div class="form-group1">

                             <button type="submit"  class="btn green button-submit pull-right"> Save
                                 <i class="fa fa-check"></i>
                             </button>
                         </div>

                        <div class="clear-fix clearfix"></div>
                    </form>
                </div>
            </div>
            </div>


            <div class="col-md-6">
                <div class="portlet light bordered" style="margin-top:20px;">
                    <div class="portlet-body">
                        <h4>Referral Plan Settings </h4>
                        <form class="form-horizontal" method="post"  action="{{route('admin.saveReferalSettings')}}">
                            @csrf
                            @php
                                $rF = $settings['referalPlan']? json_decode($settings['referalPlan']['data']):[];
                            @endphp
                            <div class="form-group">
                                <label class="control-label col-md-6">For Super Freelancer (%)
                                </label>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="for_super_freelancer" value="{{@$rF->for_super_freelancer}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-6">For Freelancer (%)
                                </label>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="for_freelancer" value="{{@$rF->for_freelancer}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-6">For Referrer (%)
                                </label>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="for_referrer" value="{{@$rF->for_referrer}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-6">For Purchaser (%)
                                </label>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="for_purchaser" value="{{@$rF->for_purchaser}}">
                                </div>
                            </div>

                            <div class="form-group1">

                                <button type="submit"  class="btn green button-submit pull-right"> Save
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>

                            <div class="clear-fix clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

