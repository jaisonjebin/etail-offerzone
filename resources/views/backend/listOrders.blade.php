@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <script>
        $('#listLuckyDrawTable').dataTable({
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                info: "Showing _START_ to _END_ of _TOTAL_ records",
                infoEmpty: "No records found",
                infoFiltered: "(filtered1 from _MAX_ total records)",
                lengthMenu: "Show _MENU_",
                search: "Search:",
                zeroRecords: "No matching records found",
                paginate: {previous: "Prev", next: "Next", last: "Last", first: "First"}
            },
            bStateSave: !0,
            lengthMenu: [[5, 15, 20, -1], [5, 15, 20, "All"]],
            pageLength: 5,
            pagingType: "bootstrap_full_number",
            columnDefs: [{orderable: !1, targets: [0]}, {searchable: !1, targets: [0]}, {className: "dt-left"}],
            order: [[4, "desc"]]
        });





        $('.delete_confirmation').click(function(e) {
            e.preventDefault(); // Prevent the href from redirecting directly
            var linkURL = $(this).attr("href");
            warnBeforeRedirect(linkURL);
        });

        function warnBeforeRedirect(linkURL) {

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    );
                    window.location=linkURL;

                }
            })
        }
    </script>
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>List Orders</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

            <div class="portlet light bordered">
                <div class="portlet-body">
                    <h4>Orders List</h4>
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="listLuckyDrawTable">
                        <thead>
                        <tr>
                            <th>
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                    <span></span>
                                </label>
                            </th>
                            <th> Deal Code</th>
                            <th> Type </th>
                            <th> Name </th>
                            <th> Image </th>
                            <th> Qty </th>
                            <th> Amt </th>
                            <th> Store </th>
                            <th> Date </th>
                            <th> Payment </th>
                            <th> Delivery </th>
                            <th> Actions </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orderDetails as $order)
                            
                            <tr class="odd gradeX">
                                <td>
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" value="{{$order->id}}" />
                                        <span></span>
                                    </label>
                                </td>
                                <td> {{$order->coupon_code}} </td>
                                <td> {{$order->product_type}} </td>
                                <td> {{@$order->getDeal->title}} </td>
                                <td> <img src="/uploads/{{@$order->getDeal->small_image}}" width="100px" height="100px"/> </td>
                                <td> {{$order->quatity}} </td>
                                <td> {{$order->price}} </td>
                                <td> {{$order->getStore->name}} </td>
                                <td> {{Carbon\Carbon::parse($order->created_at)->format('d M,Y')}} </td>
                                <td> {!! $order->purchase_complete!=0?'<span class="label label-sm label-success"> Success </span>':'<span class="label label-sm label-danger"> Failed </span>' !!} </td>
                                <td> {!! $order->delivery_status!=0?'<span class="label label-sm label-success"> Delivered </span>':'<span class="label label-sm label-danger"> Not Delivered </span>' !!} </td>

                                <td>
                                    <a href="{{route('admin.viewOrder',$order->id)}}" class="btn btn-info btn-xs">
                                    <i class="fa fa-pencil-square-o"></i> View</a>
                                {{--<a href="{{route('admin.FlashSaleEdit',$order->id)}}" class="btn btn-info btn-xs">--}}
                                        {{--<i class="fa fa-pencil-square-o"></i> Edit</a>--}}

                                    {{--<a href="{{route('admin.FlashSaleDelete',$order->id)}}" class="delete_confirmation btn btn-danger btn-xs">--}}
                                        {{--<i class="fa fa-trash"></i> Delete</a>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                <hr>

            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

