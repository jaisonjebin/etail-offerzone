@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

@endpush
@push('scripts')
    <script src="/back/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script>
        $('.addFileBlock').on('click',function(){
            $(this).closest('li').prev().clone().insertBefore($(this).closest('li'));
            $(this).closest('li').prev().find('input').val('');
        })
        $('.remove_btn').on('click',function () {
            $(this).closest('.fileinput').find('.thumbnail').hide();
            console.log($(this).closest('.hidden_file').find('input[type="hidden"]').val());
            $(this).closest('.hidden_file').find('input[type="hidden"]').val('');
            // $('input[name="small_image1"]').val('');
            $(this).hide();
        });
        $('.delete_confirmation').click(function(e) {
            e.preventDefault(); // Prevent the href from redirecting directly
            var linkURL = $(this).attr("href");
            warnBeforeRedirect(linkURL);
        });
        $('.clone_phone').on('click',function () {
            $('.phone_div:last').after($('.phone_div:last').clone().removeAttr('value'));
        });
        function warnBeforeRedirect(linkURL) {

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                swal(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                );
                window.location=linkURL;

            }
        })
        }
        $(document).ready(function () {
            $(".form_datetime").datepicker({
                autoclose: true,
                isRTL: false,
                format: "dd/mm/yyyy",
                fontAwesome: !0,
                pickerPosition: "bottom-right",
                endDate: new Date(),
                todayBtn:true
            });
            $('.j_state_input').trigger('change');
        });

        $('.j_state_input').on('change',function () {

            $state_id   =   $('.j_state_input').val();
            if($state_id=='19')
            {
                $.ajax({
                    type: "GET",
                    url: '{{route("getDistricts","")}}/'+$state_id,
                    dataType:'json',
                    success: function( data )
                    {
                        $('.j_district_div').show();
                        $(".j_district_input").html('');
                        $.each(data,function(key, value)
                        {
                            option='';
                            option= '<option  value=' + value.id + '>' + value.name + '</option>';

                            $(".j_district_input").append(option);
                        });
                        $('.j_district_input').select2();
                        $('.j_district_input').val($('.j_district_input').attr('data-selected'));
                        $('.j_district_input').trigger('change');
                    }
                });
            }
            else
            {
                $(".j_district_div").hide();
                $.ajax({
                    type: "GET",
                    url: '{{route("getCities","")}}/'+$state_id,
                    dataType:'json',
                    success: function( data )
                    {
                        $(".j_cities_input").html('');
                        // $('.j_cities_input').select2('destroy');

                        $.each(data,function(key, value)
                        {
                            option='';
                            option= '<option  value=' + value.id + '>' + value.name + '</option>';

                            $(".j_cities_input").append(option);
                        });
                        $('.j_cities_input').select2();
                        // $('.j_cities_input').val($('.j_cities_input').attr('data-selected'));
                    }
                });
            }

        });
        $('.j_district_input').on('change',function () {
            $district_id   =   $('.j_district_input').val();
            $.ajax({
                type: "GET",
                url: '{{route("getCities","")}}/'+$district_id+'/KL',
                dataType:'json',
                success: function( data )
                {
                    $(".j_cities_input").html('');
                    // $('.j_cities_input').select2('destroy');

                    $.each(data,function(key, value)
                    {
                        option='';
                        option= '<option  value=' + value.id + '>' + value.name + '</option>';

                        $(".j_cities_input").append(option);
                    });
                    $('.j_cities_input').select2();
                    // $('.j_cities_input').val($('.j_cities_input').attr('data-selected'));
                    $('.j_cities_input').trigger('change');
                }
            });
        })
            </script>
        @if($type=='freelancer')
        @elseif($type=='merchant')
            <script>
                var geocoder;
                    @php

                        $loc[]    =   json_decode($store->location,true)['latitude'];
                        $loc[]    =   json_decode($store->location,true)['longitude'];
                         if($loc[0]=='')
                         {
                            $loc[0]=9.876764;
                         }
                         if($loc[1]=='')
                         {
                            $loc[1]=76.193342;
                         }
                    @endphp

            var address = 'Kondotty, Kozhikode, Kerala';
            $('.j_cities_input').on('change',function () {
                address = $('.j_cities_input option:selected').text()+', '+$('.j_district_input option:selected').text()+', '+$('.j_state_input option:selected').text();
                initMap();
            });
            var marker;
            function initMap() {
                geocoder = new google.maps.Geocoder();
                var myLatlng = {lat: -25.363, lng: 131.044};

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: myLatlng
                });
                if (geocoder) {
                    geocoder.geocode({
                        'address': address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                                map.setCenter(results[0].geometry.location);

                                var infowindow = new google.maps.InfoWindow({
                                    content: '<b>' + address + '</b>',
                                    size: new google.maps.Size(150, 50)
                                });

                                marker = new google.maps.Marker({
                                    position: {lat: @php echo $loc[0]; @endphp,lng: @php echo $loc[1]; @endphp},
                                    map: map,
                                    draggable:true,
                                    title: address
                                });

                                $('#l_latitudeInput').val(marker.position.lat());
                                $('#l_longitudeInput').val(marker.position.lng());
                                // google.maps.event.addListener(marker, 'click', function() {
                                //     infowindow.open(map, marker);
                                // });
                                google.maps.event.addListener(marker, 'dragend', function() {
                                    $('#l_latitudeInput').val(marker.position.lat());
                                    $('#l_longitudeInput').val(marker.position.lng());
                                    $('.l_map').html('');
                                    $('.l_map').html('<strong>Success!</strong> Location has been saved.');
                                    $('.l_map').show();
                                    console.log(marker.position.lat());
                                });

                            } else {
                                // alert("No results found");
                                    marker = new google.maps.Marker({
                                    position:{ lat: @php echo $loc[0]; @endphp,lng: @php echo $loc[1]; @endphp },
                                    map: map,
                                    draggable:true,
                                    title: address
                                });

                            }
                        } else {
                            // alert("Geocode was not successful for the following reason: " + status);
                            marker = new google.maps.Marker({
                                position:{ lat: @php echo $loc[0]; @endphp,lng: @php echo $loc[1]; @endphp },
                                map: map,
                                draggable:true,
                                title: address
                            });
                        }
                    });
                }
                //
                // var marker = new google.maps.Marker({
                //     position: myLatlng,
                //     map: map,
                //     title: 'Click to zoom'
                // });

                map.addListener('center_changed', function() {
                    // 3 seconds after the center of the map has changed, pan back to the
                    // marker.
                    window.setTimeout(function() {
                        map.panTo(marker.getPosition());
                    }, 3000);
                });

                // google.maps.event.addListener(marker, 'click', function () {
                //     // do something with this marker ...
                //     this.setTitle('I am clicked');
                //     console.log('test');
                // });

                // marker.addListener('click', function() {
                //     map.setZoom(8);
                //     map.setCenter(marker.getPosition());
                // });
            }
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADPiaU7KWZ6B1zpN6Ps1IrjDpP1IUduRg&callback=initMap">
        </script>
        @elseif($type=='admin')
        @else
        @endif

@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Edit {{ucfirst($type)}}</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

            <div class="portlet light bordered">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if($type=='freelancer')
                    <div class="portlet-body">
                        <h4>Edit {{ucfirst($user->name)}} Profile</h4>
                        <form class="form-horizontal" method="post" role="form" action="{{route('freelancer.profileEdit')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <div class="form-body">
                                <div class="form-group col-lg-6">
                                    <label class="col-md-4 control-label">Referer Code</label>
                                    <div class="col-md-8">
                                        <input type="text" value="{{$user->superFreelancerRefID}}" class="form-control" name="superFreelancerRefID" placeholder="Enter Super Freelancer RefID">
                                        <span class="help-block error">{{$errors->first('superFreelancerRefID')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Name</label>
                                    <div class="col-md-8">
                                        <input type="text" value="{{$user->name}}" class="form-control" name="name" placeholder="Enter Name" required>
                                        <span class="help-block error">{{$errors->first('name')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Phone No</label>
                                    <div class="col-md-8">
                                        <input type="number" value="{{$user->phone}}" name="phone" class="form-control" placeholder="Enter Phone Number" required>
                                        <span class="help-block error">{{$errors->first('phone')}}</span>
                                    </div>
                                    @php $phone1    =   json_decode($user->phone1,true); @endphp
                                    @if($phone1)
                                    @foreach($phone1 as $ph)
                                        <div class="phone_div">
                                            <label class="col-md-4 control-label">Alt Phone No</label>
                                            <div class="col-md-6">
                                                <input type="number"  value="{{$ph}}" name="phone1" class="form-control" placeholder="Enter Alt Phone Number">
                                                <span class="help-block error">{{$errors->first('phone1')}}</span>
                                            </div>
                                        </div>
                                        @if($loop->last)
                                            <div class="col-md-2">
                                                <button type="button" class="clone_phone col-md-2 btn btn btn-icon-only green"><i class="fa fa-plus"></i></button>
                                            </div>
                                        @endif
                                    @endforeach
                                    @else
                                        <div class="phone_div">
                                            <label class="col-md-4 control-label">Alt Phone No</label>
                                            <div class="col-md-6">
                                                <input type="number" name="phone1[]" class="form-control" placeholder="Enter Alt Phone Number">
                                                <span class="help-block error">{{$errors->first('phone1')}}</span>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <button type="button" class="clone_phone col-md-2 btn btn btn-icon-only green"><i class="fa fa-plus"></i></button>
                                        </div>
                                    @endif
                                    <div class="clearfix"></div>

                                    <label class="col-md-4 control-label">Email Id</label>
                                    <div class="col-md-8">
                                        <input type="email" value="{{$user->email}}" name="email" class="form-control" placeholder="Enter Email Id">
                                        <span class="help-block error">{{$errors->first('email')}}</span>
                                    </div>
                                    <h4>Enter your bank account details below:</h4>
                                    <label class="control-label col-md-4">PAN Card No</label>
                                    <div class="col-md-8">
                                        <input name="account_pan" value="{{old('account_pan')?old('account_pan'):$user->PAN_Card_No}}" class="form-control {{ $errors->has('account_pan') ? ' has-error' : '' }}" type="text" placeholder="PAN Card No"/>
                                        <span class="help-block">
                                        {{ $errors->first('account_pan') }}
                                    </span>
                                    </div>
                                    <label class="control-label col-md-4">Account Name</label>
                                    <div class="col-md-8">
                                        <input name="account_name" value="{{old('account_name')?old('account_name'):$user->Payee_Name}}" class="form-control {{ $errors->has('account_name') ? ' has-error' : '' }}" type="text" placeholder="Account Name"/>
                                        <span class="help-block">
                                            {{ $errors->first('account_name') }}
                                        </span>
                                    </div>
                                    <label class="control-label col-md-4">Account No</label>
                                    <div class="col-md-8">
                                        <input name="account_no" value="{{old('account_no')? old('account_no'):$user->Account_No}}" class="form-control {{ $errors->has('account_no') ? ' has-error' : '' }}" type="text" placeholder="Account No"/>
                                        <span class="help-block">
                                            {{ $errors->first('account_no') }}
                                        </span>
                                    </div>
                                    <label class="control-label col-md-4">Name of Bank</label>
                                    <div class="col-md-8">
                                        <input name="account_bank" value="{{old('account_bank')?old('account_bank'):$user->bank}} "class="form-control  {{ $errors->has('account_bank') ? ' has-error' : '' }}" type="text" placeholder="Bank Name"/>
                                        <span class="help-block">
                                            {{ $errors->first('account_bank') }}
                                        </span>
                                    </div>
                                    <label class="control-label col-md-4">Name of Branch</label>
                                    <div class="col-md-8">
                                        <input name="account_bank_branch" value="{{old('account_bank_branch')?old('account_bank_branch'):$user->branch}}" class="form-control placeholder-no-fix {{ $errors->has('account_bank_branch') ? ' has-error' : '' }}" type="text" placeholder="Bank Branch"/>
                                        <span class="help-block">
                                            {{ $errors->first('account_bank_branch') }}
                                        </span>
                                    </div>
                                    <label class="control-label col-md-4">IFSC Code</label>
                                    <div class="col-md-8">
                                        <input name="account_ifsc" value="{{old('account_ifsc')?old('account_ifsc'):$user->IFSC}}" class="form-control placeholder-no-fix {{ $errors->has('account_ifsc') ? ' has-error' : '' }}" type="text" placeholder="IFSC Code"/>
                                        <span class="help-block">
                                            {{ $errors->first('account_ifsc') }}
                                        </span>
                                    </div>
                                    <label class="col-md-4 control-label">Password</label>
                                    <div class="col-md-8">
                                        <input type="password" name="password" class="form-control error" placeholder="Leave empty to if no change">
                                        <span class="help-block error">{{$errors->first('password')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Password Confirm</label>
                                    <div class="col-md-8">
                                        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">
                                        <span class="help-block error">{{$errors->first('password_confirm')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Profile Picture</label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                <img src="{{$user->avatar?'/uploads/'.$user->avatar:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" />
                                        </div>
                                        <div>
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="avatar"> </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="col-md-4 control-label">Gender</label>
                                    <div class="col-md-8">
                                        <select name="gender" class="form-control">
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Other">Other</option>
                                        </select>
                                        <span class="help-block error">{{$errors->first('gender')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Date Of Birth</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form_datetime" name="dob" value="{{old('dob')?old('dob'):date('d/m/Y',strtotime($user->dob))}}" required readonly>
                                        <span class="help-block error">{{$errors->first('dob')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Known Languages</label>
                                    <div class="col-md-8">
                                       <input type="text" class="form-control" value="{{old('languages')?old('languages'):$user->languages}}">
                                        <span class="help-block error">{{$errors->first('languages')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Qualification</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="education" value="{{$user->education}}" placeholder="Enter Education Level">
                                        <span class="help-block error">{{$errors->first('education')}}</span>
                                    </div>

                                    <label class="col-md-4 control-label">Pincode</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" value="{{old('picode')?old('pincode'):$user->pincode}}" name="pincode" placeholder="Enter Pincode" required>
                                        <span class="help-block error">{{$errors->first('pincode')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Address</label>
                                    <div class="col-md-8">
                                        <textarea name="address" class="form-control">{{$user->address}}</textarea>
                                        <span class="help-block error">{{$errors->first('address')}}</span>
                                    </div>
                                    <label class="control-label col-md-4">Address Proof</label>
                                    @php
                                        $add    =    json_decode($user->address_proof);
                                        $i=0;
                                    @endphp
                                    <div class="form-group">
                                        <label class="control-label ">Address Proof side 1</label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                <img src="{{(isset($add[0]))?'/uploads/'.$add[0]:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" />
                                            </div>
                                            <div>
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="address_proof[]"> </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                        @if ($errors->has('address_proof'))
                                            <span class="help-block">
                                                {{ $errors->first('address_proof') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Address Proof side21</label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                <img src="{{isset($add[1])?'/uploads/'.$add[1]:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" />
                                            </div>
                                            <div>
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="address_proof[]"> </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                        @if ($errors->has('address_proof'))
                                            <span class="help-block">
                                                {{ $errors->first('address_proof') }}
                                            </span>
                                        @endif
                                    </div>
                                    <label class="col-md-4 control-label">State</label>
                                    <div class="col-md-8">
                                        <select class="form-control j_state_input" name="state" data-selected="{{$user->state}}">
                                            @foreach($states as $state)
                                                <option value="{{$state->id}}" {{$state->id==19?'selected':''}}>{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block error">{{$errors->first('state')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label j_district_div">District</label>
                                    <div class="col-md-8 j_district_div">
                                        <select class="form-control j_district_input" name="district" data-selected="{{$user->district}}" value="{{$user->district}}">
                                        </select>
                                        <span class="help-block error">{{$errors->first('district')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">City</label>
                                    <div class="col-md-8">
                                        <select class="form-control j_cities_input" name="city" data-selected="{{$user->city}}" value="{{$user->city}}">
                                        </select>
                                        <span class="help-block error">{{$errors->first('city')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Referal Code</label>
                                    <div class="col-md-8">
                                       <input type="text" disabled readonly value="DDFL{{$user->id}}" class="form-control">
                                        <span class="help-block error">{{$errors->first('city')}}</span>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-5 col-md-8">
                                            <button type="submit" class="btn green">Submit</button>
                                            <button type="button" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                @elseif($type=='merchant')
                    <div class="portlet-body">
                        <h4>Edit {{ucfirst($user->name)}} Profile</h4>
                        <form class="register-form" method="POST" action="{{ route('merchant.updateMerchant') }}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h4>Enter your firm details below:</h4>
                                    <div class="form-group">
                                        <label class="control-label ">Firm/Shop Name *</label>
                                        <input name="firm_name" value="{{old('firm_name')?old('firm_name'):$store->name}}" class="form-control placeholder-no-fix {{ $errors->has('firm_name') ? ' has-error' : '' }}" type="text" placeholder="Firm/Shop Name" required/>
                                        @if ($errors->has('firm_name'))
                                            <span class="help-block">
                                                {{ $errors->first('firm_name') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Category *</label>
                                        <select name="category" class="select2 form-control placeholder-no-fix {{ $errors->has('category') ? ' has-error' : '' }}" required >
                                            <option>Select Category</option>

                                            @foreach($categories as $category)

                                                <option value="{{$category->id}}" {{old('category')?(old('category')==$category->id?'selected':''):$store->category==$category->id?'selected':''}}>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category'))
                                            <span class="help-block">
                                                {{ $errors->first('category') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Mobile No *</label>
                                        <input name="firm_phone"
                                               value="{{old('firm_phone')?old('firm_phone'):$store->firm_phone}}"
                                               class="form-control placeholder-no-fix {{ $errors->has('firm_phone') ? ' has-error' : '' }}"
                                               type="number" placeholder="Mobile No" required/>
                                        @if ($errors->has('firm_phone'))
                                            <span class="help-block">
                                        {{ $errors->first('firm_phone') }}
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Email Id</label>
                                        <input name="firm_email"
                                               value="{{old('firm_email')?old('firm_email'):$store->firm_email}}"
                                               class="form-control placeholder-no-fix {{ $errors->has('firm_email') ? ' has-error' : '' }}"
                                               type="email" placeholder="Email Id"/>
                                        @if ($errors->has('firm_email'))
                                            <span class="help-block">
                                    {{ $errors->first('firm_email') }}
                                    </span>
                                        @endif
                                    </div>
                                    @php $phone1    =   json_decode($user->phone1,true); @endphp
                                    @if($phone1)
                                        @foreach($phone1 as $ph)
                                            <div class="phone_div">
                                                <label class="col-md-4 control-label">Alt Phone No</label>
                                                <div class="col-md-6">
                                                    <input type="number"  value="{{$ph}}" name="phone1" class="form-control" placeholder="Enter Alt Phone Number">
                                                    <span class="help-block error">{{$errors->first('phone1')}}</span>
                                                </div>
                                            </div>
                                            @if($loop->last)
                                                <div class="col-md-2">
                                                    <button type="button" class="clone_phone col-md-2 btn btn btn-icon-only green"><i class="fa fa-plus"></i></button>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <label class="control-label ">Business Description</label>
                                        <textarea class="form-control placeholder-no-fix {{ $errors->has('descrption') ? ' has-error' : '' }}" autocomplete="off" placeholder="Business Description" name="descrption" style="height: 60px;">{{old('descrption')?old('descrption'):$store->descrption}}</textarea>
                                        @if ($errors->has('descrption'))
                                            <span class="help-block">
                                                {{ $errors->first('descrption') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">GSTIN</label>
                                        <input name="gstin" value="{{old('gstin')?old('gstin'):$store->gstin}}" class="form-control placeholder-no-fix {{ $errors->has('gstin') ? ' has-error' : '' }}" type="text" placeholder="GSTIN" />
                                        @if ($errors->has('gstin'))
                                            <span class="help-block">
                                                {{ $errors->first('gstin') }}
                                            </span>
                                        @endif
                                    </div>
                                    @if(Auth::user()->type=='admin')
                                        <div class="form-group">
                                            <label class="control-label ">Referal Code</label>
                                            <select name="ref_code" data-selected="{{old('ref_code')?old('ref_code'):substr($store->ref_code,4)}}" class="form-control placeholder-no-fix {{ $errors->has('ref_code') ? ' has-error' : '' }}">
                                                @foreach($freelancers as $freelancer)
                                                    <option value="DDFL{{$freelancer->id}}" {{$freelancer->id==substr($store->ref_code,4)?'selected':''}}>{{$freelancer->name.($freelancer->getCity?'/'.$freelancer->getCity->name:'').'/DDFL'.$freelancer->id}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('ref_code'))
                                                <span class="help-block">
                                                    {{ $errors->first('ref_code') }}
                                                </span>
                                            @endif
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label class="control-label ">Contact Person *</label>
                                        <input name="name" value="{{old('name')?old('name'):$user->name}}" class="form-control placeholder-no-fix {{ $errors->has('name') ? ' has-error' : '' }}" type="text" placeholder="Full Name" required/>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                {{ $errors->first('name') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Designation</label>
                                        <input name="designation" value="{{old('designation')?old('designation'):$store->designation}}" class="form-control placeholder-no-fix {{ $errors->has('designation') ? ' has-error' : '' }}" type="text" placeholder="Designation" />
                                        @if ($errors->has('designation'))
                                            <span class="help-block">
                                                {{ $errors->first('designation') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Mobile No *</label>
                                        <input name="phone" value="{{old('phone')?old('phone'):$user->phone}}" class="form-control placeholder-no-fix {{ $errors->has('phone') ? ' has-error' : '' }}" type="number" placeholder="Mobile No" required/>
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                {{ $errors->first('phone') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Address *</label>
                                        <input name="address" value="{{old('address')?old('address'):$user->address}}" class="form-control placeholder-no-fix {{ $errors->has('address') ? ' has-error' : '' }}" type="text" placeholder="Address" required/>
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                                {{ $errors->first('address') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6">
                                            <label class="control-label ">Pincode *</label>
                                            <input type="number" name="pincode" value="{{old('pincode')?old('pincode'):$user->pincode}}" class="form-control placeholder-no-fix {{ $errors->has('pincode') ? ' has-error' : '' }}" required  placeholder="Pincode"/>
                                            @if ($errors->has('pincode'))
                                                <span class="help-block">
                                                    {{ $errors->first('pincode') }}
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label class="control-label ">State *</label>
                                            <select data-selected="{{old('state')?old('state'):$user->state}}" name="state" class="j_state_input select2 form-control placeholder-no-fix {{ $errors->has('state') ? ' has-error' : '' }}" required>
                                                @foreach($states as $state)
                                                    <option value="{{$state->id}}" {{$user->state==$state->id?'selected':$state->id==19?'selected':''}}>{{$state->name}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('state'))
                                                <span class="help-block">
                                                    {{ $errors->first('state') }}
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group j_district_div">
                                        <label class="control-label ">District</label>
                                        <select data-selected="{{old('district')?old('district'):$user->district}}" name="district" class="j_district_input form-control placeholder-no-fix {{ $errors->has('district') ? ' has-error' : '' }}">
                                        </select>
                                        @if ($errors->has('district'))
                                            <span class="help-block">
                                                {{ $errors->first('district') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">City *</label>
                                        <select data-selected="{{old('city')?old('city'):$user->city}}" name="city" class="j_cities_input form-control placeholder-no-fix {{ $errors->has('city') ? ' has-error' : '' }}" required>
                                        </select>
                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                                {{ $errors->first('city') }}
                                            </span>
                                        @endif
                                    </div>

                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label ">Contact Person *</label>--}}
                                        {{--<input name="contact_person" value="{{old('contact_person')?old('contact_person'):$store->contact_person}}" class="form-control placeholder-no-fix {{ $errors->has('contact_person') ? ' has-error' : '' }}" type="text" placeholder="Contact Person" required/>--}}
                                        {{--@if ($errors->has('contact_person'))--}}
                                            {{--<span class="help-block">--}}
                                                {{--{{ $errors->first('contact_person') }}--}}
                                            {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}

                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label ">Mobile No *</label>--}}
                                        {{--<input name="firm_phone" value="{{old('firm_phone')?old('firm_phone'):$store->firm_phone}}" class="form-control placeholder-no-fix {{ $errors->has('firm_phone') ? ' has-error' : '' }}" type="number" placeholder="Mobile No" required/>--}}
                                        {{--@if ($errors->has('firm_phone'))--}}
                                            {{--<span class="help-block">--}}
                                                {{--{{ $errors->first('firm_phone') }}--}}
                                            {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label ">Email Id</label>--}}
                                        {{--<input name="firm_email" value="{{old('firm_email')?old('firm_email'):$store->firm_email}}" class="form-control placeholder-no-fix {{ $errors->has('firm_email') ? ' has-error' : '' }}" type="email" placeholder="Email Id"/>--}}
                                        {{--@if ($errors->has('firm_email'))--}}
                                            {{--<span class="help-block">--}}
                                                {{--{{ $errors->first('firm_email') }}--}}
                                            {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}

                                    <div class="form-group">
                                        <label class="control-label ">Select Firm Photos</label>
                                        <div class="portlet light bordered" style="margin-top:20px;">
                                            <div class="portlet-body">
                                                <h4>Firm Images</h4>
                                                <div class="form-group">
                                                    <ol class="list-group">
                                                        @php $sponsers    =    json_decode($store->firm_images); $i=0; @endphp
                                                        @if($sponsers)
                                                        @foreach($sponsers as $sponser)
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 390px; height: 120px;">
                                                                    <img src="{{$sponser!=''?'/uploads/'.$sponser:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" style="width:  390px; height: 120px;"/>
                                                                </div>
                                                                <div  class="hidden_file">
                                                                    <a href="javascript:;" class="btn red remove_btn" data-dismiss="fileinput"> Remove </a>
                                                                    <input type="hidden" class="form-control" name="sponsers_1[{{$i}}]" value="{{$sponser}}">
                                                                </div>
                                                            </div>
                                                            @php $i++; @endphp
                                                        @endforeach
                                                        @endif
                                                        <li class="list-group-item"><input type="file" name="firm_images[]"></li>
                                                        <li class="list-group-item"><a class=" btn btn-primary btn-block addFileBlock"><i class="fa fa-plus"></i> Add New</a></li>
                                                    </ol>
                                                </div>
                                                <div class="clear-fix clearfix"></div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label ">Avatar</label>--}}
                                        {{--<div class="fileinput fileinput-new" data-provides="fileinput">--}}
                                            {{--<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">--}}
                                                {{--<img src="{{$user->avatar?'/uploads/'.$user->avatar:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" />--}}
                                            {{--</div>--}}
                                            {{--<div>--}}
                                                                {{--<span class="btn red btn-outline btn-file">--}}
                                                                    {{--<span class="fileinput-new"> Select image </span>--}}
                                                                    {{--<span class="fileinput-exists"> Change </span>--}}
                                                                    {{--<input type="file" name="avatar"> </span>--}}
                                                {{--<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--@if ($errors->has('avatar'))--}}
                                            {{--<span class="help-block">--}}
                                                {{--{{ $errors->first('avatar') }}--}}
                                            {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                    @php
                                        $add    =    json_decode($store->address_proof);
                                        $i=0;
                                    @endphp
                                    <div class="form-group">
                                        <label class="control-label ">Address Proof side 1</label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                <img src="{{$add[0]?'/uploads/'.$add[0]:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" />
                                            </div>
                                            <div>
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="address_proof"> </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                        @if ($errors->has('address_proof'))
                                            <span class="help-block">
                                                {{ $errors->first('address_proof') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Address Proof side21</label>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                <img src="{{$add[1]?'/uploads/'.$add[1]:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" />
                                            </div>
                                            <div>
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="address_proof"> </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                        @if ($errors->has('address_proof'))
                                            <span class="help-block">
                                                {{ $errors->first('address_proof') }}
                                            </span>
                                        @endif
                                    </div>
                                    <h4>Bank Details</h4>
                                    <div class="form-group">
                                        <label class="control-label ">PAN Card No</label>
                                        <input name="account_pan" value="{{old('account_pan')?old('account_pan'):$store->account_pan}}" class="form-control placeholder-no-fix {{ $errors->has('account_pan') ? ' has-error' : '' }}" type="text" placeholder="PAN Card No"/>
                                        @if ($errors->has('account_pan'))
                                            <span class="help-block">
                                                {{ $errors->first('account_pan') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Account Name</label>
                                        <input name="account_name" value="{{old('account_name')?old('account_name'):$store->account_name}}" class="form-control placeholder-no-fix {{ $errors->has('account_name') ? ' has-error' : '' }}" type="text" placeholder="Account Name"/>
                                        @if ($errors->has('account_name'))
                                            <span class="help-block">
                                                {{ $errors->first('account_name') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Account No</label>
                                        <input name="account_no" value="{{old('account_no')?old('account_no'):$store->account_no}}" class="form-control placeholder-no-fix {{ $errors->has('account_no') ? ' has-error' : '' }}" type="text" placeholder="Account No"/>
                                        @if ($errors->has('account_no'))
                                            <span class="help-block">
                                                {{ $errors->first('account_no') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Bank Name</label>
                                        <input name="account_bank" value="{{old('account_bank')?old('account_bank'):$store->account_bank}} "class="form-control placeholder-no-fix {{ $errors->has('account_bank') ? ' has-error' : '' }}" type="text" placeholder="Bank Name"/>
                                        @if ($errors->has('account_bank'))
                                            <span class="help-block">
                                                {{ $errors->first('account_bank') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">Bank Branch</label>
                                        <input name="account_bank_branch" value="{{old('account_bank_branch')?old('account_bank_branch'):$store->account_bank_branch}}" class="form-control placeholder-no-fix {{ $errors->has('account_bank_branch') ? ' has-error' : '' }}" type="text" placeholder="Bank Branch"/>
                                        @if ($errors->has('account_bank_branch'))
                                            <span class="help-block">
                                                {{ $errors->first('account_bank_branch') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label ">IFSC Code</label>
                                        <input name="account_ifsc" value="{{old('account_ifsc')?old('account_ifsc'):$store->account_ifsc}}" class="form-control placeholder-no-fix {{ $errors->has('account_ifsc') ? ' has-error' : '' }}" type="text" placeholder="IFSC Code"/>
                                        @if ($errors->has('account_ifsc'))
                                            <span class="help-block">
                                                {{ $errors->first('account_ifsc') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div id="map" style="height: 280px;"></div>
                                    <div class="alert alert-success l_map">
                                        Drag marker to you Firm Location.
                                    </div>
                                    @php
                                        $location = json_decode($store->location,true);
                                    @endphp
                                    <input type="hidden" name="latitude" id="l_latitudeInput" value="{{old('l_latitudeInput')?old('l_latitudeInput'):($location ? $location['latitude']:'0')}}">
                                    <input type="hidden" name="longitude" id="l_longitudeInput" value="{{old('l_latitudeInput')?old('l_latitudeInput'):($location ? $location['longitude']:'0')}}">
                                </div>
                                <div class="form-actions">
                                    <button type="submit" id="register-submit-btn btn-lg" class="btn red uppercase pull-right">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                @elseif($type=='admin')
                    <div class="portlet-body">
                        <h4>Edit {{ucfirst($user->name)}} Profile</h4>
                        <form class="form-horizontal" method="post" role="form" action="{{route('admin.profileEdit')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <div class="form-body">
                                <div class="form-group col-lg-6">
                                    <label class="col-md-4 control-label">Name</label>
                                    <div class="col-md-8">
                                        <input type="text" value="{{$user->name}}" class="form-control" name="name" placeholder="Enter Name" required>
                                        <span class="help-block error">{{$errors->first('name')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Phone No</label>
                                    <div class="col-md-8">
                                        <input type="number" value="{{$user->phone}}" name="phone" class="form-control" placeholder="Enter Phone Number" required>
                                        <span class="help-block error">{{$errors->first('phone')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Email Id</label>
                                    <div class="col-md-8">
                                        <input type="email" value="{{$user->email}}" name="email" class="form-control" placeholder="Enter Email Id">
                                        <span class="help-block error">{{$errors->first('email')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Password</label>
                                    <div class="col-md-8">
                                        <input type="password" name="password" class="form-control error" placeholder="Leave empty to if no change">
                                        <span class="help-block error">{{$errors->first('password')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Password Confirm</label>
                                    <div class="col-md-8">
                                        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">
                                        <span class="help-block error">{{$errors->first('password_confirm')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">Profile Picture</label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                            <img src="{{$user->avatar?'/uploads/'.$user->avatar:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" />
                                        </div>
                                        <div>
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="avatar"> </span>
                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="col-md-4 control-label">Address</label>
                                    <div class="col-md-8">
                                        <textarea name="address" class="form-control">{{$user->address}}</textarea>
                                        <span class="help-block error">{{$errors->first('address')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">State</label>
                                    <div class="col-md-8">
                                        <select class="form-control j_state_input" name="state" data-selected="{{$user->state?$user->state:''}}">
                                            @foreach($states as $state)
                                                <option value="{{$state->id}}" {{$user->state==$state->id?'selected':''}}>{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block error">{{$errors->first('state')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label j_district_div">District</label>
                                    <div class="col-md-8 j_district_div">
                                        <select class="form-control j_district_input" name="district" data-selected="{{$user->district}}" value="{{$user->district}}">
                                        </select>
                                        <span class="help-block error">{{$errors->first('district')}}</span>
                                    </div>
                                    <label class="col-md-4 control-label">City</label>
                                    <div class="col-md-8">
                                        <select class="form-control j_cities_input" name="city" data-selected="{{$user->city}}" value="{{$user->city}}">
                                        </select>
                                        <span class="help-block error">{{$errors->first('city')}}</span>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-5 col-md-8">
                                            <button type="submit" class="btn green">Submit</button>
                                            <button type="button" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                @else
                    <div class="portlet-body">
                        <h4>Nothing</h4>

                    </div>
                @endif
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

