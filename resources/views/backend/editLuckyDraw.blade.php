@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-summernote/summernote.js" type="text/javascript"></script><script src="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script>
        $('.remove_btn').on('click',function () {
            $(this).closest('.fileinput').find('.thumbnail').hide();
            console.log($(this).closest('.hidden_file').find('input[type="hidden"]').val());
            $(this).closest('.hidden_file').find('input[type="hidden"]').val('');
            // $('input[name="small_image1"]').val('');
            $(this).hide();
        });
        $(".summernote").summernote({height:250});
        $(".form_datetime").datetimepicker({
            autoclose: !0,
            isRTL: false,
            format: "dd MM yyyy - hh:ii",
            fontAwesome: !0,
            pickerPosition: "bottom-right",
            // startDate: new Date(),
            todayBtn:true
        });
        $('.addFileBlock').on('click',function(){
            $(this).closest('li').prev().clone().insertBefore($(this).closest('li'));
            $(this).closest('li').prev().find('input').val('');
        })
    </script>
    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="">Lucy Draw</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Edit Lucky Draw</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

            <form class="form-horizontal" method="post" action="{{route('admin.updateLuckyDraw')}}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$luckyDraw->id}}">

                <div class="row"  style="margin-top:20px;">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group1">
                                <button type="submit"  class="btn green button-submit pull-right"> Update
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="portlet light bordered" style="margin-top:20px;">
                        <div class="portlet-body">
                            <h4>Lucky Draw General Details</h4>
                            <div class="form-group">
                                <label class="control-label col-md-2">Lucky Draw Title
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="title" value="{{old('title')?old('title'):$luckyDraw->title}}"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Description
                                </label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="description">{{old('description')?old('description'):$luckyDraw->description}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Start Time
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control form_datetime" name="start_time" value="{{old('start_time')?old('start_time'):Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$luckyDraw->start_time)->format('j M Y - H:i')}}" required readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">End Time
                                </label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control form_datetime" name="end_time" value="{{old('end_time')?old('end_time'):Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$luckyDraw->end_time)->format('j M Y - H:i')}}"  required readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Banner Image (2:3)
                                </label>
                                <div class="col-md-10">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 390px; height: 120px;">
                                            <img src="{{$luckyDraw->banner?'/uploads/'.$luckyDraw->banner:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" style="width: 390px; height: 120px;"/>
                                        </div>
                                        <div class="hidden_file">
                                            <a href="javascript:;" class="btn red remove_btn"> Remove </a>
                                            <input type="hidden" class="form-control " name="banner1" value="{{$luckyDraw->banner}}">
                                            <input type="file" class="form-control " name="banner" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Advertisement Type</label>
                                <div class="col-md-9">
                                    <input type="checkbox" name="ad_flag" value="1" {{$luckyDraw->ad_flag?'checked':''}} class="make-switch" data-on-text="&nbsp;Internal&nbsp;" data-off-text="&nbsp;Google&nbsp;">
                                </div>
                            </div>
                            <div class="clear-fix clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group1">
                                <button type="submit"  class="btn green button-submit pull-right"> Update
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

