@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <script>
        $('#listclubpoints').dataTable({
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                info: "Showing _START_ to _END_ of _TOTAL_ records",
                infoEmpty: "No records found",
                infoFiltered: "(filtered1 from _MAX_ total records)",
                lengthMenu: "Show _MENU_",
                search: "Search:",
                zeroRecords: "No matching records found",
                paginate: {previous: "Prev", next: "Next", last: "Last", first: "First"}
            },
            bStateSave: !0,
            lengthMenu: [[5, 50, 100, -1], [5, 50, 100, "All"]],
            pageLength: 5,
            pagingType: "bootstrap_full_number",
            columnDefs: [{orderable: !1, targets: [0]}, {searchable: !1, targets: [0]}, {className: "dt-left"}],
            order: [[0, "desc"]]
        });
        $('#listmypurchasepoints').dataTable({
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                info: "Showing _START_ to _END_ of _TOTAL_ records",
                infoEmpty: "No records found",
                infoFiltered: "(filtered1 from _MAX_ total records)",
                lengthMenu: "Show _MENU_",
                search: "Search:",
                zeroRecords: "No matching records found",
                paginate: {previous: "Prev", next: "Next", last: "Last", first: "First"}
            },
            bStateSave: !0,
            lengthMenu: [[5, 50, 100, -1], [5, 50, 100, "All"]],
            pageLength: 5,
            pagingType: "bootstrap_full_number",
            columnDefs: [{orderable: !1, targets: [0]}, {searchable: !1, targets: [0]}, {className: "dt-left"}],
            order: [[0, "desc"]]
        });

        $('.delete_confirmation').click(function(e) {
            e.preventDefault(); // Prevent the href from redirecting directly
            var linkURL = $(this).attr("href");
            var text = $(this).attr("data-text");
            warnBeforeRedirect(linkURL,text);
        });

        function warnBeforeRedirect(linkURL,text) {

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: text
            }).then((result) => {
                if (result.value) {
                // swal(
                //     'Deleted!',
                //     'Your file has been deleted.',
                //     'success'
                // );
                window.location=linkURL;

            }
        })
        }
    </script>
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Wallet</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->
{{--            {{dd($transactionsHistory)}}--}}
            <div class="portlet light bordered">

                <div class="portlet-body form">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                        <div class="visual">
                                            <i class="fa fa-bar-chart-o"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span data-counter="counterup" data-value="12,5">&#x20b9; {{$accountSummary['totalPoints']}}</span></div>
                                            <div class="desc"> Total Points </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                        <div class="visual">
                                            <i class="fa fa-comments"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span data-counter="counterup" data-value="1349">&#x20b9; {{$accountSummary['clubPoints']}}</span>
                                            </div>
                                            <div class="desc"> Club Points </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                        <div class="visual">
                                            <i class="fa fa-shopping-cart"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span data-counter="counterup" data-value="549">&#x20b9; {{$accountSummary['myPurchasePoints']}}</span>
                                            </div>
                                            <div class="desc"> My Purchase Points </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                                        <div class="visual">
                                            <i class="fa fa-globe"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span data-counter="counterup" data-value="89"></span>&#x20b9; {{$accountSummary['withdrawalPoints']}}</div>
                                            <div class="desc"> Withdrawal Points </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <h1>Club Points</h1>
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="listclubpoints">
                                <thead>
                                <tr>
                                    <th>
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th>Date</th>
                                    <th>Transaction ID</th>
                                    <th>User ID</th>
                                    <th>Merchant ID</th>
                                    <th>Points</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($transactionsHistory)
                                    @php $total =   0; @endphp
                                    @foreach($transactionsHistory as $history)
                                        @if($history['metaType']=='clubPoints')
                                            @php $total =   $total+$history['amount']; @endphp
                                            <tr>
                                                <td>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="{{$history['id']}}" />
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td>{{$history['date']}}</td>
                                                <td>{{substr($history['transactionId'],5)}}</td>
                                                <td>{{isset(json_decode($history['details'])->user_id)?(json_decode($history['details'])->user_id):'NA'}}</td>
                                                <td>{{isset(json_decode($history['details'])->product_id)?(json_decode($history['details'])->product_id):'NA'}}</td>
                                                <td>{{$history['amount']}}</td>
                                            </tr>
                                        @endif
                                        @if($loop->last)
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td class="text-right"><strong>Total Points</strong></td>
                                                <td><strong>{{$total}}</strong></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center"><strong>You do have any transaction in your
                                                account</strong></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <h1>My Purchase Points</h1>
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="listmypurchasepoints">
                                <thead>
                                <tr>
                                    <th>
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                            <span></span>
                                        </label>
                                    </th>
                                    <th>Date</th>
                                    <th>Transaction ID</th>
                                    <th>Merchant ID</th>
                                    <th>Points</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($transactionsHistory)
                                    @php $total =   0; @endphp
                                    @foreach($transactionsHistory as $history)
                                        @if($history['metaType']=='myPurchase')
                                            @php $total =   $total+$history['amount']; @endphp
                                            <tr>
                                                <td>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="{{$history['id']}}" />
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td>{{$history['date']}}</td>
                                                <td>{{substr($history['transactionId'],5)}}</td>
                                                <td>{{isset(json_decode($history['details'])->product_id)?(json_decode($history['details'])->product_id):'NA'}}</td>
                                                <td>{{$history['amount']}}</td>
                                            </tr>
                                        @endif
                                        @if($loop->last)
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td  class="text-right"><strong>Total Points</strong></td>
                                                <td><strong>{{$total}}</strong></td>
                                            </tr>
                                        @endif
                                    @endforeach

                                @else
                                    <tr>
                                        <td colspan="4" class="text-center"><strong>You do have any transaction in your
                                                account</strong></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

