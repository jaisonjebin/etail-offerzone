@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<style>

    .m-10{
        margin: 10px;

    }

</style>
@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <script>
        $('#listLuckyDrawTable').dataTable({
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                info: "Showing _START_ to _END_ of _TOTAL_ records",
                infoEmpty: "No records found",
                infoFiltered: "(filtered1 from _MAX_ total records)",
                lengthMenu: "Show _MENU_",
                search: "Search:",
                zeroRecords: "No matching records found",
                paginate: {previous: "Prev", next: "Next", last: "Last", first: "First"}
            },
            bStateSave: !0,
            lengthMenu: [[5, 15, 20, -1], [5, 15, 20, "All"]],
            pageLength: 5,
            pagingType: "bootstrap_full_number",
            columnDefs: [{orderable: !1, targets: [0]}, {searchable: !1, targets: [0]}, {className: "dt-left"}],
            order: [[4, "desc"]]
        });

        $('.delete_confirmation').click(function(e) {
            e.preventDefault(); // Prevent the href from redirecting directly
            var linkURL = $(this).attr("href2");
            warnBeforeRedirect(linkURL);
        });

        function warnBeforeRedirect(linkURL) {

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    );
                    window.location=linkURL;

                }
            })
        }



        $(document).on('click','.toppicksbtn',function () {
            $(this).toggleClass('btn-success');
            $(this).addClass('disabled');
            $status = $(this).attr('data-status');
            if($status == 1){
                $status = 0;
            }
            else{
                $status =1;
            }
            $.ajax({
                context:$(this),
                url:'{{route('admin.addToTopPicks',['',''])}}'+'/'+$(this).closest('td').data('id')+'/'+$status,
                dataType:'json',
                success:function (data) {
                    $(this).removeClass('disabled');
                    if(data.status){
                        $(this).attr('data-status',data.value);
                        if(data.value==1){
                            toastr.success("Added to Top Picks");
                        }
                        else {
                            toastr.success("Removed From Top Picks");
                        }
                    }
                }
            })
        });

        $(document).on('click','.dealofthedaybtn',function () {
            $(this).toggleClass('btn-success');
            $(this).addClass('disabled');
            $status = $(this).attr('data-status');
            if($status == 1){
                $status = 0;
            }
            else{
                $status =1;
            }
            $.ajax({
                context:$(this),
                url:'{{route('admin.addToDealoftheDay',['',''])}}'+'/'+$(this).closest('td').data('id')+'/'+$status,
                dataType:'json',
                success:function (data) {
                    $(this).removeClass('disabled');
                    if(data.status){
                        $(this).attr('data-status',data.value);
                        if(data.value==1){
                            toastr.success("Added to Deal of the Day");
                        }
                        else {
                            toastr.success("Removed From Deal of the Day");
                        }
                    }
                }


            })

        });
    </script>
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>List Merchant Deals</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

            <div class="portlet light bordered">

                <div class="portlet-body form">
                    <div class="table-toolbar">
                        <div class="row">
							@if(Auth::user()->type!='freelancer')
								<div class="col-md-6">
									<div class="btn-group">
										<a href="{{route(Auth::user()->type.'.addMerchantDeal')}}" class="btn sbold green"> Add New
											<i class="fa fa-plus"></i>
										</a>
									</div>
								</div>
							@endif
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="listLuckyDrawTable">
                        <thead>
                        <tr>
                            <th>
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                    <span></span>
                                </label>
                            </th>
                            <th> Title </th>
                            <th> Image </th>
                            <th> Type </th>
                            <th> Stock </th>
                            <th> Price </th>
                            <th> Sales </th>
                            <th> Start to End </th>
                            <th> Store </th>
                            <th> Status </th>
                            <th> Actions </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($merchantDeals as $merchantDeal)
                            <tr class="odd gradeX">
                                <td>
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" value="{{$merchantDeal->id}}" />
                                        <span></span>
                                    </label>
                                </td>
                                <td> {{$merchantDeal->title}} </td>
                                <td> <img src="/uploads/{{$merchantDeal->small_image}}" width="100px" height="100px"/> </td>
                                <td class="text-capitalize"> {{$merchantDeal->dealType}} </td>
                                <td> {{$merchantDeal->stock}} </td>
                                <td> {{$merchantDeal->offer_price}} </td>
                                <td> {{$merchantDeal->sold}} </td>
                                <td> {{Carbon\Carbon::parse($merchantDeal->start_time)->format('d M,Y')}} to
                                    {{Carbon\Carbon::parse($merchantDeal->end_time)->format('d M,Y')}} </td>
                                <td> {{$merchantDeal->getOwner?$merchantDeal->getOwner->name:''}} </td>
                                <td> 
								@if($merchantDeal->status == 0)
									<span class="label label-sm label-info"> Freelancer Approved </span>
								@elseif($merchantDeal->status == 1)
									<span class="label label-sm label-success"> Admin Approved </span>
								@elseif($merchantDeal->status == 2)
									<span class="label label-sm label-warning"> Pending Approval </span>
								@elseif($merchantDeal->status == 3)
									<span class="label label-sm label-danger"> Freelancer Rejected </span>
								@elseif($merchantDeal->status == 4)
									<span class="label label-sm label-danger"> Admin Rejected </span>
								@endif
								</td>
                                <td data-id="{{$merchantDeal->id}}">
                                    <a href="{{route(Auth::user()->type.'.merchantDealEdit',$merchantDeal->id)}}" class="btn btn-info btn-xs">
                                        <i class="fa fa-pencil-square-o"></i> Edit</a>
									
									@if(Auth::user()->type!='freelancer')
                                    <a href2="{{route(Auth::user()->type.'.merchantDealDelete',$merchantDeal->id)}}" class="delete_confirmation btn btn-danger btn-xs">
                                        <i class="fa fa-trash"></i> Delete</a>
									@endif

                                    @if(Auth::user()->type=='admin' && $merchantDeal->dealType!='coupon')


                                    <a class="btn @if($merchantDeal->dealoftheday) btn-success @else btn-default @endif btn-xs dealofthedaybtn" data-status="{{$merchantDeal->dealoftheday}}">
                                        <i class="fa fa-smile-o"></i>  Deal of the Day</a>


                                    <a class="btn @if($merchantDeal->toppicks) btn-success @else btn-default @endif btn-xs toppicksbtn" data-status="{{$merchantDeal->toppicks}}">
                                            <i class="fa fa-ticket"></i> Top Picks</a>
                                    @endif
										
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

