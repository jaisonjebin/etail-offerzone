<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li><li class="sidebar-search-wrapper">
    </li>
    <li class="nav-item start {{isActiveRoute(['merchant.showDashboard'])}}">
        <a href="{{route('merchant.showDashboard')}}" class="nav-link">
            <i class="icon-home"></i>
            <span class="title">Dashboard</span>
            <span class="selected"></span>
        </a>
    </li>
	
	
    <li class="nav-item  {{areActiveRoutes(['merchant.addMerchantDeal', 'merchant.listMerchantDeal'])}}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">Merchant Deals</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item  {{isActiveRoute('merchant.addMerchantDeal')}}">
                <a href="{{route('merchant.addMerchantDeal')}}" class="nav-link ">
                    <span class="title">Add Deals</span>
                </a>
            </li>
            <li class="nav-item  {{isActiveRoute('merchant.listMerchantDeal')}}">
                <a href="{{route('merchant.listMerchantDeal')}}" class="nav-link ">
                    <span class="title">List Deal</span>
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item  {{areActiveRoutes(['merchant.listOrder'])}}">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-diamond"></i>
            <span class="title">Orders</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li class="nav-item  {{isActiveRoute('merchant.listOrder')}}">
                <a href="{{route('merchant.listOrder')}}" class="nav-link ">
                    <span class="title">List Order</span>
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item start {{isActiveRoute('admin.wallet')}}">
        <a href="{{route('admin.wallet')}}" class="nav-link">
            <i class="icon-wallet"></i>
            <span class="title">Wallet</span>
            <span class="selected"></span>
        </a>
    </li>
	
	

    {{--<li class="nav-item  {{areActiveRoutes(['freelancer.listMerchant','freelancer.listMerchant'])}}">--}}
        {{--<a href="javascript:;" class="nav-link nav-toggle">--}}
            {{--<i class="icon-users"></i>--}}
            {{--<span class="title">Merchant</span>--}}
            {{--<span class="arrow"></span>--}}
        {{--</a>--}}
        {{--<ul class="sub-menu">--}}
            {{--<li class="nav-item  {{isActiveRoute('freelancer.listMerchant')}}">--}}
                {{--<a href="{{route('freelancer.listMerchant')}}" class="nav-link ">--}}
                    {{--<span class="title">Merchant List</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item ">--}}
                {{--<a href="{{route('addMerchant')}}" class="nav-link " target="_blank">--}}
                    {{--<span class="title">Add Merchant</span>--}}
                {{--</a>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</li>--}}

</ul>
