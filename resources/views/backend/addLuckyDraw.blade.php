@extends('backend.master')
@push('styles')

<link href="/back/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<link href="/back/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<link href="/back/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/back/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
{{--<script src="/back/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>--}}
<!-- END PAGE LEVEL PLUGINS -->
<script src="/back/assets/pages/scripts/form-wizard.js" type="text/javascript"></script>
    <script>
        $(".form_datetime").datetimepicker({
            autoclose: !0,
            isRTL: false,
            format: "dd MM yyyy - hh:ii",
            fontAwesome: !0,
            pickerPosition: "bottom-right"
        });
    </script>
@endpush
@section('pagebody')
 <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
    <ul class="page-breadcrumb">
    <li>
        <a href="">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Add Lucky Draw</span>
    </li>
    </ul>

    </div>
    <!-- END PAGE BAR -->
    <!-- END PAGE HEADER-->
    <!-- BEGIN DASHBOARD STATS 1-->

    <div class="portlet light bordered" id="form_wizard_1">

        <div class="portlet-body form">
            <form class="form-horizontal" action="{{route('admin.addLuckyDrawPost')}}" id="submit_form" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-wizard">
                    <div class="form-body">
                        <ul class="nav nav-pills nav-justified steps">
                            <li>
                                <a href="#tab1" data-toggle="tab" class="step">
                                    <span class="number"> 1 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i> General Details </span>
                                </a>
                            </li>
                            <li>
                                <a href="#tab2" data-toggle="tab" class="step">
                                    <span class="number"> 2 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i> Banner Image </span>
                                </a>
                            </li>
                            <li>
                                <a href="#tab3" data-toggle="tab" class="step active">
                                    <span class="number"> 3 </span>
                                    <span class="desc">
                                        <i class="fa fa-check"></i> Question </span>
                                </a>
                            </li>
                        </ul>
                        <div id="bar" class="progress progress-striped" role="progressbar">
                            <div class="progress-bar progress-bar-success"> </div>
                        </div>
                        <div class="tab-content">
                            <div class="alert alert-danger display-none">
                                <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                            <div class="alert alert-success display-none">
                                <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                            <div class="tab-pane active" id="tab1">
                                <h3 class="block">Provide Lucky Draw Details</h3>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Title
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="title" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description
                                    </label>
                                    <div class="col-md-4">
                                        <textarea class="form-control" name="description"></textarea>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3">Start Time
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <div class="input-group date form_datetime  bs-datetime">
                                            <input type="text"  class="form-control" name="start_time" readonly>
                                            <span class="input-group-addon">
                                                <button class="btn default date-set" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">End Time
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">

                                        <div class="input-group date form_datetime bs-datetime">
                                            <input type="text"  class="form-control" name="end_time" readonly>
                                            <span class="input-group-addon">
                                                <button class="btn default date-set" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Advertisement Type</label>
                                    <div class="col-md-9">
                                        <input type="checkbox" name="ad_flag" value="1" class="make-switch" data-on-text="&nbsp;Internal&nbsp;" data-off-text="&nbsp;Google&nbsp;">
                                    </div>
                                </div>
                            </div>



                            <div class="tab-pane" id="tab2">
                                <h3 class="block">Provide your Banner</h3>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Banner Image</label>
                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail fileinput-preview" style="width: 200px; height: 150px;">
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="banner"> </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane" id="tab3">
                                <h3 class="block">Question & Options</h3>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Question
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="question" />
                                        <span class="help-block"> </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Option 1
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="option_1" />
                                        <label><input type="radio" name="answer" value="1"> Answer</label>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Option 2
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="option_2" />
                                        <label><input type="radio" name="answer" value="2"> Answer</label>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Option 3
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="option_3" />
                                        <label><input type="radio" name="answer" value="3"> Answer</label>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Option 4
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="option_4" />
                                        <label><input type="radio" name="answer" value="4"> Answer</label>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <a href="javascript:;" class="btn default button-previous">
                                    <i class="fa fa-angle-left"></i> Back </a>
                                <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                    <i class="fa fa-angle-right"></i>
                                </a>
                                <button class="btn green button-submit" type="submit" style="display: none"> Submit
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    </div>
    <!-- END CONTENT BODY -->
</div>
@stop

