@extends('backend.master')
@push('styles')

    <link href="/back/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="/back/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    {{--<script src="/back/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>--}}
    <!-- END PAGE LEVEL PLUGINS -->
    {{--<script src="/back/assets/pages/scripts/form-wizard.js" type="text/javascript"></script>--}}
    <script src="/back/assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            $('.j_state_input').trigger('change');
            $(".form_datetime").datepicker({
                autoclose: true,
                isRTL: false,
                format: "dd/mm/yyyy",
                fontAwesome: !0,
                pickerPosition: "bottom-right",
                endDate: new Date(),
                todayBtn:true
            });
        });
    </script>
    <script>
        $(document).ready(function () {

            $('.j_state_input').trigger('change');
            $('.j_state_input').select2();
        });

        $('.j_state_input').on('change',function () {
            console.log($('.j_state_input').val());
            $state_id   =   $('.j_state_input').val();
            if($state_id=='19')
            {
                $.ajax({
                    type: "GET",
                    url: '{{route("getDistricts","")}}/'+$state_id,
                    dataType:'json',
                    success: function( data )
                    {
                        $('.j_district_div').show();
                        $(".j_district_input").html('');
                        $.each(data,function(key, value)
                        {
                            option='';
                            option= '<option  value=' + value.id + '>' + value.name + '</option>';

                            $(".j_district_input").append(option);
                        });
                        $('.j_district_input').select2();
                        // $('.j_district_input').val($('.j_district_input').attr('data-selected'));
                        $('.j_district_input').trigger('change');
                    }
                });
            }
            else
            {
                $(".j_district_div").hide();
                $.ajax({
                    type: "GET",
                    url: '{{route("getCities","")}}/'+$state_id,
                    dataType:'json',
                    success: function( data )
                    {
                        $(".j_cities_input").html('');
                        // $('.j_cities_input').select2('destroy');

                        $.each(data,function(key, value)
                        {
                            option='';
                            option= '<option  value=' + value.id + '>' + value.name + '</option>';

                            $(".j_cities_input").append(option);
                        });
                        $('.j_cities_input').select2();
                        $('.j_cities_input').val($('.j_cities_input').attr('data-selected'));
                    }
                });
            }

        });
        $('.j_district_input').on('change',function () {
            $district_id   =   $('.j_district_input').val();
            $.ajax({
                type: "GET",
                url: '{{route("getCities","")}}/'+$district_id+'/KL',
                dataType:'json',
                success: function( data )
                {
                    $(".j_cities_input").html('');
                    // $('.j_cities_input').select2('destroy');

                    $.each(data,function(key, value)
                    {
                        option='';
                        option= '<option  value=' + value.id + '>' + value.name + '</option>';

                        $(".j_cities_input").append(option);
                    });
                    $('.j_cities_input').select2();
                    $('.j_cities_input').val($('.j_cities_input').attr('data-selected'));
                    $('.j_cities_input').trigger('change');
                }
            });
        });
        $('.clone_phone').on('click',function () {
           $('.phone_div:last').after($('.phone_div:last').clone().removeAttr('value'));
        });

    </script>
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Add Freelancer</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject font-dark sbold uppercase">Add Freelancer</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form class="form-horizontal" method="post" role="form" action="{{route('admin.saveFreelancer')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <div class="form-group col-lg-6">
                                <label class="col-md-4 control-label">Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="name" placeholder="Enter Name" required>
                                    <span class="help-block error">{{$errors->first('name')}}</span>
                                </div>
                                    <label class="col-md-4 control-label">Phone No</label>
                                    <div class="col-md-8">
                                    <input type="number" name="phone" class="form-control" placeholder="Enter Phone Number" required>
                                    <span class="help-block error">{{$errors->first('phone')}}</span>
                                    </div>

                                <div class="phone_div">
                                    <label class="col-md-4 control-label">Alt Phone No</label>
                                    <div class="col-md-6">
                                        <input type="number" name="phone1[]" class="form-control" placeholder="Enter Alt Phone Number">
                                        <span class="help-block error">{{$errors->first('phone1')}}</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="clone_phone col-md-2 btn btn btn-icon-only green"><i class="fa fa-plus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                                <label class="col-md-4 control-label">Email Id</label>
                                <div class="col-md-8">
                                    <input type="email" name="email" class="form-control" placeholder="Enter Email Id">
                                    <span class="help-block error">{{$errors->first('email')}}</span>
                                </div>
                                <label class="col-md-4 control-label">Password</label>
                                <div class="col-md-8">
                                    <input type="password" name="password" class="form-control error" placeholder="Leave empty to generate one">
                                    <span class="help-block error">{{$errors->first('password')}}</span>
                                </div>
                                <label class="col-md-4 control-label">Password Confirm</label>
                                <div class="col-md-8">
                                    <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">
                                    <span class="help-block error">{{$errors->first('password_confirm')}}</span>
                                </div>
                                <h4>Bank Details</h4>
                                <label class="control-label col-md-4">PAN Card No</label>
                                <div class="col-md-8">
                                    <input name="account_pan" value="{{old('account_pan')}}" class="form-control {{ $errors->has('account_pan') ? ' has-error' : '' }}" type="text" placeholder="PAN Card No"/>
                                    <span class="help-block">
                                        {{ $errors->first('account_pan') }}
                                    </span>
                                </div>
                                    <label class="control-label col-md-4">Account Name</label>
                                <div class="col-md-8">
                                    <input name="account_name" value="{{old('account_name')}}" class="form-control {{ $errors->has('account_name') ? ' has-error' : '' }}" type="text" placeholder="Account Name"/>
                                        <span class="help-block">
                                            {{ $errors->first('account_name') }}
                                        </span>
                                </div>
                                    <label class="control-label col-md-4">Account No</label>
                                <div class="col-md-8">
                                    <input name="account_no" value="{{old('account_no')}}" class="form-control {{ $errors->has('account_no') ? ' has-error' : '' }}" type="text" placeholder="Account No"/>
                                        <span class="help-block">
                                            {{ $errors->first('account_no') }}
                                        </span>
                                </div>
                                    <label class="control-label col-md-4">Name of Bank</label>
                                <div class="col-md-8">
                                    <input name="account_bank" value="{{old('account_bank')}}" class="form-control  {{ $errors->has('account_bank') ? ' has-error' : '' }}" type="text" placeholder="Bank Name"/>
                                        <span class="help-block">
                                            {{ $errors->first('account_bank') }}
                                        </span>
                                </div>
                                    <label class="control-label col-md-4">Name of Branch</label>
                                <div class="col-md-8">
                                    <input name="account_bank_branch" value="{{old('account_bank_branch')}}" class="form-control placeholder-no-fix {{ $errors->has('account_bank_branch') ? ' has-error' : '' }}" type="text" placeholder="Bank Branch"/>
                                        <span class="help-block">
                                            {{ $errors->first('account_bank_branch') }}
                                        </span>
                                </div>
                                    <label class="control-label col-md-4">IFSC Code</label>
                                    <div class="col-md-8">
                                    <input name="account_ifsc" value="{{old('account_ifsc')}}" class="form-control placeholder-no-fix {{ $errors->has('account_ifsc') ? ' has-error' : '' }}" type="text" placeholder="IFSC Code"/>
                                        <span class="help-block">
                                            {{ $errors->first('account_ifsc') }}
                                        </span>
                                </div>
								<div class="clearfix"></div>
                                <label class="col-md-4 control-label">Profile_Picture</label>
								<div class="col-md-8">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                    <div>
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="avatar"> </span>
                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
								</div>
							</div>
                            <div class="form-group col-lg-6">
                                <label class="col-md-4 control-label">Gender</label>
                                <div class="col-md-8">
                                    <select name="gender" class="form-control">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Other">Other</option>
                                    </select>
                                    <span class="help-block error">{{$errors->first('gender')}}</span>
                                </div>
                                <label class="col-md-4 control-label">Date Of Birth</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control form_datetime" name="dob" value="" required readonly>
                                    <span class="help-block error">{{$errors->first('dob')}}</span>
                                </div>
                                <label class="col-md-4 control-label">Known Languages</label>
                                <div class="col-md-8">
                                   <input type="text" name="languages" class="form-control" value="{{old('languages')}}">
                                    <span class="help-block error">{{$errors->first('languages')}}</span>
                                </div>
                                <label class="col-md-4 control-label">Qualification</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="education" placeholder="Enter Education Level">
                                    <span class="help-block error">{{$errors->first('education')}}</span>
                                </div>
                                <label class="col-md-4 control-label">Address</label>
                                <div class="col-md-8">
                                    <textarea name="address" class="form-control"></textarea>
                                    <span class="help-block error">{{$errors->first('address')}}</span>
                                </div>
                                <label class="col-md-4 control-label">Pincode</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="pincode" placeholder="Enter Pincode" required>
                                    <span class="help-block error">{{$errors->first('pincode')}}</span>
                                </div>
                                {{--<div class="form-group">--}}
                                    <label class="control-label col-md-4">Address Proof side 1</label>
                                    <div class="col-md-8">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 400px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Address+Proof"
                                                 alt="" style="width:400px; height: 150px;"/>
                                        </div>
                                        <div>
                                <span class="btn red btn-outline btn-file">
                                <span class="fileinput-new"> Select image </span>
                                <span class="fileinput-exists"> Change </span>
                                <input type="file" name="address_proof[]">
                                </span>
                                            <a href="javascript:;" class="btn red fileinput-exists"
                                               data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                    @if ($errors->has('address_proof'))
                                        <span class="help-block">
                                            {{ $errors->first('address_proof') }}
                                        </span>
                                    @endif
                                    </div>
                                <label class="control-label col-md-4">Address Proof side 2</label>
                                <div class="col-md-8">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 400px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Address+Proof"
                                                 alt="" style="width:400px; height: 150px;"/>
                                        </div>
                                        <div>
                                <span class="btn red btn-outline btn-file">
                                <span class="fileinput-new"> Select image </span>
                                <span class="fileinput-exists"> Change </span>
                                <input type="file" name="address_proof[]">
                                </span>
                                            <a href="javascript:;" class="btn red fileinput-exists"
                                               data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                    @if ($errors->has('address_proof'))
                                        <span class="help-block">
                                            {{ $errors->first('address_proof') }}
                                        </span>
                                    @endif
                                </div>
                                {{--</div>--}}
                                <label class="col-md-4 control-label">State</label>
                                <div class="col-md-8">
                                    <select class="form-control select2 j_state_input" name="state" data-selected="">
                                        @foreach($states as $state)
                                            <option value="{{$state->id}}" {{$state->id==19?'selected':''}}>{{$state->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block error">{{$errors->first('state')}}</span>
                                </div>
                                <label class="col-md-4 control-label j_district_div">District</label>
                                <div class="col-md-8 j_district_div">
                                    <select class="form-control j_district_input" name="district" data-selected="">
                                    </select>
                                    <span class="help-block error">{{$errors->first('district')}}</span>
                                </div>
                                <label class="col-md-4 control-label">City</label>
                                <div class="col-md-8">
                                    <select class="form-control j_cities_input" name="city" data-selected="">
                                    </select>
                                    <span class="help-block error">{{$errors->first('city')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-5 col-md-8">
                                    <button type="submit" class="btn green">Submit</button>
                                    <button type="button" class="btn default">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

