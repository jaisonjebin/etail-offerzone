@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <script>
        $('#listUserTable').dataTable({
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                info: "Showing _START_ to _END_ of _TOTAL_ records",
                infoEmpty: "No records found",
                infoFiltered: "(filtered1 from _MAX_ total records)",
                lengthMenu: "Show _MENU_",
                search: "Search:",
                zeroRecords: "No matching records found",
                paginate: {previous: "Prev", next: "Next", last: "Last", first: "First"}
            },
            bStateSave: !0,
            lengthMenu: [[5, 50, 100, -1], [5, 50, 100, "All"]],
            pageLength: 5,
            pagingType: "bootstrap_full_number",
            columnDefs: [{orderable: !1, targets: [0]}, {searchable: !1, targets: [0]}, {className: "dt-left"}],
            order: [[4, "desc"]]
        });

        $('.delete_confirmation').click(function(e) {
            e.preventDefault(); // Prevent the href from redirecting directly
            var linkURL = $(this).attr("href");
            var text = $(this).attr("data-text");
            warnBeforeRedirect(linkURL,text);
        });

        function warnBeforeRedirect(linkURL,text) {

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: text
            }).then((result) => {
                if (result.value) {
                    // swal(
                    //     'Deleted!',
                    //     'Your file has been deleted.',
                    //     'success'
                    // );
                    window.location=linkURL;

                }
            })
        }
    </script>
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Users List</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

            <div class="portlet light bordered">

                <div class="portlet-body form">
                    <div class="table-toolbar">
                        <div class="row">
                            {{--<div class="col-md-6">--}}
                                {{--<div class="btn-group">--}}
                                    {{--<a href="{{route('admin.addLuckyDraw')}}" class="btn sbold green"> Add New--}}
                                        {{--<i class="fa fa-plus"></i>--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6">--}}
                            {{--<div class="btn-group pull-right">--}}
                            {{--<button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools--}}
                            {{--<i class="fa fa-angle-down"></i>--}}
                            {{--</button>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                            {{--<li>--}}
                            {{--<a href="javascript:;">--}}
                            {{--<i class="fa fa-print"></i> Print </a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                            {{--<a href="javascript:;">--}}
                            {{--<i class="fa fa-file-pdf-o"></i> Save as PDF </a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                            {{--<a href="javascript:;">--}}
                            {{--<i class="fa fa-file-excel-o"></i> Export to Excel </a>--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="listUserTable">
                        <thead>
                        <tr>
                            <th>
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                    <span></span>
                                </label>
                            </th>
                            <th> User ID </th>
                            <th> Name </th>
                            <th> Email </th>
                            <th> District </th>
                            <th> Phone </th>
                            <th> Sponsor </th>
                            {{--<th> Join Date </th>--}}
                            <th> Status </th>
                            <th> Actions </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr class="odd gradeX">
                                <td>
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" value="{{$user->id}}" />
                                        <span></span>
                                    </label>
                                </td>
                                <td> DD-{{$user->id}} </td>
                                <td> {{$user->name}} {!! $user->isSuperFreelancer?'<span class="badge badge-danger"><i class="fa fa-star"></i></span>':'' !!}</td>
                                <td> {{$user->email}} </td>
                                <td> {{$user->district}} </td>
                                <td> {{$user->phone}} {!! $user->otpVerified?'<i class="fa fa-check-circle" style="color:green;"></i>':'' !!} </td>
{{--                                <td> {{Carbon\Carbon::parse($user->start_time)->format('d M,Y')}} </td>--}}
                                @if($user->ref_code!='')
                                    @php $ref_code   =   user($user->ref_code);@endphp
                                    @if($ref_code)
                                        <td> {{ $ref_code->name.'('.$user->ref_code.')'}} </td>
                                    @else
                                        <td> No Sponsor</td>
                                    @endif
                                @else
                                    <td> No Sponsor</td>
                                @endif
                                <td> {!! $user->status?'<span class="label label-sm label-success"> Active </span>':'<span class="label label-sm label-danger"> Inactive </span>' !!} </td>
                                <td>
                                    <a href="{{route('admin.covertToFreelancer',$user->id)}}" class="btn purple btn-xs">
                                        <i class="fa fa-star"></i> Covert to Freelancer</a>
                                    <a href="{{route('admin.UserStatus',$user->id)}}" data-text="{{$user->status?'Yes, Block user!':'Yes, Activate user!'}}" class="delete_confirmation btn btn-xs {{$user->status?'btn-warning':'btn-success'}}">
                                        <i class="fa {{$user->status?'fa-ban':'fa-check-square-o'}}"></i> {{$user->status?'Block':'Activate'}}</a>
                                    <a href="{{route('admin.UserDelete',$user->id)}}" class="delete_confirmation btn btn-xs btn-danger" data-text="Yes, delete it!">
                                        <i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

