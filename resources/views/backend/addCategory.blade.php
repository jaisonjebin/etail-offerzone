@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <link href="/site/assets/fonts/flaticon/flaticon.css" rel="stylesheet">


    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
    <script>

        $('#listCategoryTable').dataTable({
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                info: "Showing _START_ to _END_ of _TOTAL_ records",
                infoEmpty: "No records found",
                infoFiltered: "(filtered1 from _MAX_ total records)",
                lengthMenu: "Show _MENU_",
                search: "Search:",
                zeroRecords: "No matching records found",
                paginate: {previous: "Prev", next: "Next", last: "Last", first: "First"}
            },
            bStateSave: !0,
            lengthMenu: [[5, 50, 100, -1], [5, 50, 100, "All"]],
            pageLength: 5,
            pagingType: "bootstrap_full_number",
            columnDefs: [{orderable: !1, targets: [0]}, {searchable: !1, targets: [0]}, {className: "dt-left"}],
            order: [[4, "desc"]]
        });

        $('.delete_confirmation').click(function(e) {
            e.preventDefault(); // Prevent the href from redirecting directly
            var linkURL = $(this).attr("href");
            var text = $(this).attr("data-text");
            warnBeforeRedirect(linkURL,text);
        });

        function warnBeforeRedirect(linkURL,text) {

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: text
            }).then((result) => {
                if (result.value) {
                // swal(
                //     'Deleted!',
                //     'Your file has been deleted.',
                //     'success'
                // );
                window.location=linkURL;

            }
        })
        }
    </script>
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Category List</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

            <div class="portlet light bordered">
                <div class="portlet-body">
                    <h4>Add Category</h4>
                    <form class="form-inline l-saveCategoryForm" action="{{route("admin.saveCategory")}}" role="form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="sr-only" for="InputCategory">Name</label>
                            <input type="text" class="form-control" id="InputCategory" required placeholder="Enter Name" name="name">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="InputCategory">CSS Icon Class</label>
                            <input type="text" class="form-control" id="InputCategory" placeholder="eg. flaticon-fast-food" name="cssClass">
                        </div>
                        <div class="form-group hidden">
                            <label class="sr-only">Image</label>
                            <input type="file" name="image" class="form-control" accept="image/*">
                        </div>
                        <input type="submit" class="btn btn-primary l-saveCategory" value="Save"/>
                    </form>
                </div>
                <hr>

                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="listCategoryTable">
                        <thead>
                        <tr>
                            <th>
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                    <span></span>
                                </label>
                            </th>
                            <th> Icon </th>
                            <th> Name </th>
                            <th> Added By </th>
                            <th> Status </th>
                            <th> Actions </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr class="odd gradeX">
                                <td>
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" value="{{$category->id}}" />
                                        <span></span>
                                    </label>
                                </td>
                                <td class="">
                                    <div class="text-secondary">
                                        <i class="{{$category->cssIconClass}}"></i>
                                       {{--<img src="/uploads/{{$category->image}}" class="timeline-badge-userpic" height="80px"/>--}}
                                    </div>
                                </td>
                                <td> {{$category->name}} </td>
                                <td> {{$category->getUser->name}} </td>
                                <td> {!! $category->status?'<span class="label label-sm label-success"> Active </span>':'<span class="label label-sm label-danger"> Inactive </span>' !!} </td>
                                <td>
                                    <a href="{{route('admin.CategoryStatus',$category->id)}}" data-text="{{$category->status?'Yes, Disable!':'Yes, Activate!'}}" class="delete_confirmation btn {{$category->status?'btn-warning':'btn-success'}}">
                                        <i class="fa {{$category->status?'fa-ban':'fa-check-square-o'}}"></i> {{$category->status?'Block':'Activate'}}</a>
                                    <a href="{{route('admin.CategoryDelete',$category->id)}}" class="delete_confirmation btn btn-danger" data-text="Yes, delete it!">
                                        <i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

