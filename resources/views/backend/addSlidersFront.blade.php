@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<link href="/back/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<link href="/back/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<style>
.imgitem {
    width: 100%;
    /*height: 120px;*/
    overflow: hidden;
    display: inline-block;
    margin: 0px 3px 10px 0px;
}
.imgitem>img {
	width:100%;
}
</style>
@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-summernote/summernote.js" type="text/javascript"></script><script src="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script>
        $(".summernote").summernote({height:250});
        $(".form_datetime").datetimepicker({
            autoclose: !0,
            isRTL: false,
            format: "dd MM yyyy - hh:ii",
            fontAwesome: !0,
            pickerPosition: "bottom-right",
            startDate: new Date(),
            todayBtn:true
        });
        $('.addFileBlock').on('click',function(){
            $(this).closest('li').prev().clone().insertBefore($(this).closest('li'));
            $(this).closest('li').prev().find('input').val('');
        })
    </script>

	<script>
		$('.deleteSiderbtn').on('click',function(){
			$(this).closest('.sliderItem').remove();
		});
	</script>

    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="">Settings</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Silders</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->
			<form class="form-horizontal" method="post" action="{{route('admin.frontsliders')}}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Silders 1 (655 x 465)</h4>
								<div class="img-containers">
									@if(isset($sliders->one))
										@foreach($sliders->one as $slider)
											<div class="imgitem sliderItem">									
												<a class="btn btn-danger deleteSiderbtn" style="position:absolute;">
                                                    <i class="fa fa-trash" ></i> Remove
                                                </a>
												<img src="/uploads/{{$slider->image}}">
												<input name="sliderOld[one][]" value="{{$slider->image}}"  type="hidden">
                                                <input class="form-control" type="text" name="sliderOldLink[oneLink][]" value="{{$slider->link}}">
											</div>
										@endforeach
									@endif
									
								</div>
                                <div class="form-group">
                                    <ol class="list-group">
                                        <li class="list-group-item">
                                            <input type="file" name="slider[one][]">
                                            <input class="form-control" type="text" name="sliderLink[oneLink][]">
                                        </li>
                                        <li class="list-group-item">
                                            <input type="file" name="slider[one][]">
                                            <input class="form-control" type="text" name="sliderLink[oneLink][]">
                                        </li>
                                        <li class="list-group-item"><a class=" btn btn-primary btn-block addFileBlock"><i class="fa fa-plus"></i> Add New</a></li>
                                    </ol>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Silder 2 (655 x 230)</h4>
								
									@if(isset($sliders->two))
									@foreach($sliders->two as $slider)
										<div class="imgitem sliderItem">									
											<a class="btn btn-danger deleteSiderbtn" style="position:absolute;"><i class="fa fa-trash" ></i> Remove</a>
											<img src="/uploads/{{$slider->image}}">
											<input class="form-control" name="sliderOld[two][]" value="{{$slider->image}}" type="hidden">
                                            <input class="form-control" type="text" name="sliderOldLink[twoLink][]" value="{{$slider->link}}">

                                        </div>
									@endforeach
									@endif
                                <div class="form-group">
                                    <ol class="list-group">
                                        <li class="list-group-item">
                                            <input type="file" name="slider[two][]">
                                            <input class="form-control" type="text" name="sliderLink[twoLink][]">
                                        </li>
                                        <li class="list-group-item">
                                            <input type="file" name="slider[two][]">
                                            <input class="form-control" type="text" name="sliderLink[twoLink][]">
                                        </li>
                                        <li class="list-group-item"><a class=" btn btn-primary btn-block addFileBlock"><i class="fa fa-plus"></i> Add New</a></li>
                                    </ol>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Silders 3 (320 x 230)</h4>

                                @if(isset($sliders->three))
                                    @foreach($sliders->three as $slider)
                                        <div class="imgitem sliderItem">
                                            <a class="btn btn-danger deleteSiderbtn" style="position:absolute;"><i class="fa fa-trash" ></i> Remove</a>
                                            <img src="/uploads/{{$slider->image}}">
                                            <input class="form-control" name="sliderOld[three][]" value="{{$slider->image}}" type="hidden">
                                            <input class="form-control" type="text" name="sliderOldLink[threeLink][]" value="{{$slider->link}}">
                                        </div>
                                    @endforeach
                                @endif

                                <div class="form-group">
                                    <ol class="list-group">
                                        <li class="list-group-item">
                                            <input type="file" name="slider[three][]"  >
                                            <input class="form-control" type="text" name="sliderLink[threeLink][]">
                                        </li>
                                        <li class="list-group-item">
                                            <input type="file" name="slider[three][]">
                                            <input class="form-control" type="text" name="sliderLink[threeLink][]">
                                        </li>
                                        <li class="list-group-item"><a class=" btn btn-primary btn-block addFileBlock"><i class="fa fa-plus"></i> Add New</a></li>
                                    </ol>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                </div>

				
                <div class="row">
                    <div class="col-md-6 hidden">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Silder 4 (320 x 230)</h4>
								
								
									@if(isset($sliders->four))
									@foreach($sliders->four as $slider)
										<div class="imgitem sliderItem">									
											<a class="btn btn-danger deleteSiderbtn" style="position:absolute;"><i class="fa fa-trash" ></i> Remove</a>
											<img src="/uploads/{{$slider->image}}">
											<input class="form-control" name="sliderOld[four][]" value="{{$slider->image}}" type="hidden">
                                            <input class="form-control" type="text" name="sliderOldLink[fourLink][]" value="{{$slider->link}}">
										</div>
									@endforeach
									@endif
                                <div class="form-group">
                                    <ol class="list-group">
                                        <li class="list-group-item">
                                            <input type="file" name="slider[four][]">
                                            <input class="form-control" type="text" name="sliderLink[fourLink][]">
                                        </li>
                                        <li class="list-group-item">
                                            <input type="file" name="slider[four][]">
                                            <input class="form-control" type="text" name="sliderLink[fourLink][]">
                                        </li>
                                        <li class="list-group-item"><a class=" btn btn-primary btn-block addFileBlock"><i class="fa fa-plus"></i> Add New</a></li>
                                    </ol>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                </div>

				
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                        <div class="form-group1">
                            <button type="submit"  class="btn green button-submit pull-right"> Save
                                <i class="fa fa-check"></i>
                            </button>
                        </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop
