@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
	
<link href="/back/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="/back/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<style>
.select2-container, .select2-drop, .select2-search, .select2-search input {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 100%;
}
.select2-container .select2-choice {
    height: 34px;
}
.select2-container .select2-choice .select2-arrow {
    display: none;
}
</style>


@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-summernote/summernote.js" type="text/javascript"></script><script src="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
	<script src="/back/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script>
			$(".select2, .select2-multiple").select2({
				placeholder:'Select Options',
				width: null
			});
		</script>
		
    <script>
        $('.remove_btn').on('click',function () {
            $(this).closest('.fileinput').find('.thumbnail').hide();
            console.log($(this).closest('.hidden_file').find('input[type="hidden"]').val());
            $(this).closest('.hidden_file').find('input[type="hidden"]').val('');
            // $('input[name="small_image1"]').val('');
            $(this).hide();
        });
        $(".summernote").summernote({height:250});
        $(".form_datetime").datetimepicker({
            autoclose: !0,
            isRTL: false,
            format: "dd MM yyyy - hh:ii",
            fontAwesome: !0,
            pickerPosition: "bottom-right",
            startDate: new Date(),
            todayBtn:true
        });
        $('.addFileBlock').on('click',function(){
            $(this).closest('li').prev().clone().insertBefore($(this).closest('li'));
            $(this).closest('li').prev().find('input').val('');
        })

        $('[name="offetType"]').on('change',function () {
            console.log($(this).val());
            if($(this).val() =='voucher'){
                $('.voucherPriceBlock').find('.control-label').html('Voucher Price');
                $('.couponPriceBlock').slideUp();
                $('.voucherPriceBlock').slideDown();

            }
            else if($(this).val() =='coupon'){
                $('.voucherPriceBlock').find('.control-label').html('Coupon Price');

                // $('.voucherPriceBlock').slideDown();
                $('.voucherPriceBlock').slideUp();
                $('.couponPriceBlock').slideDown();
            }
            else {
                $('.voucherPriceBlock').slideUp();
                $('.couponPriceBlock').slideUp();
            }
        })
    </script>
    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="">Merchant Deal</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Edit Deal</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

            <form class="form-horizontal" method="post" action="{{route(Auth::user()->type.'.updatemerchantDeal')}}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$merchantDeal->id}}">

                <div class="row"  style="margin-top:20px;">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group1">
                                <button type="submit"  class="btn green button-submit pull-right"> Update
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="portlet light bordered" style="margin-top:20px;">
                        <div class="portlet-body">
                            <h4>Merchant Deal General Details</h4>
                            <div class="form-group">
                                <label class="control-label col-md-4">Product Title
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="title" value="{{old('title')?old('title'):$merchantDeal->title}}"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Short Description
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="small_description" value="{{old('small_description')?old('small_description	'):$merchantDeal->small_description}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Price / MRP
                                </label>
                                <div class="col-md-8">
                                    <input type="number" class="form-control" name="price" value="{{old('price')?old('price'):$merchantDeal->price}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Offer Price
                                </label>
                                <div class="col-md-8">
                                    <input type="number" class="form-control" name="offer_price" value="{{old('offer_price')?old('offer_price'):$merchantDeal->offer_price}}">
                                </div>
                            </div>


                                <div class="form-group">
                                    <label class="col-md-4 control-label">Deal Type{{$merchantDeal->dealType}}</label>
                                    <div class="col-md-8">
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="offetType" value="offer" {{old('offetType')? (old('offetType')=='offer' ?'checked':''):($merchantDeal->dealType=='offer'?'checked':'')}}> Offer
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="offetType" value="voucher" {{old('offetType')? (old('offetType')=='voucher' ?'checked':''):($merchantDeal->dealType=='voucher'?'checked':'')}} > Voucher
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="offetType" value="coupon" {{old('offetType')? (old('offetType')=='coupon' ?'checked':''):($merchantDeal->dealType=='coupon'?'checked':'')}}> Coupon
                                                {{--<span></span>--}}
                                            {{--</label>--}}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group voucherPriceBlock" style="{{old('offetType')? (((old('offetType')=='offer')||(old('offetType')=='coupon')) ?'display: none':''):(((old('offetType')=='offer')||(old('offetType')=='coupon'))?'display: none':'')}}" >
                                    <label class="control-label col-md-4">{{old('offetType')? (old('offetType')=='coupon' ?'Coupon':'Voucher'):($merchantDeal->dealType=='coupon'?'Coupon':'Voucher')}} Price
                                    </label>
                                    <div class="col-md-8">
                                        <input type="number" class="form-control" name="voucher_price" value="{{old('voucher_price')?old('voucher_price'):$merchantDeal->online_sell_price}}">
                                    </div>
                                </div>
                                <div class="form-group couponPriceBlock" style="{{old('offetType')? (old('offetType')=='coupon' ?'display: none':''):($merchantDeal->dealType=='coupon'?'display: none':'')}}" >
                                    <label class="control-label col-md-4">Cash Back Amount
                                    </label>
                                    <div class="col-md-8">
                                        <input type="number" class="form-control" name="cashbackamount" value="{{old('cashbackamount')?old('cashbackamount'):$merchantDeal->cashbackamount}}">
                                    </div>
                                </div>

                            @if(Auth::user()->type == 'freelancer' || Auth::user()->type == 'admin')

                                <div class="form-group" >
                                    <label class="control-label col-md-4">Commission Amount
                                    </label>
                                    <div class="col-md-8">
                                        <input type="number" class="form-control" name="commission_price"  value="{{old('commission_price')?old('commission_price'):$merchantDeal->commission_price}}">
                                    </div>
                                </div>

                            @endif
							
							<div class="form-group">
								<label class="control-label col-md-4">Category
								</label>
								<div class="col-md-8">
									<select type="number" class="form-control select2" name="category" required >
										@foreach($category as $cat)
										<option @if($cat->id == $merchantDeal->category_id) selected @endif value="{{$cat->id}}">{{$cat->name}}</option>
										@endforeach
									<select>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-4">Current Deal Status
								</label>
								<div class="col-md-8">
									@if($merchantDeal->status == 0)
										<span class="label label-sm label-info"> Freelancer Approved </span>
									@elseif($merchantDeal->status == 1)
										<span class="label label-sm label-success"> Admin Approved </span>
									@elseif($merchantDeal->status == 2)
										<span class="label label-sm label-warning"> Pending Approval </span>
									@elseif($merchantDeal->status == 3)
										<span class="label label-sm label-danger"> Freelancer Rejected </span>
									@elseif($merchantDeal->status == 4)
										<span class="label label-sm label-danger"> Admin Rejected </span>
									@endif
								</div>
							</div>




									
							@if(Auth::user()->type == 'freelancer' || Auth::user()->type == 'admin')
								<div class="form-group">
									<label class="control-label col-md-4">Change Status
									</label>
									<div class="col-md-8">
										<select type="number" class="form-control select2" name="status" required >		
											
											<option value="0" @if($merchantDeal->status=='0') selected @endif >Freelancer Approved</option>
											<option value="2" @if($merchantDeal->status=='2') selected @endif>Freelancer Approval Pending</option>
											<option value="3" @if($merchantDeal->status=='3') selected @endif>Freelancer Rejected</option>
                                            @if( Auth::user()->type == 'admin')
											<option value="1" @if($merchantDeal->status=='1') selected @endif>Admin Approved</option>
											<option value="4" @if($merchantDeal->status=='4') selected @endif>Admin Rejected</option>
											@endif
										<select>
									</div>
								</div>
							@endif
                            <div class="clear-fix clearfix"></div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet light bordered" style="margin-top:20px;">
                        <div class="portlet-body">
                            <h4>Deal Publish Deatils</h4>
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Time
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control form_datetime" name="start_time" value="{{old('start_time')?old('start_time'):Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$merchantDeal->start_time)->format('j M Y - H:i')}}" required readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">End Time
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control form_datetime" name="end_time" value="{{old('end_time')?old('end_time'):Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$merchantDeal->end_time)->format('j M Y - H:i')}}"  required readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Stock
                                </label>
                                <div class="col-md-8">
                                    <input type="number" class="form-control " name="stock" value="{{old('stock')?old('stock'):$merchantDeal->stock}}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">User Buy Limit
                                </label>
                                <div class="col-md-8">
                                    <input placeholder="Use -1 for Unlimited Buy Limit" type="number" class="form-control " name="userbuylimit" value="{{old('userbuylimit')?old('userbuylimit'):$merchantDeal->userBuyLimit}}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">List Image (2:3)
                                </label>
                                <div class="col-md-8">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="height: 300px;">
                                            <img src="{{$merchantDeal->small_image?'/uploads/'.$merchantDeal->small_image:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" style=" height: 300px;"/>
                                        </div>
                                        <div class="hidden_file">
                                            <a href="javascript:;" class="btn red remove_btn"> Remove </a>
                                            <input type="hidden" class="form-control " name="small_image1" value="{{$merchantDeal->small_image}}">
                                            <input type="file" class="form-control " name="small_image" value="">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clear-fix clearfix"></div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Deal Description</h4>
                                <div class="form-group">
                                    <textarea name="description" class="summernote" style="display: none;">{!! $merchantDeal->description !!}</textarea>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Terms & Condtions</h4>
                                <div class="form-group">
                                    <textarea name="termsConditions" class="summernote" style="display: none;">{!! $merchantDeal->termsConditions !!}</textarea>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Deal Banners (13 : 4)</h4>
                                <div class="form-group">
                                    <ol class="list-group">
                                        @php $banners    =    json_decode($merchantDeal->banners); $i=0; @endphp
                                        @foreach($banners as $banner)
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 390px; height: 120px;">
                                                    <img src="{{$banner!=''?'/uploads/'.$banner:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" style="width:  390px; height: 120px;"/>
                                                </div>
                                                <div  class="hidden_file">
                                                    <a href="javascript:;" class="btn red remove_btn" data-dismiss="fileinput"> Remove </a>
                                                    <input type="hidden" class="form-control" name="banners_1[{{$i}}]" value="{{$banner}}">
                                                </div>
                                            </div>
                                        @php $i++; @endphp
                                        @endforeach
                                        <li class="list-group-item"><input type="file" name="banners[]" {{count($banners)<0?'required': ''}}></li>
                                        <li class="list-group-item"><input type="file" name="banners[]"></li>
                                        <li class="list-group-item"><a class=" btn btn-primary btn-block addFileBlock"><i class="fa fa-plus"></i> Add New</a></li>
                                    </ol>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Sponser/Other Images</h4>
                                <div class="form-group">
                                    <ol class="list-group">
                                        @php $sponsers    =    json_decode($merchantDeal->sponsers); $i=0; @endphp
                                        @foreach($sponsers as $sponser)
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 390px; height: 120px;">
                                                    <img src="{{$sponser!=''?'/uploads/'.$sponser:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" style="width:  390px; height: 120px;"/>
                                                </div>
                                                <div  class="hidden_file">
                                                    <a href="javascript:;" class="btn red remove_btn" data-dismiss="fileinput"> Remove </a>
                                                    <input type="hidden" class="form-control" name="sponsers_1[{{$i}}]" value="{{$sponser}}">
                                                </div>
                                            </div>
                                            @php $i++; @endphp
                                        @endforeach
                                        <li class="list-group-item"><input type="file" name="sponsers[]" {{count($sponsers)<0?'required': ''}}></li>
                                        <li class="list-group-item"><input type="file" name="sponsers[]"></li>
                                        <li class="list-group-item"><a class=" btn btn-primary btn-block addFileBlock"><i class="fa fa-plus"></i> Add New</a></li>
                                    </ol>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group1">
                                <button type="submit"  class="btn green button-submit pull-right"> Update
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

