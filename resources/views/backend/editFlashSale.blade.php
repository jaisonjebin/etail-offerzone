@extends('backend.master')
@push('styles')
    <link href="/back/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
    <script src="/back/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-summernote/summernote.js" type="text/javascript"></script><script src="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script>
        $('.remove_btn').on('click',function () {
            $(this).closest('.fileinput').find('.thumbnail').hide();
            console.log($(this).closest('.hidden_file').find('input[type="hidden"]').val());
            $(this).closest('.hidden_file').find('input[type="hidden"]').val('');
            // $('input[name="small_image1"]').val('');
            $(this).hide();
        });
        $(".summernote").summernote({height:250});
        $(".form_datetime").datetimepicker({
            autoclose: !0,
            isRTL: false,
            format: "dd MM yyyy - hh:ii",
            fontAwesome: !0,
            pickerPosition: "bottom-right",
            startDate: new Date(),
            todayBtn:true
        });
        $('.addFileBlock').on('click',function(){
            $(this).closest('li').prev().clone().insertBefore($(this).closest('li'));
            $(this).closest('li').prev().find('input').val('');
        })
    </script>
    {{--<script src="/back/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
@endpush
@section('pagebody')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="">Flash Sale</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Edit Flash Sale</span>
                    </li>
                </ul>

            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <!-- BEGIN DASHBOARD STATS 1-->

            <form class="form-horizontal" method="post" action="{{route('admin.updateFlashSale')}}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$flashSale->id}}">

                <div class="row"  style="margin-top:20px;">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group1">
                                <button type="submit"  class="btn green button-submit pull-right"> Update
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="portlet light bordered" style="margin-top:20px;">
                        <div class="portlet-body">
                            <h4>Flash Sale General Details</h4>
                            <div class="form-group">
                                <label class="control-label col-md-4">Product Title
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="title" value="{{old('title')?old('title'):$flashSale->title}}"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Short Description
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="small_description" value="{{old('small_description')?old('small_description	'):$flashSale->small_description}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Price / MRP
                                </label>
                                <div class="col-md-8">
                                    <input type="number" class="form-control" name="price" value="{{old('price')?old('price'):$flashSale->price}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Offer Price
                                </label>
                                <div class="col-md-8">
                                    <input type="number" class="form-control" name="offer_price" value="{{old('offer_price')?old('offer_price'):$flashSale->offer_price}}" required>
                                </div>
                            </div>

                            <div class="clear-fix clearfix"></div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet light bordered" style="margin-top:20px;">
                        <div class="portlet-body">
                            <h4>Flash Sale Publish Deatils</h4>
                            <div class="form-group">
                                <label class="control-label col-md-4">Start Time
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control form_datetime" name="start_time" value="{{old('start_time')?old('start_time'):Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$flashSale->start_time)->format('j M Y - H:i')}}" required readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">End Time
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control form_datetime" name="end_time" value="{{old('end_time')?old('end_time'):Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$flashSale->end_time)->format('j M Y - H:i')}}"  required readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Stock
                                </label>
                                <div class="col-md-8">
                                    <input type="number" class="form-control " name="stock" value="{{old('stock')?old('stock'):$flashSale->stock}}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">List Image (2:3)
                                </label>
                                <div class="col-md-8">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 300px;">
                                            <img src="{{$flashSale->small_image?'/uploads/'.$flashSale->small_image:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" style="width: 200px; height: 300px;"/>
                                        </div>
                                        <div class="hidden_file">
                                            <a href="javascript:;" class="btn red remove_btn"> Remove </a>
                                            <input type="hidden" class="form-control " name="small_image1" value="{{$flashSale->small_image}}">
                                            <input type="file" class="form-control " name="small_image" value="">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clear-fix clearfix"></div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Product Description</h4>
                                <div class="form-group">
                                    <textarea name="description" class="summernote" style="display: none;">{!! $flashSale->description !!}</textarea>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Terms & Condtions</h4>
                                <div class="form-group">
                                    <textarea name="termsConditions" class="summernote" style="display: none;">{!! $flashSale->termsConditions !!}</textarea>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Product Banners (13 : 4)</h4>
                                <div class="form-group">
                                    <ol class="list-group">
                                        @php $banners    =    json_decode($flashSale->banners); $i=0; @endphp
                                        @foreach($banners as $banner)
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 390px; height: 120px;">
                                                    <img src="{{$banner!=''?'/uploads/'.$banner:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" style="width:  390px; height: 120px;"/>
                                                </div>
                                                <div  class="hidden_file">
                                                    <a href="javascript:;" class="btn red remove_btn" data-dismiss="fileinput"> Remove </a>
                                                    <input type="hidden" class="form-control" name="banners_1[{{$i}}]" value="{{$banner}}">
                                                </div>
                                            </div>
                                        @php $i++; @endphp
                                        @endforeach
                                        <li class="list-group-item"><input type="file" name="banners[]" {{count($banners)<0?'required':''}}></li>
                                        <li class="list-group-item"><input type="file" name="banners[]"></li>
                                        <li class="list-group-item"><a class=" btn btn-primary btn-block addFileBlock"><i class="fa fa-plus"></i> Add New</a></li>
                                    </ol>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet light bordered" style="margin-top:20px;">
                            <div class="portlet-body">
                                <h4>Sponser Images</h4>
                                <div class="form-group">
                                    <ol class="list-group">
                                        @php $sponsers    =    json_decode($flashSale->sponsers); $i=0; @endphp
                                        @foreach($sponsers as $sponser)
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 390px; height: 120px;">
                                                    <img src="{{$sponser!=''?'/uploads/'.$sponser:'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}" alt="" style="width:  390px; height: 120px;"/>
                                                </div>
                                                <div  class="hidden_file">
                                                    <a href="javascript:;" class="btn red remove_btn" data-dismiss="fileinput"> Remove </a>
                                                    <input type="hidden" class="form-control" name="sponsers_1[{{$i}}]" value="{{$sponser}}">
                                                </div>
                                            </div>
                                            @php $i++; @endphp
                                        @endforeach
                                        <li class="list-group-item"><input type="file" name="sponsers[]" {{count($sponsers)<0?'required':''}}></li>
                                        <li class="list-group-item"><input type="file" name="sponsers[]"></li>
                                        <li class="list-group-item"><a class=" btn btn-primary btn-block addFileBlock"><i class="fa fa-plus"></i> Add New</a></li>
                                    </ol>
                                </div>
                                <div class="clear-fix clearfix"></div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group1">
                                <button type="submit"  class="btn green button-submit pull-right"> Update
                                    <i class="fa fa-check"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop

