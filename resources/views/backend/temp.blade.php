<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Mearchant Register | Dealdaa</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/back/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="/back/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/back/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="/back/assets/pages/css/register.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" /> </head>
<style>
    .login .content .form-control:active, .login .content .form-control:focus {
        border: 1px solid #83b8db;
    }
    .select2,.select2-selection__rendered{
        background-color: #6ba3c8 !important;
        border: 1px solid #6ba3c8 !important;
        color: #d9ecf9 !important;
    }
    .select2-selection,.select2-results{
        background-color: #6ba3c8 !important;
        border: 1px solid #6ba3c8 !important;
        color: #d9ecf9 !important;
    }
</style>
<!-- END HEAD -->

<body class=" login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="javascript:void(0);">18
        <img src="/back/assets/pages/img/logo-big-white.png" style="height: 40px;" alt="" /> </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN REGISTRATION FORM -->
    <form class="register-form" method="POST" action="{{ route('saveMerchant') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-title">
            <span class="form-title">Sign Up</span>
        </div>
        <p class="hint"> Enter your details below: </p>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Referal Code</label>
                    <input name="ref_code" value="{{old('ref_code')}}" class="form-control placeholder-no-fix {{ $errors->has('ref_code') ? ' has-error' : '' }}" type="text" value="{{$ref_id}}" placeholder="Referal Code"/>
                    @if ($errors->has('ref_code'))
                        <span class="help-block">
                        {{ $errors->first('ref_code') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Full Name *</label>
                    <input name="name" value="{{old('name')}}" class="form-control placeholder-no-fix {{ $errors->has('name') ? ' has-error' : '' }}" type="text" placeholder="Full Name" required/>
                    @if ($errors->has('name'))
                        <span class="help-block">
                        {{ $errors->first('name') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Mobile No *</label>
                    <input name="phone" value="{{old('phone')}}" class="form-control placeholder-no-fix {{ $errors->has('phone') ? ' has-error' : '' }}" type="number" placeholder="Mobile No" required/>
                    @if ($errors->has('phone'))
                        <span class="help-block">
                        {{ $errors->first('phone') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Email Id</label>
                    <input name="email" value="{{old('email')}}" class="form-control placeholder-no-fix {{ $errors->has('email') ? ' has-error' : '' }}" type="email" placeholder="Email Id" />
                    @if ($errors->has('email'))
                        <span class="help-block">
                        {{ $errors->first('email') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Address *</label>
                    <input name="address" value="{{old('address')}}" class="form-control placeholder-no-fix {{ $errors->has('address') ? ' has-error' : '' }}" type="text" placeholder="Address" required/>
                    @if ($errors->has('address'))
                        <span class="help-block">
                        {{ $errors->first('address') }}
                    </span>
                    @endif
                </div>
                <div class="row">
                    <div class="form-group col-lg-6">
                        <label class="control-label visible-ie8 visible-ie9">Pincode *</label>
                        <input type="number" name="pincode" value="{{old('pincode')}}" class="form-control placeholder-no-fix {{ $errors->has('pincode') ? ' has-error' : '' }}" required  placeholder="Pincode"/>
                        @if ($errors->has('pincode'))
                            <span class="help-block">
                        {{ $errors->first('pincode') }}
                    </span>
                        @endif
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label visible-ie8 visible-ie9">State *</label>
                        <select name="state" class="j_state_input select2 form-control placeholder-no-fix {{ $errors->has('state') ? ' has-error' : '' }}" required>
                            @foreach($states as $state)
                                <option value="{{$state->id}}" {{$state->id==19?'selected':''}}>{{$state->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('state'))
                            <span class="help-block">
                        {{ $errors->first('state') }}
                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group j_district_div">
                    <label class="control-label visible-ie8 visible-ie9">District</label>
                    <select name="district" class="j_district_input form-control placeholder-no-fix {{ $errors->has('district') ? ' has-error' : '' }}">
                    </select>
                    @if ($errors->has('district'))
                        <span class="help-block">
                        {{ $errors->first('district') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">City *</label>
                    <select name="city" class="j_cities_input form-control placeholder-no-fix {{ $errors->has('city') ? ' has-error' : '' }}" required>
                    </select>
                    @if ($errors->has('city'))
                        <span class="help-block">
                        {{ $errors->first('city') }}
                    </span>
                    @endif
                </div>
                <p class="hint"> Enter your firm details below: </p>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Firm/Shop Name *</label>
                    <input name="firm_name" value="{{old('firm_name')}}" class="form-control placeholder-no-fix {{ $errors->has('firm_name') ? ' has-error' : '' }}" type="text" placeholder="Firm/Shop Name" required/>
                    @if ($errors->has('firm_name'))
                        <span class="help-block">
                        {{ $errors->first('firm_name') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Category *</label>
                    <select name="category" class="select2 form-control placeholder-no-fix {{ $errors->has('category') ? ' has-error' : '' }}" required>
                        <option>Select Category</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" {{old('category')==$category->id?'selected':''}}>{{$category->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('category'))
                        <span class="help-block">
                        {{ $errors->first('category') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Business Description</label>
                    <textarea class="form-control placeholder-no-fix {{ $errors->has('descrption') ? ' has-error' : '' }}" autocomplete="off" placeholder="Business Description" name="descrption" style="height: 60px;">{{old('descrption')}}</textarea>
                    @if ($errors->has('descrption'))
                        <span class="help-block">
                            {{ $errors->first('descrption') }}
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">GSTIN</label>
                    <input name="gstin" value="{{old('gstin')}}" class="form-control placeholder-no-fix {{ $errors->has('gstin') ? ' has-error' : '' }}" type="text" placeholder="GSTIN" />
                    @if ($errors->has('gstin'))
                        <span class="help-block">
                        {{ $errors->first('gstin') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Contact Person *</label>
                    <input name="contact_person" value="{{old('contact_person')}}" class="form-control placeholder-no-fix {{ $errors->has('contact_person') ? ' has-error' : '' }}" type="text" placeholder="Contact Person" required/>
                    @if ($errors->has('contact_person'))
                        <span class="help-block">
                        {{ $errors->first('contact_person') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Designation</label>
                    <input name="designation" value="{{old('designation')}}" class="form-control placeholder-no-fix {{ $errors->has('designation') ? ' has-error' : '' }}" type="text" placeholder="Designation" />
                    @if ($errors->has('designation'))
                        <span class="help-block">
                        {{ $errors->first('designation') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Mobile No *</label>
                    <input name="firm_phone" value="{{old('firm_phone')}}" class="form-control placeholder-no-fix {{ $errors->has('firm_phone') ? ' has-error' : '' }}" type="number" placeholder="Mobile No" required/>
                    @if ($errors->has('firm_phone'))
                        <span class="help-block">
                        {{ $errors->first('firm_phone') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Email Id</label>
                    <input name="firm_email" value="{{old('firm_email')}}" class="form-control placeholder-no-fix {{ $errors->has('firm_email') ? ' has-error' : '' }}" type="email" placeholder="Email Id"/>
                    @if ($errors->has('firm_email'))
                        <span class="help-block">
                        {{ $errors->first('firm_email') }}
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password *</label>
                    <input class="form-control placeholder-no-fix {{ $errors->has('password') ? ' has-error' : '' }}" type="password" autocomplete="off" placeholder="Password" name="password" required/>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            {{ $errors->first('password') }}
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Re-type Your Password *</label>
                    <input class="form-control placeholder-no-fix {{ $errors->has('password_confirmation') ? ' has-error' : '' }}" type="password" autocomplete="off" placeholder="Re-type Your Password" name="password_confirmation" required/>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            {{ $errors->first('password_confirmation') }}
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Select Firm Photos</label>
                    {{--<div class="col-md-12">--}}
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                <span class="fileinput-filename"> </span>
                            </div>
                            <span class="input-group-addon btn default btn-file">
                                                                    <span class="fileinput-new">Select Firm Photos </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="firm_images[]" multiple> </span>
                            <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                        </div>
                    </div>
                    {{--</div>--}}
                </div>
                <div class="form-group margin-top-20 margin-bottom-20">
                    <label class="mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" {{old('tnc')?'checked':''}}name="tnc" value="agree" required/> I agree to the
                        <a href="javascript:;">Terms of Service </a> &
                        <a href="javascript:;">Privacy Policy </a>
                        <span></span>
                    </label>
                    @if ($errors->has('tnc'))
                        <span class="help-block">
                            {{ $errors->first('tnc') }}
                        </span>
                    @endif
                    {{--<div id="register_tnc_error"> </div>--}}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Avatar</label>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Avatar" alt="" />
                        </div>
                        <div style="display: inline;">
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="avatar" accept="image/*">
                                                                </span>
                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                        </div>
                    </div>
                    @if ($errors->has('avatar'))
                        <span class="help-block">
                        {{ $errors->first('avatar') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Address Proof</label>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 400px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Address+Proof" alt=""  style="width:400px; height: 150px;"/>
                        </div>
                        <div>
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="address_proof">
                                                                </span>
                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                        </div>
                    </div>
                    @if ($errors->has('address_proof'))
                        <span class="help-block">
                        {{ $errors->first('address_proof') }}
                    </span>
                    @endif
                </div>
                <p class="hint"> Enter your bank account details below:
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">PAN Card No</label>
                    <input name="account_pan" value="{{old('account_pan')}}" class="form-control placeholder-no-fix {{ $errors->has('account_pan') ? ' has-error' : '' }}" type="text" placeholder="PAN Card No"/>
                    @if ($errors->has('account_pan'))
                        <span class="help-block">
                        {{ $errors->first('account_pan') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Account Name</label>
                    <input name="account_name" value="{{old('account_name')}}" class="form-control placeholder-no-fix {{ $errors->has('account_name') ? ' has-error' : '' }}" type="text" placeholder="Account Name"/>
                    @if ($errors->has('account_name'))
                        <span class="help-block">
                        {{ $errors->first('account_name') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Account No</label>
                    <input name="account_no" value="{{old('account_no')}}" class="form-control placeholder-no-fix {{ $errors->has('account_no') ? ' has-error' : '' }}" type="text" placeholder="Account No"/>
                    @if ($errors->has('account_no'))
                        <span class="help-block">
                        {{ $errors->first('account_no') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Bank Name</label>
                    <input name="account_bank" value="{{old('account_bank')}} "class="form-control placeholder-no-fix {{ $errors->has('account_bank') ? ' has-error' : '' }}" type="text" placeholder="Bank Name"/>
                    @if ($errors->has('account_bank'))
                        <span class="help-block">
                        {{ $errors->first('account_bank') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Bank Branch</label>
                    <input name="account_bank_branch" value="{{old('account_bank_branch')}}" class="form-control placeholder-no-fix {{ $errors->has('account_bank_branch') ? ' has-error' : '' }}" type="text" placeholder="Bank Branch"/>
                    @if ($errors->has('account_bank_branch'))
                        <span class="help-block">
                        {{ $errors->first('account_bank_branch') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">IFSC Code</label>
                    <input name="account_ifsc" value="{{old('account_ifsc')}}" class="form-control placeholder-no-fix {{ $errors->has('account_ifsc') ? ' has-error' : '' }}" type="text" placeholder="IFSC Code"/>
                    @if ($errors->has('account_ifsc'))
                        <span class="help-block">
                        {{ $errors->first('account_ifsc') }}
                    </span>
                    @endif
                </div>
                <div id="map" style="height: 280px;"></div>
                <div class="alert alert-success l_map">
                    Drag marker to you Firm Location.
                </div>
                <input type="hidden" name="latitude" id="l_latitudeInput">
                <input type="hidden" name="longitude" id="l_longitudeInput">
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" id="register-submit-btn" class="btn red uppercase pull-right">Submit</button>
        </div>
    </form>
    <!-- END REGISTRATION FORM -->
</div>
<div class="copyright hide"> {{date('Y')}} © Dealdaa. </div>
<!-- END LOGIN -->
<!--[if lt IE 9]>
<script src="/back/assets/global/plugins/respond.min.js"></script>
<script src="/back/assets/global/plugins/excanvas.min.js"></script>
<script src="/back/assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="/back/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/back/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="/back/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="/back/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{{--<script src="/back/assets/pages/scripts/login.min.js" type="text/javascript"></script>--}}
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<!-- END THEME LAYOUT SCRIPTS -->
<script>
    $(document).ready(function () {
        $('.j_state_input').trigger('change');
        $('.j_state_input').select2();
    });

    $('.j_state_input').on('change',function () {

        $state_id   =   $('.j_state_input').val();
        if($state_id=='19')
        {
            $.ajax({
                type: "GET",
                url: '{{route("getDistricts","")}}/'+$state_id,
                dataType:'json',
                success: function( data )
                {
                    $('.j_district_div').show();
                    $(".j_district_input").html('');
                    $.each(data,function(key, value)
                    {
                        option='';
                        option= '<option  value=' + value.id + '>' + value.name + '</option>';

                        $(".j_district_input").append(option);
                    });
                    $('.j_district_input').select2();
                    // $('.j_district_input').val($('.j_district_input').attr('data-selected'));
                    $('.j_district_input').trigger('change');
                }
            });
        }
        else
        {
            $(".j_district_div").hide();
            $.ajax({
                type: "GET",
                url: '{{route("getCities","")}}/'+$state_id,
                dataType:'json',
                success: function( data )
                {
                    $(".j_cities_input").html('');
                    // $('.j_cities_input').select2('destroy');

                    $.each(data,function(key, value)
                    {
                        option='';
                        option= '<option  value=' + value.id + '>' + value.name + '</option>';

                        $(".j_cities_input").append(option);
                    });
                    $('.j_cities_input').select2();
                    $('.j_cities_input').val($('.j_cities_input').attr('data-selected'));
                }
            });
        }

    });
    $('.j_district_input').on('change',function () {
        $district_id   =   $('.j_district_input').val();
        $.ajax({
            type: "GET",
            url: '{{route("getCities","")}}/'+$district_id+'/KL',
            dataType:'json',
            success: function( data )
            {
                $(".j_cities_input").html('');
                // $('.j_cities_input').select2('destroy');

                $.each(data,function(key, value)
                {
                    option='';
                    option= '<option  value=' + value.id + '>' + value.name + '</option>';

                    $(".j_cities_input").append(option);
                });
                $('.j_cities_input').select2();
                $('.j_cities_input').val($('.j_cities_input').attr('data-selected'));
                $('.j_cities_input').trigger('change');
            }
        });
    });
</script>
<script>
    var geocoder;
    var address = "San Diego, CA";
    $('.j_cities_input').on('change',function () {
        address = $('.j_cities_input option:selected').text()+', '+$('.j_district_input option:selected').text()+', '+$('.j_state_input option:selected').text();
        initMap();
    });
    var marker;
    function initMap() {
        geocoder = new google.maps.Geocoder();
        var myLatlng = {lat: -25.363, lng: 131.044};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: myLatlng
        });
        if (geocoder) {
            geocoder.geocode({
                'address': address
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                        map.setCenter(results[0].geometry.location);

                        var infowindow = new google.maps.InfoWindow({
                            content: '<b>' + address + '</b>',
                            size: new google.maps.Size(150, 50)
                        });

                        marker = new google.maps.Marker({
                            position: results[0].geometry.location,
                            map: map,
                            draggable:true,
                            title: address
                        });

                        $('#l_latitudeInput').val(marker.position.lat());
                        $('#l_longitudeInput').val(marker.position.lng());
                        // google.maps.event.addListener(marker, 'click', function() {
                        //     infowindow.open(map, marker);
                        // });
                        google.maps.event.addListener(marker, 'dragend', function() {
                            $('#l_latitudeInput').val(marker.position.lat());
                            $('#l_longitudeInput').val(marker.position.lng());
                            $('.l_map').html('');
                            $('.l_map').html('<strong>Success!</strong> Location has been saved.');
                            $('.l_map').show();
                            console.log(marker.position.lat());
                        });

                    } else {
                        alert("No results found");
                    }
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }
        //
        // var marker = new google.maps.Marker({
        //     position: myLatlng,
        //     map: map,
        //     title: 'Click to zoom'
        // });

        map.addListener('center_changed', function() {
            // 3 seconds after the center of the map has changed, pan back to the
            // marker.
            window.setTimeout(function() {
                map.panTo(marker.getPosition());
            }, 3000);
        });

        // google.maps.event.addListener(marker, 'click', function () {
        //     // do something with this marker ...
        //     this.setTitle('I am clicked');
        //     console.log('test');
        // });

        // marker.addListener('click', function() {
        //     map.setZoom(8);
        //     map.setCenter(marker.getPosition());
        // });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADPiaU7KWZ6B1zpN6Ps1IrjDpP1IUduRg&callback=initMap">
</script>
</body>

</html>