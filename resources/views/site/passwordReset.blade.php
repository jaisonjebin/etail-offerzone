@extends('site.layout.master')


@section('title', 'Password Reset')

@push('style')

@endpush

@push('scripts')
    <script>
        $('.sentOtp').on('click',function(){

            if($(this).closest('form').find('[name="phone"]').val() ==''){
                $(this).closest('form').find('[name="phone"]').focus();
                $(this).closest('form').find('[name="phone"]').addClass('animated shake').one('webkitAnimationEnd` mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                    $(this).removeClass('animated shake');
                });
                return false;
            }

            $(this).html('Sending..');
            $(this).addClass('disabled');
            $.ajax({
                url:'{{route('sentOtp1')}}',
                type:'post',
                context:$(this),
                data:{'phone': $(this).closest('form').find('[name="phone"]').val()},
                success:function(data){
                    if(data=='sent')
                    {
                        $(this).closest('form').find('[name="otp"]').prop("disabled", false);
                        $('.sendotp_div').remove();
                        $('.resetLabel').remove();
                        $('.alert-danger').remove();
                        $('#formDiv').prepend('<label class="label label-success resetLabel">An OTP sent to phone</label>');
                    }
                    else
                    {
                        $(this).html('Send');
                        $(this).removeClass('disabled');
                        $('.alert-danger').remove();
                        $('.formDiv').prepend('<div class="alert alert-danger">'+data+'</div>');
                        // toastr.error(data);
                    }

                }
            })
        });
        $('.verifyOtp').on('click',function(){

            if($(this).closest('form').find('[name="otp"]').val() ==''){
                $(this).closest('form').find('[name="otp"]').focus();
                $(this).closest('form').find('[name="otp"]').addClass('animated shake').one('webkitAnimationEnd` mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                    $(this).removeClass('animated shake');
                });
                return false;
            }
            var verifyBtn   =   $(this);
            verifyBtn.html('Verifying..');
            verifyBtn.addClass('disabled');
            $.ajax({
                url:'{{route('otpVerification')}}',
                type:'POST',
                context:$(this),
                data:{'otp': $(this).closest('form').find('[name="otp"]').val()},
                success:function(data){
                    console.log(data);
                    if(data=='Otp Verified')
                    {
                        $('.verifyotp_div').remove();
                        $(this).html('Verified');
                        $('.alert-danger').remove();
                        $('.resetLabel').remove();
                        $('#formDiv').prepend('<label class="label label-success resetLabel">Verified</label>');
                        $('.disabled').prop("disabled", false);
                    }
                    else
                    {
                        verifyBtn.html('Verify');
                        verifyBtn.removeClass('disabled');
                        $('.alert-danger').remove();
                        $('.formDiv').prepend('<div class="alert alert-danger">'+data+'</div>');

                    }

                }
            })
        });
        setInterval(function () {
            $('.countdown').each(function(){
                $(this).html( parseInt($(this).html())-1);
                if($(this).html()=='0'){
                    $(this).closest('a').html('Resent OTP').removeClass('disabled');
                }
            })

        },1000);
    </script>
@endpush

@section('content')
    <section class="sign-area panel p-40">
        <h3 class="sign-title">Forgot Password</h3>
        <div class="row row-rl-0">
            <div class="col-sm-12 col-md-7 col-md-offset-2 formDiv">
                @foreach ($errors->all() as $message)
                    <div class="alert alert-danger">{{$message}}</div>
                @endforeach
                <form class="p-40" action="{{route('passwordReset')}}" method="post" id="resetForm">
                    @csrf
                    <div class="input-group form-group sendotp_div" {{($errors->any())?'style=display:none':''}}>
                        <label class="sr-only">Mobile</label>
                        <input type="text" name="phone" class="form-control" placeholder="Enter Mobile No">
                        <span class="input-group-btn">
        <a class="btn btn-danger sentOtp" type="button">Send OTP</a>
      </span>
                    </div>
                    <div class="input-group form-group verifyotp_div" {{($errors->any())?'style=display:none':''}}>
                        <label class="sr-only">OTP</label>
                        <input type="text" name="otp" class="form-control" disabled placeholder="OTP">
                        <span class="input-group-btn">
        <a class="btn btn-danger verifyOtp" disabled type="button">Verify OTP</a>
      </span>
                    </div>
                    <div class="form-group">
                        <label class="sr-only">New Password</label>
                        <input type="password" name="password" class="form-control input-lg {{($errors->any())?'':'disabled'}}" {{($errors->any())?'':'disabled'}} placeholder="New Password" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control input-lg {{($errors->any())?'':'disabled'}}" {{($errors->any())?'':'disabled'}} placeholder="Confirm Password" required>
                    </div>
                    <button type="submit" class="btn btn-block btn-lg">Reset Password</button>
                </form>
            </div>
        </div>
    </section>
@stop