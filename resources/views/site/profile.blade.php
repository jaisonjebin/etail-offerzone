@extends('site.layout.master')


@section('title', 'Blank Page')

@push('style')

@endpush

@push('scripts')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){
            getDistricts();
            $('.select2').select2();
        });
        //edit_profile_btn
        //edit_profile
        $('.edit_profile_btn').click(function () {
            $('.edit_profile').show();
            $('.view_profile').hide();
            $('.edit_profile_btn').hide();
        });
        $('.edit_profile_cancel').click(function () {
            $('.edit_profile').hide();
            $('.view_profile').show();
            $('.edit_profile_btn').show();
        });
        function getDistricts()
        {
            $data   =   $(".j_district_input").data('id');
            $.ajax({
                type: "GET",
                url: '{{route("getDistricts","")}}/19',
                dataType:'json',
                success: function( data )
                {
                    $(".j_district_input").html('');
                    $(".j_district_input").append('<option>Select District</option>');

                    $.each(data,function(key, value)
                    {
                        option='';
                        option= '<option  value="'+value.id+'" ' +((value.id==$data)?"selected":"")+ '>' + value.name + '</option>';
                        $(".j_district_input").append(option);
                    });
                    // $('.j_district_input').select2();
                    // $('.j_district_input').val($('.j_district_input').attr('data-selected'));
                    $('.j_district_input').trigger('change');
                }
            });
        }
        $('.j_district_input').on('change',function () {

            $district_id   =   $('.j_district_input').val();
            $data   =   $(".j_cities_input").data('id');
            $.ajax({
                type: "GET",
                url: '{{route("getCities","")}}/'+$district_id+'/KL',
                dataType:'json',
                success: function( data )
                {
                    $('.j_cities_input').select2('destroy');
                    $(".j_cities_input").html('');
                    $(".j_cities_input").append('<option>Select City</option>');
                    $.each(data,function(key, value)
                    {
                        option='';
                        option= '<option  value="'+value.id+'" ' +((value.id==$data)?"selected":"")+ '>' + value.name + '</option>';
                        $(".j_cities_input").append(option);
                    });
                    $('.j_cities_input').select2();
                    $('.select2-container').css('width','100%');
                    $('.j_cities_input').val($('.j_cities_input').attr('data-selected'));
                }
            });
        });
    </script>
@endpush

@section('content')
    @include('site.includes.profile_sidebar')
    <div class="page-content col-sm-8 col-md-9">
        <div class=" deal_single_view">
            <div class="deal-deatails panel myprofile_minhieight">
                <div class="deal-body p-20">
                    <header class="panel pb-15 prl-0 pos-r mb-20">
                        <h3 class="h-title font-18">My Profile</h3>
                        <a href="javascript:void(0);" class="btn btn-o btn-xs pos-a top-15 right-10 pos-tb-center edit_profile_btn">Edit</a>
                    </header>
                    <div class="flashsale_terms">
                        <div class="view_profile">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <h5>{{Auth::user()->name}}</h5>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <h5>{{Auth::user()->email}}</h5>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <h5>{{Auth::user()->phone}}</h5>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>City</label>
                                    <h5>{{Auth::user()->getCity?Auth::user()->getCity->name:''}}</h5>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>District</label>
                                    <h5>{{Auth::user()->getDistrict?Auth::user()->getDistrict->name:''}}</h5>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="edit_profile" style="display: none;">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form class="mb-30" method="post" action="{{route('user.profileEdit')}}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" name="name" class="form-control" placeholder="Enter you Name" value="{{Auth::user()->name}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Mobile</label>
                                            <input type="text" name="phone" class="form-control" placeholder="Enter your Mobile" value="{{Auth::user()->phone}}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" name="email" class="form-control" placeholder="Email" value="{{Auth::user()->email}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>District</label>
                                            <select class="form-control w-100 j_district_input" name="district" required data-id="{{Auth::user()->district}}">
                                                <option>Select District</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>City</label>
                                            <select class="form-control w-100 j_cities_input select2" name="city" required data-id="{{Auth::user()->city}}">
                                                <option>Select City</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group pull-right">
                                            <button type="submit" class="btn btn-rounded">UPDATE</button>
                                            <button type="button" class="btn btn-o btn-rounded ml-10 edit_profile_cancel">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
@stop