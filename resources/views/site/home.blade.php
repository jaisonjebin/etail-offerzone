@extends('site.layout.master')


@push('style')
    <!-- Owl Carousel -->
    <link href="/site/assets/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="/site/assets/vendors/owl-carousel/owl.theme.min.css" rel="stylesheet">
    <!-- Flex Slider -->
    <link href="/site/assets/vendors/flexslider/flexslider.css" rel="stylesheet">
@endpush

@push('scripts')
    <!-- Owl Carousel -->
    <script type="text/javascript" src="/site/assets/vendors/owl-carousel/owl.carousel.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="/site/assets/vendors/flexslider/jquery.flexslider-min.js"></script>

    <!-- Coutdown -->
    <script type="text/javascript" src="/site/assets/vendors/countdown/jquery.countdown.js"></script>
@endpush

@section('content')
    <div class="section deals-header-area pb-30">
        <div class="row row-tb-20">
            <div class="col-xs-12 col-md-4 col-lg-3">
                <aside>
                    <ul class="nav-coupon-category panel">
                        @foreach($categories as $category)
                        <li>
                            <a href="{{route('deallistDealsCategory',$category->id)}}"><i class="{{$category->cssIconClass}}"></i>{{$category->name}}<span>{{$category->get_deal_count}}</span></a>
                        </li>
                        @endforeach
                        <li class="all-cat">
                            <a class="font-14" href="{{route('allCategories')}}">All Categories</a>
                        </li>
                    </ul>
                </aside>
            </div>
            <div class="col-xs-12 col-md-8 col-lg-9">
                <div class="header-deals-slider owl-slider home_slider1" data-loop="true" data-autoplay="true"
                     data-autoplay-timeout="10000" data-smart-speed="1000" data-nav-speed="false"
                     data-nav="true" data-xxs-items="1" data-xxs-nav="true" data-xs-items="1" data-xs-nav="true"
                     data-sm-items="1" data-sm-nav="true" data-md-items="1" data-md-nav="true" data-lg-items="1"
                     data-lg-nav="true">

                    @if(isset($sliders->one))
                        @php $i=0; @endphp
                        @foreach($sliders->one as $slider)
                            <div class="deal-single panel item">
                                <figure class="deal-thumbnail" data-bg-img="/uploads/{{$slider->image}}">
                                    <div class="deal-about p-20 pos-a bottom-0 left-0 w-100">
                                        <div class="rating mb-10">
                                            <h3 class="deal-title float-right">
                                                <a href="{{$slider->link}}" class="btn btn-primary">Click to
                                                    Participate</a>
                                            </h3>
                                        </div>
                                    </div>
                                </figure>
                            </div>
                        @endforeach
                    @endif

                </div>

                <div class="col-md-12 p-0 mt-15">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="header-deals-slider owl-slider home_slider2" data-loop="true" data-autoplay="true"
                                 data-autoplay-timeout="10000" data-smart-speed="1000" data-nav-speed="false"
                                 data-nav="true" data-xxs-items="1" data-xxs-nav="true" data-xs-items="1"
                                 data-xs-nav="true" data-sm-items="1" data-sm-nav="true" data-md-items="1"
                                 data-md-nav="true" data-lg-items="1" data-lg-nav="true">

                                @if(isset($sliders->two))
                                    @php $i=0; @endphp
                                    @foreach($sliders->two as $slider)
                                        <div class="deal-single panel item">
                                            <figure class="deal-thumbnail" data-bg-img="/uploads/{{$slider->image}}">
                                                <div class="deal-about p-20 pos-a bottom-0 left-0 w-100">
                                                    <div class="rating mb-10">
                                                        <h3 class="deal-title float-right">
                                                            <a href="{{$slider->link}}" class="btn btn-primary">Get it Now</a>
                                                        </h3>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 mobile_slider">
                            <div class="header-deals-slider owl-slider  home_slider3" data-loop="true" data-autoplay="true"
                                 data-autoplay-timeout="10000" data-smart-speed="1000" data-nav-speed="false"
                                 data-nav="true" data-xxs-items="1" data-xxs-nav="true" data-xs-items="1"
                                 data-xs-nav="true" data-sm-items="1" data-sm-nav="true" data-md-items="1"
                                 data-md-nav="true" data-lg-items="1" data-lg-nav="true">
                                @if(isset($sliders->three))
                                    @php $i=0; @endphp
                                    @foreach($sliders->three as $slider)
                                        <div class="deal-single panel item">
                                            <figure class="deal-thumbnail" data-bg-img="/uploads/{{$slider->image}}">
                                                <div class="deal-about p-20 pos-a bottom-0 left-0 w-100">
                                                    <div class="rating mb-10">
                                                        <h3 class="deal-title float-right">
                                                            <a href="{{$slider->link}}" class="btn btn-primary">Get it Now</a>
                                                        </h3>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
    <section class="section latest-deals-area ptb-30">
        <header class="panel ptb-15 prl-20 pos-r mb-30">
            <h3 class="section-title font-18">Featured Flash Sales</h3>
            <a href="{{route('flashsaleItemList')}}" class="btn btn-o btn-xs pos-a right-10 pos-tb-center">View All</a>
        </header>

        <div class="row row-masnory row-tb-20">
            @foreach($flashSales as $flashSale)
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="deal-single panel">
                    <figure class=" embed-responsive embed-responsive-16by9"
                            data-bg-img="/uploads/{{$flashSale->small_image}}">
                        <div class="ribbon-flahssale is-hidden-xs-down">
                            <div class="ribbon-flah">On sales now</div>
                        </div>
                        <ul class="deal-actions top-15 right-15  v_shareParent" data-shareUrl="{{route('flashsaleItem',$flashSale->id)}}?ref={{getReferalId()}}" >
                            @include('site.includes.shareDropDownContents')
                            <li class="like-deal">
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$flashSale->userFavorites()->count()}}" class="@if($flashSale->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="flashSaleItem" data-id="{{$flashSale->id}}" >
                                    @if($flashSale->isFavourited()->count())
                                        <img src="/site/assets/images/favorited.png">
                                    @else
                                        <img src="/site/assets/images/favorite.png">
                                    @endif
                                </span>
                            </li>
                            <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="{{$flashSale->avg_rating}}">
                                <img src="/site/assets/images/star.png">
                            </span>
                            </li>
                        </ul>


                    </figure>
                    <div class="featured_flashsalebtn text-right">
                        <ul class="list-inline m-0">
                            <li class="pull-left">{{ number_format((100-(($flashSale->offer_price / ($flashSale->price??1))*100)),0) }}% OFF</li>
                            <li class=""><a href="{{route('flashsaleItem',$flashSale->id)}}" class="btn btn-primary btn-sm ">View Sale</a></li>
                        </ul>

                    </div>
                </div>
            </div>
            @endforeach
            @foreach($flashSalesYetToStart as $flashSale)
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="deal-single panel">
                    <figure class=" embed-responsive embed-responsive-16by9"
                            data-bg-img="/uploads/{{$flashSale->small_image}}">
                        <div class="ribbon-flahssale is-hidden-xs-down">
                            <div class="ribbon-flah" style="background: green;">Coming Soon</div>
                        </div>
                        <ul class="deal-actions top-15 right-15  v_shareParent" data-shareUrl="{{route('flashsaleItem',$flashSale->id)}}?ref={{getReferalId()}}" >
                            @include('site.includes.shareDropDownContents')

                            <li class="like-deal">
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$flashSale->userFavorites()->count()}}" class="@if($flashSale->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="flashSaleItem" data-id="{{$flashSale->id}}" >
                                    @if($flashSale->isFavourited()->count())
                                        <img src="/site/assets/images/favorited.png">
                                    @else
                                        <img src="/site/assets/images/favorite.png">
                                    @endif
                                </span>
                            </li>

                            <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="{{$flashSale->avg_rating}}">
                                <img src="/site/assets/images/star.png">
                            </span>
                            </li>
                        </ul>


                    </figure>
                    <div class="featured_flashsalebtn text-right">
                        <ul class="list-inline m-0">
                            <li class="pull-left">{{ number_format((100-(($flashSale->offer_price / ($flashSale->price??1))*100)),0) }}% OFF</li>
                            <li class=""><a href="{{route('flashsaleItem',$flashSale->id)}}" class="btn btn-primary btn-sm ">Coming Soon</a></li>
                        </ul>

                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </section>

    <section class="section latest-deals-area ptb-30">
        <header class="panel ptb-15 prl-20 pos-r mb-30">
            <h3 class="section-title font-18">Latest Deals</h3>
            <a href="{{route('dealFrontist')}}" class="btn btn-o btn-xs pos-a right-10 pos-tb-center">View All</a>
        </header>

        <div class="row row-masnory row-tb-20">
            @foreach($deals as $deal)
            <div class="col-sm-6 col-lg-4">
                <div class="deal-single panel">
                    <figure class="embed-responsive embed-responsive-16by9"
                            data-bg-img="/uploads/{{$deal->small_image}}">
                        <div class="label-discount_primary left-20 top-15">{{ number_format((100-(($deal->offer_price / ($deal->price??1))*100)),0) }}%<br>OFF</div>
                        <ul class="deal-actions top-15 right-15 v_shareParent" data-shareUrl="{{route('dealItem',$deal->id)}}?ref={{getReferalId()}}" >
                            @include('site.includes.shareDropDownContents')
                            <li class="like-deal">
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$deal->userFavorites()->count()}}" class="@if($deal->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="dealItem" data-id="{{$deal->id}}" >
                                    @if($deal->isFavourited()->count())
                                        <img src="/site/assets/images/favorited.png">
                                    @else
                                        <img src="/site/assets/images/favorite.png">
                                    @endif
                                </span>
                            </li>
                            <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="{{$deal->avg_rating}}">
                                <img src="/site/assets/images/star.png">
                            </span>
                            </li>
                        </ul>
                        <div class="viewdeal"><a href="{{route('dealItem',$deal->id)}}">VIEW DEAL</a></div>
                    </figure>
                    <div class="bg-white pt-20 pl-20 pr-15">
                        <div class="time-left_dealdaa font-md-14 mb-10">
                                            <span>
                            <i class="ico fa fa-clock-o mr-5"></i>
                            <span class="t-uppercase" data-countdown="{{ date('Y/m/d H:i:s',strtotime($deal->end_time))}}"></span>
                        </span>
                        </div>
                        <div class="pr-md-10">
                            <h3 class="deal-title mb-10">
                                <a href="{{route('dealItem',$deal->id)}}">
                                    {{$deal->title}}
                                </a>
                            </h3>
                            <ul class="deal-meta list-unstyled mb-10 color-mid latest_deal_location">
                                <li><i class="ico lnr lnr-map-marker mr-10"></i>{{$deal->getOwner->address}}</li>
                                <li><i class="ico lnr lnr-store mr-10"></i>{{$deal->getOwner->name}}</li>
                            </ul>
                        </div>
                        <div class="deal-price pos-r mb-15">
                            <ul class="list-inline m-0">
                                @if($deal->dealType != 'voucher')
                                    <li>
                                        <h3 class="price ptb-5 text-left price-sale">
                                            <i class="fa fa-rupee"></i> {{$deal->price}}
                                        </h3>
                                    </li>
                                @endif
                                <li>
                                    <h3 class="price ptb-5 text-left">
                                        <i class="fa fa-rupee"></i> {{$deal->online_sell_price}}
                                    </h3>
                                </li>
                                <li class="float-right" data-toggle="tooltip" data-placement="left" title="Add to Cart"><a class="btn btn-primary deal_addcart"  data-id="{{$deal->id}}" data-type="deal" ><i class="lnr lnr-cart"></i></a> </li>
                            </ul>


                        </div>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </section>
    <section class="section latest-coupons-area ptb-30">
        <header class="panel ptb-15 prl-20 pos-r mb-30">
            <h3 class="section-title font-18">Cashback Coupons</h3>
            <a class="btn btn-o btn-xs pos-a right-10 pos-tb-center">View All</a>
        </header>

        <div class="latest-coupons-slider owl-slider" data-autoplay-hover-pause="true" data-loop="true"
             data-autoplay="true" data-smart-speed="1000" data-autoplay-timeout="10000" data-margin="30"
             data-nav-speed="false" data-items="1" data-xxs-items="1" data-xs-items="2" data-sm-items="2"
             data-md-items="3" data-lg-items="4">
            @foreach($coupons as $deal)
            <div class="coupon-item">
                <div class="coupon-single panel t-center">
                    @if($deal->isFeatured)
                        <div class="ribbon-wrapper is-hidden-xs-down">
                            <div class="ribbon">Featured</div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center p-20">
                                <img class="store-logo" src="/uploads/{{$deal->small_image}}" alt="">
                            </div>
                            <!-- end media -->
                            <ul class="deal-actions left-0 right-0 cashback_share v_shareParent" data-shareUrl="{{route('couponItem',$deal->id)}}?ref={{getReferalId()}}" >
                                @include('site.includes.shareDropDownContents')
                                <li class="like-deal">
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$deal->userFavorites()->count()}}" class="@if($deal->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="dealItem" data-id="{{$deal->id}}" >
                                    @if($deal->isFavourited()->count())
                                        <img src="/site/assets/images/favorited.png">
                                    @else
                                        <img src="/site/assets/images/favorite.png">
                                    @endif
                                </span>
                                </li>

                                <li class="like-deal">
                                        <span data-toggle="tooltip" data-placement="bottom" title="{{$deal->avg_rating}}">
                                <img src="/site/assets/images/star.png">
                            </span>
                                </li>
                            </ul>
                        </div>
                        <!-- end col -->

                        <div class="col-xs-12">
                            <div class="panel-body">
                                <h5 class="deal-title mb-10 nowrap">
                                    <a href="#">{{$deal->title}}</a>
                                </h5>
                                <h4 class="text-success mb-10 font-12 nowrap">{{$deal->small_description}}</h4>

                                <ul class="deal-meta list-unstyled mb-10 color-mid latest_deal_location">
                                    <li><i class="ico lnr lnr-map-marker mr-10"></i>{{$deal->getOwner->address}}</li>
                                    <li><i class="ico lnr lnr-store mr-10"></i>{{$deal->getOwner->name}}</li>
                                </ul>
                                <div class="time-left_dealdaa font-md-12 color-green mb-10">
                                            <span>
                            <i class="ico fa fa-clock-o mr-5"></i>
                            <span class="t-uppercase" data-countdown="{{ date('Y/m/d H:i:s',strtotime($deal->end_time))}}"></span>
                        </span>
                                </div>
                                <a href="{{route('couponItem',$deal->id)}}" class="btn btn-sm btn-block">Get Coupon
                                </a>
                            </div>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
            </div>
            @endforeach

        </div>
    </section>

    @if(count($topPics))
        <section class="section latest-deals-area ptb-30">
            <header class="panel ptb-15 prl-20 pos-r mb-30">
                <h3 class="section-title font-18">Top Picks Deals</h3>
                <a href="{{route('deallistDealsCategory','top-picks')}}" class="btn btn-o btn-xs pos-a right-10 pos-tb-center">View All</a>
            </header>

            <div class="row row-masnory row-tb-20">
                @foreach($topPics as $deal)
                    <div class="col-sm-6 col-lg-4">
                        <div class="deal-single panel">
                            <figure class="embed-responsive embed-responsive-16by9"
                                    data-bg-img="/uploads/{{$deal->small_image}}">
                                <div class="label-discount_primary left-20 top-15">{{ number_format((100-(($deal->offer_price / ($deal->price??1))*100)),0) }}%<br>OFF</div>
                                <ul class="deal-actions top-15 right-15 v_shareParent" data-shareUrl="{{route('dealItem',$deal->id)}}?ref={{getReferalId()}}" >
                                    @include('site.includes.shareDropDownContents')
                                    <li class="like-deal">
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$deal->userFavorites()->count()}}" class="@if($deal->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="dealItem" data-id="{{$deal->id}}" >
                                    @if($deal->isFavourited()->count())
                                        <img src="/site/assets/images/favorited.png">
                                    @else
                                        <img src="/site/assets/images/favorite.png">
                                    @endif
                                </span>
                                    </li>
                                    <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="3.5">
                                <img src="/site/assets/images/star.png">
                            </span>
                                    </li>
                                </ul>
                                <div class="viewdeal"><a href="{{route('dealItem',$deal->id)}}">VIEW DEAL</a></div>
                            </figure>
                            <div class="bg-white pt-20 pl-20 pr-15">
                                <div class="time-left_dealdaa font-md-14 mb-10">
                                            <span>
                            <i class="ico fa fa-clock-o mr-5"></i>
                            <span class="t-uppercase" data-countdown="{{ date('Y/m/d H:i:s',strtotime($deal->end_time))}}"></span>
                        </span>
                                </div>
                                <div class="pr-md-10">
                                    <h3 class="deal-title mb-10">
                                        <a href="{{route('couponItem',$deal->id)}}">
                                            {{$deal->title}}
                                        </a>
                                    </h3>
                                    <ul class="deal-meta list-unstyled mb-10 color-mid latest_deal_location">
                                        <li><i class="ico lnr lnr-map-marker mr-10"></i>{{$deal->getOwner->address}}</li>
                                        <li><i class="ico lnr lnr-store mr-10"></i>{{$deal->getOwner->name}}</li>
                                    </ul>
                                </div>
                                <div class="deal-price pos-r mb-15">
                                    <ul class="list-inline m-0">
                                        @if($deal->dealType != 'voucher')
                                        <li>
                                            <h3 class="price ptb-5 text-left price-sale">
                                                <i class="fa fa-rupee"></i> {{$deal->price}}
                                            </h3>
                                        </li>
                                        @endif
                                        <li>
                                            <h3 class="price ptb-5 text-left">
                                                <i class="fa fa-rupee"></i> {{$deal->online_sell_price}}
                                            </h3>
                                        </li>
                                        <li class="float-right" data-toggle="tooltip" data-placement="left" title="Add to Cart"><a class="btn btn-primary deal_addcart"  data-id="{{$deal->id}}" data-type="deal" ><i class="lnr lnr-cart"></i></a> </li>
                                    </ul>


                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    @endif


    @if(!$winners->isEmpty())
    <section class="section stores-area stores-area-v1 ptb-30">
        <header class="panel ptb-15 prl-20 pos-r mb-30">
            <h3 class="section-title font-18">LUCKY DRAW WINNERS</h3>

        </header>
        <div class="popular-stores-slider owl-slider" data-loop="true" data-autoplay="false"
             data-smart-speed="1000" data-autoplay-timeout="10000" data-margin="20" data-items="2"
             data-xxs-items="2" data-xs-items="2" data-sm-items="3" data-md-items="4" data-lg-items="5">

            @foreach($winners as $winner)
            <div class="store-item t-center">
                <a href="#" class="panel is-block">
                    <div class="embed-responsive embed-responsive-4by3">
                        <div class="store-logo">
                            <img src="/uploads/{{$winner->image_url}}" alt="">
                        </div>
                    </div>
                    <h6 class="store-name pt-10 pb-0">{{$winner->name}}</h6>
                    <p class="luckywinner_des font-10 pb-0 text-muted pl-10 pr-10">{{$winner->description}}}</p>
                </a>
            </div>
            @endforeach
        </div>
    </section>
    @endif
@stop
