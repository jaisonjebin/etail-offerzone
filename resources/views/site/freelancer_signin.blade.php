@extends('site.layout.master')


@section('title', 'Freelancer SignIn')

@push('style')

@endpush

@push('scripts')

@endpush

@section('content')
    <section class="sign-area panel p-40">
        <h3 class="sign-title">Freelancer Login</h3>
        <div class="row row-rl-0">
            <div class="col-sm-12 col-md-7 col-md-offset-2">
                @foreach ($errors->all() as $message)
                    <div class="alert alert-danger">{{$message}}</div>
                @endforeach
                <form class="p-40" action="{{route('admin.loginPost')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label class="sr-only">Email</label>
                        <input type="text" class="form-control input-lg" placeholder="Email" name="email"  value="{{old('email')}}">
                    </div>
                    <div class="form-group">
                        <label class="sr-only">Password</label>
                        <input type="password" class="form-control input-lg" placeholder="Password" name="password">
                    </div>
                    <div class="form-group">
                    <a href="{{ route('passwordReset.view') }}" class="forgot-pass-link color-green">Forget Password ?</a>
                    </div>
                    <div class="custom-checkbox mb-20">
                        <input type="checkbox" name="remember" value="true" id="remember_account" checked>
                        <label class="color-mid" for="remember_account">Keep me signed in on this computer.</label>
                    </div>
                    <button type="submit" class="btn btn-block btn-lg">Login</button>
                </form>
            </div>
        </div>
    </section>
@stop