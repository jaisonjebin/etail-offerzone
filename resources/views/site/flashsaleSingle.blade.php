@extends('site.layout.master')


@section('title', 'Flash Sale')

@push('style')
    <!-- Owl Carousel -->
    <link href="/site/assets/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="/site/assets/vendors/owl-carousel/owl.theme.min.css" rel="stylesheet">
    <!-- Flex Slider -->
    <link href="/site/assets/vendors/flexslider/flexslider.css" rel="stylesheet">
    <link href="/site/assets/css/counter.css" rel="stylesheet">
    <link href="/site/assets/css/rating.css" rel="stylesheet">
@endpush
@push('scripts')
    <!-- Owl Carousel -->
    <script type="text/javascript" src="/site/assets/vendors/owl-carousel/owl.carousel.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="/site/assets/vendors/flexslider/jquery.flexslider-min.js"></script>

    <!-- Coutdown -->
    <script type="text/javascript" src="/site/assets/vendors/countdown/jquery.countdown.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js'></script>
    <script src="/site/assets/js/readmore.js"></script>
    <script>
        $(".read-more").readMore({previewHeight: 200});
    </script>
    <script>

        $(document).ready(function () {
            var current = new Date();

            ;['Date', 'Hours', 'Minutes'].forEach(function (val, i) {
                current['set' + val](current['get' + val]() + (i + 1) * 3);
            });


            $('#count-down')
                .attr('datetime', current) // updates the date every time the page loads this is for the example only
                .countdown({
                    // wordFormat: "full", // uncomment this for full words
                    numberFormat: true
                }, function () {
                    // Replace this alert function with your callback function. AKA what you want to happen after the time is up.
                    alert('Alright times up Leroy Jenkins!!!');
                });
        });


        // Below is the code for the countdown plugin. This code should be saved in a seperate document and linked to the page that it is being used on.

        /* Author : Tyler Benton
           Date : 05/04/12
           jQuery : 1.7.2, works with 2.0.3
           Purpose : light weight countdown timer
        */
        ;(function ($) {
            $.fn.countdown = function (options, callback) {
                var obj = this;
                var $obj = $(obj);
                var settings = {
                    numberFormat: null
                };
                var interval = '';
                if (options) {
                    $.extend(settings, options);
                }

                function init() {
                    var newObj = '<ul><li><div class="days"></div></li><li><div class="hours"></div></li><li><div class="minutes"></div></li><li><div class="seconds"></div></li></ul>';
                    $obj
                        .append(newObj);
                    countdown_process();
                }

                function countdown_process() {
                    eventDate = new Date($('#count-down').attr('datetime'));
                    eventDate = Date.parse(eventDate) / 1000;
                    currentDate = Math.floor($.now() / 1000);
                    seconds = eventDate - currentDate;
                    $days = $obj.find('.days');
                    $hours = $obj.find('.hours');
                    $minutes = $obj.find('.minutes');
                    $seconds = $obj.find('.seconds');
                    days = Math.floor(seconds / (24 * 60 * 60));
                    seconds -= days * 24 * 60 * 60;
                    hours = Math.floor(seconds / (60 * 60));
                    seconds -= hours * 60 * 60;
                    minutes = Math.floor(seconds / 60);
                    seconds -= minutes * 60;
                    if (eventDate <= currentDate) {
                        callback.call(obj);
                        clearInterval(interval);
                        days = 0;
                        seconds = 0;
                        hours = 0;
                        minutes = 0;
                        seconds = 0;
                    }
                    if (settings.wordFormat === 'full') {
                        days === 1 ? $days.attr('data-interval-text', 'day') : $days.attr('data-interval-text', 'days');
                        hours === 1 ? $hours.attr('data-interval-text', 'hour') : $hours.attr('data-interval-text', 'hours');
                        minutes === 1 ? $minutes.attr('data-interval-text', 'minute') : $minutes.attr('data-interval-text', 'minutes');
                        seconds === 1 ? $seconds.attr('data-interval-text', 'second') : $seconds.attr('data-interval-text', 'seconds');
                    } else {
                        days === 1 ? $days.attr('data-interval-text', 'day') : $days.attr('data-interval-text', 'days');
                        hours === 1 ? $hours.attr('data-interval-text', 'hr') : $hours.attr('data-interval-text', 'hrs');
                        minutes === 1 ? $minutes.attr('data-interval-text', 'min') : $minutes.attr('data-interval-text', 'mins');
                        seconds === 1 ? $seconds.attr('data-interval-text', 'sec') : $seconds.attr('data-interval-text', 'secs');
                    }

                    if (settings.numberFormat) {
                        days = String(days).length >= 2 ? days : '0' + days;
                        hours = String(hours).length >= 2 ? hours : '0' + hours;
                        minutes = String(minutes).length >= 2 ? minutes : '0' + minutes;
                        seconds = String(seconds).length >= 2 ? seconds : '0' + seconds;
                    }

                    if (!isNaN(eventDate)) {
                        $days.text(days);
                        $hours.text(hours);
                        $minutes.text(minutes);
                        $seconds.text(seconds);
                    } else {
                        console.log('Invalid date. Here\'s an example: 16 may 2012 11:59:59');
                        clearInterval(interval);
                    }
                }

                init();
                interval = setInterval(countdown_process, 1000);
            };
        })(jQuery); // end plugin

        // $("#your-selector")
        //  .countdown({
        //   date: "6 january 2014 00:00:00",
        //   format: "on",
        //  },function(){
        //   Your call back function
        //  });
    </script>
    <script>
        $(document).ready(function() {
            $('.bar span').hide();

            $('.vv-bar-animate').each(function () {
                $percentage = $(this).data('percentage');
                $(this).animate({
                    width: $percentage+'%'}, 1000);
            });

            setTimeout(function() {
                $('.bar span').fadeIn('slow');
            }, 1000);

        });
    </script>
@endpush
@section('content')
    <div class="section deals-header-area pb-30">
        <div class="row row-tb-20">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="header-deals-slider owl-slider">

                    @if($flashSale->banners)
                        @php
                            $banners = json_decode($flashSale->banners);
                        @endphp
                        @foreach($banners as $banner)
                            <div class="deal-single panel item">
                                <figure class="flashsale-thumbnail" data-bg-img="/uploads/{{$banner}}">
                                </figure>
                            </div>
                            @break
                        @endforeach
                    @endif


                </div>

            </div>
        </div>
    </div>

    <div class="row row-rl-10 row-tb-20">
        <div class="page-content col-xs-12 col-sm-12 col-md-8">
            <div class="row row-tb-20">
                <div class="col-xs-12">
                    <div class="deal-deatails panel">
                        <div class="deal-body p-20">
                            <h3 class="mb-10">{{$flashSale->title}}</h3>
                            <div class="rating mb-10">
                                   <span class="rating-stars" data-rating="3">
				                        <i class="fa fa-star-o"></i>
				                        <i class="fa fa-star-o"></i>
				                        <i class="fa fa-star-o"></i>
				                        <i class="fa fa-star-o"></i>
				                        <i class="fa fa-star-o"></i>
				                    </span>
                            </div>
                            <ul class="list-inline mb-15 price_rages">
                                <li>
                                    <h2 class="price">
                                        <span class="price-sale"><i class="fa fa-rupee"></i>{{$flashSale->price}}</span>
                                    </h2>
                                </li>
                                <li>
                                    <h2 class="price">
                                        <i class="fa fa-rupee"></i>{{$flashSale->offer_price}}
                                    </h2>
                                </li>
                                <li>
                                    <h2 class="price">
                                        <span class="price-save">{{ number_format((100-(($flashSale->offer_price / ($flashSale->price??1))*100)),0) }}% OFF</span>
                                    </h2>
                                </li>
                            </ul>

                            <div class="mb-15">
                                {!! $flashSale->description !!}
                            </div>

                            <h5 class="pt-15 mb-15">SPONSORED BY</h5>

                            @if($flashSale->sponsers)
                                @php
                                    $sponsers = json_decode($flashSale->sponsers);
                                @endphp
                                    @foreach($sponsers as $sponser)
                                        <div class="mb-15">
                                            <img src="/uploads/{{$sponser}}" class="img-responsive">
                                        </div>
                                    @endforeach
                            @endif

                            <h5 class="pt-15 mb-15">TERMS & CONDITIONS</h5>
                            <div class="flashsale_terms read-more">
                                <div>
                                    {!! $flashSale->termsConditions !!}
                                </div>
                                <p class="prompt">
                                    <a class="btn btn-danger" href="#">Read More</a>
                                </p>

                            </div>
                            <hr>
                            @if($flashSale->stock > 0)
                                @if($flashSale->yettoStart)
                                    <div class="timecounter text-center">
                                        <time id="count-down" datetime="{{$flashSale->start_time}}"></time>
                                    </div>

                                    <div class="notify_btn text-center">

                                    @if(Session::has('type'))
                                        <!-- {{Session::get('type')}} -->
                                        @endif
                                        <form action="{{route('flashSale.notifyme',$flashSale->id)}}" method="post">
                                            @csrf
                                            <input type="text" name="email" class="form-control mb-15 @if(Auth::user()->email) hidden @endif" placeholder="Enter Your Email" value="{{Auth::user()->email}}">
                                            <input type="text" name="phone" class="form-control mb-15 @if(Auth::user()->phone) hidden @endif" placeholder="Enter Your Phone" value="{{Auth::user()->phone}}">
                                            <button class="btn btn-danger" type="submit">NOTIFY ME</button>
                                        </form>

                                    </div>
                                @else

                                    <div class="bynow_btn text-center pt-15">
                                        <h4 class="">Sale Open</h4>
                                        <p class="small pb-10">Buy now before the sale time runs out!</p>
                                        <a href="{{route('flashsaleItemcheckout',$flashSale->id)}}" class="btn btn-danger">BUY NOW</a>
                                        <p class="small pt-20">By clicking BUY NOW button, you are accepting all related
                                            terms and conditions</p>
                                    </div>
                                @endif

                            @else
                                <div class="row justify-content-center  text-center">
                                    <label class=" alert alert-danger">
                                        Sorry, This product is currently out of stock
                                    </label>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
                @php
                    $product_id    =   $flashSale->id;
                    $product_type    =   'flashSale';
                    $productReviews    =   $flashSale->getReviews()->get();
                @endphp
                @include('site.includes.writeReviewSection')
                @include('site.includes.reviewsView')


            </div>
        </div>
        <div class="page-sidebar col-md-4 col-sm-12 col-xs-12">
            <!-- Blog Sidebar -->
            <aside class="sidebar blog-sidebar">
                <div class="row row-tb-10">
                    <div class="col-xs-12">
                        <div class="widget single-deal-widget panel ptb-30 prl-20">
                            <div class="widget-body text-center">

                                @if($flashSale->stock > 0)
                                    @if($flashSale->yettoStart)

                                        <img src="/site/assets/images/flashsale-notify.jpg"
                                             class="img-responsive mb-10 mrl-auto">

                                        <hr>
                                        <div class="notify_btn text-center">
                                            @if(Session::has('type'))
                                            <!-- {{Session::get('type')}} -->
                                            @endif
                                            <form action="{{route('flashSale.notifyme',$flashSale->id)}}" method="post">
                                                @csrf
                                                <input type="text" name="email" class="form-control mb-15 @if(Auth::user()->email) hidden @endif" placeholder="Enter Your Email" value="{{Auth::user()->email}}">
                                                <input type="text" name="phone" class="form-control mb-15 @if(Auth::user()->phone) hidden @endif" placeholder="Enter Your Phone" value="{{Auth::user()->phone}}">
                                            <button class="btn btn-danger" type="submit">NOTIFY ME</button>
                                            </form>
                                        </div>

                                    @else

                                        <img src="/site/assets/images/notify_modal.svg" class="img-responsive" style="max-width: 60px; margin: 0 auto;">

                                        <hr>
                                        <div class="bynow_btn text-center pt-15">
                                            <h4 class="">Sale Open</h4>
                                            <p class="small pb-10">Buy now before the sale time runs out!</p>
                                            <a href="{{route('flashsaleItemcheckout',$flashSale->id)}}" class="btn btn-danger">BUY NOW</a>
                                            <p class="small pt-20">By clicking BUY NOW button, you are accepting all related
                                                terms and conditions</p>
                                        </div>
                                    @endif
                                @else
                                    <div class="row justify-content-center">
                                        <label class=" alert alert-danger">
                                            Sorry, This product is currently out of stock
                                        </label>
                                    </div>
                                @endif


                                <hr>
                                <div class="text-center pb-30">
                                    <span >
                                        <a class="@if($flashSale->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="flashSaleItem" data-id="{{$flashSale->id}}">
                                            @if($flashSale->isFavourited()->count())
                                                <img src="/site/assets/images/favorited.png" class="mr-5">
                                            @else
                                                <img src="/site/assets/images/favorite_black.png" class="mr-5">
                                            @endif
                                        </a>

                                        {{$flashSale->userFavorites()->count()}} People Favorited
                                    </span>
                                </div>

                                <ul class="list-inline social-icons social-icons--colored t-center v_shareParent" data-shareUrl="{{route('flashsaleItem',$flashSale->id)}}?ref={{getReferalId()}}">
                                    <li class="social-icons__item">
                                        <a href="#" class="v_sharebtn" title="Facebook" data-type="Facebook"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li class="social-icons__item">
                                        <a href="#" class="v_sharebtn" title="Twitter" data-type="Twitter"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li class="social-icons__item">
                                        <a href="#" class="v_sharebtn" title="Google Plus" data-type="GooglePlus"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li class="social-icons__item">
                                        <a href="#" class="v_sharebtn" title="Whatsapp" data-type="Whatsapp"><i class="fa fa-whatsapp"></i></a>
                                    </li>
                                </ul>


                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <!-- Latest Deals Widegt -->
                        <div class="widget latest-deals-widget panel prl-20">
                            <div class="widget-body ptb-20">
                                <style>
                                    .vvStyle img {
                                        width: auto !important;
                                        max-width: 100%;
                                        margin: 6px;
                                    }
                                </style>
                                <div class="owl-slider" data-loop="true" data-autoplay="true"
                                     data-autoplay-timeout="10000" data-smart-speed="1000"
                                     data-nav-speed="false" data-nav="true" data-xxs-items="1"
                                     data-xxs-nav="true" data-xs-items="1" data-xs-nav="true" data-sm-items="1"
                                     data-sm-nav="true" data-md-items="1" data-md-nav="true" data-lg-items="1"
                                     data-lg-nav="true">
                                    @foreach($flashSales as $eflashSale)
                                    <div class="latest-deals__item item">
                                        <figure class="deal-thumbnail embed-responsive embed-responsive-4by3"
                                                data-bg-img="/uploads/{{$eflashSale->small_image}}">
                                            <div class="label-discount top-10 right-10">-{{ number_format((100-(($eflashSale->offer_price / ($eflashSale->price??1))*100)),0) }}%</div>
                                            <ul class="deal-actions top-10 left-10 vvStyle">
                                                @include('site.includes.shareDropDownContents')

                                                <li class="like-deal v_shareParent" data-shareUrl="{{route('flashsaleItem',$eflashSale->id)}}?ref={{getReferalId()}}">
                                                    <span data-toggle="tooltip" data-placement="bottom" title="{{$eflashSale->userFavorites()->count()}}" class="@if($eflashSale->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="flashSaleItem" data-id="{{$eflashSale->id}}" >
                                                        @if($eflashSale->isFavourited()->count())
                                                            <img src="/site/assets/images/favorited.png">
                                                        @else
                                                            <img src="/site/assets/images/favorite.png">
                                                        @endif
                                                    </span>
                                                </li>
                                            </ul>
                                            <div class="deal-about p-10 pos-a bottom-0 left-0">
                                                <div class="rating mb-10">
                                                                    <span class="rating-stars rate-allow"
                                                                          data-rating="5">
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </span>
                                                    <span class="rating-reviews color-lighter">
		                    	(<span class="rating-count">0</span> Reviews)
                                                                    </span>
                                                </div>
                                                <h5 class="deal-title mb-10">
                                                    <a href="{{route('flashsaleItem',$eflashSale->id)}}" class="color-lighter">{{$eflashSale->title}}</a>
                                                </h5>
                                            </div>
                                        </figure>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                        <!-- End Latest Deals Widegt -->
                    </div>
                    <div class="col-xs-12">
                        <!-- Best Rated Deals -->
                        <div class="widget best-rated-deals panel pt-20 prl-20">
                            <h3 class="widget-title h-title">Best Rated Deals</h3>
                            <div class="widget-body ptb-30">

                                @foreach($topPics as $deal)
                                     <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object" src="/uploads/{{$deal->small_image}}"
                                                 alt="Thumb" width="80">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="mb-5">
                                            <a href="{{route('dealItem',$deal->id)}}"> {{$deal->title}}</a>
                                        </h6>
                                        <div class="mb-5">
                                            <span class="rating">
                                            <span class="rating-stars" data-rating="4">
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </span>
                                            </span>
                                            </div>
                                        <h4 class="price font-16">{{$deal->price}} <span
                                                    class="price-sale color-muted"> {{$deal->offer_price}}</span>
                                        </h4>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                        <!-- Best Rated Deals -->
                    </div>

                </div>
            </aside>
            <!-- End Blog Sidebar -->
        </div>
    </div>

    <!--notify modal-->

    <div class="modal fade notifymodal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body text-center p-20">
                    <img src="/site/assets/images/notify_modal.svg" class="img-responsive">
                    <h5 class="pb-20"><strong>Good to know that you are interested in this!</strong><br>
                        We will notify you before the sale is open!</h5>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!--end notify modal-->
@stop