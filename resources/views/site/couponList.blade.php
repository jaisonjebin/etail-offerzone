@extends('site.layout.master')


@section('title', 'Coupons')

@push('style')
    <!-- Owl Carousel -->
    <link href="/site/assets/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="/site/assets/vendors/owl-carousel/owl.theme.min.css" rel="stylesheet">
    <!-- Flex Slider -->
    <link href="/site/assets/vendors/flexslider/flexslider.css" rel="stylesheet">
@endpush

@push('scripts')
    <!-- Owl Carousel -->
    <script type="text/javascript" src="/site/assets/vendors/owl-carousel/owl.carousel.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="/site/assets/vendors/flexslider/jquery.flexslider-min.js"></script>

    <!-- Coutdown -->
    <script type="text/javascript" src="/site/assets/vendors/countdown/jquery.countdown.js"></script>


    <script>
        $(function () {
            loadCoupons();
        });
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() > $(document).height() - 850) {
                loadCoupons();
            }
        });

        $genesis = 0;
        $page = 1;
        $ajaxPrssing = 0;
        function loadCoupons() {
            if ($genesis == 0 && $ajaxPrssing==0){
                $ajaxPrssing=1;
                $('.ajaxLoaderAni').slideDown();
                $.ajax({
                    url: '{{route('loadCouponsAjax',[''])}}/' + ($page++),
                    type: 'post',
                    success: function (data) {
                        $('.ajaxLoaderAni').slideUp();
                        $ajaxPrssing = 0;
                        if (data == 'genisys') {
                            $genesis = 1;
                            if($page ==2 && $genesis == 1){
                                $('.couponSaleContainer').append('<div class="col-lg-12">Could not find any Deals on this Category</div>');
                            }
                        }
                        else{
                            $('.couponSaleContainer').append(data);
                            reInitPlugins();
                        }
                    }
                });
            }
        }


        function reInitPlugins() {
            $("[data-bg-img]").each(function() {
                var attr = $(this).attr('data-bg-img');
                if (typeof attr !== typeof undefined && attr !== false && attr !== "") {
                    $(this).css('background-image', 'url('+attr+')');
                }
            });

            var countdown_select = $("[data-countdown]");
            countdown_select.each(function(){
                $(this).countdown($(this).data('countdown'))
                    .on('update.countdown', function(e){
                        var format = '%H : %M : %S';
                        if (e.offset.totalDays > 0) {
                            format = '%d Day%!d '+format;
                        }
                        if (e.offset.weeks > 0) {
                            format = '%w Week%!w '+format;
                        }
                        $(this).html(e.strftime(format));
                    });
            }).on('finish.countdown', function(e){
                $(this).html('This offer ha expired!').addClass('disabled');
            });

            var share_action = $('.deal-actions .share-btn');
            share_action.on('click',function(){
                var share_icons = $(this).children('.share-tooltip');
                share_icons.toggleClass('in');
            });
        }
    </script>



@endpush

@section('content')
    <section class="section latest-coupons-area  coupon_page">
        <header class="panel ptb-15 prl-20 pos-r mb-30">
            <h3 class="section-title font-18">Latest Coupons </h3>
            <!--<span class="pos-a right-20 pos-tb-center">6 Deals</span>-->
        </header>

        <div class="latest-coupons-slider">
            <div class="row couponSaleContainer">

            </div>
        </div>


    </section>
@stop