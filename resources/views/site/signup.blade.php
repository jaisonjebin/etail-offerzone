@extends('site.layout.master')


@section('title', 'Sign Up')

@push('style')
<style>
   .select2.select2-container.select2-container--default
    {
       height: 40px !important;
    }
    .select2-selection.select2-selection--single
    {
        border: 2px solid #ddd !important;
        height: 40px !important;
        /* line-height: 24px !important; */
        background-image: url(../images/icons/select-arrow.png);
        background-repeat: no-repeat;
        background-position: right 12px center;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
    }
   .select2-container--default .select2-selection--single .select2-selection__rendered {
       color: #444;
       line-height: 36px !important;
   }
   .select2-container--default .select2-selection--single .select2-selection__arrow {
       height: 26px;
       position: absolute;
       top: 6px !important;
       right: 7px !important;
       width: 20px;
   }
   .select2-container .select2-selection--single .select2-selection__rendered {
       display: block;
       padding-left: 15px !important;
       padding-right: 20px;
       overflow: hidden;
       text-overflow: ellipsis;
       white-space: nowrap;
   }
   .select2-container--default .select2-selection--single {
       background-color: #fff;
       border: 1px solid #aaa;
       border-radius: 0px !important;
   }
</style>
@endpush

@push('scripts')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>

        $(document).ready(function(){
            getDistricts();
            $('.select2').select2();
        });

        $('.sentOtp').on('click',function(){

            if($(this).closest('form').find('[name="phone"]').val() ==''){
                $(this).closest('form').find('[name="phone"]').focus();
                $(this).closest('form').find('[name="phone"]').addClass('animated shake').one('webkitAnimationEnd` mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                    $(this).removeClass('animated shake');
                });
                return false;
            }

            $(this).html('Sending..');
            $(this).addClass('disabled');
            $.ajax({
                url:'{{route('sentOTP')}}',
                type:'post',
                context:$(this),
                data:{'phone': $(this).closest('form').find('[name="phone"]').val()},
                success:function(data){
                    if(data=='sent')
                    {
                        $('.alert-danger').remove();
                        $(this).html('Resent OTP(<span class="countdown">45</span>)');
                        $(this).closest('form').find('[name="phone"]').addClass('disabled');
                        $('.otp_number_div').show();
                    }
                    else
                    {
                        $('.alert-danger').remove();
                        $('.signup_main').prepend('<div class="alert alert-danger">'+data+'</div>');
                    }
                }
            })
        });

        $('.termsCondtions').on('click',function(){
            if($(this).is(':checked')){
                $(this).closest('form').find('.submit').removeClass('disabled');
            }
            else{

                $(this).closest('form').find('.submit').addClass('disabled');
            }
        });

        setInterval(function () {
            $('.countdown').each(function(){
                $(this).html( parseInt($(this).html())-1);
                if($(this).html()=='0'){
                    $(this).closest('a').html('Resent OTP').removeClass('disabled');
                }
            })

        },1000);
        function getDistricts()
        {
            $.ajax({
                type: "GET",
                url: '{{route("getDistricts","")}}/19',
                dataType:'json',
                success: function( data )
                {
                    $(".j_district_input").html('');
                    $(".j_district_input").append('<option>Select District</option>');

                    $.each(data,function(key, value)
                    {
                        option='';
                        option= '<option  value=' + value.id + '>' + value.name + '</option>';

                        $(".j_district_input").append(option);
                    });
                    // $('.j_district_input').select2();
                    // $('.j_district_input').val($('.j_district_input').attr('data-selected'));
                    // $('.j_district_input').trigger('change');
                }
            });
        }
        $('.j_district_input').on('change',function () {

            $district_id   =   $('.j_district_input').val();
            $.ajax({
                type: "GET",
                url: '{{route("getCities","")}}/'+$district_id+'/KL',
                dataType:'json',
                success: function( data )
                {
                    $('.j_cities_input').select2('destroy');
                    $(".j_cities_input").html('');
                    $(".j_cities_input").append('<option>Select City</option>');
                    $.each(data,function(key, value)
                    {
                        option='';
                        option= '<option  value=' + value.id + '>' + value.name + '</option>';

                        $(".j_cities_input").append(option);
                    });
                    $('.j_cities_input').select2();
                    $('.select2-container').css('width','100%');
                    $('.j_cities_input').val($('.j_cities_input').attr('data-selected'));
                }
            });
        });
    </script>
@endpush

@section('content')
    <section class="sign-area panel p-40">
        <h3 class="sign-title">Sign Up <small>Or <a href="{{route('signin')}}" class="color-green">Sign In</a></small></h3>
        <div class="row row-rl-0">
            <div class="col-sm-6 col-md-7 col-left">
                <div class="signup_main">
                    @if ($errors->any())

                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        {{--<script>--}}
                            {{--document.addEventListener("DOMContentLoaded", function(event) {--}}
                                {{--$('#loginmodal').modal('show');--}}
                            {{--});--}}
                        {{--</script>--}}

                    @endif
                    <form class="p-40" method="post" action="{{route('user.register')}}">
                        @csrf
                        <div class="form-group">
                            <label class="sr-only">Full Name</label>
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control input-lg" placeholder="Full Name" required>
                        </div>
                        <div class="form-group">
                            <label class="sr-only">Email Id</label>
                            <input type="email" name="email" value="{{ old('email') }}" class="form-control input-lg" placeholder="Email Id">
                        </div>
                        <div class="input-group form-group">
                            <label class="sr-only">Mobile No</label>
                            <input type="tel" name="phone" value="{{ old('phone') }}" class="form-control input-lg" placeholder="Mobile Number" required>
                            <span class="input-group-btn">
                                <a href="javascript:void(0);" class="btn btn-danger sentOtp btn-lg" type="button">Send OTP</a>
                            </span>
                        </div>
                        <div class="otp_number_div form-group">
                            <label class="sr-only">OTP</label>
                            <input type="number" name="otp" value="{{ old('otp') }}" class="form-control otp_number" placeholder="OTP">
                        </div>
                        <div class="form-group">
                            <label class="sr-only">District</label>
                            <select class="form-control w-100 j_district_input" name="district" required>
                                <option>Select District</option>
                            </select>
                            {{--<span class="input-group-btn">--}}
                                {{--<button class="btn btn-danger" type="button" title="Current Location"><i class="fa fa-map-marker"></i></button>--}}
                            {{--</span>--}}
                        </div>
                        <div class="form-group">
                            <label class="sr-only">City</label>
                            <select class="form-control w-100 j_cities_input select2" name="city" required>
                                <option>Select City</option>
                            </select>
                            {{--<span class="input-group-btn">--}}
                                {{--<button class="btn btn-danger" type="button" title="Current Location"><i class="fa fa-map-marker"></i></button>--}}
                            {{--</span>--}}
                        </div>
                        <div class="form-group">
                            <label class="sr-only">Password</label>
                            <input type="password" name="password" value="{{ old('password') }}" class="form-control input-lg" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <label class="sr-only">Confirm Password</label>
                            <input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" class="form-control input-lg" placeholder="Confirm Password" required>
                        </div>



                        <div class="custom-checkbox mb-20">
                            <input type="checkbox" id="agree_terms" name="agree_terms" required>
                            <label class="color-mid" for="agree_terms">I agree to the <a href="{{route('termsandconditions')}}" target="_blank" class="color-green">Terms of Use</a> and <a href="{{route('privacy_policy')}}" class="color-green">Privacy Statement</a>.</label>
                        </div>
                        <button type="submit" class="btn btn-block btn-lg signupmain_btn">Sign Up</button>
                    </form>
                </div>
                {{--<span class="or">Or</span>--}}
            </div>
            <div class="col-sm-6 col-md-5 col-right">
                <div class="social-login p-40">
                    <img src="/site/assets/images/signup.jpg">
                    {{--<div class="mb-20">--}}
                        {{--<button class="btn btn-lg btn-block btn-social btn-facebook"><i class="fa fa-facebook-square"></i>Sign Up with Facebook</button>--}}
                    {{--</div>--}}
                    {{--<div class="mb-20">--}}
                        {{--<button class="btn btn-lg btn-block btn-social btn-twitter"><i class="fa fa-twitter"></i>Sign Up with Twitter</button>--}}
                    {{--</div>--}}
                    {{--<div class="mb-20">--}}
                        {{--<button class="btn btn-lg btn-block btn-social btn-google-plus"><i class="fa fa-google-plus"></i>Sign Up with Google</button>--}}
                    {{--</div>--}}
                    {{--<div class="custom-checkbox mb-20">--}}
                        {{--<input type="checkbox" id="remember_social" checked>--}}
                        {{--<label class="color-mid" for="remember_social">Keep me signed in on this computer.</label>--}}
                    {{--</div>--}}
                    <div class="text-center color-mid pt-30">
                        <a href="{{route('signin')}}" class="btn btn-block btn-lg">Already have an Account ? Login</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
