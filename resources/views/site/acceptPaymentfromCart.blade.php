@extends('site.layout.master')


@section('title', 'Blank Page')

@push('style')

    <style>
        input.razorpay-payment-button {
            height: 34px;
            padding: 5px 15px;
            font-size: 12px;
            border-style: none;
            line-height: 24px;
            display: block;
            width: 100%;
            border-radius: 1px;
            text-transform: uppercase;
            letter-spacing: 1px;
            overflow: hidden;
            background-color: #da0000;
            color: #fff;
        }
    </style>
@endpush

@push('scripts')


    <!-- FlexSlider -->
    <script type="text/javascript" src="/site/assets/vendors/flexslider/jquery.flexslider-min.js"></script>

    <!-- Coutdown -->
    <script type="text/javascript" src="/site/assets/vendors/countdown/jquery.countdown.js"></script>

@endpush

@section('content')
    <div class="row row-rl-10 row-tb-20">
        <div class="page-content col-xs-12 col-sm-8 col-md-9">
            <section class="section coupons-area coupons-area-list">
                <div class="row row-masnory row-tb-20">

                    @php
                        $total = 0;
                        $dealsSent = json_decode($order->order_details);
                    @endphp

                    @if(isset($order->getDetails))
                        @foreach($order->getDetails as $items)
                        @if($items->product_type != 'flashsale')
                            @php
                                $item = $items->getDeal;
                            @endphp
                            <div class="col-xs-12">
                            <div class="coupon-single panel t-center t-sm-left">
                            <div class="row row-sm-cell row-tb-0 row-rl-0">
                                <div class="col-sm-5 order_summary_img">
                                    <figure class="p-15">
                                        <img class="store-logo" src="/uploads/{{$item->small_image}}" alt="">
                                    </figure>
                                </div>
                                <!-- end col -->
                                <div class="col-sm-7">
                                    <div class="panel-body">
                                        <h4>{{$item->title}}</h4>

                                        <ul class="deal-meta list-unstyled mb-10 color-mid">
                                            <li><i class="ico lnr lnr-store mr-10"></i>
                                                <a href="#" class="color-mid">{{$item->small_description}}</a>
                                            </li>
                                        </ul>
                                        <ul class="deal-meta list-inline mb-10 color-dark">
                                            <li class="pull-right">
                                                <div class="quantity">
                                                    <label class="label label-info">{{$items->quatity}}</label>
                                                </div>

                                            </li>
                                        </ul>
                                        <ul class="list-inline mb-15 price_rages pt-15">
                                            <li>
                                                <h4 class="price">
                                                    <span class="price-sale"><i class="fa fa-rupee"></i>{{$item->price}}</span>
                                                </h4>
                                                Orginal Price
                                            </li>
                                            <li>
                                                <h4 class="price">
                                                    <i class="fa fa-rupee"></i>{{$item->offer_price}}
                                                </h4>
                                                Offer Price
                                            </li>
                                            @if($item->offer_price != $item->online_sell_price)

                                                <li>
                                                    <h4 class="price">
                                                        <span class="price-save"><i class="fa fa-rupee"></i>{{$item->online_sell_price}}</span>
                                                    </h4>
                                                    Voucher Price
                                                </li>

                                            @else
                                            <li>
                                                <h4 class="price">
                                                    <span class="price-save"><i class="fa fa-rupee"></i>{{ (($item->price - $items->price)  * $items->quatity) }}</span>
                                                </h4>
                                                You Save
                                            </li>
                                            @endif
                                        </ul>

                                        <div>
                                        @if($item->dealType == 'coupon')
                                            @if($order->order_status == 1)
                                                @if($order->payment_status == 0)
                                                    @if($items->expiry < \Carbon\Carbon::now() && $items->coupon_used!=1)
                                                        COUPON EXPIRED
                                                    @else

                                                        <div class="showcode" data-toggle-class="coupon-showen" data-toggle-event="click" style="float: none; margin: 0 auto;">
                                                            <button class="show-code btn btn-sm btn-block" data-toggle="modal" data-target="#coupon_05">Show Code</button>
                                                            <div class="coupon-hide" style="color: black">{{$items->coupon_code}}</div>
                                                        </div>
                                                    @endif
                                                @else
                                                        <div class="showcode" data-toggle-class="coupon-showen" data-toggle-event="click" style="float: none; margin: 0 auto;">
                                                            <button class="show-code btn btn-sm btn-block" data-toggle="modal" data-target="#coupon_05">Show Code</button>
                                                            <div class="coupon-hide text-black-50" style="color: black">{{$items->coupon_code}}</div>
                                                        </div>
                                                @endif
                                            @endif
                                        @endif

                                        </div>
                                    </div>
                                </div>
                                <!-- end col -->
                            </div>
                            <!-- end row -->
                            </div>
                        </div>

                    @else
                                @php
                                    $item = $items->getFlashSale;
                                @endphp

                            <div class="col-xs-12">
                                <div class="coupon-single panel t-center t-sm-left">
                                    <div class="row row-sm-cell row-tb-0 row-rl-0">
                                        <div class="col-sm-5 order_summary_img">
                                            <figure class="p-15">
                                                <img class="store-logo" src="/uploads/{{$item->small_image}}" alt="">
                                            </figure>
                                        </div>
                                        <!-- end col -->
                                        <div class="col-sm-7">
                                            <div class="panel-body">
                                                <h4>{{$item->title}}</h4>

                                                <ul class="deal-meta list-unstyled mb-10 color-mid">
                                                    <li><i class="ico lnr lnr-store mr-10"></i>
                                                        <a href="#" class="color-mid">{{$item->small_description}}</a>
                                                    </li>
                                                </ul>
                                                <ul class="deal-meta list-inline mb-10 color-dark">
                                                    <li class="pull-right">
                                                        <div class="quantity">
                                                            <label class="label label-info">{{$items->quatity}}</label>
                                                        </div>

                                                    </li>
                                                </ul>
                                                <ul class="list-inline mb-15 price_rages pt-15">
                                                    <li>
                                                        <h4 class="price">
                                                            <span class="price-sale"><i class="fa fa-rupee"></i>{{$item->price}}</span>
                                                        </h4>
                                                        Original Price
                                                    </li>
                                                    <li>
                                                        <h4 class="price">
                                                            <i class="fa fa-rupee"></i>{{$item->offer_price}}
                                                        </h4>
                                                        Offer Price
                                                    </li>
                                                    <li>
                                                        <h4 class="price">
                                                            <span class="price-save"><i class="fa fa-rupee"></i>{{ ($item->price - $item->offer_price) * $items->quatity}}</span>
                                                        </h4>
                                                        You Save
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end col -->
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div>
                    @endif
                @endforeach

                    @endif


                </div>
            </section>
            <!-- End Checkout Area -->
        </div>
        <div class="page-sidebar col-xs-12 col-sm-4 col-md-3">
            <!-- Blog Sidebar -->
            <aside class="sidebar blog-sidebar">
                <div class="row row-tb-10">
                    <div class="col-xs-12">
                        <!-- Recent Posts -->
                        <div class="widget checkout-widget panel p-20">
                            <div class="widget-body">
                                <table class="table mb-15">
                                    <tbody>
                                    <tr>
                                        <td class="color-mid"><b>Delivery Address</b></td>
                                    </tr>
                                    <tr>
                                        <td class="color-mid" colspan="2">
                                            @php
                                                $address = json_decode($order->address_details);
                                            @endphp
                                            @if(isset($address->name))
                                                {{$address->name}}
                                            @else
                                                {{@$address->fullName}}<br>
                                                {{@$address->addr1}},
                                                {{@$address->addr2}}<br>
                                                {{@$address->city}}<br>
                                                Pin : {{@$address->pincode}}<br>
                                                Phone : {{@$address->mobileNumber}}<br>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr class="font-15">
                                        <td class="color-mid">Total @if($order->payment_status == 1) Payed @else Payable @endif</td>
                                        <td class="color-green"><i class="fa fa-rupee"></i> {{$order->order_total}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                @if($order->expiry > \Carbon\Carbon::now())

                                    @if($order->order_status != 1)
                                        <div class="col-lg-12 paynowbtnDeal">
                                            <form action="{{route('user.viewOrder',[$order->id])}}" method="POST">
                                            @csrf
                                            <!-- Note that the amount is in paise = 50 INR -->
                                                <script
                                                        src="https://checkout.razorpay.com/v1/checkout.js"
                                                        data-key="{{config('razorpay.key_id')}}"
                                                        data-order_id="{{$order->razorpay_order_id}}"
                                                        data-buttontext="Pay Now"
                                                        data-name="Etail Offers"
                                                        data-description="{{Auth::user()->name}}"
                                                        data-image="{{url('/uploads/etailIcon.png')}}"
                                                        data-prefill.name="{{Auth::user()->name}}"
                                                        data-prefill.contact="{{Auth::user()->phone}}"
                                                        data-theme.color="#da0000"
                                                ></script>
                                                <input type="hidden" value="Hidden Element" name="hidden">
                                            </form>
                                        </div>


                                        <div class="time-left_dealdaa font-md-14 mb-10 text-center">
                                        <span>
                                            <span class="t-uppercase" data-url="/" data-countdown="{{ date('Y/m/d H:i:s',strtotime($order->expiry))}}" data-result="Order Expired"></span>
                                        </span>
                                        </div>
                                    @else
                                        <div class="col-lg-12 text-success">
                                            <i class="fa fa-check-circle"></i> Order Completed.
                                            @if($order->payment_status == 1) Payment Successful @else COD @endif
                                        </div>
                                    @endif


                                @else
                                    @if($order->order_status != 1)
                                        <a class="btn btn-danger w-100">Order Expired</a>
                                    @else
                                        <div class="col-lg-12 text-success">
                                            <i class="fa fa-check-circle"></i> Order Completed. @if($order->order_status == 1) Payment Successful @endif
                                        </div>
                                    @endif
                                @endif

                            </div>
                        </div>
                        <!-- End Recent Posts -->
                    </div>
                </div>
            </aside>
            <!-- End Blog Sidebar -->
        </div>
    </div>
@stop