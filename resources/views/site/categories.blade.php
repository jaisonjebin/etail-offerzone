@extends('site.layout.master')


@section('title', 'Blank Page')

@push('style')

@endpush

@push('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
    <script>
        var options = {
            valueNames: [ 'category' ]
        };

        var userList = new List('allcategories', options);
    </script>
@endpush

@section('content')
    <section class="stores-area stores-area-v2" id="allcategories">
        <h3 class="mb-40 t-uppercase">View All Categories</h3>
        <div class="letters-toolbar p-10 panel mb-40">
            <span><input type="text" class="search form-control" placeholder="Search Category"> </span>
        </div>
        <div class="stores-cat panel mb-40 ">
            <ul class="row stores-cat-body list">
                @foreach($categories as $category)
                    <li class="col-sm-4"><a href="{{route('deallistDealsCategory',$category->id)}}" class="category"> {{$category->name}} ({{$category->get_deal_count}})</a></li>
                @endforeach
            </ul>
        </div>
    </section>
@stop