@extends('site.layout.master')


@section('title', 'Sign In')

@push('style')

@endpush

@push('scripts')

@endpush

@section('content')
    <section class="sign-area panel p-40">
        <h3 class="sign-title">Sign In <small>Or <a href="{{route('signup')}}" class="color-green">Sign Up</a></small></h3>
        <div class="row row-rl-0">
            <div class="col-sm-6 col-md-7 col-left">
                @foreach ($errors->all() as $message)
                    <div class="alert alert-danger">{{$message}}</div>
                @endforeach
                <form class="p-40" action="{{ route('login')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label class="sr-only">Email</label>
                        <input type="text" name="email" class="form-control input-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Enter Email or Mobile" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only">Password</label>
                        <input type="password" name="password" class="form-control input-lg {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <a href="{{ route('passwordReset.view') }}" class="forgot-pass-link color-green">Forget Password ?</a>
                    </div>
                    <div class="custom-checkbox mb-20">
                        <input type="checkbox" name="remember" value="true" id="remember_account" checked>
                        <label class="color-mid" for="remember_account">Keep me signed in on this computer.</label>
                    </div>
                    <button type="submit" class="btn btn-block btn-lg">Sign In</button>
                </form>
                {{--<span class="or">Or</span>--}}
            </div>
            <div class="col-sm-6 col-md-5 col-right">
                <div class="social-login p-40">
                    <img src="/site/assets/images/signup.jpg">
                    {{--<div class="mb-20">--}}
                        {{--<button class="btn btn-lg btn-block btn-social btn-facebook"><i class="fa fa-facebook-square"></i>Sign In with Facebook</button>--}}
                    {{--</div>--}}
                    {{--<div class="mb-20">--}}
                        {{--<button class="btn btn-lg btn-block btn-social btn-twitter"><i class="fa fa-twitter"></i>Sign In with Twitter</button>--}}
                    {{--</div>--}}
                    {{--<div class="mb-20">--}}
                        {{--<button class="btn btn-lg btn-block btn-social btn-google-plus"><i class="fa fa-google-plus"></i>Sign In with Google</button>--}}
                    {{--</div>--}}
                    {{--<div class="custom-checkbox mb-20">--}}
                        {{--<input type="checkbox" id="remember_social" checked>--}}
                        {{--<label class="color-mid" for="remember_social">Keep me signed in on this computer.</label>--}}
                    {{--</div>--}}
                    <div class="text-center color-mid">
                        <div class="text-center color-mid pt-30">
                            <a href="{{route('signup')}}" class="btn btn-block btn-lg">Create Account</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop