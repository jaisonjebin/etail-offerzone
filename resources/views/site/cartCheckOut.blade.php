@extends('site.layout.master')


@section('title', 'Select Delivery Address')

@push('style')
    <style>
        .isHidden {
            display: none; /* hide radio buttons */
        }

        .label_check {
            display: block;
            background-color: #fff;
            padding: 5px 10px;
            margin-bottom: 0;

        }
        .label_check:hover{cursor: pointer;}

        .radio:checked + .label_check {   /* target next sibling (+) label */
            background-color: #da0000;
            color: #fff;
        }
        .greenbtn{
             background: #00af4e !important;
         }
        .greenbtn:hover{
            border-color: #007b3d !important;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
//add_newaddress_btn
            $('[name=id]').val('0');
            $('.new_saved_address').hide(300);
            $('.add_newaddress_btn .btn').click(function () {
                $('.addDadress .form-control').val('');
                $('[name=id]').val('0');
                $('.add_newaddress').slideDown(300);
                $('.add_newaddress_btn').hide(300);
            });
        });
        $('.new_delry_addr_cancel').click(function () {
            $('.new_saved_address').slideUp();
            $('.addnew_address_btn').show();
        });

        $('.editAddrBtn').on('click', function () {
            $('.new_saved_address').slideDown();
            $(this).closest('.addessBlock').find('[data-edit]').each(function () {
                console.log($(this).html());
                $('.addDadress').find('[name=' + $(this).attr('data-edit') + ']').val($(this).html());
            });
        });
        $('.addnew_address_btn').click(function () {
            $('.new_saved_address').show();
            $('.addnew_address_btn').slideUp();
        });
        $('.deleteBtn').on('click',function (event) {
            event.preventDefault();
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    window.location =   $(this).attr('href');
                }
            });
        });
    </script>
@endpush

@section('content')
    <div class="row row-rl-10 row-tb-20">
        <div class="page-content col-xs-12 col-sm-8 col-md-9">
            <!-- Checkout Area -->
            <header class="pb-15 prl-0 mb-20">
                <h3 class="h-title font-18">Select Delivery Address
                    <a href="javascript:void(0);" class="btn btn-o btn-xs pos-a top-15 right-10 pos-tb-center addnew_address_btn"><i class="fa fa-plus"></i> Add New</a>

                </h3>

            </header>

            <section class="section checkout-area panel pt-20 pb-40 new_saved_address panel" style="display: none;">
                <form class="mb-30 addDadress panel-body" method="post" action="{{route('user.addDeliveryAddess')}}">
                    @csrf
                    <input type="hidden" value="0" name="id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Enter you First Name"  name="fullName"
                                       required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="text" class="form-control" placeholder="(XXX) - XXXX - XXX" maxlength="10" name="mobile"
                                       required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Address Line 1</label>
                                <input type="text" class="form-control" placeholder="Enter your Address" name="addr1"
                                       required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Address Line 2 <span class="color-mid">(Optional)</span>
                                </label>
                                <input type="text" class="form-control" placeholder="Enter your Address" name="addr2">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>City / Town</label>
                                <input type="text" class="form-control" placeholder="Enter City" name="city" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>District</label>
                                <input type="text" class="form-control" placeholder="Enter District" name="state" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Postal Code</label>
                                <input type="text" class="form-control" placeholder="Enter Postal Code" name="pincode" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Landmark</label>
                                <input type="text" class="form-control" placeholder="Enter Landmark" name="landmark">
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn">SAVE</button>
                        <button type="button" class="btn btn-o new_delry_addr_cancel">CANCEL</button>
                    </div>
                </form>
            </section>
            @if($dAddress)
                <div class="row mt-15">
                    @foreach($dAddress as $addr)
                        <div class="col-md-6">
                            <div class="col-md-12 mb-15 p-15 saved_address addessBlock panel">
                                <span style="display:none;" data-edit="id">{{$addr->id}}</span>
                                <h4 class="pb-10" data-edit="fullName">{{$addr->fullName}} </h4>
                                <span data-edit="mobile">{{$addr->mobileNumber}}</span>
                                <p><span data-edit="addr1">{{$addr->addr1}}</span><br>
                                    <span data-edit="addr2">{{$addr->addr2}}</span><br>
                                    <span data-edit="city">{{$addr->city}}</span>,
                                    <span data-edit="state">{{$addr->state}}</span>,
                                    <span data-edit="pincode">{{$addr->pincode}}</span><br>
                                    <span data-edit="landmark">{{$addr->landmark}}</span></p>
                                <ul class="list-inline pt-10">
                                    <li><a href="javascript:void(0);" class="addnew_address_btn editAddrBtn btn btn-info btn-o btn-xs">Edit</a> </li>
                                    <li><a class="btn btn-o btn-xs deleteBtn" href="{{route('user.deleteDeliveryAddress',$addr->id)}}">Delete</a> </li>
                                    @if(Route::currentRouteName() == 'flashsaleItemcheckout')
                                        <li class="pull-right"><a class="btn btn-success btn-xs btn-dark greenbtn" href="{{route('flashsaleOrder',[$flashSale->id,$addr->id])}}">Deliver Here</a> </li>
                                    @else
                                        <li class="pull-right"><a class="btn btn-success btn-xs btn-dark greenbtn" href="{{route('user.checkOutAddress',$addr->id)}}">Deliver Here</a> </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="col-md-12 ptb-15 mb-15">
                    <div class="text-center valign-middle pt-20">
                        <h6 class="title mb-15 t-uppercase">No Saved Address Found.</h6></div>
                </div>
            @endif

        </div>
        <div class="page-sidebar col-xs-12 col-sm-4 col-md-3">
            <!-- Blog Sidebar -->
            <aside class="sidebar blog-sidebar">
                <div class="row row-tb-10">
                    <div class="col-xs-12">
                        <!-- Recent Posts -->
                        <div class="widget checkout-widget panel p-20">
                            <div class="widget-body">
                                <table class="table mb-15">
                                    <tbody>
                                    <tr>
                                        <td class="color-mid" colspan="2"><b>Payment Summary</b></td>
                                    </tr>
                                    <tr>
                                        <td class="color-mid">No. of Items</td>
                                        <td>{{$paymentSummary['quantity']}}</td>
                                    </tr>
                                    <tr>
                                        <td class="color-mid">Total Payable</td>
                                        <td><i class="fa fa-rupee"></i>  {{$paymentSummary['total']}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- End Recent Posts -->
                    </div>
                </div>
            </aside>
            <!-- End Blog Sidebar -->
        </div>
    </div>
@stop