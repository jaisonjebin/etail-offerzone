@extends('site.layout.master')


@section('title', 'About Us')

@push('style')

@endpush

@push('scripts')

@endpush

@section('content')
    <section class="terms-area panel">
        <div class="ptb-30 prl-30">
            <h3 class="t-uppercase h-title mb-40">ABOUT</h3>
            <p class="mb-20">We are Dealdaa Digital Trading and Services Private Limited here to begin a
                revolution in the retail industry by providing a platform "etailoffer" where retail business
                owners can benefit from the latest innovations in the digital media and advertising with
                our creative ideas for effective customer targeting and marketing strategies. This is an
                e-tailing (retail e-commerce) platform, where touch and feel experience of retail shop will be
                combined with convenience of e-commerce. Users will benefit further with exclusive deals,
                offers, flash sales, lucky draws and much more.
            </p>
            <p class="mb-20"> We are fully dedicated to in depth analysis of market trends and customer
                habit fluctuations, whether it is product sales or service, and to ensure our solutions
                and marketing strategies help retailers in resolving issues such as targeting new customers
                locally and also growth of e-commerce. Our fully equipped office is located near Calicut
                International Airport Junction (Neettanimmal) and this is the place from where we
                will be piloting our full-fledged mobile application and website.</p>

        </div>
    </section>
@stop