@extends('site.layout.master')


@section('title', 'Deal')

@push('style')
    <!-- Owl Carousel -->
    <link href="/site/assets/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="/site/assets/vendors/owl-carousel/owl.theme.min.css" rel="stylesheet">
    <!-- Flex Slider -->
    <link href="/site/assets/vendors/flexslider/flexslider.css" rel="stylesheet">
    <link href="/site/assets/css/counter.css" rel="stylesheet">
    <link href="/site/assets/css/rating.css" rel="stylesheet">
@endpush

@push('scripts')
    <!-- Owl Carousel -->
    <script type="text/javascript" src="/site/assets/vendors/owl-carousel/owl.carousel.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="/site/assets/vendors/flexslider/jquery.flexslider-min.js"></script>

    <!-- Coutdown -->
    <script type="text/javascript" src="/site/assets/vendors/countdown/jquery.countdown.js"></script>
    <script type="text/javascript" src="/site/assets/js/quantity.js"></script>
    <script>
        $(document).ready(function() {
            $('.vv-bar-animate').each(function () {
                $percentage = $(this).data('percentage');
                $(this).animate({
                    width: $percentage+'%'}, 1000);
            });

            setTimeout(function() {
                $('.bar span').fadeIn('slow');
            }, 1000);

        });

    </script>
@endpush

@section('content')
    <div class="row row-rl-10 row-tb-20">
        <div class="page-content col-xs-12 col-sm-12 col-md-8">
            <div class="row row-tb-20">
                <div class="col-xs-12 deal_single_view">
                    <div class="deal-deatails panel">
                        <div class="deal-slider">
                            <div class="section deals-header-area">
                                <div class="row row-tb-20">
                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                        <div class="header-deals-slider owl-slider" data-loop="true" data-autoplay="true"
                                             data-autoplay-timeout="10000" data-smart-speed="1000" data-nav-speed="false"
                                             data-nav="true" data-xxs-items="1" data-xxs-nav="true" data-xs-items="1" data-xs-nav="true"
                                             data-sm-items="1" data-sm-nav="true" data-md-items="1" data-md-nav="true" data-lg-items="1"
                                             data-lg-nav="true">
                                            @if($singleDeal->banners)
                                                @php $banners = json_decode($singleDeal->banners) @endphp
                                            @else
                                                @php $banners = [] @endphp
                                            @endif

                                            @php $i = 1 @endphp
                                            @foreach($banners as $banner)
                                                <div class="deal-single panel item">
                                                    <figure class="dealsingle-thumbnail" data-bg-img="/uploads/{{$banner}}">
                                                    </figure>
                                                </div>
                                            @endforeach
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="label-discount_primary left-30 top-15">{{ number_format((100-(($singleDeal->offer_price / ($singleDeal->price??1))*100)),0) }}%<br>OFF</div>
                            <ul class="deal-actions top-15 right-20">
                                <li class="like-deal">
                                     <span data-toggle="tooltip" data-placement="bottom" title="{{$singleDeal->userFavorites()->count()}}" class="@if($singleDeal->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="dealItem" data-id="{{$singleDeal->id}}" >
                                        @if($singleDeal->isFavourited()->count())
                                             <img src="/site/assets/images/favorited.png">
                                         @else
                                             <img src="/site/assets/images/favorite.png">
                                         @endif
                                    </span>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="col-xs-12 deal_single_view">
                    <div class="deal-deatails panel">
                        <div class="deal-body p-20">
                            <h3 class="mb-10">{{$singleDeal->title}}</h3>
                            <div class="rating mb-15">
                                                <span class="rating-stars" data-rating="5">
				                        <i class="fa fa-star-o"></i>
				                        <i class="fa fa-star-o"></i>
				                        <i class="fa fa-star-o"></i>
				                        <i class="fa fa-star-o"></i>
				                        <i class="fa fa-star-o"></i>
				                    </span><!--(3)-->
                            </div>
                            <ul class="list-inline mb-15 price_rages">
                                <li>
                                    <h2 class="price">
                                        <span class="price-sale"><i class="fa fa-rupee"></i>{{$singleDeal->price}}</span>
                                    </h2>
                                    Orginal Price
                                </li>
                                <li>
                                    <h2 class="price">
                                        <i class="fa fa-rupee"></i>{{$singleDeal->offer_price}}
                                    </h2>
                                    Offer Price
                                </li>
                                <li>
                                    <h2 class="price">
                                        <span class="price-save"><i class="fa fa-rupee"></i>{{$singleDeal->price - $singleDeal->offer_price}}</span>
                                    </h2>
                                    You Save
                                </li>
                            </ul>
                            <div class="mb-15">
                                {!! $singleDeal->description !!}
                            </div>
                            <img src="/site/assets/images/how_it_work.jpg" class="img-responsive">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 deal_single_view">
                    <div class="deal-deatails panel">
                        <div class="deal-body p-20">
                            <h3 class="h-title mb-30">Terms & Conditions</h3>
                            <div class="flashsale_terms">
                                <div>
                                    {!! $singleDeal->termsConditions !!}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @php
                    $product_id    =   $singleDeal->id;
                    $product_type    =   'deal';
                    $productReviews    =   $singleDeal->getReviews()->get();
                @endphp
                @include('site.includes.writeReviewSection')
                @include('site.includes.reviewsView')

            </div>
        </div>
        <div class="page-sidebar col-md-4 col-sm-12 col-xs-12 pt-0">
            <!-- Blog Sidebar -->
            <aside class="sidebar blog-sidebar">
                <div class="row row-tb-10">
                    <div class="col-xs-12">
                        <div class="widget single-deal-widget panel ptb-30 prl-20">
                            <div class="widget-body text-left">
                                <p class="color-muted">
                                    {{$singleDeal->small_description}}
                                </p>
                                <ul class="deal-meta list-unstyled mb-10 color-mid nowrap">
                                    <li><i class="ico lnr lnr-store mr-10"></i>
                                        <a href="#" class="color-mid">{{$singleDeal->getOwner->name}}</a>
                                    </li>
                                    <li><i class="ico lnr lnr-map-marker mr-10"></i> {{$singleDeal->getOwner->address}}</li>
                                </ul>
                                <ul class="deal-meta list-inline mb-10 color-dark">
                                    <li><i class="ico fa lnr lnr-tag mr-10"></i>{{$singleDeal->sold}} Bought</li>
                                </ul>
                                <div class="price mb-20 text-center pt-15">
                                    <h5 class="color-green">
                                        <strong>
                                            @if(Route::currentRouteName() == 'couponItem')
                                                Coupon Price :
                                            @else
                                                Voucher Price:
                                            @endif
                                            <i class="fa fa-rupee"></i> {{$singleDeal->online_sell_price}}
                                        </strong>
                                    </h5>
                                </div>

                                <div class="buy-now mb-40 text-center">
                                    <div class="btn-group" role="group" aria-label="">

                                        @if(Route::currentRouteName() == 'couponItem')
                                            <a href="javascript:void(0);" class="btn btn-warning w-100"
                                               data-toggle="modal" data-target="#coupon_04">
                                                <i class="fa fa-credit-card font-16"></i> GENERATE COUPON & AVAIL AT STORE
                                            </a>

                                            <p class="t-uppercase color-muted pt-10 or_text">
                                                - OR -
                                            </p>
                                            <a href="javascript:void(0);" class="btn  w-100 deal_addcart"  data-id="{{$singleDeal->id}}" data-type="deal" data-url="{{route('cart')}}">
                                                <i class="fa fa-credit-card font-16"></i> BUY ONLINE
                                            </a>

                                        @else
                                            @if(($singleDeal->totalstock == -1) || ( ($singleDeal->totalstock - $singleDeal->sold)>0 ))
                                                <a class="btn btn-o deal_addcart" data-id="{{$singleDeal->id}}" data-type="deal">
                                                    <i class="fa fa-shopping-cart font-16"></i> Add to Cart
                                                </a>

                                                <a class="btn deal_addcart" data-id="{{$singleDeal->id}}" data-type="deal" data-url="{{route('cart')}}">
                                                    <i class="fa fa-bolt font-16"></i> Buy now
                                                </a>
                                            @else
                                                <a href="javascript:void(0);" class="btn w-100">
                                                    <i class="fa fa-archive font-16"></i> Sorry Out of Stock
                                                </a>

                                            @endif
                                        @endif

                                    </div>
                                </div>


                                <div class="time-left mb-30 text-center">
                                    <p class="t-uppercase color-muted">
                                        Hurry up Only a few deals left
                                    </p>
                                    <div class="color-green font-14 font-lg-16 text-center">
                                        <i class="ico fa fa-clock-o mr-10"></i>
                                        <span data-countdown="{{date('Y/m/d H:i:s',strtotime($singleDeal->end_time))}}"></span>
                                    </div>
                                </div>


                                <ul class="list-inline social-icons social-icons--colored t-center v_shareParent" data-shareUrl="{{route('dealItem',$singleDeal->id)}}?ref={{getReferalId()}}">
                                    <li class="social-icons__item">
                                        <a href="#" class="v_sharebtn" title="Facebook" data-type="Facebook"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li class="social-icons__item">
                                        <a href="#" class="v_sharebtn" title="Twitter" data-type="Twitter"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li class="social-icons__item">
                                        <a href="#" class="v_sharebtn" title="Google Plus" data-type="GooglePlus"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li class="social-icons__item">
                                        <a href="#" class="v_sharebtn" title="Whatsapp" data-type="Whatsapp"><i class="fa fa-whatsapp"></i></a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                    @if($singleDeal->sponsers)
                    <div class="col-xs-12">
                        <!-- Latest Deals Widegt -->
                        <p class="t-uppercase color-muted">
                            {{$singleDeal->getOwner->name}}
                        </p>
                        <div class="widget latest-deals-widget panel prl-20">
                            <div class="widget-body ptb-20">
                                <div class="owl-slider" data-loop="true" data-autoplay="true" data-autoplay-timeout="10000" data-smart-speed="1000" data-nav-speed="false" data-nav="true" data-xxs-items="1" data-xxs-nav="true" data-xs-items="1" data-xs-nav="true" data-sm-items="1" data-sm-nav="true" data-md-items="1" data-md-nav="true" data-lg-items="1" data-lg-nav="true">

                                    @php
                                        $sponsers = json_decode($singleDeal->getOwner->firm_images);
                                    @endphp

                                    @foreach($sponsers as $sponser)
                                        <div class="latest-deals__item item">
                                            <figure class="deal-thumbnail embed-responsive embed-responsive-4by3" data-bg-img="/uploads/{{$sponser}}">
                                            </figure>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- End Latest Deals Widegt -->
                    </div>

                    @endif

                    <div class="col-xs-12">
                        <!-- Subscribe Widget -->
                        <div class="widget subscribe-widget panel pt-20 prl-20">
                            <h3 class="widget-title h-title">Contact Seller</h3>
                            <div class="widget-content ptb-30">
                                <ul class="deal-meta list-unstyled mb-10 color-mid deal_contactseller">
                                    <li><i class="ico lnr lnr-map-marker"></i>
                                        <strong>{{$singleDeal->getOwner->name}}</strong>
                                        <br>
                                        {{$singleDeal->getOwner->address}}</a>
                                    </li>
                                    @if($singleDeal->getOwner->firm_phone)<li class="color-green"><i class="ico fa lnr lnr-phone-handset"></i> {{$singleDeal->getOwner->firm_phone}}</li>@endif
                                    @if($singleDeal->getOwner->firm_email)<li><a href="mailto:{{$singleDeal->getOwner->firm_email}}" ><i class="ico lnr lnr-envelope"></i> {{$singleDeal->getOwner->firm_email}}</a></li>@endif
                                    <li><i class="ico lnr lnr-clock"></i> 10:00 am - 9:00 pm</li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Subscribe Widget -->
                    </div>
                    <div class="col-xs-12">
                        <!-- Subscribe Widget -->
                        <div class="widget subscribe-widget panel pt-20 prl-20">
                            <h3 class="widget-title h-title">Deal Location</h3>
                            <div class="widget-content ptb-30">
                                <iframe src="https://www.google.com/maps/embed/v1/place?q={{(json_decode($singleDeal->getOwner->location)?(json_decode($singleDeal->getOwner->location)->latitude??0):'0')}},{{(json_decode($singleDeal->getOwner->location)?(json_decode($singleDeal->getOwner->location)->longitude??0):'0')}}&amp;key=AIzaSyAyCFC_khEXg_0wH2KoUGT1uACDD3ZUIvI"   class="googlemap" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <!-- End Subscribe Widget -->
                    </div>
                    <div class="col-xs-12">
                        <!-- Best Rated Deals -->
                        <div class="widget best-rated-deals panel pt-20 prl-20">
                            <h3 class="widget-title h-title">Best Rated Deals</h3>
                            <div class="widget-body ptb-30">

                                @foreach($topPics as $deal)
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object" src="/uploads/{{$deal->small_image}}"
                                                     alt="Thumb" width="80">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="mb-5">
                                                <a href="{{route('dealItem',$deal->id)}}"> {{$deal->title}}</a>
                                            </h6>
                                            <div class="mb-5">
                                            <span class="rating">
                                            <span class="rating-stars" data-rating="4">
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </span>
                                            </span>
                                            </div>
                                            <h4 class="price font-16">{{$deal->price}} <span
                                                        class="price-sale color-muted"> {{$deal->offer_price}}</span>
                                            </h4>
                                        </div>
                                    </div>
                                @endforeach


                            </div>
                        </div>
                        <!-- Best Rated Deals -->
                    </div>
                </div>
            </aside>
            <!-- End Blog Sidebar -->
        </div>
    </div>
    @if(count($similarDeals))
        <section class="section latest-deals-area ptb-30">
            <header class="panel ptb-15 prl-20 pos-r mb-30">
                <h3 class="section-title font-18">Similar Deals</h3>
                <a class="btn btn-o btn-xs pos-a right-10 pos-tb-center" href="{{route('deallistDealsCategory',$singleDeal->category_id)}}">View All</a>
            </header>

            <div class="row row-masnory row-tb-20">
                @foreach($similarDeals as $deal)
                    <div class="col-sm-6 col-lg-4">
                        <div class="deal-single panel">
                            <figure class="embed-responsive embed-responsive-16by9"
                                    data-bg-img="/uploads/{{$deal->small_image}}">
                                <div class="label-discount_primary left-20 top-15">{{ number_format((100-(($deal->offer_price / ($deal->price??1))*100)),0) }}%<br>OFF</div>
                                <ul class="deal-actions top-15 right-15 v_shareParent" data-shareUrl="{{route('dealItem',$deal->id)}}?ref={{getReferalId()}}" >
                                    @include('site.includes.shareDropDownContents')
                                    <li class="like-deal">
                                    <span data-toggle="tooltip" data-placement="bottom" title="{{$deal->userFavorites()->count()}}" class="@if($deal->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="dealItem" data-id="{{$deal->id}}" >
                                        @if($deal->isFavourited()->count())
                                            <img src="/site/assets/images/favorited.png">
                                        @else
                                            <img src="/site/assets/images/favorite.png">
                                        @endif
                                    </span>
                                    </li>
                                    <li class="like-deal">
                                                    <span data-toggle="tooltip" data-placement="bottom" title="3.5">
                                    <img src="/site/assets/images/star.png">
                                </span>
                                    </li>
                                </ul>
                                <div class="viewdeal"><a href="{{route('dealItem',$deal->id)}}">VIEW DEAL</a></div>
                            </figure>
                            <div class="bg-white pt-20 pl-20 pr-15">
                                <div class="time-left_dealdaa font-md-14 mb-10">
                                                <span>
                                <i class="ico fa fa-clock-o mr-5"></i>
                                <span class="t-uppercase" data-countdown="{{ date('Y/m/d H:i:s',strtotime($deal->end_time))}}"></span>
                            </span>
                                </div>
                                <div class="pr-md-10">
                                    <h3 class="deal-title mb-10">
                                        <a href="{{route('dealItem',$deal->id)}}">
                                            {{$deal->title}}
                                        </a>
                                    </h3>
                                    <ul class="deal-meta list-unstyled mb-10 color-mid latest_deal_location">
                                        <li><i class="ico lnr lnr-map-marker mr-10"></i>{{$deal->getOwner->address}}</li>
                                        <li><i class="ico lnr lnr-store mr-10"></i>{{$deal->getOwner->name}}</li>
                                    </ul>
                                </div>
                                <div class="deal-price pos-r mb-15">
                                    <ul class="list-inline m-0">
                                        <li>
                                            <h3 class="price ptb-5 text-left">
                                                <i class="fa fa-rupee"></i> {{$deal->offer_price}}
                                            </h3>
                                        </li>
                                        <li class="float-right" data-toggle="tooltip" data-placement="left" title="Add to Cart"><a class="btn btn-primary deal_addcart"  data-id="{{$deal->id}}" data-type="deal" ><i class="lnr lnr-cart"></i></a> </li>
                                    </ul>


                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    @endif


    @if(Route::currentRouteName() == 'couponItem')
    <div class="modal fade get-coupon-area" tabindex="-1" role="dialog" id="coupon_04">
        <div class="modal-dialog">
            <div class="modal-content panel">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <div class="row row-v-10 pt-20">
                        <div class="col-md-12">
                            <img src="/site/assets/images/generate_coupon_howit.jpg" class="img-responsive">
                        </div>
                        <div class="col-md-12 text-left pt-20">

                            <h3 class="widget-title h-title">Contact Seller</h3>
                            <div class="widget-content pt-20 col-md-6">

                                <ul class="deal-meta list-unstyled mb-10 color-mid deal_contactseller">
                                    <li><i class="ico lnr lnr-map-marker"></i>
                                        <strong>{{$singleDeal->getOwner->name}}</strong>
                                        <br>
                                        {{$singleDeal->getOwner->address}}</a>
                                    </li>
                                    @if($singleDeal->getOwner->firm_phone)<li class="color-green"><i class="ico fa lnr lnr-phone-handset"></i> {{$singleDeal->getOwner->firm_phone}}</li>@endif
                                    @if($singleDeal->getOwner->firm_email)<li><a href="mailto:{{$singleDeal->getOwner->firm_email}}" ><i class="ico lnr lnr-envelope"></i> {{$singleDeal->getOwner->firm_email}}</a></li>@endif
                                    <li><i class="ico lnr lnr-clock"></i> 10:00 am - 9:00 pm</li>
                                </ul>
                            </div>
                            <div class="widget-content pt-20 col-md-6">
                                <iframe src="https://www.google.com/maps/embed/v1/place?q={{(json_decode($singleDeal->getOwner->location)?(json_decode($singleDeal->getOwner->location)->latitude??0):'0')}},{{(json_decode($singleDeal->getOwner->location)?(json_decode($singleDeal->getOwner->location)->longitude??0):'0')}}&amp;key=AIzaSyAyCFC_khEXg_0wH2KoUGT1uACDD3ZUIvI"   class="googlemap" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col-lg-12 pt-10">
                            <a href="{{route('user.generateCoupon',$singleDeal->id)}}" class="btn btn-danger" >GENERATE COUPON</a>
                            <p class="pt-15">
                                <small>Coupon Expiry: <span class="text-danger">{{Carbon\Carbon::now()->addDays(7)->format('M d, Y')}}</span></small>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif


@stop