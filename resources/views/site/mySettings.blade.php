@extends('site.layout.master')


@section('title', 'Settings')

@push('style')

@endpush

@push('scripts')

@endpush

@section('content')
    @include('site.includes.profile_sidebar')
    <div class="page-content col-sm-8 col-md-9">
        <div class=" deal_single_view">
            <div class="deal-deatails panel myprofile_minhieight">
                <div class="deal-body p-20">
                    <header class="panel pb-15 prl-0 pos-r mb-20">
                        <h3 class="h-title font-18">Change Password</h3>
                    </header>
                    <div class="flashsale_terms">
                        <div class="">
                            <div class="col-md-12">
                                @foreach ($errors->all() as $message)
                                    <div class="alert alert-danger">{{$message}}</div>
                                @endforeach
                            </div>
                            <form class="mb-30" method="post" action="{{route('user.changePassword')}}">
                                @csrf
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Current Password</label>
                                        <input type="password" name="current_password" class="form-control" placeholder="Current Password" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>New Password</label>
                                        <input type="password" name="password" class="form-control" placeholder="New Password" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-rounded">UPDATE PASSWORD</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
@stop