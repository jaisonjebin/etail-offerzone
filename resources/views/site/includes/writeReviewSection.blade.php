<div class="col-xs-12">
    <div class="post-review panel p-15">
        <div class="col-lg-12 ">
            <h3 class="h-title">Total Rating</h3>
            <div class="rating_histogram">
                <div class="inner">
                    <div class="rating">
                        <span class="rating-num">{{$review['avg']}}</span>
                        <div class="rating-stars1">
                                                     <span class="rating-stars" data-rating="4">
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                    </span>
                        </div>
                        <div class="rating-users">
                            <i class="icon-user"></i> {{$review['totalCount']}} total
                        </div>
                    </div>

                    @php
                        $review['totalCount'] = $review['totalCount']==0?1:$review['totalCount'];
                    @endphp
                    <div class="histo">


                        <div class="five histo-rate">
                    <span class="histo-star">
          <i class="active fa fa-star"></i> 5           </span>
                            <span class="bar-block">
          <a href=""><span id="bar-five" class="bar vv-bar-animate" data-percentage = "{{(($review['5']/$review['totalCount'])*100)}}">
            <span>{{$review['5']}}</span>&nbsp;</span>
          </a></span>
                        </div>

                        <div class="four histo-rate">
                    <span class="histo-star">
          <i class="active  fa fa-star"></i> 4           </span>
                            <span class="bar-block">
          <a href=""><span id="bar-four" class="bar vv-bar-animate" data-percentage = "{{(($review['4']/$review['totalCount'])*100)}}">
            <span>{{$review['4']}}</span>&nbsp;
                    </span>
                    </a>
                    </span>
                        </div>

                        <div class="three histo-rate">
                    <span class="histo-star">
          <i class="active fa fa-star"></i> 3           </span>
                            <span class="bar-block">
          <a href="">
              <span id="bar-three" class="bar vv-bar-animate" data-percentage = "{{(($review['3']/$review['totalCount'])*100)}}">
            <span>{{$review['3']}}</span>&nbsp;
                    </span>
                    </a>
                    </span>
                        </div>

                        <div class="two histo-rate">
                    <span class="histo-star">
          <i class="active fa fa-star"></i> 2           </span>
                            <span class="bar-block">
          <a href=""><span id="bar-two" class="bar vv-bar-animate" data-percentage = "{{(($review['2']/$review['totalCount'])*100)}}">
            <span>{{$review['2']}}</span>&nbsp;
                    </span>
                    </a>
                    </span>
                        </div>

                        <div class="one histo-rate">
                    <span class="histo-star">
          <i class="active fa fa-star"></i> 1&nbsp;           </span>
                            <span class="bar-block">
          <a href=""><span id="bar-one" class="bar vv-bar-animate" data-percentage = "{{(($review['1']/$review['totalCount'])*100)}}">
            <span>{{$review['1']}}</span>&nbsp;
                    </span>
                    </a>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @auth
            @if(isset($review['isPurchased']) && $review['isPurchased']>0)
            <div class="col-lg-12">

                <h3 class="h-title">Post Review</h3>
                @if ($errors->any())

                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="horizontal-form" action="{{route('user.writeReview')}}" method="post" id="reviewForm">
                    @csrf
                    <input type="hidden" name="product_id" required value="{{$product_id}}">
                    <input type="hidden" name="product_type" required value="{{$product_type}}">
                    <div class="row row-v-10">
                        <div class="col-xs-12 star_rating">
                            <form>
                                <fieldset>
                                    <span class="star-cb-group">
                                      <input type="radio" id="rating-5" name="rating" value="5"/><label for="rating-5" title="5 Star">5</label>
                                      <input type="radio" id="rating-4" name="rating" value="4"/><label for="rating-4" title="4 Star">4</label>
                                      <input type="radio" id="rating-3" name="rating" value="3"/><label for="rating-3" title="3 Star">3</label>
                                      <input type="radio" id="rating-2" name="rating" value="2"/><label for="rating-2" title="2 Star">2</label>
                                      <input type="radio" id="rating-1" name="rating" value="1"/><label for="rating-1" title="1 Star">1</label>
                                      <input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear"/><label for="rating-0">0</label>
                                    </span>
                                </fieldset>
                            </form>
                        </div>
                        <div class="col-xs-12">
                            <textarea class="form-control" name="description" placeholder="Your Review"
                                                              rows="6" required>{{old('description')}}</textarea>
                        </div>
                        <div class="col-xs-12 text-right">
                            <button type="submit" class="btn mt-20">Submit review</button>
                        </div>
                    </div>
                </form>
            </div>
            @endif
        @endauth

    </div>
</div>