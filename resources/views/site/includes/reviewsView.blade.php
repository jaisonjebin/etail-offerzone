<div class="col-xs-12">
    <div class="posted-review panel p-30">
        <h3 class="h-title">{{$productReviews->count()}} Review</h3>
        @if(!$productReviews->isEmpty())
            @foreach($productReviews as $productReview)
                <div class="review-single pt-30">
                    <div class="media">
                        <div class="media-left">
                            <img class="media-object mr-10 radius-4"
                                 src="/site/assets/images/avatars/avatar_02.jpg" width="90" alt="">
                        </div>
                        <div class="media-body">
                            <div class="review-wrapper clearfix">
                                <ul class="list-inline">
                                    <li>
                                        <span class="review-holder-name h5">{{$productReview->getOwner->name}}</span>

                                    </li>
                                    <li>
                                        <div class="rating">
                                                                    <span class="rating-stars" data-rating="{{$productReview->rating}}">
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                    </span>
                                            @auth
                                            @if($productReview->user_id==Auth::user()->id)
                                                <a href="javascipt:void(0);" class="edit_review" data-id="{{$productReview->id}}" data-rating="{{$productReview->rating}}" data-description="{{$productReview->description}}"><label class="pull-right pl-10"><i class="fa fa-pencil-square-o"></i></label></a>
                                            @endif
                                            @endauth

                                        </div>

                                    </li>
                                </ul>
                                <p class="review-date mb-5">{{$productReview->updated_at->format('F d, Y')}}</p>
                                <p class="copy">{{$productReview->description}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
        @endif
        {{--<!-- Page Pagination -->--}}
        {{--<div class="page-pagination text-right mtb-0 panel">--}}
            {{--<nav>--}}
                {{--<!-- Page Pagination -->--}}
                {{--<ul class="page-pagination">--}}
                    {{--<li><a class="page-numbers previous" href="#">Previous</a>--}}
                    {{--</li>--}}
                    {{--<li><a href="#" class="page-numbers">1</a>--}}
                    {{--</li>--}}
                    {{--<li><span class="page-numbers current">2</span>--}}
                    {{--</li>--}}
                    {{--<li><a href="#" class="page-numbers">3</a>--}}
                    {{--</li>--}}
                    {{--<li><a href="#" class="page-numbers">4</a>--}}
                    {{--</li>--}}
                    {{--<li><span class="page-numbers dots">...</span>--}}
                    {{--</li>--}}
                    {{--<li><a href="#" class="page-numbers">20</a>--}}
                    {{--</li>--}}
                    {{--<li><a href="#" class="page-numbers next">Next</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
                {{--<!-- End Page Pagination -->--}}
            {{--</nav>--}}
        {{--</div>--}}
        {{--<!-- End Page Pagination -->--}}
    </div>
</div>