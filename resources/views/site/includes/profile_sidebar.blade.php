<div class="page-sidebar col-sm-4 col-md-3 myprofile">
    <aside class="store-header-area panel">
        <div class="row">
            <div class="col-md-12 avathar text-center ptb-15">
                <img src="/site/assets/images/avatars/avatar_03.jpg" class="img-circle img-responsive mrl-auto">
            </div>
            <div class="col-md-12 myprofile_navbar">
                <ul class="list-unstyled">
                    <li class="{{isActiveRoute('user.myProfile')}}"><a href="{{route('user.myProfile')}}"> <i class="lnr lnr-user"></i> MY PROFILE</a></li>
                    <li class="{{isActiveRoute('user.myPurchase')}}"><a href="{{route('user.myPurchase')}}"><i class="lnr lnr-tag"></i> MY PURCHASE</a></li>
                    <li class="{{isActiveRoute('user.myTeam')}}"><a href="{{route('user.myTeam')}}"><i class="lnr lnr-users"></i> MY TEAM</a></li>
                    <li class="{{isActiveRoute('user.myNotifications')}}"><a href="{{route('user.myNotifications')}}"><i class="lnr lnr-alarm"></i> NOTIFICATION</a></li>
                    <li class="{{isActiveRoute('user.myRewards')}}"><a href="{{route('user.myRewards')}}"><i class="lnr lnr-magic-wand"></i> REWARDS</a></li>
                    <li class="{{isActiveRoute('user.user.myAddress')}}"><a href="{{route('user.myAddress')}}"><i class="lnr lnr-license"></i> SAVED ADDRESS</a></li>
                    <li class="{{isActiveRoute('user.mySettings')}}"><a href="{{route('user.mySettings')}}"><i class="lnr lnr-cog"></i> SETTINGS</a></li>
                </ul>
            </div>

        </div>

    </aside>
</div>