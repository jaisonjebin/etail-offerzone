<li class="share-btn">
    <div class="share-tooltip fade">
        <a class="v_sharebtn" title="Facebook" data-type="Facebook"><i class="fa fa-facebook"></i></a>
        <a class="v_sharebtn" title="Twitter" data-type="Twitter"><i class="fa fa-twitter"></i></a>
        <a class="v_sharebtn" title="Google Plus" data-type="GooglePlus"><i class="fa fa-google-plus"></i></a>
        <a class="v_sharebtn" title="Whatsapp" data-type="Whatsapp"><i class="fa fa-whatsapp"></i></a>
    </div>
    <span><img src="/site/assets/images/share.png"> </span>
</li>

