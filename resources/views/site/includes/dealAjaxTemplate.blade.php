@foreach($deals as $deal)
    <div class="col-sm-6 col-lg-4 animated slideInUp">
        <div class="deal-single panel">
            <figure class="embed-responsive embed-responsive-16by9"
                    data-bg-img="/uploads/{{$deal->small_image}}">
                <div class="label-discount_primary left-20 top-15">{{ number_format((100-(($deal->offer_price / ($deal->price??1))*100)),0) }}%<br>OFF</div>
                <ul class="deal-actions top-15 right-15 v_shareParent" data-shareUrl="{{route('dealItem',$deal->id)}}?ref={{getReferalId()}}" >
                    @include('site.includes.shareDropDownContents')
                    <li class="like-deal">
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$deal->userFavorites()->count()}}" class="@if($deal->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="dealItem" data-id="{{$deal->id}}" >
                                    @if($deal->isFavourited()->count())
                                        <img src="/site/assets/images/favorited.png">
                                    @else
                                        <img src="/site/assets/images/favorite.png">
                                    @endif
                                </span>
                    </li>
                    <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="3.5">
                                <img src="/site/assets/images/star.png">
                            </span>
                    </li>
                </ul>
                <div class="viewdeal"><a href="{{route('dealItem',$deal->id)}}">VIEW DEAL</a></div>
            </figure>
            <div class="bg-white pt-20 pl-20 pr-15">
                <div class="time-left_dealdaa font-md-14 mb-10">
                                            <span>
                            <i class="ico fa fa-clock-o mr-5"></i>
                            <span class="t-uppercase" data-countdown="{{ date('Y/m/d H:i:s',strtotime($deal->end_time))}}"></span>
                        </span>
                </div>
                <div class="pr-md-10">
                    <h3 class="deal-title mb-10">
                        <a href="{{route('dealItem',$deal->id)}}">
                            {{$deal->title}}
                        </a>
                    </h3>
                    <ul class="deal-meta list-unstyled mb-10 color-mid latest_deal_location">
                        <li><i class="ico lnr lnr-map-marker mr-10"></i>{{$deal->getOwner->address}}</li>
                        <li><i class="ico lnr lnr-store mr-10"></i>{{$deal->getOwner->name}}</li>
                    </ul>
                </div>
                <div class="deal-price pos-r mb-15">
                    <ul class="list-inline m-0">
                        @if($deal->dealType != 'voucher')
                            <li>
                                <h3 class="price ptb-5 text-left price-sale">
                                    <i class="fa fa-rupee"></i> {{$deal->price}}
                                </h3>
                            </li>
                        @endif
                        <li>
                            <h3 class="price ptb-5 text-left">
                                <i class="fa fa-rupee"></i> {{$deal->online_sell_price}}
                            </h3>
                        </li>
                        <li class="float-right" data-toggle="tooltip" data-placement="left" title="Add to Cart"><a class="btn btn-primary deal_addcart"  data-id="{{$deal->id}}" data-type="deal" ><i class="lnr lnr-cart"></i></a> </li>
                    </ul>


                </div>
            </div>
        </div>
    </div>
@endforeach