@foreach($coupons as $deal)
    <div class="coupon-item col-md-3 col-sm-6 mb-20">
        <div class="coupon-single panel t-center">
            @if($deal->isFeatured)
                <div class="ribbon-wrapper is-hidden-xs-down">
                    <div class="ribbon">Featured</div>
                </div>
            @endif

            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center p-20">
                        <img class="store-logo" src="/uploads/{{$deal->small_image}}" alt="">
                    </div>
                    <!-- end media -->
                    <ul class="deal-actions left-0 right-0 cashback_share v_shareParent" data-shareUrl="{{route('couponItem',$deal->id)}}?ref={{getReferalId()}}" >
                        @include('site.includes.shareDropDownContents')
                        <li class="like-deal">
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$deal->userFavorites()->count()}}" class="@if($deal->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="dealItem" data-id="{{$deal->id}}" >
                                    @if($deal->isFavourited()->count())
                                        <img src="/site/assets/images/favorited.png">
                                    @else
                                        <img src="/site/assets/images/favorite.png">
                                    @endif
                                </span>
                        </li>

                        <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="3.5">
                                <img src="/site/assets/images/star.png">
                            </span>
                        </li>
                    </ul>
                </div>
                <!-- end col -->

                <div class="col-xs-12">
                    <div class="panel-body">
                        <h5 class="deal-title mb-10 nowrap">
                            <a href="#">{{$deal->title}}</a>
                        </h5>
                        <h4 class="text-success mb-10 font-12 nowrap">{{$deal->small_description}}</h4>

                        <ul class="deal-meta list-unstyled mb-10 color-mid latest_deal_location">
                            <li><i class="ico lnr lnr-map-marker mr-10"></i>{{$deal->getOwner->address}}</li>
                            <li><i class="ico lnr lnr-store mr-10"></i>{{$deal->getOwner->name}}</li>
                        </ul>
                        <div class="time-left_dealdaa font-md-12 color-green mb-10">
                                            <span>
                            <i class="ico fa fa-clock-o mr-5"></i>
                            <span class="t-uppercase" data-countdown="{{ date('Y/m/d H:i:s',strtotime($deal->end_time))}}"></span>
                        </span>
                        </div>
                        <a href="{{route('couponItem',$deal->id)}}" class="btn btn-sm btn-block">Get Coupon
                        </a>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
    </div>
@endforeach