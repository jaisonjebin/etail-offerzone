@foreach($flashSales as $flashSale)
    <div class="col-sm-6 col-md-4 col-lg-4  animated slideInUp">
        <div class="deal-single panel">
            <figure class=" embed-responsive embed-responsive-16by9"
                    data-bg-img="/uploads/{{$flashSale->small_image}}">
                <div class="ribbon-flahssale is-hidden-xs-down">
                    @if($flashSale->start_time <= \Carbon\Carbon::now())
                        <div class="ribbon-flah"> On sales now</div>
                    @else
                        <div class="ribbon-flah" style="background: green;">Coming Soon</div>
                    @endif
                </div>
                <ul class="deal-actions top-15 right-15  v_shareParent" data-shareUrl="{{route('flashsaleItem',$flashSale->id)}}?ref={{getReferalId()}}" >
                    @include('site.includes.shareDropDownContents')
                    <li class="like-deal">
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$flashSale->userFavorites()->count()}}" class="@if($flashSale->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="flashSaleItem" data-id="{{$flashSale->id}}" >
                                    @if($flashSale->isFavourited()->count())
                                        <img src="/site/assets/images/favorited.png">
                                    @else
                                        <img src="/site/assets/images/favorite.png">
                                    @endif
                                </span>
                    </li>
                    <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="3.5">
                                <img src="/site/assets/images/star.png">
                            </span>
                    </li>
                </ul>


            </figure>
            <div class="featured_flashsalebtn text-right">
                <ul class="list-inline m-0">
                    <li class="pull-left">{{ number_format((100-(($flashSale->offer_price / ($flashSale->price??1))*100)),0) }}% OFF</li>
                    <li class="">
                        <a href="{{route('flashsaleItem',$flashSale->id)}}" class="btn btn-primary btn-sm ">
                            @if($flashSale->start_time <= \Carbon\Carbon::now())
                                View Sale
                            @else
                                Coming Soon
                            @endif
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
@endforeach