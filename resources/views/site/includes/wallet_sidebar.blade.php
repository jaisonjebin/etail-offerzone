<div class="page-sidebar col-sm-4 col-md-3 myprofile">
    <aside class="store-header-area panel">
        <div class="row">
            <div class="col-md-12 myprofile_navbar">
                <ul class="list-unstyled">
                    <li class="{{isActiveRoute('user.myWallet')}}"><a href="{{route('user.myWallet')}}"> ACCOUNT SUMMARY</a></li>
                    <li class="{{isActiveRoute('user.myWalletTransaction')}}"><a href="{{route('user.myWalletTransaction')}}"> TRANSACTION HISTORY</a></li>
                    <li class="{{isActiveRoute('user.myWalletWithdrawals')}}"><a href="{{route('user.myWalletWithdrawals')}}"> WITHDRAWALS</a></li>
                    {{--<li class="{{isActiveRoute('user.myWallet')}}"><a href="wallet.php"> INBOX</a></li>--}}
                </ul>
            </div>

        </div>

    </aside>
</div>