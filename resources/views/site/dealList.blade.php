@extends('site.layout.master')


@section('title', 'Deals')

@push('style')
    <!-- Owl Carousel -->
    <link href="/site/assets/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="/site/assets/vendors/owl-carousel/owl.theme.min.css" rel="stylesheet">
    <!-- Flex Slider -->
    <link href="/site/assets/vendors/flexslider/flexslider.css" rel="stylesheet">
@endpush

@push('scripts')
    <!-- Owl Carousel -->
    <script type="text/javascript" src="/site/assets/vendors/owl-carousel/owl.carousel.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="/site/assets/vendors/flexslider/jquery.flexslider-min.js"></script>

    <!-- Coutdown -->
    <script type="text/javascript" src="/site/assets/vendors/countdown/jquery.countdown.js"></script>

    <script>

        $(function () {
            loadDeals();
        });
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() > $(document).height() - 850) {
                loadDeals();
            }
        });

        $genesis = 0;
        $page = 1;
        $ajaxPrssing = 0;
        function loadDeals() {
            if ($genesis == 0 && $ajaxPrssing==0){
                $ajaxPrssing=1;
                $('.ajaxLoaderAni').slideDown();
                $.ajax({
                    url: '{{route('deallistDealsAjax',[$categoryId,''])}}/' + ($page++),
                    type: 'post',
                    success: function (data) {
                        $('.ajaxLoaderAni').slideUp();
                        $ajaxPrssing = 0;
                        if (data == 'genisys') {
                            $genesis = 1;
                            if($page ==2 && $genesis == 1){
                                $('.dealsListData').append('<div class="col-lg-12">Could not find any Deals on this Category</div>');
                            }
                        }
                        else{
                            $('.dealsListData').append(data);
                            reInitPlugins();
                        }
                    }
                });
            }
        }

        function reInitPlugins() {
            $("[data-bg-img]").each(function() {
                var attr = $(this).attr('data-bg-img');
                if (typeof attr !== typeof undefined && attr !== false && attr !== "") {
                    $(this).css('background-image', 'url('+attr+')');
                }
            });

            var countdown_select = $("[data-countdown]");
            countdown_select.each(function(){
                $(this).countdown($(this).data('countdown'))
                    .on('update.countdown', function(e){
                        var format = '%H : %M : %S';
                        if (e.offset.totalDays > 0) {
                            format = '%d Day%!d '+format;
                        }
                        if (e.offset.weeks > 0) {
                            format = '%w Week%!w '+format;
                        }
                        $(this).html(e.strftime(format));
                    });
            }).on('finish.countdown', function(e){
                $(this).html('This offer ha expired!').addClass('disabled');
            });

            var share_action = $('.deal-actions .share-btn');
            share_action.on('click',function(){
                var share_icons = $(this).children('.share-tooltip');
                share_icons.toggleClass('in');
            });
        }
    </script>
@endpush
@section('content')
    @if(count($dealoftheday)!=0)
    <section class="section latest-deals-area ptb-30">
        <header class="panel ptb-15 prl-20 pos-r mb-30">
            <h3 class="section-title font-18">Deal of the day</h3>
            <a href="{{route('deallistDealsCategory','deal-of-the-day')}}" class="btn btn-o btn-xs pos-a right-10 pos-tb-center">View All</a>
        </header>
        <div class="row row-masnory row-tb-20">
            @foreach($dealoftheday as $deal)
                <div class="col-sm-6 col-lg-4">
                    <div class="deal-single panel">
                        <figure class="embed-responsive embed-responsive-16by9"
                                data-bg-img="/uploads/{{$deal->small_image}}">
                            <div class="label-discount_primary left-20 top-15">{{ number_format((100-(($deal->offer_price / ($deal->price??1))*100)),0) }}%<br>OFF</div>
                            <ul class="deal-actions top-15 right-15 v_shareParent" data-shareUrl="{{route('dealItem',$deal->id)}}?ref={{getReferalId()}}" >
                                @include('site.includes.shareDropDownContents')
                                <li class="like-deal">
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$deal->userFavorites()->count()}}" class="@if($deal->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="dealItem" data-id="{{$deal->id}}" >
                                    @if($deal->isFavourited()->count())
                                        <img src="/site/assets/images/favorited.png">
                                    @else
                                        <img src="/site/assets/images/favorite.png">
                                    @endif
                                </span>
                                </li>
                                <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="3.5">
                                <img src="/site/assets/images/star.png">
                            </span>
                                </li>
                            </ul>
                            <div class="viewdeal"><a href="{{route('dealItem',$deal->id)}}">VIEW DEAL</a></div>
                        </figure>
                        <div class="bg-white pt-20 pl-20 pr-15">
                            <div class="time-left_dealdaa font-md-14 mb-10">
                                            <span>
                            <i class="ico fa fa-clock-o mr-5"></i>
                            <span class="t-uppercase" data-countdown="{{ date('Y/m/d H:i:s',strtotime($deal->end_time))}}"></span>
                        </span>
                            </div>
                            <div class="pr-md-10">
                                <h3 class="deal-title mb-10">
                                    <a href="{{route('dealItem',$deal->id)}}">
                                        {{$deal->title}}
                                    </a>
                                </h3>
                                <ul class="deal-meta list-unstyled mb-10 color-mid latest_deal_location">
                                    <li><i class="ico lnr lnr-map-marker mr-10"></i>{{$deal->getOwner->address}}</li>
                                    <li><i class="ico lnr lnr-store mr-10"></i>{{$deal->getOwner->name}}</li>
                                </ul>
                            </div>
                            <div class="deal-price pos-r mb-15">
                                <ul class="list-inline m-0">
                                    @if($deal->dealType != 'voucher')
                                        <li>
                                            <h3 class="price ptb-5 text-left price-sale">
                                                <i class="fa fa-rupee"></i> {{$deal->price}}
                                            </h3>
                                        </li>
                                    @endif
                                    <li>
                                        <h3 class="price ptb-5 text-left">
                                            <i class="fa fa-rupee"></i> {{$deal->online_sell_price}}
                                        </h3>
                                    </li>
                                    <li class="float-right" data-toggle="tooltip" data-placement="left" title="Add to Cart"><a class="btn btn-primary deal_addcart"  data-id="{{$deal->id}}" data-type="deal" ><i class="lnr lnr-cart"></i></a> </li>
                                </ul>


                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    @endif
    @if(count($topPics)!=0)
    <section class="section latest-deals-area ptb-30">
        <header class="panel ptb-15 prl-20 pos-r mb-30">
            <h3 class="section-title font-18">Top Picks</h3>
            <a href="{{route('deallistDealsCategory','top-picks')}}" class="btn btn-o btn-xs pos-a right-10 pos-tb-center">View All</a>
        </header>
        <div class="row row-masnory row-tb-20">
            @foreach($topPics as $deal)
                <div class="col-sm-6 col-lg-4">
                    <div class="deal-single panel">
                        <figure class="embed-responsive embed-responsive-16by9"
                                data-bg-img="/uploads/{{$deal->small_image}}">
                            <div class="label-discount_primary left-20 top-15">{{ number_format((100-(($deal->offer_price / ($deal->price??1))*100)),0) }}%<br>OFF</div>
                            <ul class="deal-actions top-15 right-15 v_shareParent" data-shareUrl="{{route('dealItem',$deal->id)}}?ref={{getReferalId()}}" >
                                @include('site.includes.shareDropDownContents')
                                <li class="like-deal">
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$deal->userFavorites()->count()}}" class="@if($deal->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="dealItem" data-id="{{$deal->id}}" >
                                    @if($deal->isFavourited()->count())
                                        <img src="/site/assets/images/favorited.png">
                                    @else
                                        <img src="/site/assets/images/favorite.png">
                                    @endif
                                </span>
                                </li>
                                <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="3.5">
                                <img src="/site/assets/images/star.png">
                            </span>
                                </li>
                            </ul>
                            <div class="viewdeal"><a href="{{route('dealItem',$deal->id)}}">VIEW DEAL</a></div>
                        </figure>
                        <div class="bg-white pt-20 pl-20 pr-15">
                            <div class="time-left_dealdaa font-md-14 mb-10">
                                            <span>
                            <i class="ico fa fa-clock-o mr-5"></i>
                            <span class="t-uppercase" data-countdown="{{ date('Y/m/d H:i:s',strtotime($deal->end_time))}}"></span>
                        </span>
                            </div>
                            <div class="pr-md-10">
                                <h3 class="deal-title mb-10">
                                    <a href="{{route('dealItem',$deal->id)}}">
                                        {{$deal->title}}
                                    </a>
                                </h3>
                                <ul class="deal-meta list-unstyled mb-10 color-mid latest_deal_location">
                                    <li><i class="ico lnr lnr-map-marker mr-10"></i>{{$deal->getOwner->address}}</li>
                                    <li><i class="ico lnr lnr-store mr-10"></i>{{$deal->getOwner->name}}</li>
                                </ul>
                            </div>
                            <div class="deal-price pos-r mb-15">
                                <ul class="list-inline m-0">
                                    {{dd($deal->dealType)}}
                                    <li>
                                        <h3 class="price ptb-5 text-left price-sale">
                                            <i class="fa fa-rupee"></i> {{$deal->price}}
                                        </h3>
                                    </li>


                                    <li>
                                        <h3 class="price ptb-5 text-left">
                                            <i class="fa fa-rupee"></i> {{$deal->online_sell_price}}
                                        </h3>
                                    </li>
                                    <li class="float-right" data-toggle="tooltip" data-placement="left" title="Add to Cart"><a class="btn btn-primary deal_addcart"  data-id="{{$deal->id}}" data-type="deal" ><i class="lnr lnr-cart"></i></a> </li>
                                </ul>


                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    @endif

    <section class="section latest-deals-area ptb-30">
        <header class="panel ptb-15 prl-20 pos-r mb-30">
            <h3 class="section-title font-18">{{$listTitle}}</h3>
            <span class="pos-a right-20 pos-tb-center"></span>
        </header>
        <div class="row row-masnory row-tb-20 dealsListData">

        </div>


        <div class="col-sm-12 text-center font-28 color-dark ajaxLoaderAni">
            <i class="fa fa-spinner fa-spin"></i>
        </div>
    </section>

@stop