@extends('site.layout.master')


@section('title', 'Blank Page')

@push('style')

@endpush

@push('scripts')

@endpush

@section('content')
    <div class="row row-rl-10 row-tb-20">
       @include('site.includes.wallet_sidebar')
        <div class="page-content col-sm-8 col-md-9">
            <div class=" deal_single_view">
                <div class="deal-deatails panel myprofile_minhieight">
                    <div class="deal-body p-20">
                        <header class="panel pb-15 prl-0 pos-r mb-20">
                            <h3 class="h-title font-18">Account Summary</h3>
                        </header>
                        <div class="row wallet_summary">
                            <div class="col-md-4 wallet_summary_list">
                                <div class="col-lg-12 bg-danger ">
                                    <h6 class="text-uppercase">Credit</h6>
                                    <h3>{{$accountSummary['totalPoints']}}</h3>
                                </div>
                            </div>
                            <div class="col-md-4 wallet_summary_list">
                                <div class="col-lg-12 bg-blue">
                                    <h6 class="text-uppercase">Withdrawal</h6>
                                    <h3>{{$accountSummary['withdrawalPoints']}}</h3>
                                </div>
                            </div>
                            <div class="col-md-4 wallet_summary_list">
                                <div class="col-lg-12 bg-green">
                                    <h6 class="text-uppercase">TOTAL Balance</h6>
                                    <h3>{{Auth::user()->balance?Auth::user()->balance:0}}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="section store-tabs-area mt-30 wallet_summary_report">
                            <div class="tabs tabs-v1">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs panel" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#purchase" aria-controls="purchase"
                                           role="tab" data-toggle="tab"
                                           aria-expanded="false">MY PURCHASE</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#clubpoint"
                                           aria-controls="clubpoint" role="tab"
                                           data-toggle="tab"
                                           aria-expanded="true">CLUB POINTS</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel"
                                         class="tab-pane ptb-20 active"
                                         id="purchase">
                                        <div class="table-responsive-vertical">
                                            <table class="table table-condensed mb-10 table-mc-light-blue">
                                                <thead class="panel t-uppercase">
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Transaction ID</th>
                                                    <th>Details</th>
                                                    <th>Point</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php $total =   0; $i=0; @endphp
                                                @if($transactionsHistory)
                                                    @foreach($transactionsHistory as $history)
                                                        @if($history['metaType']=='myPurchase')
                                                            @php $total =   $total+$history['amount']; $i++;@endphp
                                                            <tr class="panel alert">
                                                                <td data-title="{{$history['date']}}">{{$history['date']}}</td>
                                                                <td data-title="{{substr($history['transactionId'],5)}}"> {{substr($history['transactionId'],5)}}</td>
                                                                <td data-title="{{isset(json_decode($history['details'])->product_id)?(json_decode($history['details'])->product_id):'NA'}}">{{isset(json_decode($history['details'])->product_id)?(json_decode($history['details'])->product_id):'NA'}}</td>
                                                                <td data-title="{{$history['amount']}}">{{$history['amount']}}</td>
                                                            </tr>
                                                        @endif
                                                        @if($loop->last)
                                                            @if($i!=0)
                                                                {{--<tr class="panel alert">--}}
                                                                    {{--<td colspan="2" ><strong>Total Points</strong></td>--}}
                                                                    {{--<td><strong>{{$total}}</strong></td>--}}
                                                                {{--</tr>--}}

                                                                <tr>
                                                                    <td colspan="3" class="text-right"><strong>Total Points</strong></td>
                                                                    <td><strong>{{$total}}</strong></td>
                                                                </tr>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                                @if($i==0)
                                                    {{--<tr class="panel alert">--}}
                                                        {{--<td colspan="3">--}}
                                                            {{--<div class="text-center valign-middle ptb-20">--}}
                                                                {{--<h6 class="title t-uppercase">No Date Found</h6>--}}
                                                            {{--</div>--}}
                                                        {{--</td>--}}
                                                    {{--</tr>--}}
                                                    <tr>
                                                        <td colspan="4" class="text-center"><strong>You do have any transaction in your
                                                                account</strong></td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                            <div class="type font-12 text-center pb-15">If you need any help or support, please contact us at <a href="mailto:support@dealdaa.com">support@dealdaa.com</a></div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane ptb-20"
                                         id="clubpoint">
                                        <div class="table-responsive-vertical">
                                            <table class="table table-condensed mb-10 table-mc-light-blue">
                                                <thead class="panel t-uppercase">
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Transaction ID</th>
                                                    <th>Details</th>
                                                    <th>Point</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php $total =   0; $i=0; @endphp
                                                @if($transactionsHistory)
                                                    @foreach($transactionsHistory as $history)
                                                        @if(($history['metaType']=='superfreelancerCommission')||($history['metaType']=='freelancerCommission')||($history['metaType']=='clubPoints'))
                                                            @php $total =   $total+$history['amount']; $i++;@endphp
                                                            <tr class="panel alert">
                                                                <td data-title="{{$history['date']}}">{{$history['date']}}</td>
                                                                <td data-title="{{substr($history['transactionId'],5)}}"> {{substr($history['transactionId'],5)}}</td>
                                                                <td data-title="{{isset(json_decode($history['details'])->product_id)?(json_decode($history['details'])->product_id):'NA'}}">{{isset(json_decode($history['details'])->product_id)?(json_decode($history['details'])->product_id):'NA'}}</td>
                                                                <td data-title="{{$history['amount']}}">{{$history['amount']}}</td>
                                                            </tr>
                                                        @endif
                                                        @if($loop->last)
                                                            @if($i!=0)
                                                                {{--<tr class="panel alert">--}}
                                                                {{--<td colspan="2" ><strong>Total Points</strong></td>--}}
                                                                {{--<td><strong>{{$total}}</strong></td>--}}
                                                                {{--</tr>--}}

                                                                <tr>
                                                                    <td colspan="3" class="text-right"><strong>Total Points</strong></td>
                                                                    <td><strong>{{$total}}</strong></td>
                                                                </tr>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                                @if($i==0)
                                                    <tr>
                                                        <td colspan="4" class="text-center"><strong>You do have any transaction in your
                                                                account</strong></td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                            <div class="type font-12 text-center pb-15">If you need any help or support, please contact us at <a href="mailto:support@dealdaa.com">support@dealdaa.com</a></div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop