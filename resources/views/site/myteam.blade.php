@extends('site.layout.master')


@section('title', 'Blank Page')

@push('style')

@endpush

@push('scripts')

@endpush

@section('content')
    @include('site.includes.profile_sidebar')
    <div class="page-content col-sm-8 col-md-9">

        <div class=" deal_single_view">
            <div class="deal-deatails panel myprofile_minhieight">
                <div class="deal-body p-20">
                    <header class="panel pb-15 prl-0 pos-r mb-20">
                        <h3 class="h-title font-18">My Team</h3>
                    </header>
                    <div class="table-responsive-vertical">
                        <table class="mb-10 table table-mc-light-blue" style="border: 1px solid #ddd;">
                            <thead class="panel t-uppercase">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!$myteam->isEmpty())
                                @php $i=1;@endphp
                                @foreach($myteam as $team)
                                    <tr class="panel alert">
                                        <td>{{$i++}}</td>
                                        <td data-title="{{$team->name}}"><a href="javascript:void(0);">{{$team->name}}</a></td>
                                        <td data-title="{{$team->email}}"> {{$team->email}}</td>
                                        <td data-title="{{$team->created_at->format('d,F Y')}}"> {{$team->created_at->format('d,F Y')}} </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="panel alert">
                                    <td colspan="3">
                                        <div class="text-center valign-middle ptb-20">
                                            <h6 class="title mb-15 t-uppercase">You have no refferal in your team list. Invite friends to earn points !</h6>
                                            <ul class="list-inline social-icons social-icons--colored t-center v_shareParent" data-shareUrl="{{route('signup')}}?ref={{getReferalId()}}">
                                                <li class="social-icons__item">
                                                    <a href="#" class="v_sharebtn" title="Facebook" data-type="Facebook"><i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li class="social-icons__item">
                                                    <a href="#" class="v_sharebtn" title="Twitter" data-type="Twitter"><i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li class="social-icons__item">
                                                    <a href="#" class="v_sharebtn" title="Google Plus" data-type="GooglePlus"><i class="fa fa-google-plus"></i></a>
                                                </li>
                                                <li class="social-icons__item">
                                                    <a href="#" class="v_sharebtn" title="Whatsapp" data-type="Whatsapp"><i class="fa fa-whatsapp"></i></a>
                                                </li>
                                            </ul>
                                            {{--<div class="type font-12"><a href="" class="btn btn-rounded btn-xs">Start Shopping</a> </div>--}}
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop