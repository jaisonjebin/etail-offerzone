@extends('site.layout.master')


@section('title', 'Flash Sales')

@push('style')
    <!-- Owl Carousel -->
    <link href="/site/assets/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="/site/assets/vendors/owl-carousel/owl.theme.min.css" rel="stylesheet">
    <!-- Flex Slider -->
    <link href="/site/assets/vendors/flexslider/flexslider.css" rel="stylesheet">
@endpush

@push('scripts')
    <!-- Owl Carousel -->
    <script type="text/javascript" src="/site/assets/vendors/owl-carousel/owl.carousel.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="/site/assets/vendors/flexslider/jquery.flexslider-min.js"></script>

    <!-- Coutdown -->
    <script type="text/javascript" src="/site/assets/vendors/countdown/jquery.countdown.js"></script>


    <script>
        $(function () {
            loadFlashSale();
        });
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() > $(document).height() - 850) {
                loadFlashSale();
            }
        });

        $genesis = 0;
        $page = 2;
        $ajaxPrssing = 0;
        function loadFlashSale() {
            if ($genesis == 0 && $ajaxPrssing==0){
                $ajaxPrssing=1;
                $('.ajaxLoaderAni').slideDown();
                $.ajax({
                    url: '{{route('loadFlashSaleAjax',[''])}}/' + ($page++),
                    type: 'post',
                    success: function (data) {
                        $('.ajaxLoaderAni').slideUp();
                        $ajaxPrssing = 0;
                        if (data == 'genisys') {
                            $genesis = 1;
                            if($page ==2 && $genesis == 1){
                                $('.flashSaleContainer').append('<div class="col-lg-12">Could not find any Deals on this Category</div>');
                            }
                        }
                        else{
                            $('.flashSaleContainer').append(data);
                            reInitPlugins();
                        }
                    }
                });
            }
        }


        function reInitPlugins() {
            $("[data-bg-img]").each(function() {
                var attr = $(this).attr('data-bg-img');
                if (typeof attr !== typeof undefined && attr !== false && attr !== "") {
                    $(this).css('background-image', 'url('+attr+')');
                }
            });

            var countdown_select = $("[data-countdown]");
            countdown_select.each(function(){
                $(this).countdown($(this).data('countdown'))
                    .on('update.countdown', function(e){
                        var format = '%H : %M : %S';
                        if (e.offset.totalDays > 0) {
                            format = '%d Day%!d '+format;
                        }
                        if (e.offset.weeks > 0) {
                            format = '%w Week%!w '+format;
                        }
                        $(this).html(e.strftime(format));
                    });
            }).on('finish.countdown', function(e){
                $(this).html('This offer ha expired!').addClass('disabled');
            });

            var share_action = $('.deal-actions .share-btn');
            share_action.on('click',function(){
                var share_icons = $(this).children('.share-tooltip');
                share_icons.toggleClass('in');
            });
        }
    </script>


@endpush

@section('content')
    <div class="section deals-header-area pb-30">
        <div class="row row-tb-20">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="header-deals-slider owl-slider" data-loop="true" data-autoplay="true"
                     data-autoplay-timeout="10000" data-smart-speed="1000" data-nav-speed="false"
                     data-nav="true" data-xxs-items="1" data-xxs-nav="true" data-xs-items="1" data-xs-nav="true"
                     data-sm-items="1" data-sm-nav="true" data-md-items="1" data-md-nav="true" data-lg-items="1"
                     data-lg-nav="true">

                    @php $i =0;@endphp
                    @foreach($flashSales as $flashSale)
                        @php $banners = json_decode($flashSale->banners);
                        @endphp

                        @foreach($banners as $banner)
                            <div class="deal-single panel item" link="{{route('flashsaleItem',$flashSale->id)}}">
                                <figure class="flashsale-thumbnail" data-bg-img="/uploads/{{$banner}}">
                                </figure>
                            </div>
                            @break
                        @endforeach
                    @endforeach
                </div>


            </div>
        </div>
    </div>

    <section class="section latest-deals-area ptb-30">
        <div class="row row-masnory row-tb-20 flashSaleContainer">

            @foreach($flashSales as $flashSale)
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="deal-single panel">
                        <figure class=" embed-responsive embed-responsive-16by9"
                                data-bg-img="/uploads/{{$flashSale->small_image}}">
                            <div class="ribbon-flahssale is-hidden-xs-down">
                                @if($flashSale->start_time <= \Carbon\Carbon::now())
                                    <div class="ribbon-flah"> On sales now</div>
                                @else
                                    <div class="ribbon-flah" style="background: green;">Coming Soon</div>
                                @endif
                            </div>
                            <ul class="deal-actions top-15 right-15  v_shareParent" data-shareUrl="{{route('flashsaleItem',$flashSale->id)}}?ref={{getReferalId()}}" >
                                @include('site.includes.shareDropDownContents')
                                <li class="like-deal">
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$flashSale->userFavorites()->count()}}" class="@if($flashSale->isFavourited()->count()) removeFromFavoriteBtn @else addtoFavoriteBtn @endif" data-type="flashSaleItem" data-id="{{$flashSale->id}}" >
                                    @if($flashSale->isFavourited()->count())
                                        <img src="/site/assets/images/favorited.png">
                                    @else
                                        <img src="/site/assets/images/favorite.png">
                                    @endif
                                </span>
                                </li>
                                <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="3.5">
                                <img src="/site/assets/images/star.png">
                            </span>
                                </li>
                            </ul>


                        </figure>
                        <div class="featured_flashsalebtn text-right">
                            <ul class="list-inline m-0">
                                <li class="pull-left">{{ number_format((100-(($flashSale->offer_price / ($flashSale->price??1))*100)),0) }}% OFF</li>
                                <li class="">
                                    <a href="{{route('flashsaleItem',$flashSale->id)}}" class="btn btn-primary btn-sm ">
                                        @if($flashSale->start_time <= \Carbon\Carbon::now())
                                            View Sale
                                        @else
                                            Coming Soon
                                        @endif

                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            @endforeach
        </div>


        <div class="col-sm-12 text-center font-28 color-dark ajaxLoaderAni">
            <i class="fa fa-spinner fa-spin"></i>
        </div>
    </section>
@stop