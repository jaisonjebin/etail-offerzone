@extends('site.layout.master')


@section('title', 'Blank Page')

@push('style')

    <style>
        .quantity-label {
            padding: 5px;
            text-align: center;
            border: 1px solid #e4e4e4 !important;
        }
    </style>
@endpush

@push('scripts')

@endpush

@section('content')
    <div class="cart-wrapper">
        <h3 class="h-title mb-30 t-uppercase">My Cart</h3>
        <div class="table-responsive-vertical">
            <table id="cart_list" class="cart-list mb-30 table table-mc-light-blue">
                <thead class="panel t-uppercase">
                <tr>
                    <th>Product name</th>
                    <th>Unit price</th>
                    <th>Quantity</th>
                    <th>Sub total</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($cart as $cartItem)


                    <tr  data-id="{{$cartItem->id}}"  class="panel alert cartItemTr">
                        <td data-title="Product">
                            <div class="media-left is-hidden-sm-down">
                                <figure class="product-thumb">
                                    <img src="{{json_decode($cartItem->meta)?json_decode($cartItem->meta,true)['product_img']:''}}" alt="product">
                                </figure>
                            </div>
                            <div class="media-body valign-middle">
                                <h6 class="title mb-15 t-uppercase"><a href="@if($cartItem->type == 'deal'){{route('dealItem',[$cartItem->product_id])}}@endif">{{$cartItem->name}}</a></h6>
                                {{--<div class="type font-12"><span class="t-uppercase">Type : </span>Women's Cloths</div>--}}
                            </div>
                        </td>
                        <td data-title="Price"><i class="fa fa-rupee"></i> {{$cartItem->price}}</td>
                        <td data-title="Qty" class="quantitys">
                            <input class="quantity-label input-number" type="number" value="{{$cartItem->quantity}}">
                        </td>
                        <td data-title="Total">
                            <div class="sub-total"><i class="fa fa-rupee"></i> <span class="itemTotal" data-price="{{$cartItem->price}}">{{ ((float)$cartItem->price) * ((float)$cartItem->quantity) }}</span></div>
                        </td>
                        <td data-title="">
                            <a class="deleteFromCart">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>



                @endforeach

                </tbody>
            </table>
        </div>
        <div class="cart-price">
            <h5 class="t-uppercase mb-20">Cart total</h5>
            <ul class="panel mb-20">
                <li>
                    <div class="item-name">
                        Subtotal
                    </div>
                    <div class="price">
                        <i class="fa fa-rupee"></i> <span class="grandTotal"></span>
                    </div>
                </li>
                <li>
                    <div class="item-name">
                        Shipping
                    </div>
                    <div class="price">
                        0
                    </div>
                </li>
                <li>
                    <div class="item-name">
                        <strong class="t-uppercase">Order total</strong>
                    </div>
                    <div class="price">
                        <i class="fa fa-rupee"></i> <span class="grandTotal"></span>
                    </div>
                </li>
            </ul>
            <div class="t-right">
                <a href="{{route('user.checkOut')}}" class="btn btn-rounded btn-lg">CHECKOUT</a>
            </div>
        </div>
    </div>
@stop