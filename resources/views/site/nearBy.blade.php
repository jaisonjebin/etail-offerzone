@extends('site.layout.master')


@section('title', 'Blank Page')

@push('style')

@endpush

@push('scripts')
    <script type="text/javascript">
        function initMap(){

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 7,
                center: new google.maps.LatLng(10.5381289,76.8832685),
                // mapTypeId: google.maps.MapTypeId.ROADMAP

            });

            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            var image = {
                url: '/uploads/etailIcon.png', // image is 512 x 512
                scaledSize : new google.maps.Size(32, 32)
            };

            $('.deal_nearby_list').each(function () {
                $this = $(this);
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng($this.data('latitude'), $this.data('longitude')),
                    map: map,
                    icon: image
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent($this.html());
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            });
        }

        $(function () {
            initMap();
        });
    </script>
    {{--<script async defer--}}
            {{--src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATnZNYxBe_MLclLnDYeo6dWXE4iBrFYzY&callback=initMap">--}}
    {{--</script>--}}
@endpush

@section('content')
    <section class="wishlist-area pb-30">
        <div class="container">

            <div class="wishlist-wrapper">
                <h3 class="h-title mb-40 t-uppercase">Near By Deals</h3>
                <div class="row">
                    <div class="col-md-3 deal_nearby ">
                        <div class="col-md-12 scrolling">

                            @foreach($deals as $deal)
                                <div class="col-xs-12 deal_nearby_list" data-latitude="{{$deal->latitude}}"  data-longitude="{{$deal->longitude}}" >
                                    <div style="max-width: 250px">
                                        <h6 class="">
                                            <a href="{{route('dealItem',$deal->id)}}">{{$deal->title}}</a>
                                        </h6>
                                        <div class="media mt-0">
                                            <div class="media-left">
                                                <a href="{{route('dealItem',$deal->id)}}">
                                                    <img class="media-object" src="/uploads/{{$deal->small_image}}" alt="Thumb" width="60">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <ul class="deal-meta list-unstyled mb-10 color-mid latest_deal_location">
                                                    <li><i class="ico lnr lnr-map-marker mr-10"></i>{{$deal->getOwner->address}}</li>
                                                    <li><i class="ico lnr lnr-store mr-10"></i>{{$deal->getOwner->name}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="col-md-9 google_map">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop