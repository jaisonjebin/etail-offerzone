@extends('site.layout.master')


@section('title', 'Blank Page')

@push('style')

@endpush

@push('scripts')

@endpush

@section('content')
    @include('site.includes.profile_sidebar')
    <div class="page-content col-sm-8 col-md-9">
        <div class=" deal_single_view">
            <div class="deal-deatails panel myprofile_minhieight">
                <div class="deal-body p-20">
                    <header class="panel pb-15 prl-0 pos-r mb-20">
                        <h3 class="h-title font-18">My Purchase</h3>
                    </header>
                    <div class="table-responsive-vertical">
                        <table id="cart_list" class="cart-list mb-30 table table-mc-light-blue">
                            <thead class="panel t-uppercase">
                            <tr>
                                <th>Purchases</th>
                                <th>Purchase Id</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orderDetails as $orderDetail)
                                @if($orderDetail->product_type == 'flashsale')
                                    @if($orderDetail->getOrder)
                                        <tr class="panel alert">
                                            <td data-title="Purchase">
                                                <div class="media-left is-hidden-sm-down">
                                                    <figure class="product-thumb">
                                                        <img src="/uploads/{{$orderDetail->getFlashSale->small_image}}" alt="product">
                                                    </figure>
                                                </div>
                                                <div class="media-body valign-middle">
                                                    <h6 class="title mb-15 t-uppercase"><a href="{{route('flashsaleItem',$orderDetail->getFlashSale->id)}}">{{$orderDetail->getFlashSale->title}}</a></h6>
                                                    <div class="type font-12"><span class="t-uppercase">Qty : </span>{{$orderDetail->quatity}}</div>
                                                    <div class="type font-12"><span class="t-uppercase">Status : </span>
                                                        <span>
                                                            @if($orderDetail->getOrder->order_status)
                                                                <label class="label" style="background-color: green">Purchase Complete</label>
                                                            @else
                                                                @if($orderDetail->getOrder->expiry >= \Carbon\Carbon::now())
                                                                    @if($orderDetail->getOrder->payment_status == 1)
                                                                        <a class="label label-warning" href="{{route('user.viewOrder', $orderDetail->order_id)}}">Pending</a>
                                                                    @else
                                                                        <a class="label label-warning"  href="{{route('user.viewOrder', $orderDetail->order_id)}}">Pending Payment. Pay NOW</a>
                                                                    @endif

                                                                @else
                                                                    <span class="label label-warning">Expired</span>
                                                                @endif

                                                            @endif

                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-title="Purchase ID">
                                                <a class="btn btn-default btn-xs" href="{{route('user.viewOrder', $orderDetail->order_id)}}">
                                                    ODR{{ str_pad($orderDetail->order_id, 5, '0', STR_PAD_LEFT)}}
                                                </a><br>
                                                <a href="{{route('user.viewOrder', $orderDetail->order_id)}}">
                                                    Price : <i class="fa fa-rupee"></i> {{$orderDetail->price}}
                                                </a>

                                            </td>
                                            <td data-title="Date">
                                                {{date('M d, Y',strtotime($orderDetail->getOrder->order_date))}}
                                            </td>
                                        </tr>

                                    @endif
                                @else
                                    @if($orderDetail->getOrder)
                                        <tr class="panel alert">
                                            <td data-title="Purchase">
                                                <div class="media-left is-hidden-sm-down">
                                                    <figure class="product-thumb">
                                                        <img src="/uploads/{{$orderDetail->getDeal->small_image}}" alt="product">
                                                    </figure>
                                                </div>
                                                <div class="media-body valign-middle">
                                                    <h6 class="title mb-15 t-uppercase"><a href="{{route('dealItem',$orderDetail->getDeal->id)}}">{{$orderDetail->getDeal->title}}</a></h6>
                                                    <div class="type font-12"><span class="t-uppercase">Qty : </span>{{$orderDetail->quatity}}</div>
                                                    <div class="type font-12">
                                                        <span class="t-uppercase">Status : </span>
                                                        <span>
                                                            @if($orderDetail->getOrder->order_status)
                                                                    <label class="label" style="background-color: green">Purchase Complete</label>
                                                                @else
                                                                    @if($orderDetail->getOrder->expiry >= \Carbon\Carbon::now())
                                                                        @if($orderDetail->getOrder->payment_status == 1)
                                                                            <a class="label label-warning" href="{{route('user.viewOrder', $orderDetail->order_id)}}">Pending</a>
                                                                        @else
                                                                            <a class="label label-warning"  href="{{route('user.viewOrder', $orderDetail->order_id)}}">Pending Payment. Pay NOW</a>
                                                                        @endif

                                                                    @else
                                                                        <span class="label label-danger">Expired</span>
                                                                    @endif

                                                                @endif

                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-title="Purchase ID">
                                                <a class="btn btn-default btn-xs" href="{{route('user.viewOrder', $orderDetail->order_id)}}">
                                                    ODR{{ str_pad($orderDetail->order_id, 5, '0', STR_PAD_LEFT)}}
                                                </a><br>
                                                <a href="{{route('user.viewOrder', $orderDetail->order_id)}}">
                                                    Price : <i class="fa fa-rupee"></i> {{$orderDetail->price}}
                                                </a>

                                            </td>
                                            <td data-title="Date">
                                                {{date('M d, Y',strtotime($orderDetail->getOrder->order_date))}}
                                            </td>
                                        </tr>
                                    @endif
                                @endif
                            @endforeach

                            @if($orderDetails->isEmpty())
                                <tr class="panel alert">
                                    <td colspan="3">
                                        <div class="text-center valign-middle pt-20">
                                            <h6 class="title mb-15 t-uppercase">You have no items in your purchase. Start adding!</h6>
                                            <div class="type font-12"><a href="{{route('home')}}" class="btn btn-rounded btn-xs">Start Shopping</a> </div>
                                        </div>
                                    </td>

                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop