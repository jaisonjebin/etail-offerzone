
<!--home About-->
<section class=" home-about">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-xl-4 col-md-4 col-sm-6">
                    <h4>ABOUT US</h4>
                    <p>“We are Dealdaa Digital Trading and Services Private Limited here to begin a revolution in the retail industry by providing a platform "Etailoffer" where retail business owners can benefit from the latest innovations in the digital media and advertising with our creative ideas for effective customer targeting and marketing strategies. This is an e-tailing (retail e-commerce) platform, where touch and feel experience of retail shop will be combined with convenience of e-commerce. Users will benefit further with exclusive deals, offers</p>
                    <a href="{{route('about')}}" class="float-right">Read More</a>
                </div>
                <div class="col-lg-4 col-xl-4 col-md-4 col-sm-6 link-block">
                    <h4>QUICK LINKS</h4>
                    <ul class="list-group ">
                        <li class="list-group-item" > <a href="{{route('mertchant.signin')}}"  title=""> <i class="fa fa-angle-double-right" aria-hidden="true"> </i>MERCHANT LOGIN</a></li>
                        <li class="list-group-item" > <a href="{{route('freelancer.signin')}}"  title=""> <i class="fa fa-angle-double-right" aria-hidden="true"> </i>FREELANCER LOGIN</a></li>
                        {{--<li class="list-group-item" > <a href="freelancer.php"  title=""> <i class="fa fa-angle-double-right" aria-hidden="true"> </i>BECOME A FREELANCER</a></li>--}}
                        {{--<li class="list-group-item" > <a href="prize-drawterms.php" title=""> <i class="fa fa-angle-double-right" aria-hidden="true"> </i>PRIZE DRAW TERMS</a></li>--}}
                        <li class="list-group-item" > <a href="{{route('termsandconditions')}}" title=""> <i class="fa fa-angle-double-right" aria-hidden="true"> </i>TERMS AND CONDITIONS</a></li>
                        <li class="list-group-item" > <a href="{{route('privacy_policy')}}" title=""> <i class="fa fa-angle-double-right" aria-hidden="true"> </i>PRIVACY POLICY</a></li>
                        <li class="list-group-item" > <a href="{{route('faq')}}" title=""> <i class="fa fa-angle-double-right" aria-hidden="true"> </i>FAQs</a></li>
                        {{--<li class="list-group-item" > <a href="testimonials.php" title=""> <i class="fa fa-angle-double-right" aria-hidden="true"> </i>TESTIMONIALS</a></li>--}}
                    </ul>
                </div>
                <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12 getin-touch">
                    <h4>GET IN TOUCH</h4>
                    <form class="contactform1" method="post">
                        <div class="col-9 alert"></div>
                        @csrf
                        <div class="form-group">
                            <input id="j_name" name="name" placeholder="Name" type="text" class="form-control getin-touch-form"  aria-describedby="text">
                        </div>
                        <div class="form-group">
                            <input type="email" id="j_email1" name="email" placeholder="Email" class="form-control getin-touch-form" aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <input type="tel" id="j_tel1" name="subject" class="form-control getin-touch-form"  aria-describedby="Number" placeholder="Mobile Number">
                        </div>
                        <div class="form-group">
                            <textarea id="j_message1" name="message" class="form-control getin-touch-form" rows="3" placeholder="MESSAGE"></textarea>
                        </div>
                        <a href="javascript:void(0);" class="btn btn-primary float-right j_contactformsubmit1">SUBMIT</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Footer-->

<div class="container-fluid footer" style="background-color:#fff;">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-xl-4 col-md-4 col-sm-6  text-center">
                <h4>Contact Us</h4>
                <img src="/site/assets/images/footer-logo.png" class="img-fluid ">
                <p>Calicut Airport Jn.
                    Kerala<br>
                    info@dealdaa.com</p>
            </div>
            <div class="col-lg-4 col-xl-4 col-md-4 col-sm-6 ">
                <h4>Support</h4>
                <ul class="list-group text-center">
                    <li class="list-group-item" > <a href="{{route('faq')}}"  title="">Help Center</a></li>
                    <li class="list-group-item" > <a href="{{route('faq')}}"  title=""> Get Started</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-xl-4 col-md-4 col-sm-12">
                <h4 class="pb-20">Follow us on</h4>
                <ul class="list-inline social-media  text-center"  >
                    <li class="list-inline-item"> <a href="https://www.facebook.com/etailoffer/" title="facebook" target="_blank"><i class="fa fa-facebook-f" style="color:#4979db"></i></a> </li>
                    <li class="list-inline-item"> <a href="https://www.instagram.com/etailoffer/" title="instagram" target="_blank"> <i class="fa fa-instagram" style="color:#9a4518"></i> </a> </li>
                    <li class="list-inline-item" > <a href="" title="twitter" target="_blank"><i class="fa fa-twitter" style="color:#33c3ff"></i></a> </li>
                    <li class="list-inline-item"> <a href="" title="google-plus" target="_blank"><i class="fa fa-google-plus" style="color:#ff121c"></i></a> </li>
                </ul>
                <h6 class="pb-5 pt-20 text-center">Have you tried our mobile app?</h6>
                <ul class="list-inline text-center footer_mobileapps">
                    <li><a href="" target="_blank"><img src="/site/assets/images/playstore.png" class="img-responsive"></a> </li>
                    <li><a href="" target="_blank"><img src="/site/assets/images/applestore.png" class="img-responsive"></a> </li>
                </ul>
            </div>
        </div>
        <p class="pb-20">© {{date('Y')}} Etailoffer. All Rights Reserved. </p>
    </div>
</div>

<!--location-->
<div class="modal fade modal-fullscreen select_location" id="select-location-fullscreen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="text-center pt-15">
                    <h4>Pick a city</h4>
                    <p>To find awesome offers around you</p>
                    <div class="input-group search_location">
                        <input type="hidden" id="latitude" name="latitude" value="">
                        <input type="hidden" id="longitude" name="longitude" value="">
                        <input type="text" class="form-control" placeholder="Enter your city name" name="locationText" id="googlePlaces">
                        <span class="input-group-btn">
                            <button class="btn btn-danger setLocationBtn" type="button"><i class="fa fa-map-o"></i> Set Location</button>
                      </span>
                    </div>
                    <div class="top_cities">
                        <h4>Top Cities</h4>
                        <ul class="list-inline">
                            <li><a class="btn btn-location" onclick="selectLocationOption('11.1052619', '75.9163071', 'Malappuram')">Malappuram</a> </li>
                            <li><a class="btn btn-location" onclick="selectLocationOption('11.1531749', '75.9107995', 'Kondotty')">Kondotty</a> </li>
                            <li><a class="btn btn-location" onclick="selectLocationOption('11.1662445', '75.8618275', 'Ramanattukara')">Ramanattukara</a> </li>
                            <li><a class="btn btn-location" onclick="selectLocationOption('11.2561387', '75.6707252', 'Kozhikkode')">Kozhikkode</a> </li>
                            <li><a class="btn btn-location" onclick="selectLocationOption('9.9710364', ',76.2382523', 'Ernakulam')">Ernakulam</a> </li>
                            <li><a class="btn btn-location" onclick="selectLocationOption('8.4869447', '76.9506107', 'Thiruvananthapuram')">Thiruvananthapuram</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--location-->

