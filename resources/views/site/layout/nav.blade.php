
<noscript>
    <div class="noscript alert-error">
        For full functionality of this site it is necessary to enable JavaScript. Here are the <a href="http://www.enable-javascript.com/" target="_blank">
            instructions how to enable JavaScript in your web browser</a>.
    </div>
</noscript>


<!-- Preloader -->
<div id="preloader" class="preloader">
    <div class="loader-cube">
        <div class="loader-cube__item1 loader-cube__item"></div>
        <div class="loader-cube__item2 loader-cube__item"></div>
        <div class="loader-cube__item4 loader-cube__item"></div>
        <div class="loader-cube__item3 loader-cube__item"></div>
    </div>
</div>
<!-- End Preloader -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- WRAPPER                                   -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->

<div id="pageWrapper" class="page-wrapper">
    <!-- –––––––––––––––[ HEADER ]––––––––––––––– -->
    <header id="mainHeader" class="main-header">

        <!-- Top Bar -->
        <div class="top-bar bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4 is-hidden-sm-down">
                        <ul class="nav-top nav-top-left list-inline t-left">
                            <li><a href="{{route('contact')}}"><i class="fa fa-support"></i>Customer Assistance</a>
                            </li>
                            <li><a href="{{route('contact')}}"><i class="fa fa-inr"></i>Refer & Earn</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <ul class="nav-top nav-top-right list-inline t-xs-center t-md-right">

                            <li>
                                <a onclick="getLocation()" title="Get Current Device Location">
                                    <i class="fa fa-location-arrow"></i>
                                </a>
                            </li>

                            <li>
                                <a data-toggle="modal" data-target="#select-location-fullscreen" title="Select Location">
                                    <i class="fa fa-map-marker"></i><span class="currentLocation">{{Session::get('locationText')}}</span>
                                </a>
                            </li>
                            <li class="mobile_active"><a href="{{route('contact')}}"><i class="fa fa-inr"></i>Refer & Earn</a></li>
                            @auth
                                <li class="mobile_right">

                                <a href="javascript:void(0);"> <i class="fa fa-user"></i> Hi {{Auth::user()->name}} <i class="fa fa-caret-down"></i></a>
                                <ul>
                                    <li>
                                        <a href="{{route('user.myProfile')}}">
                                            <i class="lnr lnr-user"></i>My Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('user.myPurchase')}}">
                                            <i class="lnr lnr-tag"></i>My Purchase
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('user.myTeam')}}">
                                            <i class="lnr lnr-users"></i>My Team
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('user.myNotifications')}}">
                                            <i class="lnr lnr-alarm"></i>Notification
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('user.myRewards')}}">
                                            <i class="lnr lnr-magic-wand"></i>Rewards
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <i class="lnr lnr-power-switch"></i>Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endauth
                            @guest
                                <li><a href="{{route('signup')}}"><i class="fa fa-user"></i>Sign Up</a>
                                </li>
                                <li><a href="{{route('signin')}}"><i class="fa fa-lock"></i>Sign In</a>
                                </li>
                            @endguest


                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Top Bar -->

        <!-- Header Header -->
        <div class="header-header bg-white">
            <div class="container">
                <div class="row row-rl-0 row-tb-20 row-md-cell">
                    <div class="brand col-md-3 t-xs-center t-md-left valign-middle">
                        <a href="/" class="logo">
                            <img src="/site/assets/images/logo.png" alt="" width="220">
                        </a>
                    </div>
                    <div class="header-search col-md-9">
                        <div class="row row-tb-10 ">
                            <div class="col-sm-8 search_form_mobile">
                                <form class="search-form">
                                    <div class="input-group">
                                        <input type="text" class="form-control input-lg search-input" placeholder="Enter Keywork Here ..." required="required">
                                        <div class="input-group-btn">
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <button type="submit" class="btn btn-lg btn-search btn-block">
                                                        <i class="fa fa-search font-16"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-4 t-xs-center t-md-right">
                                <!--<div class="header-cart">-->
                                <!--<a href="nearby.php" title="Nearby">-->
                                <!--<img src="/site/assets/images/map.png" class="img-responsive">-->
                                <!--</a>-->
                                <!--</div>-->
                                <div class="header-cart">
                                    <a href="{{route('user.myFavorites')}}" title="Wish list">
                                        <span class="icon lnr lnr-heart font-30"></span>
                                        <div><span class="cart-number">{{isset($favCount)?$favCount:0}}</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="header-cart">
                                    <a href="{{route('user.myWallet')}}" title="Wallet">
                                        <img src="/site/assets/images/wallet.png" class="img-responsive">
                                    </a>
                                </div>
                                <div class="header-cart" title="Cart">
                                    <a href="{{route('cart')}}">
                                        <span class="icon lnr lnr-cart"></span>
                                        <div><span class="cart-number" id="cart-number">{{isset($cartCount)?$cartCount:0}}</span>
                                        </div>
                                        <!--<span class="title">Cart</span>-->
                                    </a>
                                </div>
                                <div class="header-cart search_icon">
                                    <a href="javascript:void(0);" title="Search" class="search_btn">
                                        <img src="/site/assets/images/search_icon.png" class="img-responsive">
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Header -->

        <!-- Header Menu -->
        <div class="header-menu bg-blue">
            <div class="container">
                <nav class="nav-bar">
                    <div class="nav-header">
                            <span class="nav-toggle" data-toggle="#header-navbar">
		                        <i></i>
		                        <i></i>
		                        <i></i>
		                    </span>
                    </div>
                    <div id="header-navbar" class="nav-collapse">
                        <ul class="nav-menu">
                            <li class="{{isActiveRoute('home')?'active':(isActiveRoute('user.showDashboard')?'active':'')}}">
                                <a href="{{route('home')}}">Home</a>
                            </li>
                            <li class="{{isActiveRoute('flashsaleItemList')}}">
                                <a href="{{route('flashsaleItemList')}}">Flash Sale</a>
                            </li>
                            <li class="{{isActiveRoute('dealFrontist')?'active':(isActiveRoute('dealItem')?'active':'')}}">
                                <a href="{{route('dealFrontist')}}">Deals</a>
                            </li>
                            <li class="{{isActiveRoute('couponFrontist')}}">
                                <a href="{{route('couponFrontist')}}">Coupons</a>

                            </li>
                            <li class="{{isActiveRoute('storeFrontist')}}">
                                <a href="{{route('storeFrontist')}}">Stores</a>

                            </li>
                            <li class="{{isActiveRoute('contact')}}">
                                <a href="{{route('contact')}}">Contact Us</a>

                            </li>

                        </ul>
                    </div>

                    <div class="nav-menu nav-menu-fixed">
                        <a href="{{route('nearBy')}}"><i class="lnr lnr-map-marker"></i> Near By</a>
                    </div>
                </nav>
            </div>
        </div>
        <!-- End Header Menu -->

    </header>