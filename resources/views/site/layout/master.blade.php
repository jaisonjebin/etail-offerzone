<!DOCTYPE html>
<html lang="en" dir="ltr" class="no-js">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Etailoffer - @yield('title','Retail offers around you')</title>
    {{--<title>VTOQA</title>--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="">
    <meta name="robots" content="index, follow">
    <meta name="author" content="">
    <link rel="apple-touch-icon" href="/site/assets/images/favicon/apple-touch-icon.png">
    <link rel="icon" href="/site/assets/images/favicon/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600" rel="stylesheet">
    <link href="/site/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/site/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/site/assets/vendors/linearicons/css/linearicons.css" rel="stylesheet">
    <link href="/site/assets/css/toastr.min.css" rel="stylesheet" type="text/css" />
    <link href="/site/assets/css/animate.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.8/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />

    @stack('style')

<!-- Template Stylesheet -->
    <link href="/site/assets/css/base.css" rel="stylesheet">
    <link href="/site/assets/css/style.css?v=1.20" rel="stylesheet">

    <link href="/site/assets/fonts/flaticon/flaticon.css" rel="stylesheet">
    <style>
        .pac-container {
            z-index: 1100;
        }
    </style>
</head>

<body id="body" class="wide-layout preloader-active">

@include('site.layout.nav')

<!-- –––––––––––––––[ HEADER ]––––––––––––––– -->

<!-- –––––––––––––––[ PAGE CONTENT ]––––––––––––––– -->
<main id="mainContent" class="main-content">
    <div class="page-container {{Route::currentRouteName()=='contact'?'pt-40':'pt-40'}}">
        <div class="container">

        @yield('content')

        </div>
    </div>

</main>

<!-- –––––––––––––––[ FOOTER ]––––––––––––––– -->
@include('site.layout.footer')
<!-- –––––––––––––––[ END FOOTER ]––––––––––––––– -->

<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- END WRAPPER                               -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<!-- BACK TO TOP                               -->
<!-- ––––––––––––––––––––––––––––––––––––––––– -->
<div id="backTop" class="back-top is-hidden-sm-down">
    <i class="fa fa-angle-up" aria-hidden="true"></i>
</div>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
<script src="/site/assets/js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="/site/assets/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.8/dist/sweetalert2.min.js" type="text/javascript"></script>
<script src="/site//assets/js/toastr.min.js" type="text/javascript"></script>
<!-- Modernizer JS -->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADPiaU7KWZ6B1zpN6Ps1IrjDpP1IUduRg&libraries=places" ></script>

@stack('scripts')

<script src="/site/assets/vendors/modernizr/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="/site/assets/js/main.js"></script>
<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    @if(Session::has('type'))
        toastr["{{Session::get('type')}}"]("{!!Session::get('text')!!}");
    @endif
</script>
<script>
    @auth
    $(document).on('click','.addtoFavoriteBtn',function () {
        $type = $(this).attr('data-type');
        $id = $(this).attr('data-id');
        $this= $(this);

        $.ajax({
            url:'{{route('user.addtoFavorite',['',''])}}/'+$type+'/'+$id,
            dataType:'json',
            success:function (data) {
                toastr.info(data.msg);
                $this.html('<img src="/site/assets/images/favorited.png">');
                $this.addClass('removeFromFavoriteBtn');
                $this.removeClass('addtoFavoriteBtn');
                $this.attr('data-original-title',parseInt($this.attr('data-original-title'))+1);
            }
        });

    });




    $(document).on('click','.removeFromFavoriteBtn',function () {
        $type = $(this).attr('data-type');
        $id = $(this).attr('data-id');
        $this= $(this);

        $.ajax({
            url:'{{route('user.removeFavorite',['',''])}}/'+$type+'/'+$id,
            dataType:'json',
            success:function (data) {
                toastr.info(data.msg);
                $this.html('<img src="/site/assets/images/favorite.png">');
                $this.addClass('addtoFavoriteBtn');
                $this.removeClass('removeFromFavoriteBtn');
                $this.attr('data-original-title',parseInt($this.attr('data-original-title'))-1);
            }
        });

    });

    $('.edit_review').on('click',function () {
        $('#reviewForm').find('[for="rating-'+$('.edit_review').data('rating')+'"]').trigger('click');
        $('#reviewForm').find('[name=description]').val($('.edit_review').data('description'));
    });

    @endauth

    @guest
    $('.addtoFavoriteBtn').on('click',function () {
        toastr.info('Please Login to add to Favorites');
    });
    @endguest

    //social share function
    $(document).on('click','.v_sharebtn',function(){

        $url = $(this).closest('.v_shareParent').attr('data-shareUrl');
        console.log($url);

        if($(this).attr('data-type') == 'Facebook'){
            window.open('https://www.facebook.com/sharer/sharer.php?u='+$url, '_blank');
        }
        else if($(this).attr('data-type') == 'Twitter'){
            window.open('http://twitter.com/share?text=Checkout this Deal on DealDaa.com&url='+$url+'&hashtags=DealDaa', '_blank');
        }
        else if($(this).attr('data-type') == 'GooglePlus'){
            window.open('https://plus.google.com/share?url='+$url, '_blank');
        }
        else if($(this).attr('data-type') == 'Whatsapp'){
            window.open('whatsapp://send?text='+$url, '_blank');
        }

    });

    //cart function
    $(document).on('click','.deal_addcart',function () {
        $id = $(this).data('id');
        $type = $(this).attr('data-type');
        $(this).addClass('animated tada').one("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function(){
            $(this).removeClass('animated tada');
        });

        $count = parseInt($('#cart-number').html());
        $('#cart-number').html('' + ($count+1));

        $.ajax({
            context:$(this),
            url:'{{route('user.addCart',['',''])}}'+'/'+$id+'/'+$type,
            dataType:'json',
            success:function (data) {
                toastr.success(data.msg);
                if($(this).attr('data-url')){
                   window.location = $(this).attr('data-url');
                }
            },
            error:function(a,b,c){
                if(a.status == '401'){
                    toastr.info("Please Login to continue");
                    $(this).removeClass('animated tada');
                }
                else{
                    toastr.error(a.responseJSON.msg);
                }
            }
        });


    });


    $(document).on('keyup','.input-number',function () {
        $tr = $(this).closest('tr');
        $id = $(this).closest('tr').data('id');
        $count = parseInt($(this).closest('.quantitys').find('.input-number').val());
        if($count > 0) {
            $.ajax({
                context: $(this),
                url: '{{route('user.countCart',['',''])}}' + '/' + $id + '/' + $count,
                dataType: 'json',
                success: function (data) {

                    $(this).closest('tr').find('.itemTotal').html($count * ($(this).closest('tr').find('.itemTotal').attr('data-price')));
                    calculateGrandTotal();
                    if (data.type == 'deleted') {
                        $tr.fadeOut();
                        $count = parseInt($('#cart-number').html());
                        $('#cart-number').html('' + ($count - 1));
                        toastr.success(data.msg);
                    }
                    else {

                    }
                }
            });
        }
    });

    $(document).on('click','.deleteFromCart',function () {
        $tr = $(this).closest('tr');
        $id = $(this).closest('tr').data('id');
        $count = 0;
        $.ajax({
            context:$(this),
            url:'{{route('user.countCart',['',''])}}'+'/'+$id+'/'+$count,
            dataType:'json',
            success:function (data) {

                $(this).closest('tr').find('.itemTotal').html(0);
                calculateGrandTotal();
                if(data.type=='deleted')
                {
                    $tr.fadeOut();
                    $count = parseInt($('#cartCount').html());
                    $('#cart-number').html('' + ($count-1));
                    toastr.success(data.msg);
                }
                if(($('#cart-number').html()) == '0'){
                    $('.totalTable').html('<tr> <td colspan="4" class="text-center py-5"> <h3>Empty Cart List</h3> <p>You have no items in your cart list. Start adding!</p> </td> </tr>');
                    $('.checkoutBtnContainer').hide();
                }
            }
        });
    });

    $(function ()
    {
        calculateGrandTotal();
    });
    function calculateGrandTotal() {
        $grandTotal= 0;
        $('.itemTotal').each(function () {
            $grandTotal = $grandTotal + parseFloat($(this).html());
        });
        $('.grandTotal,.allSubTotal').html($grandTotal);
    }
</script>
<script>
    $('.j_contactformsubmit1').on('click',function () {
        if(($("#j_name1" ).val()=='')||($("#j_email1" ).val()=='')||($("#j_message1" ).val()==''))
        {
            toastr.error('Please fill all fields');
            $('.contactform1 .alert').removeClass('alert-danger');
            $('.contactform1 .alert').removeClass('alert-success');
            $('.contactform1 .alert').addClass('alert-danger');
            $('.contactform1 .alert').html('Please fill all fields')
        }
        else {
            $.ajax({
                url: '{{route('contact.form')}}',
                type: 'POST',
                data: $('.contactform1').serialize(),
                dataType: 'json',
                success: function (data) {

                    if (data.type == 'success')
                    {
                        $('.contactform1').find('input').val('');
                        $('.contactform1').find('textarea').val('');
                        $('.getin-touch').html('');
                        $('.getin-touch').html('<div class="col-9 alert alert-success">Message send successfully.We will contact you soon</div>');
                    }
                    else {
                        $('.getin-touch').prepend('<div class="col-9 alert alert-danger">Something went wrong</div>');
                    }
                },
                error: function (response, b, c) {
                    $.each(JSON.parse(response.responseText)['msg'], function (key, val) {
                        $('.getin-touch').prepend('<div class="col-9 alert alert-danger">'+val+'</div>');
                    });
                }
            });
        }
    });


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>

<script>
    var x = document.getElementById("geoLoction");
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }
    function showPosition(position) {
        setLocation( position.coords.latitude, position.coords.longitude);
    }


    $(function () {
        //getLocation();
        @if(!Session::get('locationText'))
            getLocation();
        @endif
        initAutocomplete();
        //setLocation(0,0,'');
    });
    
    function selectLocationOption(lat, long, name='') {
        setLocation(lat,long,name);
        $('#select-location-fullscreen').modal('hide');
    }

    function setLocation(lat, long, name='') {
        $.ajax({
            url:'{{route('changeCurrentLocation')}}',
            type:'post',
            data:{
                latitude : lat,
                longitude: long,
                locationText: name
            },
            success:function (data) {
                $('.currentLocation').html(data.locationText);
                location.reload();
            }
        });

    }

    var searchBox;
    var inputPlace;
    function initAutocomplete() {

        // Create the search box and link it to the UI element.
        inputPlace = document.getElementById('googlePlaces');
        searchBox = new google.maps.places.SearchBox(inputPlace);

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            takeLocation();
        });

    }

    function takeLocation(){

        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }
        else{
            document.getElementById('latitude').value = places[0].geometry.location.lat();
            document.getElementById('longitude').value = places[0].geometry.location.lng();
        }
    }

    $('.setLocationBtn').on('click',function () {
        setLocation($('#latitude').val(), $('#longitude').val(), $('#googlePlaces').val());
        $('#select-location-fullscreen').modal('hide');
    });


</script>


</body>
</html>
