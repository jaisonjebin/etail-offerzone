@extends('site.layout.master')


@section('title', 'Blank Page')

@push('style')

@endpush

@push('scripts')
<script>
    $('.removeFavorite').on('click', function () {

        $type = $(this).attr('data-type');
        $id = $(this).attr('data-id');

        $.ajax({
            context: $(this),
            url: '{{route('user.removeFavorite',['',''])}}/' + $type + '/' + $id,
            dataType: 'json',
            success: function (data) {
                toastr.info(data.msg);
                $(this).closest('.favoriteBlock').slideUp();
            }
        })
    });
</script>
@endpush

@section('content')
    <section class="wishlist-area pb-30">
        <div class="container">
            <div class="wishlist-wrapper">
                <h3 class="h-title mb-40 t-uppercase">My Wishlist</h3>
                <table id="cart_list" class="wishlist">
                    <tbody>
                    @php
                        $i = 0;
                    @endphp
                    @foreach($favo['flashSale'] as $flashSale)
                        @php
                            $i++;
                        @endphp
                        <tr class="panel alert">
                            <td class="col-sm-8 col-md-9">
                                <div class="media-left is-hidden-sm-down">
                                    <figure class="product-thumb">
                                        <img src="/uploads/{{$flashSale->small_image}}" alt="product" width="120" height="120">
                                    </figure>
                                </div>
                                <div class="media-body valign-middle">
                                    <h5 class="title mb-5 t-uppercase"><a href="#">{{$flashSale->title}}</a></h5>
                                    <ul class="list-inline mb-15 price_rages">
                                        <li>
                                            <h5 class="price">
                                                <span class="price-sale"><i class="fa fa-rupee"></i>{{$flashSale->price}}</span>
                                            </h5>
                                        </li>
                                        <li>
                                            <h5 class="price">
                                                <i class="fa fa-rupee"></i>{{$flashSale->offer_price}}
                                            </h5>
                                        </li>
                                        <li>
                                            <h5 class="price">
                                                <span class="price-save">{{ ceil((($flashSale->price-$flashSale->offer_price)/($flashSale->price??1))*100) }}% OFF</span>
                                            </h5>
                                        </li>
                                    </ul>
                                    <button class="btn btn-rounded btn-sm mt-15 is-hidden-sm-up">Add To Cart</button>
                                </div>
                            </td>
                            <td class="col-sm-3 col-md-2 is-hidden-xs-down">
                                <a class="btn btn-rounded btn-sm" href="{{route('flashsaleItem',$flashSale->id)}}">View Sale</a>
                            </td>
                            <td class="col-sm-1">
                                <button data-type="flashSaleItem" data-id="{{$flashSale->id}}" type="button" class="removeFavorite close pr-xs-0 pr-sm-10" data-dismiss="alert" aria-hidden="true">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    @foreach($favo['dealItem'] as $deal)

                        @php
                            $i++;
                        @endphp
                        <tr class="panel alert">
                            <td class="col-sm-8 col-md-9">
                                <div class="media-left is-hidden-sm-down">
                                    <figure class="product-thumb">
                                        <img src="/uploads/{{$deal->small_image}}" alt="product" width="120" height="120">
                                    </figure>
                                </div>
                                <div class="media-body valign-middle">
                                    <h5 class="title mb-5 t-uppercase"><a href="#">{{$deal->title}}</a></h5>
                                    <ul class="list-inline mb-15 price_rages">
                                        <li>
                                            <h5 class="price">
                                                <span class="price-sale"><i class="fa fa-rupee"></i>{{$deal->price}}</span>
                                            </h5>
                                        </li>
                                        <li>
                                            <h5 class="price">
                                                <i class="fa fa-rupee"></i>{{$deal->offer_price}}
                                            </h5>
                                        </li>
                                        <li>
                                            <h5 class="price">
                                                <span class="price-save">{{ ceil((($deal->price-$deal->offer_price)/($deal->price??1))*100) }}% OFF</span>
                                            </h5>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td class="col-sm-3 col-md-2 is-hidden-xs-down">
                                <button class="btn btn-rounded btn-sm deal_addcart" data-id="{{$deal->id}}" data-type="deal">Add To Cart</button>
                            </td>
                            <td class="col-sm-1">
                                <button data-type="dealItem" data-id="{{$deal->id}}" type="button" class="removeFavorite close pr-xs-0 pr-sm-10" data-dismiss="alert" aria-hidden="true">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach

                    @if($i == 0)
                        <tr class="panel alert">
                            <td colspan="4" class="text-center">
                                <div class="type font-12"><a href="javascript:void(0);" class="btn btn-rounded btn-xs">You have no items in your favorite. Start adding!</a> </div>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@stop