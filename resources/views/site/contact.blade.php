@extends('site.layout.master')


@section('title', 'Contact Us')

@push('style')
    <style>
        .loader_main{
            position:absolute;
            width:100%;
            height:100%;
            background-color: #ccc;
            opacity: 0.8;
        }
        .loader_main_overlay{
            position:absolute;
            width:10%;
            height:10%;
            background-image:url('/site/loader.gif');
            background-color: #ccc;
            opacity: 0.8;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $('.j_contactformsubmit').on('click',function () {
            console.log($("#j_name").val());
            console.log($("#j_email").val());
            console.log($("#j_message").val());
            if(($("#j_name" ).val()=='')||($("#j_email" ).val()=='')||($("#j_message" ).val()==''))
            {
                toastr.error('Please fill all fields');
                $('.contactform .alert').removeClass('alert-danger');
                $('.contactform .alert').removeClass('alert-success');
                $('.contactform .alert').addClass('alert-danger');
                $('.contactform .alert').html('Please fill all fields')
            }
            else {
                $('.contactform').fadeIn();
                $.ajax({
                    url: '{{route('contact.form')}}',
                    type: 'POST',
                    data: $('.contactform').serialize(),
                    dataType: 'json',
                    success: function (data) {
                        $('.contactform').fadeOut();
                        if (data.type == 'success')
                        {
                            $('.contactform .alert').removeClass('alert-danger');
                            $('.contactform .alert').removeClass('alert-success');
                            $('.contactform .alert').addClass('alert-success');
                            $('.contactform .alert').html('');
                            $('.contactform').find('input').val('');
                            $('.contactform').find('textarea').val('');
                            $('.contactform .alert').html('Message send successfully.We will contact you soon');
                        }
                        else {
                            $('.contactform .alert').removeClass('alert-danger');
                            $('.contactform .alert').removeClass('alert-success');
                            $('.contactform .alert').addClass('alert-danger');
                            $('.contactform .alert').html('');
                            $('.contactform .alert').html('Something went wrong.');
                        }
                    },
                    error: function (response, b, c) {
                        $('.contactform').fadeOut();
                        $('.contactform .alert').removeClass('alert-danger');
                        $('.contactform .alert').removeClass('alert-success');
                        $('.contactform .alert').addClass('alert-danger');
                        $('.contactform .alert').html('');
                        $.each(JSON.parse(response.responseText)['msg'], function (key, val) {
                            $('.contactform .alert').append(val);
                        });
                    }
                });
            }
        });
    </script>
@endpush
@section('content')
    <div class="contact-area contact-area-v1 panel">
        <div class="ptb-30 prl-30">
            <div class="row row-tb-20">
                <div class="col-xs-12 col-md-6">
                    <div class="contact-area-col contact-info">
                        <div class="contact-info">
                            <h3 class="t-uppercase h-title mb-20">Contact informations</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam officia accusamus qui est. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam officia accusamus qui est.</p>
                            <ul class="contact-list mb-40">
                                <li>
                                    <span class="icon lnr lnr-map-marker"></span>
                                    <h5>Address</h5>
                                    <p class="color-mid">Calicut Airport Jn. Kerala</p>
                                </li>
                                <li>
                                    <span class="icon lnr lnr-envelope"></span>
                                    <h5>Email</h5>
                                    <p class="color-mid">info@dealdaa.com</p>
                                </li>
                                <li>
                                    <span class="icon lnr lnr-phone-handset"></span>
                                    <h5>Our phone</h5>
                                    <p class="color-mid">(+212) 584-241-654</p>
                                </li>
                            </ul>
                            <ul class="social-icons social-icons--colored list-inline">
                                <li class="social-icons__item">
                                    <a href="https://www.facebook.com/etailoffer/" target="_blank"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="social-icons__item">
                                    <a href="https://www.instagram.com/etailoffer/" target="_blank"><i class="fa fa-instagram"  style="color:#fff; background: #dd2a7b;"></i></a>
                                </li>
                                <li class="social-icons__item">
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="social-icons__item">
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="contact-area-col contact-form">
                        <h3 class="t-uppercase h-title mb-20">Get in touch</h3>
                        <form class="contactform" method="post">
                            <div class="col-9 alert"></div>
                            @csrf
                            <div class="form-group">
                                <label>Name</label>
                                <input id="j_name" name="name" placeholder="Name" type="text" class="form-control" required="required">
                            </div>
                            <div class="form-group">
                                <label>Email Address</label>
                                <input type="email" id="j_email" name="email" placeholder="Email" class="form-control" required="required">
                            </div>
                            <div class="form-group">
                                <label>Subject</label>
                                <input type="text" id="j_subject" name="subject" class="form-control" required="required" placeholder="Subject">
                            </div>
                            <div class="form-group">
                                <label>Message</label>
                                <textarea rows="5" id="j_message" name="message" class="form-control" required="required"></textarea>
                            </div>
                            <a href="javascript:void(0);" class="btn j_contactformsubmit">Send Message</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop