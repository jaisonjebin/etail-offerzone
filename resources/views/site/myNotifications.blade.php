@extends('site.layout.master')


@section('title', 'Notifications')

@push('style')

@endpush

@push('scripts')

@endpush

@section('content')
    @include('site.includes.profile_sidebar')
    <div class="page-content col-sm-8 col-md-9">

        <div class=" deal_single_view">
            <div class="deal-deatails panel myprofile_minhieight">
                <div class="deal-body p-20">
                    <header class="panel pb-15 prl-0 pos-r mb-20">
                        <h3 class="h-title font-18">Notification</h3>
                    </header>
                    <div class="col-md-12 notification ptb-15 mb-15">
                        <h5 class="pb-10"> <strong>Notification heading</strong> <span class="text-muted pl-10 font-12">3 Min ago</span> </h5>
                        <p>Nam at <a href="" class="text-danger">condimentum diam</a>, vitae suscipit leo. Aliquam vel erat a turpis faucibus
                            dapibus. Donec rutrum congue metus, fringilla eleifend quam luctus vitae.
                            Morbi mi mauris, ullamcorper quis erat id, rutrum</p>
                    </div>
                    <div class="col-md-12 notification ptb-15 mb-15">
                        <h5 class="pb-10"> <strong>Notification heading</strong> <span class="text-muted pl-10 font-12">Mon 12-05-2018</span> </h5>
                        <p>Nam at condimentum diam, vitae suscipit leo. Aliquam vel erat a turpis faucibus
                            dapibus. Donec rutrum congue metus, fringilla eleifend quam luctus vitae.
                            Morbi mi mauris, ullamcorper quis erat id, rutrum</p>
                    </div>
                    <div class="col-md-12 notification ptb-15 mb-15">
                        <div class="text-center valign-middle pt-20">
                            <h6 class="title mb-15 t-uppercase">You have no Notification.</h6></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop