@extends('site.layout.master')


@section('title', 'Coupon')

@push('style')
    <!-- Owl Carousel -->
    <link href="/site/assets/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="/site/assets/vendors/owl-carousel/owl.theme.min.css" rel="stylesheet">
    <!-- Flex Slider -->
    <link href="/site/assets/vendors/flexslider/flexslider.css" rel="stylesheet">
    <link href="/site/assets/css/counter.css" rel="stylesheet">
    <link href="/site/assets/css/rating.css" rel="stylesheet">
@endpush

@push('scripts')
    <!-- Owl Carousel -->
    <script type="text/javascript" src="/site/assets/vendors/owl-carousel/owl.carousel.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="/site/assets/vendors/flexslider/jquery.flexslider-min.js"></script>

    <!-- Coutdown -->
    <script type="text/javascript" src="/site/assets/vendors/countdown/jquery.countdown.js"></script>
    <script type="text/javascript" src="/site/assets/js/quantity.js"></script>
    <script>
        $(document).ready(function() {
            $('.vv-bar-animate').each(function () {
                $percentage = $(this).data('percentage');
                $(this).animate({
                    width: $percentage+'%'}, 1000);
            });

            setTimeout(function() {
                $('.bar span').fadeIn('slow');
            }, 1000);

        });
    </script>
@endpush

@section('content')
    <div class="row row-rl-10 row-tb-20">
        <div class="page-content col-xs-12 col-sm-7 col-md-8">
            <div class="row row-tb-20">
                <div class="col-xs-12 deal_single_view">
                    <div class="deal-deatails panel">
                        <div class="deal-slider">
                            <div class="section deals-header-area">
                                <div class="row row-tb-20">
                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                        <div class="header-deals-slider owl-slider" data-loop="true"
                                             data-autoplay="true"
                                             data-autoplay-timeout="10000" data-smart-speed="1000"
                                             data-nav-speed="false"
                                             data-nav="true" data-xxs-items="1" data-xxs-nav="true"
                                             data-xs-items="1" data-xs-nav="true"
                                             data-sm-items="1" data-sm-nav="true" data-md-items="1"
                                             data-md-nav="true" data-lg-items="1"
                                             data-lg-nav="true">
                                            <div class="deal-single panel item">
                                                <figure class="dealsingle-thumbnail"
                                                        data-bg-img="/site/assets/images/blog/blog_03.jpg">
                                                </figure>
                                            </div>
                                            <div class="deal-single panel item">
                                                <figure class="dealsingle-thumbnail"
                                                        data-bg-img="/site/assets/images/blog/blog_02.jpg">
                                                </figure>
                                            </div>
                                            <div class="deal-single panel item">
                                                <figure class="dealsingle-thumbnail"
                                                        data-bg-img="/site/assets/images/blog/blog_01.jpg">
                                                </figure>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="label-discount_primary left-30 top-15">50%<br>OFF</div>
                            <ul class="deal-actions top-15 right-20">
                                <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="20">
                                <img src="/site/assets/images/favorite.png">
                            </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 deal_single_view">
                    <div class="deal-deatails panel">
                        <div class="deal-body p-20">
                            <h3 class="mb-10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo
                                mollitia autem, animi fuga.</h3>
                            <div class="rating mb-15">
                                                <span class="rating-stars" data-rating="3">
				                        <i class="fa fa-star-o"></i>
				                        <i class="fa fa-star-o"></i>
				                        <i class="fa fa-star-o"></i>
				                        <i class="fa fa-star-o"></i>
				                        <i class="fa fa-star-o"></i>
				                    </span><!--(3)-->
                            </div>
                            <ul class="list-inline mb-15 price_rages">
                                <li>
                                    <h2 class="price">
                                        <span class="price-sale"><i class="fa fa-rupee"></i>200.00</span>
                                    </h2>
                                    Orginal Price
                                </li>
                                <li>
                                    <h2 class="price">
                                        <i class="fa fa-rupee"></i>60.00
                                    </h2>
                                    Offer Price
                                </li>
                                <li>
                                    <h2 class="price">
                                        <span class="price-save"><i class="fa fa-rupee"></i>60.00</span>
                                    </h2>
                                    You Save
                                </li>
                            </ul>
                            <!--<h2 class="price mb-15">-->
                            <!--<span class="price-sale"><i class="fa fa-rupee"></i> 200.00</span>-->
                            <!--<i class="fa fa-rupee"></i> 60.00-->
                            <!--<span class="price-save">70% OFF</span>-->
                            <!--</h2>-->

                            <p class="mb-15">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Laboriosam, ipsum voluptatum. Eos, saepe harum culpa consequatur aut tenetur,
                                earum, illum eum iste aliquam quam quisquam atque numquam magni voluptas
                                deserunt!
                            </p>
                            <p class="mb-15">Adipisicing elit. Laboriosam, ipsum voluptatum. Eos, saepe harum
                                culpa consequatur aut tenetur, earum, illum eum iste aliquam quam quisquam atque
                                numquam magni voluptas deserunt!</p>
                            <p class="mb-20">Laboriosam, ipsum voluptatum. Eos, saepe harum culpa consequatur
                                aut tenetur, earum, illum eum iste aliquam quam quisquam atque numquam magni
                                voluptas deserunt!</p>
                            <img src="/site/assets/images/how_it_work.jpg" class="img-responsive">

                            <!--<div class="bynow_btn text-center pt-15">-->
                            <!--<a href="" class="btn btn-danger">BUY NOW</a>-->
                            <!--<a href="" class="btn btn-o">GIFT A FRIEND</a>-->
                            <!--<p class="small pt-20">By clicking BUY NOW button, you are accepting all related terms and conditions</p>-->
                            <!--</div>-->
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 deal_single_view">
                    <div class="deal-deatails panel">
                        <div class="deal-body p-20">
                            <h3 class="h-title mb-30">Terms & Conditions</h3>
                            <div class="flashsale_terms">
                                <p>
                                    Tourist can enroll His / Her / their name(s) for the tour by paying a
                                    deposit (non-refundable) of INR 3000/- per person For booking of any class
                                    in train and / or by air, additional difference amount is required. The
                                    difference should also be paid in advance. The balance amount should be paid
                                    15 days prior to the date of departure. If the balance amount is not paid
                                    before 15 days, it will be deemed as cancelled and the deposit will not be
                                    refunded. We reserve the right to cancel the tour, alter the routes,
                                    carriers, time-tables, itinerary and accommodation without prior notice.
                                    <br>
                                    Tourist travels at their own risk. Neither the tour operator nor their
                                    representatives or staff accept any liabilities or responsibilities for any
                                    damage, loss, injury, accident, death etc, during the tour howsoever it
                                    might have caused.
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="post-review panel p-15">
                        <div class="col-lg-6 ">
                            <h3 class="h-title">Total Rating</h3>
                            <div class="rating_histogram">
                                <div class="inner">
                                    <div class="rating">
                                        <span class="rating-num">4.0</span>
                                        <div class="rating-stars1">
                                                     <span class="rating-stars" data-rating="4">
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                    </span>
                                        </div>
                                        <div class="rating-users">
                                            <i class="icon-user"></i> 1,014,004 total
                                        </div>
                                    </div>

                                    <div class="histo">
                                        <div class="five histo-rate">
                    <span class="histo-star">
          <i class="active fa fa-star"></i> 5           </span>
                                            <span class="bar-block">
          <a href=""><span id="bar-five" class="bar">
            <span>56,784</span>&nbsp;</span>
                    </span>
                                            </a>
                                        </div>

                                        <div class="four histo-rate">
                    <span class="histo-star">
          <i class="active  fa fa-star"></i> 4           </span>
                                            <span class="bar-block">
          <a href=""><span id="bar-four" class="bar">
            <span>171,298</span>&nbsp;
                    </span>
                    </a>
                    </span>
                                        </div>

                                        <div class="three histo-rate">
                    <span class="histo-star">
          <i class="active fa fa-star"></i> 3           </span>
                                            <span class="bar-block">
          <a href=""><span id="bar-three" class="bar">
            <span>94,940</span>&nbsp;
                    </span>
                    </a>
                    </span>
                                        </div>

                                        <div class="two histo-rate">
                    <span class="histo-star">
          <i class="active fa fa-star"></i> 2           </span>
                                            <span class="bar-block">
          <a href=""><span id="bar-two" class="bar">
            <span>44,525</span>&nbsp;
                    </span>
                    </a>
                    </span>
                                        </div>

                                        <div class="one histo-rate">
                    <span class="histo-star">
          <i class="active fa fa-star"></i> 1&nbsp;           </span>
                                            <span class="bar-block">
          <a href=""><span id="bar-one" class="bar">
            <span>136,457</span>&nbsp;
                    </span>
                    </a>
                    </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">

                            <h3 class="h-title">Post Review</h3>
                            <form class="horizontal-form" action="#">
                                <div class="row row-v-10">
                                    <div class="col-xs-12 star_rating">
                                        <form>
                                            <fieldset>
    <span class="star-cb-group">
      <input type="radio" id="rating-5" name="rating" value="5"/><label for="rating-5" title="5 Star">5</label>
      <input type="radio" id="rating-4" name="rating" value="4"/><label for="rating-4" title="4 Star">4</label>
      <input type="radio" id="rating-3" name="rating" value="3"/><label for="rating-3" title="3 Star">3</label>
      <input type="radio" id="rating-2" name="rating" value="2"/><label for="rating-2" title="2 Star">2</label>
      <input type="radio" id="rating-1" name="rating" value="1"/><label for="rating-1" title="1 Star">1</label>
      <input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear"/><label for="rating-0">0</label>
    </span>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="col-xs-12">
                                                <textarea class="form-control" placeholder="Your Review"
                                                          rows="6"></textarea>
                                    </div>
                                    <div class="col-xs-12 text-right">
                                        <button type="submit" class="btn mt-20">Submit review</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="posted-review panel p-30">
                        <h3 class="h-title">16 Review</h3>
                        <div class="review-single pt-30">
                            <div class="media">
                                <div class="media-left">
                                    <img class="media-object mr-10 radius-4"
                                         src="/site/assets/images/avatars/avatar_01.jpg" width="90" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="review-wrapper clearfix">
                                        <ul class="list-inline">
                                            <li>
                                                <span class="review-holder-name h5">John Doe</span>
                                            </li>
                                            <li>
                                                <div class="rating">
                                                                    <span class="rating-stars" data-rating="5">
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                    </span>
                                                </div>
                                            </li>
                                        </ul>
                                        <p class="review-date mb-5">September 9, 2016</p>
                                        <p class="copy">Ut velit mauris, egestas sed, gravida nec, ornare ut,
                                            mi. Aenean ut orci vel massa suscipit pulvinar. Nulla sollicitudin.
                                            Fusce varius, ligula non tempus aliquam.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="review-single pt-30">
                            <div class="media">
                                <div class="media-left">
                                    <img class="media-object mr-10 radius-4"
                                         src="/site/assets/images/avatars/avatar_02.jpg" width="90" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="review-wrapper clearfix">
                                        <ul class="list-inline">
                                            <li>
                                                <span class="review-holder-name h5">John Doe</span>
                                            </li>
                                            <li>
                                                <div class="rating">
                                                                    <span class="rating-stars" data-rating="2">
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                    </span>
                                                </div>
                                            </li>
                                        </ul>
                                        <p class="review-date mb-5">September 9, 2016</p>
                                        <p class="copy">Ut velit mauris, egestas sed, gravida nec, ornare ut,
                                            mi. Aenean ut orci vel massa suscipit pulvinar. Nulla sollicitudin.
                                            Fusce varius, ligula non tempus aliquam.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="review-single pt-30">
                            <div class="media">
                                <div class="media-left">
                                    <img class="media-object mr-10 radius-4"
                                         src="/site/assets/images/avatars/avatar_03.jpg" width="90" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="review-wrapper clearfix">
                                        <ul class="list-inline">
                                            <li>
                                                <span class="review-holder-name h5">John Doe</span>
                                            </li>
                                            <li>
                                                <div class="rating">
                                                                    <span class="rating-stars" data-rating="3">
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                    </span>
                                                </div>
                                            </li>
                                        </ul>
                                        <p class="review-date mb-5">September 9, 2016</p>
                                        <p class="copy">Ut velit mauris, egestas sed, gravida nec, ornare ut,
                                            mi. Aenean ut orci vel massa suscipit pulvinar. Nulla sollicitudin.
                                            Fusce varius, ligula non tempus aliquam.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="review-single pt-30">
                            <div class="media">
                                <div class="media-left">
                                    <img class="media-object mr-10 radius-4"
                                         src="/site/assets/images/avatars/avatar_04.jpg" width="90" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="review-wrapper clearfix">
                                        <ul class="list-inline">
                                            <li>
                                                <span class="review-holder-name h5">John Doe</span>
                                            </li>
                                            <li>
                                                <div class="rating">
                                                                    <span class="rating-stars" data-rating="4">
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                    </span>
                                                </div>
                                            </li>
                                        </ul>
                                        <p class="review-date mb-5">September 9, 2016</p>
                                        <p class="copy">Ut velit mauris, egestas sed, gravida nec, ornare ut,
                                            mi. Aenean ut orci vel massa suscipit pulvinar. Nulla sollicitudin.
                                            Fusce varius, ligula non tempus aliquam.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="review-single pt-30">
                            <div class="media">
                                <div class="media-left">
                                    <img class="media-object mr-10 radius-4"
                                         src="/site/assets/images/avatars/avatar_05.jpg" width="90" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="review-wrapper clearfix">
                                        <ul class="list-inline">
                                            <li>
                                                <span class="review-holder-name h5">John Doe</span>
                                            </li>
                                            <li>
                                                <div class="rating">
                                                                    <span class="rating-stars" data-rating="5">
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                        <i class="fa fa-star-o"></i>
										                    </span>
                                                </div>
                                            </li>
                                        </ul>
                                        <p class="review-date mb-5">September 9, 2016</p>
                                        <p class="copy">Ut velit mauris, egestas sed, gravida nec, ornare ut,
                                            mi. Aenean ut orci vel massa suscipit pulvinar. Nulla sollicitudin.
                                            Fusce varius, ligula non tempus aliquam.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page Pagination -->
                        <div class="page-pagination text-right mtb-0 panel">
                            <nav>
                                <!-- Page Pagination -->
                                <ul class="page-pagination">
                                    <li><a class="page-numbers previous" href="#">Previous</a>
                                    </li>
                                    <li><a href="#" class="page-numbers">1</a>
                                    </li>
                                    <li><span class="page-numbers current">2</span>
                                    </li>
                                    <li><a href="#" class="page-numbers">3</a>
                                    </li>
                                    <li><a href="#" class="page-numbers">4</a>
                                    </li>
                                    <li><span class="page-numbers dots">...</span>
                                    </li>
                                    <li><a href="#" class="page-numbers">20</a>
                                    </li>
                                    <li><a href="#" class="page-numbers next">Next</a>
                                    </li>
                                </ul>
                                <!-- End Page Pagination -->
                            </nav>
                        </div>
                        <!-- End Page Pagination -->
                    </div>
                </div>

            </div>
        </div>
        <div class="page-sidebar col-md-4 col-sm-5 col-xs-12 pt-0">
            <!-- Blog Sidebar -->
            <aside class="sidebar blog-sidebar">
                <div class="row row-tb-10">
                    <div class="col-xs-12">
                        <div class="widget single-deal-widget panel ptb-30 prl-20">
                            <div class="widget-body text-left">
                                <!--<h2 class="mb-20 h3">-->
                                <!--Get Full Alfaham Chicken-->
                                <!--</h2>-->
                                <p class="color-muted">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit harum quidem
                                    eaque amet pariatur aspernatur mollitia ratione maxime.
                                </p>
                                <ul class="deal-meta list-unstyled mb-10 color-mid nowrap">
                                    <li><i class="ico lnr lnr-store mr-10"></i><a href="store_single.php"
                                                                                  class="color-mid">Ama Lurra
                                            City Cafe</a>
                                    </li>
                                    <li><i class="ico lnr lnr-map-marker mr-10"></i>Downhill, Malappuram,
                                        kondotty asfasjf asjfsh sdf sdfsdf
                                    </li>
                                </ul>
                                <ul class="deal-meta list-inline mb-10 color-dark">
                                    <li><i class="ico fa lnr lnr-tag mr-10"></i>75 Bought</li>
                                    <li class="pull-right">
                                        <div class="quantity">
                                            <button type="button" class="btn btn-dark btn-number btn-sm"
                                                    data-type="minus" data-field="quant[1]">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <input type="text" name="quant[1]" class=" input-number qty_text"
                                                   value="1" min="1" max="5">

                                            <button type="button" class="btn btn-dark btn-number btn-sm"
                                                    data-type="plus" data-field="quant[1]">
                                                <i class="fa fa-plus"></i>
                                            </button>

                                        </div>

                                    </li>
                                </ul>
                                <div class="price mb-20 text-center pt-15">
                                    <h5 class="color-green"><strong>Offer Price: <i class="fa fa-rupee"></i>
                                            200.00</strong></h5>
                                </div>
                                <div class="buy-now coupon_btns mb-40 text-center">
                                    <!--<div class="btn-group" role="group" aria-label="">-->
                                    <!--&lt;!&ndash;<a href="#" class="btn btn-o">&ndash;&gt;-->
                                    <!--&lt;!&ndash;<i class="fa fa-shopping-cart font-16"></i> Add to Cart&ndash;&gt;-->
                                    <!--&lt;!&ndash;</a>&ndash;&gt;-->
                                    <!--<a href="#" class="btn w-100">-->
                                    <!--<i class="fa fa-credit-card font-16"></i> GET COUPON-->
                                    <!--</a>-->

                                    <!--</div>-->
                                    <a href="javascript:void(0);" class="btn btn-warning w-100"
                                       data-toggle="modal" data-target="#coupon_04">
                                        <i class="fa fa-credit-card font-16"></i> GENERATE COUPON
                                    </a>
                                    <p class="t-uppercase color-muted pt-10 or_text">
                                        - OR -
                                    </p>
                                    <a href="javascript:void(0);" class="btn w-100 availstore" data-toggle="modal"
                                       data-target="#coupon_02">
                                        AVAIL FROM STORE/SHIPPING
                                    </a>

                                </div>
                                <div class="modal fade get-coupon-area avail_shipping" tabindex="-1"
                                     role="dialog1" id="coupon_02">
                                    <div class="modal-dialog">
                                        <div class="modal-content panel">
                                            <div class="modal-body">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span
                                                            aria-hidden="true">&times;</span>
                                                </button>
                                                <div class="row row-v-10">

                                                    <div class="section store-tabs-area mt-20">
                                                        <div class="tabs tabs-v1">
                                                            <!-- Nav tabs -->
                                                            <ul class="nav nav-tabs panel" role="tablist">
                                                                <li role="presentation" class="active">
                                                                    <a href="#store" aria-controls="store"
                                                                       role="tab" data-toggle="tab"
                                                                       aria-expanded="false">AVAIL FROM
                                                                        STORE</a>
                                                                </li>
                                                                <li role="presentation">
                                                                    <a href="#shipping"
                                                                       aria-controls="shipping" role="tab"
                                                                       data-toggle="tab"
                                                                       aria-expanded="true">AVAIL BY
                                                                        SHIPPING</a>
                                                                </li>
                                                            </ul>

                                                            <!-- Tab panes -->
                                                            <div class="tab-content">
                                                                <div role="tabpanel"
                                                                     class="tab-pane ptb-20 active"
                                                                     id="store">

                                                                    <div class="col-md-12 text-left pt-20">
                                                                        <h3 class="widget-title h-title">
                                                                            Contact Seller</h3>
                                                                        <div class="widget-content pt-20 col-md-6">
                                                                            <ul class="deal-meta list-unstyled mb-10 color-mid deal_contactseller">
                                                                                <li>
                                                                                    <i class="ico lnr lnr-map-marker"></i>
                                                                                    <strong>Ama Lurra City
                                                                                        Cafe</strong>
                                                                                    <br>
                                                                                    Opp. DTPC Hall, Life
                                                                                    Gear Building
                                                                                    Kottakkunnu<br>
                                                                                    Malappuram, Kerala</a>
                                                                                </li>
                                                                                <li class="color-green"><i
                                                                                            class="ico fa lnr lnr-phone-handset"></i>
                                                                                    9846398798
                                                                                </li>
                                                                                <li>
                                                                                    <a href="mailto:info@dealdaa.com"><i
                                                                                                class="ico lnr lnr-envelope"></i>
                                                                                        info@dealdaa.com</a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="http://www.etailoffer.com"><i
                                                                                                class="ico lnr lnr-earth"></i>
                                                                                        www.etailoffer.com</a>
                                                                                </li>
                                                                                <li>
                                                                                    <i class="ico lnr lnr-clock"></i>
                                                                                    8:30 am - 11:00 pm
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="widget-content pt-20 col-md-6">
                                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3914.8032177993796!2d76.04186281418525!3d11.128028655649578!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba649a1ee837e39%3A0x321824f6fe3d2440!2sAMANA+Fruits+%26+Coolbar!5e0!3m2!1sen!2sin!4v1525630163682"
                                                                                    class="googlemap"
                                                                                    frameborder="0"
                                                                                    style="border:1px solid #ddd"
                                                                                    allowfullscreen></iframe>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <p class="price m-0"> EO Cashback <i
                                                                                    class="fa fa-rupee"></i>200.00
                                                                        </p>
                                                                        <h3 class="price">
                                                                                        <span class="price-save">Pay Online <i
                                                                                                    class="fa fa-rupee"></i>60.00</span>
                                                                        </h3>
                                                                    </div>
                                                                    <div class="col-lg-12 pt-10">
                                                                        <a href="order_summary.php"
                                                                           class="btn btn-danger">CONTINUE
                                                                            PAYMENT</a>
                                                                        <p class="pt-15">
                                                                            <small>Coupon Expiry: <span
                                                                                        class="text-danger">31/05/2019</span>
                                                                            </small>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <img src="/site/assets/images/availbystore_howit.jpg"
                                                                             class="img-responsive">
                                                                    </div>
                                                                </div>
                                                                <div role="tabpanel" class="tab-pane ptb-20"
                                                                     id="shipping">

                                                                    <!--<div class="col-md-12 text-left pt-20">-->

                                                                    <!--<h3 class="widget-title h-title">Contact Seller</h3>-->
                                                                    <!--<div class="widget-content pt-20 col-md-6">-->
                                                                    <!--<ul class="deal-meta list-unstyled mb-10 color-mid deal_contactseller">-->
                                                                    <!--<li><i class="ico lnr lnr-map-marker"></i>-->
                                                                    <!--<strong>Ama Lurra City Cafe</strong>-->
                                                                    <!--<br>-->
                                                                    <!--Opp. DTPC Hall, Life Gear Building Kottakkunnu<br>-->
                                                                    <!--Malappuram, Kerala</a>-->
                                                                    <!--</li>-->
                                                                    <!--<li class="color-green"><i class="ico fa lnr lnr-phone-handset"></i> 9846398798</li>-->
                                                                    <!--<li><a href="mailto:info@dealdaa.com" ><i class="ico lnr lnr-envelope"></i> info@dealdaa.com</a></li>-->
                                                                    <!--<li><a href="http://www.etailoffer.com" ><i class="ico lnr lnr-earth"></i> www.etailoffer.com</a></li>-->
                                                                    <!--<li><i class="ico lnr lnr-clock"></i> 8:30 am - 11:00 pm</li>-->
                                                                    <!--</ul>-->
                                                                    <!--</div>-->
                                                                    <!--<div class="widget-content pt-20 col-md-6">-->
                                                                    <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3914.8032177993796!2d76.04186281418525!3d11.128028655649578!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba649a1ee837e39%3A0x321824f6fe3d2440!2sAMANA+Fruits+%26+Coolbar!5e0!3m2!1sen!2sin!4v1525630163682" class="googlemap" frameborder="0" style="border:1px solid #ddd" allowfullscreen></iframe>-->

                                                                    <!--</div>-->
                                                                    <!--</div>-->
                                                                    <div class="col-md-12 pt-15">
                                                                        <p class="price m-0">
                                                                            EO Cashback <i
                                                                                    class="fa fa-rupee"></i>200.00
                                                                        </p>
                                                                        <h3 class="price">
                                                                                        <span class="price-save">Pay Online <i
                                                                                                    class="fa fa-rupee"></i>60.00</span>
                                                                        </h3>

                                                                    </div>
                                                                    <div class="col-lg-12 pt-10">
                                                                        <a href="checkout_billing.php"
                                                                           class="btn btn-danger">CONTINUE
                                                                            ORDER</a>
                                                                        <p class="pt-15">
                                                                            <small>Coupon Expiry: <span
                                                                                        class="text-danger">31/05/2019</span>
                                                                            </small>
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <img src="/site/assets/images/shipping_howit.jpg"
                                                                             class="img-responsive">
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="time-left mb-30 text-center">
                                    <p class="t-uppercase color-muted">
                                        Hurry up Only a few deals left
                                    </p>
                                    <div class="color-green font-14 font-lg-16 text-center">
                                        <i class="ico fa fa-clock-o mr-10"></i>
                                        <span data-countdown="2020/10/10 12:25:10"></span>
                                    </div>
                                </div>
                                <ul class="list-inline social-icons social-icons--colored t-center">
                                    <li class="social-icons__item">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li class="social-icons__item">
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li class="social-icons__item">
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li class="social-icons__item">
                                        <a href="#"><i class="fa fa-whatsapp"></i></a>
                                    </li>
                                    <!--<li class="social-icons__item">-->
                                    <!--<a href="#"><i class="fa fa-linkedin"></i></a>-->
                                    <!--</li>-->
                                    <li class="social-icons__item">
                                        137 Shares
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <!-- Recent Posts -->
                        <div class="widget about-seller-widget panel ptb-30 prl-20">
                            <h3 class="widget-title h-title">About Store</h3>
                            <div class="widget-body t-center">
                                <figure class="mt-20 pb-10">
                                    <img src="/site/assets/images/brands/store_logo.jpg" alt="">
                                </figure>
                                <div class="store-about mb-20">
                                    <h3 class="mb-10">Amazon Store</h3>
                                    <div class="rating mb-10">
                                                        <span class="rating-stars rate-allow" data-rating="3">
                                                    		<i class="fa fa-star-o"></i>
                                                    		<i class="fa fa-star-o"></i>
                                                    		<i class="fa fa-star-o star-active"></i>
                                                    		<i class="fa fa-star-o"></i>
                                                    		<i class="fa fa-star-o"></i>
                                                    	</span>
                                        <span class="rating-reviews">
            		                                      ( <span class="rating-count">205</span> rates )
                                                        </span>
                                    </div>
                                    <p class="mb-15">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                        Distinctio officiis at accusantium ducimus excepturi cumque ad commodi
                                        libero nihil rem voluptatibus veniam ipsa ullam esse quia quae fuga,
                                        quidem iusto.</p>
                                    <button class="btn btn-info">FOLLOW</button>
                                </div>
                            </div>
                        </div>
                        <!-- End Recent Posts -->
                    </div>
                    <div class="col-xs-12">
                        <!-- Latest Deals Widegt -->
                        <div class="widget latest-deals-widget panel prl-20">
                            <div class="widget-body ptb-20">
                                <div class="owl-slider" data-loop="true" data-autoplay="true"
                                     data-autoplay-timeout="10000" data-smart-speed="1000"
                                     data-nav-speed="false" data-nav="true" data-xxs-items="1"
                                     data-xxs-nav="true" data-xs-items="1" data-xs-nav="true" data-sm-items="1"
                                     data-sm-nav="true" data-md-items="1" data-md-nav="true" data-lg-items="1"
                                     data-lg-nav="true">
                                    <div class="latest-deals__item item">
                                        <figure class="deal-thumbnail embed-responsive embed-responsive-4by3"
                                                data-bg-img="/site/assets/images/deals/deal_02.jpg">
                                        </figure>
                                    </div>
                                    <div class="latest-deals__item item">
                                        <figure class="deal-thumbnail embed-responsive embed-responsive-4by3"
                                                data-bg-img="/site/assets/images/deals/deal_03.jpg">
                                        </figure>
                                    </div>
                                    <div class="latest-deals__item item">
                                        <figure class="deal-thumbnail embed-responsive embed-responsive-4by3"
                                                data-bg-img="/site/assets/images/deals/deal_04.jpg">
                                        </figure>
                                    </div>
                                    <div class="latest-deals__item item">
                                        <figure class="deal-thumbnail embed-responsive embed-responsive-4by3"
                                                data-bg-img="/site/assets/images/deals/deal_05.jpg">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Latest Deals Widegt -->
                    </div>
                    <div class="col-xs-12">
                        <!-- Subscribe Widget -->
                        <div class="widget subscribe-widget panel pt-20 prl-20">
                            <h3 class="widget-title h-title">Contact Seller</h3>
                            <div class="widget-content ptb-30">
                                <ul class="deal-meta list-unstyled mb-10 color-mid deal_contactseller">
                                    <li><i class="ico lnr lnr-map-marker"></i>
                                        <strong>Ama Lurra City Cafe</strong>
                                        <br>
                                        Opp. DTPC Hall, Life Gear Building Kottakkunnu<br>
                                        Malappuram, Kerala</a>
                                    </li>
                                    <li class="color-green"><i class="ico fa lnr lnr-phone-handset"></i>
                                        9846398798
                                    </li>
                                    <li><a href="mailto:info@dealdaa.com"><i class="ico lnr lnr-envelope"></i>
                                            info@dealdaa.com</a></li>
                                    <li><a href="http://www.etailoffer.com"><i class="ico lnr lnr-earth"></i>
                                            www.etailoffer.com</a></li>
                                    <li><i class="ico lnr lnr-clock"></i> 8:30 am - 11:00 pm</li>
                                </ul>
                            </div>
                        </div>
                        <!-- End Subscribe Widget -->
                    </div>
                    <div class="col-xs-12">
                        <!-- Subscribe Widget -->
                        <div class="widget subscribe-widget panel pt-20 prl-20">
                            <h3 class="widget-title h-title">Deal Location</h3>
                            <div class="widget-content ptb-30">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3914.8032177993796!2d76.04186281418525!3d11.128028655649578!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba649a1ee837e39%3A0x321824f6fe3d2440!2sAMANA+Fruits+%26+Coolbar!5e0!3m2!1sen!2sin!4v1525630163682"
                                        class="googlemap" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                        <!-- End Subscribe Widget -->
                    </div>
                    <div class="col-xs-12">
                        <!-- Best Rated Deals -->
                        <div class="widget best-rated-deals panel pt-20 prl-20">
                            <h3 class="widget-title h-title">Best Rated Deals</h3>
                            <div class="widget-body ptb-30">


                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object" src="/site/assets/images/deals/thumb_01.jpg"
                                                 alt="Thumb" width="80">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="mb-5">
                                            <a href="#">Aenean ut orci vel massa</a>
                                        </h6>
                                        <div class="mb-5">
                                                            <span class="rating">
                        <span class="rating-stars" data-rating="4">
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </span>
                                                            </span>
                                        </div>
                                        <h4 class="price font-16"><i class="fa fa-rupee"></i>60.00 <span
                                                    class="price-sale color-muted"><i class="fa fa-rupee"></i>200.00</span>
                                        </h4>
                                    </div>
                                </div>


                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object" src="/site/assets/images/deals/thumb_02.jpg"
                                                 alt="Thumb" width="80">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="mb-5">
                                            <a href="#">Aenean ut orci vel massa</a>
                                        </h6>
                                        <div class="mb-5">
                                                            <span class="rating">
                        <span class="rating-stars" data-rating="4">
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </span>
                                                            </span>
                                        </div>
                                        <h4 class="price font-16"><i class="fa fa-rupee"></i>60.00 <span
                                                    class="price-sale color-muted"><i class="fa fa-rupee"></i>200.00</span>
                                        </h4>
                                    </div>
                                </div>


                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object" src="/site/assets/images/deals/thumb_03.jpg"
                                                 alt="Thumb" width="80">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="mb-5">
                                            <a href="#">Aenean ut orci vel massa</a>
                                        </h6>
                                        <div class="mb-5">
                                                            <span class="rating">
                        <span class="rating-stars" data-rating="4">
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </span>
                                                            </span>
                                        </div>
                                        <h4 class="price font-16"><i class="fa fa-rupee"></i>60.00 <span
                                                    class="price-sale color-muted"><i class="fa fa-rupee"></i>200.00</span>
                                        </h4>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <!-- Best Rated Deals -->
                    </div>
                </div>
            </aside>
            <!-- End Blog Sidebar -->
        </div>
    </div>

    <section class="section latest-deals-area ptb-30">
        <header class="panel ptb-15 prl-20 pos-r mb-30">
            <h3 class="section-title font-18">Similar Deals</h3>
            <a class="btn btn-o btn-xs pos-a right-10 pos-tb-center">View All</a>
        </header>

        <div class="row row-masnory row-tb-20">
            <div class="col-sm-6 col-lg-4">
                <div class="deal-single panel">
                    <figure class="embed-responsive embed-responsive-16by9"
                            data-bg-img="/site/assets/images/deals/deal_09.jpg">
                        <div class="label-discount_primary left-20 top-15">50%<br>OFF</div>
                        <ul class="deal-actions top-15 right-15">

                            <li class="share-btn">
                                <div class="share-tooltip fade">
                                    <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                                    <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                    <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                                <span data-toggle="tooltip" data-placement="left" title="116"><img
                                            src="/site/assets/images/share.png"> </span>
                            </li>
                            <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="20">
                                                    <img src="/site/assets/images/favorited.png">
                            </span>
                            </li>
                            <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="3.5">
                                <img src="/site/assets/images/star.png">
                            </span>
                            </li>
                        </ul>
                        <div class="viewdeal"><a href="deal_single_view.php">VIEW DEAL</a></div>
                    </figure>
                    <div class="bg-white pt-20 pl-20 pr-15">
                        <div class="time-left_dealdaa font-md-14 mb-10">
                                            <span>
                            <i class="ico fa fa-clock-o mr-5"></i>
                            <span class="t-uppercase" data-countdown="2019/09/01 01:30:00"></span>
                        </span>
                        </div>
                        <div class="pr-md-10">
                            <h3 class="deal-title mb-10">
                                <a href="deal_single_view.php">The Crash Bad Instant Folding Twin Bed Lorem
                                    ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam numquam </a>
                            </h3>
                            <ul class="deal-meta list-unstyled mb-10 color-mid latest_deal_location">
                                <li><i class="ico lnr lnr-map-marker mr-10"></i>Nadakkavu, Kozhikode</li>
                                <li><i class="ico lnr lnr-store mr-10"></i>Thamoos Chinese Restaurant sdfsdjk
                                    hsdkhf sdjhfksdhfjkhsjk
                                </li>
                            </ul>
                        </div>
                        <div class="deal-price pos-r mb-15">
                            <ul class="list-inline m-0">
                                <li><h3 class="price ptb-5 text-left"><i class="fa fa-rupee"></i> 50.00
                                    </h3></li>
                                <li class="float-right" data-toggle="tooltip" data-placement="left"
                                    title="Add to Cart"><a href="" class="btn btn-primary"><i
                                                class="lnr lnr-cart"></i></a></li>
                            </ul>


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="deal-single panel">
                    <figure class="embed-responsive embed-responsive-16by9"
                            data-bg-img="/site/assets/images/deals/deal_12.jpg">
                        <div class="label-discount_primary left-20 top-15">50%<br>OFF</div>
                        <ul class="deal-actions top-15 right-15">

                            <li class="share-btn">
                                <div class="share-tooltip fade">
                                    <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                                    <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                    <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                                <span data-toggle="tooltip" data-placement="left" title="116"><img
                                            src="/site/assets/images/share.png"> </span>
                            </li>
                            <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="20">
                                <img src="/site/assets/images/favorite.png">
                            </span>
                            </li>
                            <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="3.5">
                                <img src="/site/assets/images/star.png">
                            </span>
                            </li>
                        </ul>
                        <div class="viewdeal"><a href="deal_single_view.php">VIEW DEAL</a></div>
                    </figure>
                    <div class="bg-white pt-20 pl-20 pr-15">
                        <div class="time-left_dealdaa font-md-14 mb-10">
                                            <span>
                            <i class="ico fa fa-clock-o mr-5"></i>
                            <span class="t-uppercase" data-countdown="2019/09/01 01:30:00"></span>
                        </span>
                        </div>
                        <div class="pr-md-10">
                            <h3 class="deal-title mb-10">
                                <a href="deal_single_view.php">The Crash Bad Instant Folding Twin Bed Lorem
                                    ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam numquam </a>
                            </h3>
                            <ul class="deal-meta list-unstyled mb-10 color-mid latest_deal_location">
                                <li><i class="ico lnr lnr-map-marker mr-10"></i>Nadakkavu, Kozhikode</li>
                                <li><i class="ico lnr lnr-store mr-10"></i>Thamoos Chinese Restaurant sdfsdjk
                                    hsdkhf sdjhfksdhfjkhsjk
                                </li>
                            </ul>
                        </div>
                        <div class="deal-price pos-r mb-15">
                            <ul class="list-inline m-0">
                                <li><h3 class="price ptb-5 text-left"><span class="price-sale"><i
                                                    class="fa fa-rupee"></i> 980.00</span><i class="fa fa-rupee"></i> 230.00
                                    </h3></li>
                                <li class="float-right" data-toggle="tooltip" data-placement="left"
                                    title="Add to Cart"><a href="" class="btn btn-primary"><i
                                                class="lnr lnr-cart"></i></a></li>
                            </ul>


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="deal-single panel">
                    <figure class="embed-responsive embed-responsive-16by9"
                            data-bg-img="/site/assets/images/deals/deal_06.jpg">
                        <div class="label-discount_primary left-20 top-15">50%<br>OFF</div>
                        <ul class="deal-actions top-15 right-15">

                            <li class="share-btn">
                                <div class="share-tooltip fade">
                                    <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                                    <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                    <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                                <span data-toggle="tooltip" data-placement="left" title="116"><img
                                            src="/site/assets/images/share.png"> </span>
                            </li>
                            <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="20">
                                <img src="/site/assets/images/favorite.png">
                            </span>
                            </li>
                            <li class="like-deal">
                                                <span data-toggle="tooltip" data-placement="bottom" title="3.5">
                                <img src="/site/assets/images/star.png">
                            </span>
                            </li>
                        </ul>
                        <div class="viewdeal"><a href="deal_single_view.php">VIEW DEAL</a></div>
                    </figure>
                    <div class="bg-white pt-20 pl-20 pr-15">
                        <div class="time-left_dealdaa font-md-14 mb-10">
                                            <span>
                            <i class="ico fa fa-clock-o mr-5"></i>
                            <span class="t-uppercase" data-countdown="2019/09/01 01:30:00"></span>
                        </span>
                        </div>
                        <div class="pr-md-10">
                            <h3 class="deal-title mb-10">
                                <a href="deal_single_view.php">25% Discount on any course</a>
                            </h3>
                            <ul class="deal-meta list-unstyled mb-10 color-mid latest_deal_location">
                                <li><i class="ico lnr lnr-map-marker mr-10"></i>Nadakkavu, Kozhikode</li>
                                <li><i class="ico lnr lnr-store mr-10"></i>Thamoos Chinese Restaurant sdfsdjk
                                    hsdkhf sdjhfksdhfjkhsjk
                                </li>
                            </ul>
                        </div>
                        <div class="deal-price pos-r mb-15">
                            <ul class="list-inline m-0">
                                <li><h3 class="price ptb-5 text-left"><i class="fa fa-rupee"></i> 150.00
                                    </h3></li>
                                <li class="float-right" data-toggle="tooltip" data-placement="left"
                                    title="Add to Cart"><a href="" class="btn btn-primary"><i
                                                class="lnr lnr-cart"></i></a></li>
                            </ul>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="modal fade get-coupon-area" tabindex="-1" role="dialog" id="coupon_04">
        <div class="modal-dialog">
            <div class="modal-content panel">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <div class="row row-v-10 pt-20">
                        <div class="col-md-12">
                            <img src="/site/assets/images/generate_coupon_howit.jpg" class="img-responsive">
                        </div>
                        <div class="col-md-12 text-left pt-20">

                            <h3 class="widget-title h-title">Contact Seller</h3>
                            <div class="widget-content pt-20 col-md-6">
                                <ul class="deal-meta list-unstyled mb-10 color-mid deal_contactseller">
                                    <li><i class="ico lnr lnr-map-marker"></i>
                                        <strong>Ama Lurra City Cafe</strong>
                                        <br>
                                        Opp. DTPC Hall, Life Gear Building Kottakkunnu<br>
                                        Malappuram, Kerala</a>
                                    </li>
                                    <li class="color-green"><i class="ico fa lnr lnr-phone-handset"></i>
                                        9846398798
                                    </li>
                                    <li><a href="mailto:info@dealdaa.com"><i class="ico lnr lnr-envelope"></i>
                                            info@dealdaa.com</a></li>
                                    <li><a href="http://www.etailoffer.com"><i class="ico lnr lnr-earth"></i>
                                            www.etailoffer.com</a></li>
                                    <li><i class="ico lnr lnr-clock"></i> 8:30 am - 11:00 pm</li>
                                </ul>
                            </div>
                            <div class="widget-content pt-20 col-md-6">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3914.8032177993796!2d76.04186281418525!3d11.128028655649578!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba649a1ee837e39%3A0x321824f6fe3d2440!2sAMANA+Fruits+%26+Coolbar!5e0!3m2!1sen!2sin!4v1525630163682"
                                        class="googlemap" frameborder="0" style="border:1px solid #ddd"
                                        allowfullscreen></iframe>

                            </div>
                        </div>
                        <div class="col-lg-12 pt-10">
                            <a href="javascript:void(0);"
                               onclick="alert('Coupon generated successfully! You can find it in My Purchase Menu');"
                               class="btn btn-danger">GENERATE COUPON</a>
                            <p class="pt-15">
                                <small>Coupon Expiry: <span class="text-danger">31/05/2019</span></small>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--notify modal-->

    <div class="modal fade notifymodal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body text-center p-20">
                    <img src="assets/images/notify_modal.svg" class="img-responsive">
                    <h5 class="pb-20"><strong>Good to know that you are interested in this!</strong><br>
                        We will notify you before the sale is open!</h5>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!--end notify modal-->
@stop