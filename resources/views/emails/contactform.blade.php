@component('mail::message')
#You have a new enquiry.

<table>
    <tbody style="font-weight: bolder;">
    <tr>
        <td>Date & Time: </td>
        <td>{{date('d,F,Y H:i')}}</td>
    </tr>
    <tr>
        <td>Name: </td>
        <td>{{$data['name']}}</td>
    </tr>
    <tr>
        <td>Email: </td>
        <td>{{$data['email']}}</td>
    </tr>
    <tr>
        <td>Subject: </td>
        <td>{{$data['subject']}}</td>
    </tr>
    <tr>
        <td>Message: </td>
        <td>{{$data['message']}}</td>
    </tr>
    </tbody>
</table>

{{--Thanks,<br>--}}
{{--{{ config('app.name') }}--}}
@endcomponent
