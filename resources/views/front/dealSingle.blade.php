
@extends('front.master')

@section('content')

    <div class="container-fluid product_view pt-3 pb-3">
        <div class="col-xl-12">
            <div class="wrapper1 row">
                <div class="preview col-xl-8 col-lg-8 col-md-12">
                    <div class="row">
                        <div class="col-md-2 pt-1">
                            <ul class="preview-thumbnail nav nav-tabs">
                                @if($singleDeal->banners)
                                    @php $banners = json_decode($singleDeal->banners) @endphp
                                @else
                                    @php $banners = [] @endphp
                                @endif

                                @php $i = 1 @endphp
                                @foreach($banners as $banner)
                                <li @if($i == 1)class="active" @endif>
                                    <a data-target="#pic-{{$i++}}" data-toggle="tab">
                                        <img src="/uploads/{{$banner}}" class="w-100"/>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="col-md-10 p-0">
                            <div class="preview-pic tab-content">
                                @php $i = 1 @endphp
                                @foreach($banners as $banner)
                                    <div class="tab-pane @if($i == 1)active @endif" id="pic-{{$i++}}">
                                    <img src="/uploads/{{$banner}}" />
                            </div>

                                @endforeach
                                <div class="col-lg-12 text-right share_icon">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><a href="#" class="btn btn-secondary addtoFavoriteBtn" title="Favorite"> <i class="fas fa-heart"></i></a> </li>
                                        <li class="list-inline-item">
                                            <div class="dropdown">
                                                <button class="btn btn-secondary "  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-share-alt"></i> </button>
                                                <div  data-shareUrl="{{route('dealItem',$singleDeal->id)}}?ref={{getReferalId()}}" class="v_shareParent dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">

                                                    @include('front.includes.shareDropDownContents')

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xl-12 vouchertime_details_dealclick text-right">
                                    <ul class="list-inline m-0">
                                        <li  class="list-inline-item" >
                                            <p class="m-0"><i class="far fa-clock float-left pt-1" style="color:#fff"></i><span class="timercount m-0 pl-2"></span></p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 deal_hotdeals_details">
                    <h4 class="">{{$singleDeal->title}}
                    </h4>
                    <p><strong>{{$singleDeal->small_description}}</strong></p>
                    <table>
                        <tr>
                            <td>Orginal Price</td>
                            <td>: <i class="fas fa-rupee-sign"></i>{{$singleDeal->price}}</td>
                        </tr>
                        <tr>
                            <td>Offer Price</td>
                            <td>: <i class="fas fa-rupee-sign"></i>{{$singleDeal->offer_price}}</td>
                        </tr>
                        <tr>
                            <td>You Save</td>
                            <td>: <i class="fas fa-rupee-sign"></i>{{$singleDeal->price - $singleDeal->offer_price}}</td>
                        </tr>
                        <tr>
                            <td colspan="2"><i class="fas fa-map-marker-alt"></i> <strong>13 Km</strong> - {{$singleDeal->getOwner->address}}</td>
                        </tr>
                    </table>
                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-md-3 col-6">
                            <h4><strong>{{ number_format((($singleDeal->offer_price / ($singleDeal->price??1))*100),0) }}% OFF</strong></h4>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-2 col-6 text-center" style="color:#217600;font-size:13px;"><strong>99 Bought</strong></div>
                        <div class="col-md-12"><a data-id="{{$singleDeal->id}}" data-type="deal" href="javascript:void(0)" class="btn btn-info mr-2 deal_buyBtn">BUY NOW</a><a data-toggle="modal" data-target="#giftfriend" class="btn btn-info">GIFT A FRIEND</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid deal_des">
        <div class="row">
            <div class="col-xl-8 col-lg-8">
                <h4>DEAL DISCRIPTION</h4>
                <p>
                    {!! $singleDeal->description !!}
                </p>
                <h4>TERMS AND CONDITIONS </h4>
                <p class="pl-0">
                    {!! $singleDeal->termsConditions !!}
                </p>
                <p style="color:#00aeef;"><strong>Images are used for representation purpose only</strong></p>
            </div>
            <div class="col-xl-4 col-lg-4 pl-2 pr-2">
                <div class="col-xl-12 mb-3">
                    <img src="/theme/images/discount_slider.jpg" class="img-fluid w-100">
                </div>
                <div class="col-xl-12 mb-3">
                    <img src="/theme/images/discount_slider.jpg" class="img-fluid w-100">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid pt-3 pb-3 shop_address">
        <div class="row">
            <div class="col-xl-12">
                <h4>LOCATION DEALS</h4>
            </div>
            <div class="col-md-5">
                <iframe src="https://www.google.com/maps/embed/v1/place?q={{(json_decode($singleDeal->getOwner->location)?(json_decode($singleDeal->getOwner->location)->latitude??0):'0')}},{{(json_decode($singleDeal->getOwner->location)?(json_decode($singleDeal->getOwner->location)->longitude??0):'0')}}&amp;key=AIzaSyAyCFC_khEXg_0wH2KoUGT1uACDD3ZUIvI"   class="googlemap" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="col-md-4 address">
                <ul class="list-unstyled">
                    <li><i class="fas fa-home"></i> {{$singleDeal->getOwner->name}}</li>
                    <li><i class="fas fa-map-marker-alt"></i> {{$singleDeal->getOwner->address}}</li>
                    <li><i class="fas fa-phone-volume"></i> {{$singleDeal->getOwner->firm_phone}}</li>
                    <li><i class="fas fa-user"></i> {{$singleDeal->getOwner->contact_person}}</li>
                    <li><i class="fas fa-globe"></i> {{$singleDeal->getOwner->firm_email}}</li>
                    {{--<li><i class="far fa-clock"></i> {{$singleDeal->getOwner->firm_email}}</li>--}}
                </ul>
            </div>
            <div class="col-md-3">
                <div id="carouselExampleControls" class="carousel slide  " data-interval="12000" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleControls" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleControls" data-slide-to="1"></li>
                        <li data-target="#carouselExampleControls" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active"><a href=""><img class="d-block w-100" src="/theme/images/hotdeal.jpg" alt=""> </a></div>
                        <div class="carousel-item"><a href=""> <img class="d-block w-100" src="/theme/images/hotdeal.jpg" alt=""></a> </div>
                        <div class="carousel-item"><a href=""> <img class="d-block w-100" src="/theme/images/hotdeal.jpg" alt=""></a> </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
                </div>
            </div>
        </div>
    </div>

    <!-- Subscribe flash sale-->
    @include('front.includes.flashsales')

    <!--Trending Items-->

    @include('front.includes.deals')



@endsection


@push('scripts')

<script>
    // Due to weird things with code pen the following $(document).ready(function(){});
    // code is in here instead being applied in the html.
    // Note: This code should be insterted into the bottom of the html document, you
    // can see where it should be written in the html by looking at the commented out code.
    $(document).ready(function() {
        var current = new Date();

//        ;[ 'Date', 'Hours', 'Minutes' ].forEach(function(val, i) {
//            current['set' + val](current['get' + val]() + (i + 1) * 3);
//        });


        $('#count-down')
//            .attr('datetime', current) // updates the date every time the page loads this is for the example only
            .countdown({
                // wordFormat: "full", // uncomment this for full words
                numberFormat: true
            }, function() {
                // Replace this alert function with your callback function. AKA what you want to happen after the time is up.
                location.reload();

            });
    });

    // Below is the code for the countdown plugin. This code should be saved in a seperate document and linked to the page that it is being used on.

    /* Author : Tyler Benton
     Date : 05/04/12
     jQuery : 1.7.2, works with 2.0.3
     Purpose : light weight countdown timer
     */
    ;(function($) {
        $.fn.countdown = function(options, callback) {
            var obj = this;
            var $obj = $(obj);
            var settings = {
                numberFormat: null
            };
            var interval = '';
            if (options) {
                $.extend(settings, options);
            }
            function init() {
                var newObj = '<ul><li><div class="days"></div></li><li><div class="hours"></div></li><li><div class="minutes"></div></li><li><div class="seconds"></div></li></ul>';
                $obj
                    .append(newObj);
                countdown_process();
            }

            function countdown_process() {
                eventDate = new Date($('#count-down').attr('datetime'));
                eventDate = Date.parse(eventDate) / 1000;
                currentDate = Math.floor($.now() / 1000);
                seconds = eventDate - currentDate;
                $days = $obj.find('.days');
                $hours = $obj.find('.hours');
                $minutes = $obj.find('.minutes');
                $seconds = $obj.find('.seconds');
                days = Math.floor(seconds / (24 * 60 * 60));
                seconds -= days * 24 * 60 * 60;
                hours = Math.floor(seconds / (60 * 60));
                seconds -= hours * 60 * 60;
                minutes = Math.floor(seconds / 60);
                seconds -= minutes * 60;
                if (eventDate <= currentDate) {
                    callback.call(obj);
                    clearInterval(interval);
                    days = 0;
                    seconds = 0;
                    hours = 0;
                    minutes = 0;
                    seconds = 0;
                }
                if (settings.wordFormat === 'full') {
                    days === 1 ? $days.attr('data-interval-text', 'day') : $days.attr('data-interval-text', 'days');
                    hours === 1 ? $hours.attr('data-interval-text', 'hour') : $hours.attr('data-interval-text', 'hours');
                    minutes === 1 ? $minutes.attr('data-interval-text', 'minute') : $minutes.attr('data-interval-text', 'minutes');
                    seconds === 1 ? $seconds.attr('data-interval-text', 'second') : $seconds.attr('data-interval-text', 'seconds');
                } else {
                    days === 1 ? $days.attr('data-interval-text', 'day') : $days.attr('data-interval-text', 'days');
                    hours === 1 ? $hours.attr('data-interval-text', 'hr') : $hours.attr('data-interval-text', 'hrs');
                    minutes === 1 ? $minutes.attr('data-interval-text', 'min') : $minutes.attr('data-interval-text', 'mins');
                    seconds === 1 ? $seconds.attr('data-interval-text', 'sec') : $seconds.attr('data-interval-text', 'secs');
                }

                if (settings.numberFormat) {
                    days = String(days).length >= 2 ? days : '0' + days;
                    hours = String(hours).length >= 2 ? hours : '0' + hours;
                    minutes = String(minutes).length >= 2 ? minutes : '0' + minutes;
                    seconds = String(seconds).length >= 2 ? seconds : '0' + seconds;
                }

                if (!isNaN(eventDate)) {
                    $days.text(days);
                    $hours.text(hours);
                    $minutes.text(minutes);
                    $seconds.text(seconds);
                } else {
                    console.log('Invalid date. Here\'s an example: 16 may 2012 11:59:59');
                    clearInterval(interval);
                }
            }

            init();
            interval = setInterval(countdown_process, 1000);
        };
    })(jQuery); // end plugin

    // $("#your-selector")
    //  .countdown({
    //   date: "6 january 2014 00:00:00",
    //   format: "on",
    //  },function(){
    //   Your call back function
    //  });


    $(document).on('click','.deal_buyBtn',function () {
        $id = $(this).data('id');
        $type = $(this).attr('data-type');
        $(this).addClass('animated fadeOutUpBig');
        $count = parseInt($('#cartCount').html());
        $('#cartCount').html('' + ($count+1));

        $.ajax({
            context:$(this),
            url:'{{route('user.addCart',['',''])}}'+'/'+$id+'/'+$type,
            dataType:'json',
            success:function (data) {
                window.location = '{{route('user.profile')}}#v-pills-cart';
            },
            error:function(a,b,c){
                if(a.status == '401'){
                    toastr.info("Please Login to continue");
                    $(this).removeClass('animated fadeOutUpBig');
                    $('#loginmodal').modal('show');

                }
                else{

                }
            }
        });


    });


</script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js'></script>

<script  src="/theme/js/countown.js"></script>

@endpush
