@extends('front.master')

@section('content')

    <div class="container-fluid pt-2 pb-2 deal_page">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-12 deal_page_slider">
                <div id="carouselExampleControls" class="carousel slide  " data-interval="12000" data-ride="carousel">

                    <div class="carousel-inner">

                        @php $i = 1 @endphp
                        @foreach($deals as $singleDeal)
                            @if($singleDeal->banners)
                                @php $banners = json_decode($singleDeal->banners) @endphp
                            @else
                                @php $banners = [] @endphp
                            @endif

                            @foreach($banners as $banner)
                                <div class="carousel-item @if($i++ == 1)active @endif">
                                    <a href="{{route('dealItem',$singleDeal->id)}}">
                                        <img class="d-block w-100" src="/uploads/{{$banner}}" alt="">
                                    </a>
                                </div>
                            @endforeach

                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span>
                    </a> <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                            data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span
                                class="sr-only">Next</span> </a></div>
            </div>

            <div class="col-xl-3 col-lg-4 deal_page_toppick">
                <div class="col-xl-12 p-0">
                    <h4>TOP PICKS <a href="{{route('deallistDealsCategory','top-picks')}}" class="float-right">VIEW ALL</a></h4>
                </div>

                @foreach($topPics as $deal)
                    <a href="{{route('dealItem',$deal->id)}}" class="abc">
                        <div class="media mb-2"><img class="mr-3" src="/uploads/{{$deal->small_image}}" alt="">
                            <div class="media-body">
                                <h5 class="mt-0">{{$deal->title}}</h5>
                                <p><span class=""><i class="fas fa-rupee-sign"></i>{{$deal->price}}</span> <i
                                            class="fas fa-rupee-sign"></i>{{$deal->offer_price}}</p>
                                <h6>You Saved <i class="fas fa-rupee-sign"></i>{{$deal->price-$deal->offer_price}} <span>{{ number_format((($deal->offer_price / ($deal->price??1))*100),0) }}% OFF</span></h6>
                                <ul class="list-inline">
                                    <li class="list-inline-item" style="color:#d61e1e"><i class="far fa-clock"></i></li>
                                    <li class="list-inline-item">
                                        <p id="" class="timercount m-0" data-endtime="{{ date('M d, Y H:I:s',strtotime($deal->end_time)) }}"></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </a>
                @endforeach



            </div>
        </div>
    </div>
    <section class="deal_brows_categories">
        <div class="container-fluid ">
            <div class="row">
                <h4 class="w-100 text-center text-white pt-2">BROWSE POPULAR CATEGORIES</h4>

                <div class="col-md-2 col-6 text-center"><a href="{{route('deallistDealsCategory',0)}}"> <img src="/theme/images/deal_categories_deal.png"
                                                                         class="img-fluid">
                        <p class="text-white">All Deals</p>
                    </a>
                </div>

                @foreach($category as $cat)
                    <div class="col-md-2 col-6 text-center"><a href="{{route('deallistDealsCategory',$cat->id)}}"> <img src="/uploads/{{$cat->image}}"
                                                                             class="img-fluid" style="max-height: 73px;">
                            <p class="text-white">{{$cat->name}}</p>
                        </a>
                    </div>

                @endforeach


            </div>
        </div>
    </section>


    <div class="container-fluid deal_ofthe_day pt-3 pb-5">
        <div class="row">
            <div class="col-xl-12 pt-2">
                <h4 class="right_line">DEAL OF THE DAY</h4>
            </div>

            @foreach($dealoftheday as $deal)

            <div class="col-xl-3 col-lg-3 col-md-6 deal_ofthe_day_list">
                <a href="{{route('dealItem',$deal->id)}}">
                    <img src="/uploads/{{$deal->small_image}}" class="img-fluid  w-100">
                </a>
                <div class="col-lg-12 text-right share_icon">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="javascript:void(0)" data-type="dealItem" data-id="{{$deal->id}}" class="btn btn-secondary addtoFavoriteBtn" title="Favorite"> <i
                                        class="fas fa-heart"></i></a>
                        </li>

                        <li class="list-inline-item">
                            <div class="dropdown">
                                <button class="btn btn-secondary " type="button" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                            class="fas fa-share-alt"></i></button>

                                <div data-shareUrl="{{route('dealItem',$deal->id)}}?ref={{getReferalId()}}" class="v_shareParent dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">

                                    @include('front.includes.shareDropDownContents')

                                </div>

                            </div>
                        </li>
                    </ul>
                </div>
                <a href="javascript:void(0)"  class="btn btn-primary deal_addcart"  data-id="{{$deal->id}}" data-type="deal" title="Add To Cart"><i class="fas fa-cart-plus"></i></a>
                <div class="col-xl-12 p-0">
                    <h4>{{$deal->title}}</h4>
                    <ul class="list-inline m-0">
                        <li class="list-inline-item offer_amt">{{ number_format((($deal->offer_price / ($deal->price??1))*100),0) }}% OFF</li>
                        <li class="list-inline-item"><span class="cu_amount"><i
                                        class="fas fa-rupee-sign"></i>{{$deal->price}}</span></li>
                        <li class="list-inline-item"><span class="off_amount"><i
                                        class="fas fa-rupee-sign"></i>{{$deal->offer_price}}</span></li>
                    </ul>
                    <p>{{$deal->getOwner->address}}</p>
                </div>
            </div>

            @endforeach


        </div>
    </div>

    <div class="container-fluid home_flash_sale">
        <div class="col-lg-12">
            <div class="row mb-4">
                <div class="col-xl-12 pl-2 pr-2"><h3>OTHER DEALS</h3></div>


                @foreach($deals as $deal)
                <div class=" col-lg-4 col-md-4 pl-2 p-2"><a href="{{route('dealItem',$deal->id)}}">
                        <img src="/uploads/{{$deal->small_image}}" class="img-fluid w-100"></a>
                    <div class="col-lg-12 text-right share_icon">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="javascript:void(0)"  data-type="dealItem" data-id="{{$deal->id}}" class="btn btn-secondary addtoFavoriteBtn" title="Favorite"> <i
                                            class="fas fa-heart"></i></a></li>
                            <li class="list-inline-item">
                                <div class="dropdown">
                                    <button class="btn btn-secondary " type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                                class="fas fa-share-alt"></i></button>
                                    <div  data-shareUrl="{{route('dealItem',$deal->id)}}?ref={{getReferalId()}}" class="v_shareParent dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        @include('front.includes.shareDropDownContents')
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <a href="javascript:void(0)" data-id="{{$deal->id}}" data-type="deal" class="btn btn-primary deal_addcart" title="Add To Cart"><i class="fas fa-cart-plus"></i></a>

                </div>


                @endforeach

            </div>

        </div>
    </div>


@endsection


@push('scripts')


@endpush
