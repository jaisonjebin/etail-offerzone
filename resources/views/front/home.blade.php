
@extends('front.master')

@section('content')
<!-- Slider top-->
<div class="container-fluid home_slider">
    <div class="row">
        <div class="col-lg-6 col-md-6 pl-2 pr-2">
            <div id="carouselExampleControls" class="carousel slide  " data-interval="12000" data-ride="carousel">
                
                <div class="carousel-inner">
					@if(isset($sliders->one))
					@php $i=0; @endphp
					@foreach($sliders->one as $slider)
                            <div class="carousel-item @if($i++ ==0 )active @endif"> <a href="{{$slider->link}}"><img class="d-block w-100" src="/uploads/{{$slider->image}}" alt=""></a> </div>
					@endforeach
					@endif
				</div>
				
				
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
        </div>
        <div class="col-lg-6 col-md-6 ">

            <div class="row">
			
					@if(isset($sliders->two))
					@php $i=0;  $lastImage ="default.jpg"; @endphp
					@foreach($sliders->two as $slider)
						@php $lastImage = $slider; @endphp
					@endforeach					
					
                    <div class="col-lg-12  pl-2 pr-2 mb-2">  <a href="{{$lastImage->link}}"><img src="/uploads/{{$lastImage->image}}" class="img-fluid"></a> </div>
							
					@endif
                        
					
                
                <div class="col-lg-6 col-md-6 pl-2 pr-2">
                    <div id="carouselExampleControls1" class="carousel slide" data-interval="15000" data-ride="carousel">
                        
                        <div class="carousel-inner">
					@if(isset($sliders->three))
					@php $i=0; @endphp
					@foreach($sliders->three as $slider)
                                    <div class="carousel-item  @if($i++ ==0 )active @endif"><a href="{{$slider->link}}"> <img class="d-block w-100" src="/uploads/{{$slider->image}}" alt="slide"></a> </div>

					@endforeach
					@endif                        
					
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
                </div>
                <div class="col-lg-6 col-md-6 pl-2 pr-2">
                    <div id="carouselExampleControls2" class="carousel slide" data-interval="8000" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleControls2" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleControls2" data-slide-to="1"></li>
                            <li data-target="#carouselExampleControls2" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
						@if(isset($sliders->four))
						@php $i=0; @endphp
						@foreach($sliders->four as $slider)
                                    <div class="carousel-item  @if($i++ ==0 )active @endif"> <a href="{{$slider->link}}"><img class="d-block w-100" src="/uploads/{{$slider->image}}" alt="slide"></a> </div>

						@endforeach
						@endif       
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('front.includes.flashsales')


<!--Spicy Deals-->
@include('front.includes.deals')


<!--Best offer-->
{{--@include('front.includes.bestoffers')--}}


<!--Winner Section-->
@include('front.includes.winnersBlock')



@endsection