@extends('front.master')

@section('content')

    <section class="deal_brows_categories">
        <div class="container-fluid ">
            <div class="row">
                <h4 class="w-100 text-center text-white pt-2">POPULAR CATEGORIES</h4>

                <div class="col-md-2 col-6 text-center"><a href="{{route('deallistDealsCategory',0)}}"> <img src="/theme/images/deal_categories_deal.png"
                                                                         class="img-fluid">
                        <p class="text-white">All Deals</p>
                    </a>
                </div>

                @foreach($category as $cat)
                    <div class="col-md-2 col-6 text-center"><a href="{{route('deallistDealsCategory',$cat->id)}}"> <img src="/uploads/{{$cat->image}}"
                                                                             class="img-fluid" style="max-height: 73px;">
                            <p class="text-white">{{$cat->name}}</p>
                        </a>
                    </div>

                @endforeach


            </div>
        </div>
    </section>

    <div class="container-fluid home_flash_sale">
        <div class="col-lg-12">
            <div class="row mb-4">

                @foreach($deals as $deal)
                <div class=" col-lg-4 col-md-4 pl-2 p-2"><a href="{{route('dealItem',$deal->id)}}">
                        <img src="/uploads/{{$deal->small_image}}" class="img-fluid w-100"></a>
                    <div class="col-lg-12 text-right share_icon">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="javascript:void(0)"  data-type="dealItem" data-id="{{$deal->id}}" class="btn btn-secondary addtoFavoriteBtn" title="Favorite"> <i
                                            class="fas fa-heart"></i></a></li>
                            <li class="list-inline-item">
                                <div class="dropdown">
                                    <button class="btn btn-secondary " type="button" id="dropdownMenuButton"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                                class="fas fa-share-alt"></i></button>
                                    <div  data-shareUrl="{{route('dealItem',$deal->id)}}?ref={{getReferalId()}}" class="v_shareParent dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        @include('front.includes.shareDropDownContents')
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <a href="javascript:void(0)" data-id="{{$deal->id}}" data-type="deal" class="btn btn-primary deal_addcart" title="Add To Cart"><i class="fas fa-cart-plus"></i></a>

                </div>


                @endforeach

            </div>

        </div>
    </div>


@endsection


@push('scripts')


@endpush
