
@extends('front.master')

@section('content')

    <!-- Slider top-->
    <div class="container-fluid home_slider p-0">
        <div id="carouselExampleControls" class="carousel slide" data-interval="12000" data-ride="carousel">

            <div class="carousel-inner">
            @php $i =0;@endphp
            @foreach($flashSales as $flashSale)
                @php $banners = json_decode($flashSale->banners) @endphp
                @foreach($banners as $banner)
                        <div class="carousel-item @if(!$i++) active @endif">
                            <a href="{{route('flashsaleItem',$flashSale->id)}}">
                                <img class="d-block w-100" src="/uploads/{{$banner}}" alt="">
                            </a>
                        </div>
                @endforeach
            @endforeach

            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <!-- Subscribe flash sale-->
    <div class="container-fluid Subscribe text-center ">
        <ul class="list-inline">
            <li class="list-inline-item"> <i class="fas fa-paper-plane mr-3"></i>Subscribe Flash Sale </li>
            <li class="list-inline-item">
                <div class="col-auto p-0">
                    <div class="input-group ">
                        <input type="text" class="form-control " id="" placeholder="Enter Your Email" style="">

                        <div class="input-group-append">
                            <button class="btn btn-light" type="button" style="background-color:#0081da; border:none; color:#fff; font-size:15px;" >Subscribe</button>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <!--Flash sale-->
    <div class="container-fluid home_flash_sale">
        <div class="col-lg-12">
            <div class="row mb-4">
                <div class="col-xl-12 pl-2 pr-2"><h3>Flash Sale</h3></div>
                @foreach($flashSales as $flashSale)
                <div class=" col-lg-4 col-md-4 pl-2 p-2"><a href="{{route('flashsaleItem',$flashSale->id)}}"><img src="/uploads/{{$flashSale->small_image}}" class="img-fluid w-100"></a>
                    <div class="col-lg-12 text-right share_icon">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a data-type="flashSaleItem" data-id="{{$flashSale->id}}"  class="btn btn-secondary addtoFavoriteBtn" title="Favorite"> <i class="fas fa-heart"></i></a> </li>
                            <li class="list-inline-item">
                                <div class="dropdown">
                                    <button class="btn btn-secondary "  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-share-alt"></i> </button>
                                    <div  data-shareUrl="{{route('flashsaleItem',$flashSale->id)}}?ref={{getReferalId()}}" class="v_shareParent dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        @include('front.includes.shareDropDownContents')
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>

@endsection


@push('scripts')

@endpush
