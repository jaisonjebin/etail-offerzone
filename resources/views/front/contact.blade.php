
@extends('front.master')

@section('content')

    <div class="container-fluid contact">
        <div class="row contact-condent">
            <div class="col-lg-7 col-xl-7 col-md-7 contact_bg">  </div>
            <div class="col-lg-5 col-xl-5 col-md-5 p-4 contact-address">
                <h3>We Like To Hear From You</h3>
                <p>Calicut Intl, Airport Jn<br>

                    Neetanimmal, Kerala<br>

                    Customer Care: +91 8086456666<br>

                    Email: info@dealdaa.com </p>
                <form class="contactform" method="post">
                    <div class="col-9 alert"></div>
                    <div class="form-group">
                        <input type="text" id="j_name" name="name" class="form-control w-75"  style="border-radius:0" aria-describedby="" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input type="email" id="j_email" name="email" class="form-control w-75" style="border-radius:0" aria-describedby="emailHelp  " placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="text" id="j_subject" name="subject" class="form-control w-75"  style="border-radius:0" aria-describedby=" " placeholder="Subject">
                    </div>
                    <div class="form-group">
                        <textarea id="j_message" name="message" class="form-control w-75" style="border-radius:0;max-height:120px; min-height:120px; " rows="3" placeholder="Message"></textarea>
                    </div>
                    <a class="btn btn-primary j_contactformsubmit">Submit</a>
                </form>
            </div>
        </div>
    </div>



@endsection


@push('scripts')
<script>
    $('.j_contactformsubmit').on('click',function () {
        console.log($("#j_name").val());
        console.log($("#j_email").val());
        console.log($("#j_message").val());
        if(($("#j_name" ).val()=='')||($("#j_email" ).val()=='')||($("#j_message" ).val()==''))
        {
            toastr.error('Please fill all fields');
            $('.contactform .alert').removeClass('alert-danger');
            $('.contactform .alert').removeClass('alert-success');
            $('.contactform .alert').addClass('alert-danger');
            $('.contactform .alert').html('Please fill all fields')
        }
        else {
            $.ajax({
                url: '{{route('contact.form')}}',
                type: 'POST',
                data: $('.contactform').serialize(),
                dataType: 'json',
                success: function (data) {
                    if (data.type == 'success')
                    {
                        $('.contactform .alert').removeClass('alert-danger');
                        $('.contactform .alert').removeClass('alert-success');
                        $('.contactform .alert').addClass('alert-success');
                        $('.contactform .alert').html('');
                        $('.contactform .alert').html('Message send successfully.We will contact you soon');
                    }
                    else {
                        $('.contactform .alert').removeClass('alert-danger');
                        $('.contactform .alert').removeClass('alert-success');
                        $('.contactform .alert').addClass('alert-danger');
                        $('.contactform .alert').html('');
                        $('.contactform .alert').html('Something went wrong.');
                    }
                },
                error: function (response, b, c) {
                    $('.contactform .alert').removeClass('alert-danger');
                    $('.contactform .alert').removeClass('alert-success');
                    $('.contactform .alert').addClass('alert-danger');
                    $('.contactform .alert').html('');
                    $.each(JSON.parse(response.responseText)['msg'], function (key, val) {
                        $('.contactform .alert').append(val);
                    });
                }
            });
        }
    });
</script>

@endpush
