@extends('front.master')

@section('content')
    <style>
        .wallet .wallet_summery .card h5 {
            font-size: 50px;
        }
        .wallet .wallet_summery .card p {
            font-size: 28px;
        }
    </style>
    <div class="container-fluid wallet myprofile py-4">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-12">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#accountsummery"
                       role="tab" aria-controls="v-pills-home" aria-selected="true">Account Summery</a>
                    <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#points" role="tab"
                       aria-controls="v-pills-profile" aria-selected="false">Points</a>
                    <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#transactionhistory"
                       role="tab" aria-controls="v-pills-messages" aria-selected="false">Transaction
                        History</a>
                    {{--<a class="nav-link " id="v-pills-settings-tab" data-toggle="pill" href="#withdrawals" role="tab"--}}
                       {{--aria-controls="v-pills-settings" aria-selected="false">Withdrawals</a>--}}
                    {{--<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#inbox" role="tab"--}}
                       {{--aria-controls="v-pills-settings" aria-selected="false">In box</a>--}}
                </div>
            </div>
            <div class="col-xl-9 col-lg-9 col-md-12">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="accountsummery" role="tabpanel"
                         aria-labelledby="v-pills-home-tab">
                        <div class="col-xl-12 wallet_summery pt-5">
                            <div class="card-deck">
                                {{--<div class="card">--}}
                                    {{--<div class="card-body text-center">--}}
                                        {{--<h5 class="card-title">{{date('F')}}</h5>--}}
                                        {{--<p class="card-text">{{date('Y')}}</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="card">
                                    <div class="card-body text-center">
                                        <h5 class="card-title">MY</h5>
                                        <p class="card-text">Points</p>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body text-center">
                                        <h5 class="card-title">{{$accountSummary['totalPoints']}}
                                            </h5>
                                        <p class="card-text">Credit</p>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body text-center">
                                        <h5 class="card-title">{{$accountSummary['withdrawalPoints']}}
                                            </h5>
                                        <p class="card-text">Withdrawal</p>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body text-center">
                                        <h5 class="card-title">{{Auth::user()->balance?Auth::user()->balance:0}}
                                        </h5>
                                        <p class="card-text">Total</p>
                                    </div>
                                </div>
                            </div>
                            <p class="text-center pt-5">If you need any help or support, please contact us at
                                support@dealdaa.com</p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="points" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                        <div class="col-xl-12 wallet_points">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="mypurchase-tab" data-toggle="tab" href="#mypurchase"
                                       role="tab" aria-controls="mypurchase" aria-selected="true">My Purchase</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="clubpoints-tab" data-toggle="tab" href="#clubpoints"
                                       role="tab" aria-controls="clubpoints" aria-selected="false">Club Points</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="mypurchase" role="tabpanel"
                                     aria-labelledby="mypurchase-tab">
                                    <div class="col-xl-12 border pt-3">
                                        <table class="table table-bordered table-responsive-sm">
                                            <thead class="thead-dark">
                                            <tr>
                                                <th>Date</th>
                                                <th>Transaction ID</th>
                                                <th>Merchant ID</th>
                                                <th>Points</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($transactionsHistory)
                                                @php $total =   0; @endphp
                                                @foreach($transactionsHistory as $history)
                                                    @if($history['metaType']=='myPurchase')
                                                        @php $total =   $total+$history['amount']; @endphp
                                                    <tr>
                                                        <td>{{$history['date']}}</td>
                                                        <td>{{substr($history['transactionId'],5)}}</td>
                                                        <td>{{isset(json_decode($history['details'])->product_id)?(json_decode($history['details'])->product_id):'NA'}}</td>
                                                        <td>{{$history['amount']}}</td>
                                                    </tr>
                                                    @endif
                                                    @if($loop->last)
                                                        <tr>
                                                            <td colspan="3" class="text-right"><strong>Total Points</strong></td>
                                                            <td><strong>{{$total}}</strong></td>
                                                        </tr>
                                                    @endif
                                                @endforeach

                                            @else
                                                <tr>
                                                    <td colspan="4" class="text-center"><strong>You do have any transaction in your
                                                            account</strong></td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="clubpoints" role="tabpanel"
                                     aria-labelledby="clubpoints-tab">
                                    <div class="col-xl-12 border pt-3">
                                        <table class="table table-bordered table-responsive-sm">
                                            <thead class="thead-dark">
                                            <tr>
                                                <th>Date</th>
                                                <th>Transaction ID</th>
                                                <th>User ID</th>
                                                <th>Merchant ID</th>
                                                <th>Points</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($transactionsHistory)
                                                @php $total =   0; @endphp
                                                @foreach($transactionsHistory as $history)
                                                    @if($history['metaType']=='clubPoints')
                                                        @php $total =   $total+$history['amount']; @endphp
                                                        <tr>
                                                            <td>{{$history['date']}}</td>
                                                            <td>{{substr($history['transactionId'],5)}}</td>
                                                            <td>{{isset(json_decode($history['details'])->user_id)?(json_decode($history['details'])->user_id):'NA'}}</td>
                                                            <td>{{isset(json_decode($history['details'])->product_id)?(json_decode($history['details'])->product_id):'NA'}}</td>
                                                            <td>{{$history['amount']}}</td>
                                                        </tr>
                                                    @endif
                                                    @if($loop->last)
                                                        <tr>
                                                            <td colspan="4" class="text-right"><strong>Total Points</strong></td>
                                                            <td><strong>{{$total}}</strong></td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4" class="text-center"><strong>You do have any transaction in your
                                                            account</strong></td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <p class="text-center pt-5">If you need any help or support, please contact us at
                                support@dealdaa.com</p>
                        </div>
                    </div>
                    <div class="tab-pane fade transactionhistory" id="transactionhistory" role="tabpanel"
                         aria-labelledby="v-pills-profile-tab">
                        <div class="col-xl-12 pt-3">
                            <table class="table table-bordered table-responsive-sm">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Date & Time</th>
                                    <th>Transaction ID</th>
                                    <th>Type</th>
                                    <th>Details</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($transactionsHistory)
                                    @foreach($transactionsHistory as $history)
                                        <tr>
                                            <td>{{$history['date']}}</td>
                                            <td>{{substr($history['transactionId'],5)}}</td>
                                            <td class="text-capitalize">{{$history['type']}}</td>
{{--                                            {{dd(count(json_decode($history['details'],true)))}}--}}
                                            <td>
                                            @if(json_decode($history['details'],true))
                                                {{(json_decode($history['details'])->type=='myPurchase') ? 'Purchase Point': ''}}
                                                {{(json_decode($history['details'])->type =='clubPoints')? 'Share Point':''}}
                                            @endif
                                            </td>
                                            <td>{{$history['amount']}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center"><strong>You do have any transaction in your
                                                account</strong></td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <p class="text-center pt-5">If you need any help or support, please contact us at
                                support@dealdaa.com</p>
                        </div>
                    </div>
                    {{--<div class="tab-pane fade" id="withdrawals" role="tabpanel" aria-labelledby="v-pills-messages-tab">--}}
                    {{--<div class="col-xl-12">NO DATA FOUND</div>--}}
                    {{--</div>--}}
                    {{--<div class="tab-pane fade" id="inbox" role="tabpanel" aria-labelledby="v-pills-settings-tab">--}}
                    {{--<div class="col-xl-12">NO DATA FOUND</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')

@endpush