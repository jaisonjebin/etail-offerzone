@extends('front.master')

@section('content')

    <link rel='stylesheet prefetch' href='/theme/css/tablesaw.stackonly.css'>

    <div class="container-fluid myprofile py-4">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-12">
                <div class="nav flex-column nav-pills nav-tabs" id="v-pills-tab" role="tablist"
                     aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-profile"
                       role="tab" aria-controls="v-pills-home" aria-selected="true">
                        <i class="fas fa-user"></i>
                        <span>My Profile</span></a>
                    <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-purchase" role="tab"
                       aria-controls="v-pills-profile" aria-selected="false">
                        <i class="fas fa-list"></i>
                        <span> My Purchase</span></a>
                    <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-favorite" role="tab"
                       aria-controls="v-pills-messages" aria-selected="false">
                        <i class="fas fa-heart"></i>
                        <span>Favorites</span></a>
                    <a class="nav-link " id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-team" role="tab"
                       aria-controls="v-pills-settings" aria-selected="false">
                        <i class="fas fa-users"></i>
                        <span>My Team</span></a>
                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-notification"
                       role="tab" aria-controls="v-pills-settings" aria-selected="false">
                        <i class="fas fa-bell"></i>
                        <span>Notifications</span></a>
                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-rewards" role="tab"
                       aria-controls="v-pills-settings" aria-selected="false">
                        <i class="fas fa-trophy"></i>
                        <span>Rewards</span>
                    </a>
                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-cart" role="tab"
                       aria-controls="v-pills-settings" aria-selected="false">
                        <i class="fas fa-shopping-cart"></i>
                        <span> Cart</span>
                    </a>
                </div>
            </div>
            <div class="col-xl-9 col-lg-9 col-md-12">
                <div class="tab-content" id="v-pills-tabContent">

                    <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel"
                         aria-labelledby="v-pills-home-tab">


                        <div class="col-xl-12 myprofile_home">
                            <h4>ADDRESS</h4>

                            @foreach($dAddress as $addr)
                                <div class="col-xl-12 checkout_delivery_here border mb-3 pt-3 pb-3 addessBlock">
                                    <h5><strong><span style="display:none;" data-edit="id">{{$addr->id}}</span> <span
                                                    data-edit="fullName">{{$addr->fullName}}</span> - <span
                                                    data-edit="mobile">{{$addr->mobileNumber}}</span></strong></h5>
                                    <p><span data-edit="addr1">{{$addr->addr1}}</span><br>
                                        <span data-edit="addr2">{{$addr->addr2}}</span>, <span
                                                data-edit="city">{{$addr->city}}</span>, <span
                                                data-edit="state">{{$addr->state}}</span> - <span
                                                class="font-weight-bold"><span
                                                    data-edit="pincode">{{$addr->pincode}}</span></span>
                                        <span style="display:none;" data-edit="landmark">{{$addr->landmark}}</span>
                                    </p>
                                    <a class="btn btn-link editAddrBtn">Edit</a>
                                    <a href="{{route('user.deleteDeliveryAddress',$addr->id)}}"
                                       class="btn btn-link delete">Delete</a>
                                </div>
                            @endforeach

                            <div class="col-xl-12 checkout_delivery_here border mb-3 pt-3 pb-3 add_newaddress_btn">
                                <button class="btn btn-secondary w-100 "><i class="fas fa-plus-circle"></i> Add New
                                    Address
                                </button>
                            </div>

                            <div class="col-lg-12 border p-4 add_newaddress">
                                <form method="post" action="{{route('user.addDeliveryAddess')}}" class="addDadress">
                                    @csrf
                                    <input type="hidden" value="0" name="id">
                                    <div class="form-group">
                                        <label class="checkout-label" for="inputAddress">Full Name</label>
                                        <input type="text" class="form-control" id="" placeholder="" name="fullName"
                                               required>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label class="checkout-label" for="inputEmail4">Mobile Number</label>
                                            <input type="tel" class="form-control" id="" placeholder="" name="mobile"
                                                   required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="checkout-label" for="inputPassword4">Pincode </label>
                                            <input type="tel" class="form-control" id="" placeholder="" required
                                                   name="pincode">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkout-label" for="">House No., Flat, Building, Company
                                            ,Appartment</label>
                                        <input type="text" class="form-control" id="" placeholder="" name="addr1"
                                               required>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkout-label" for="inputAddress2">Area, Street, Village,
                                            Sector </label>
                                        <input type="text" class="form-control" id="" placeholder="" name="addr2">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label class="checkout-label" for="inputCity">City</label>
                                            <input type="text" class="form-control" id="inputCity" name="city" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="checkout-label" for="inputState">State</label>
                                            <select id="inputState" class="form-control" required name="state">
                                                <option selected value="">Choose...</option>
                                                <option>Kerala</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="checkout-label" for="inputAddress2">Landmark</label>
                                        <input type="text" class="form-control" id="" placeholder="" name="landmark">
                                    </div>
                                    <div class="col-lg-12 p-0 text-right">
                                        <button type="submit" class="btn btn-primary">SAVE</button>
                                    </div>
                                </form>
                            </div>


                        </div>


                    </div>
                    <div class="tab-pane fade" id="v-pills-purchase" role="tabpanel"
                         aria-labelledby="v-pills-profile-tab">
                        <div class="col-xl-12 myprofile_purchase">
                            <div class="col-xl-12 border py-2 mb-2">
                                <div class="media"><a href=""><img class="mr-3" src="/theme/images/flash sale2.jpg"
                                                                   alt=""></a>
                                    <div class="media-body"><a href="">
                                            <h5 class="mt-0">Beats by Dr. Dre urBeats In-Ear Headphones</h5>
                                            <ul class="list-inline m-0">
                                                <li class="list-inline-item offer_amt">60% OFF</li>
                                                <li class="list-inline-item"><span class="cu_amount"><i
                                                                class="fas fa-rupee-sign"></i>400.00</span></li>
                                                <li class="list-inline-item"><span class="off_amount"><i
                                                                class="fas fa-rupee-sign"></i>100.00</span></li>
                                            </ul>
                                            <p class="m-0"><strong>7.2 Km</strong> - Nadakkavu, Kozhokkode</p>
                                        </a></div>
                                </div>
                            </div>
                            <div class="col-xl-12 border py-2 mb-2">
                                <div class="media"><a href=""><img class="mr-3" src="/theme/images/flash sale2.jpg"
                                                                   alt=""></a>
                                    <div class="media-body"><a href="">
                                            <h5 class="mt-0">Beats by Dr. Dre urBeats In-Ear Headphones</h5>
                                            <ul class="list-inline m-0">
                                                <li class="list-inline-item offer_amt">60% OFF</li>
                                                <li class="list-inline-item"><span class="cu_amount"><i
                                                                class="fas fa-rupee-sign"></i>400.00</span></li>
                                                <li class="list-inline-item"><span class="off_amount"><i
                                                                class="fas fa-rupee-sign"></i>100.00</span></li>
                                            </ul>
                                            <p class="m-0"><strong>7.2 Km</strong> - Nadakkavu, Kozhokkode</p>
                                        </a></div>
                                </div>
                            </div>
                            <div class="col-xl-12 border py-2 mb-2">
                                <div class="media"><a href=""><img class="mr-3" src="/theme/images/flash sale2.jpg"
                                                                   alt=""></a>
                                    <div class="media-body"><a href="">
                                            <h5 class="mt-0">Beats by Dr. Dre urBeats In-Ear Headphones</h5>
                                            <ul class="list-inline m-0">
                                                <li class="list-inline-item offer_amt">60% OFF</li>
                                                <li class="list-inline-item"><span class="cu_amount"><i
                                                                class="fas fa-rupee-sign"></i>400.00</span></li>
                                                <li class="list-inline-item"><span class="off_amount"><i
                                                                class="fas fa-rupee-sign"></i>100.00</span></li>
                                            </ul>
                                            <p class="m-0"><strong>7.2 Km</strong> - Nadakkavu, Kozhokkode</p>
                                        </a></div>
                                </div>
                            </div>
                            <div class="col-xl-12 border py-2 mb-2">
                                <div class="media"><a href=""><img class="mr-3" src="/theme/images/flash sale2.jpg"
                                                                   alt=""></a>
                                    <div class="media-body"><a href="">
                                            <h5 class="mt-0">Beats by Dr. Dre urBeats In-Ear Headphones</h5>
                                            <ul class="list-inline m-0">
                                                <li class="list-inline-item offer_amt">60% OFF</li>
                                                <li class="list-inline-item"><span class="cu_amount"><i
                                                                class="fas fa-rupee-sign"></i>400.00</span></li>
                                                <li class="list-inline-item"><span class="off_amount"><i
                                                                class="fas fa-rupee-sign"></i>100.00</span></li>
                                            </ul>
                                            <p class="m-0"><strong>7.2 Km</strong> - Nadakkavu, Kozhokkode</p>
                                        </a></div>
                                </div>
                            </div>
                            <div class="col-xl-12 border py-4 mb-2 text-center">
                                <h5>Empty Purchase</h5>
                                <p class="m-0">You have no items in your purchase. Start adding!</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-favorite" role="tabpanel"
                         aria-labelledby="v-pills-messages-tab">
                        <div class="col-xl-12 myprofile_purchase">

                            @php
                                $i = 0;
                            @endphp
                            @foreach($favo['flashSale'] as $flashSale)
                                @php
                                    $i++;
                                @endphp
                                <div class="col-xl-12 border py-2 mb-2 favoriteBlock">
                                    <div class="media"><a href=""><img class="mr-3"
                                                                       src="/uploads/{{$flashSale->small_image}}"
                                                                       alt=""></a>
                                        <div class="media-body">
                                            <h5 class="mt-0"><a href="">{{$flashSale->title}}</a> <a
                                                        data-type="flashSaleItem" data-id="{{$flashSale->id}}"
                                                        class="float-right removeFavorite" title="Remove"><i
                                                            class="fas fa-trash"></i></a></h5>
                                            <a href="">
                                                <ul class="list-inline m-0">
                                                    <li class="list-inline-item offer_amt">{{ ceil((($flashSale->price-$flashSale->offer_price)/($flashSale->price??1))*100) }}
                                                        % OFF
                                                    </li>
                                                    <li class="list-inline-item"><span class="cu_amount"><i
                                                                    class="fas fa-rupee-sign"></i>{{$flashSale->price}}</span>
                                                    </li>
                                                    <li class="list-inline-item"><span class="off_amount"><i
                                                                    class="fas fa-rupee-sign"></i>{{$flashSale->offer_price}}</span>
                                                    </li>
                                                </ul>
                                            </a></div>
                                    </div>
                                </div>
                            @endforeach
                            @foreach($favo['dealItem'] as $deal)

                                @php
                                    $i++;
                                @endphp
                                <div class="col-xl-12 border py-2 mb-2 favoriteBlock">
                                    <div class="media"><a href=""><img class="mr-3"
                                                                       src="/uploads/{{$deal->small_image}}" alt=""></a>
                                        <div class="media-body">
                                            <h5 class="mt-0"><a href="">{{$deal->title}}</a> <a data-type="dealItem"
                                                                                                data-id="{{$deal->id}}"
                                                                                                class="float-right removeFavorite"
                                                                                                title="Remove"><i
                                                            class="fas fa-trash"></i></a></h5>
                                            <a href="">
                                                <ul class="list-inline m-0">
                                                    <li class="list-inline-item offer_amt">{{ ceil((($deal->price-$deal->offer_price)/($deal->price??1))*100) }}
                                                        % OFF
                                                    </li>
                                                    <li class="list-inline-item"><span class="cu_amount"><i
                                                                    class="fas fa-rupee-sign"></i>{{$deal->price}}</span>
                                                    </li>
                                                    <li class="list-inline-item"><span class="off_amount"><i
                                                                    class="fas fa-rupee-sign"></i>{{$deal->offer_price}}</span>
                                                    </li>
                                                </ul>
                                            </a></div>
                                    </div>
                                </div>
                            @endforeach

                            @if($i == 0)
                                <div class="col-xl-12 border py-4 mb-2 text-center">
                                    <h5>Empty Favorite</h5>
                                    <p class="m-0">You have no items in your favorite. Start adding!</p>
                                </div>
                            @endif


                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-team" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                        <div class="col-xl-12 myprofile_team">
                            <table class="table table-striped table-responsive-sm">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($myteam)
                                    @php $i=1;@endphp
                                    @foreach($myteam as $team)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$team->name}}</td>
                                            <td>{{$team->email}}</td>
                                            <td>{{$team->created_at->format('d,F Y')}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center py-5"><h3>Empty Team List</h3>
                                            <p>You have no referral in your team list. Start adding!</p></td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-notification" role="tabpanel"
                         aria-labelledby="v-pills-profile-tab">
                        <div class="col-xl-12 myprofile_notification">
                            <div class="card mb-2">
                                <div class="card-body"> Lorem Ipsum is simply dummy text of the printing and typesetting
                                    industry. Lorem Ipsum has been the industry's standard dummy text ever since the
                                    1500s
                                    <p class="text-muted m-0 pt-2">02-05-2018</p>
                                </div>
                            </div>
                            <div class="card mb-2">
                                <div class="card-body"> Lorem Ipsum is simply dummy text of the printing and typesetting
                                    industry.
                                    <p class="text-muted m-0 pt-2">02-05-2018</p>
                                </div>
                            </div>
                            <div class="card mb-2">
                                <div class="card-body"> Lorem Ipsum is simply dummy text of the printing and typesetting
                                    industry. Lorem Ipsum has been the industry's standard
                                    <p class="text-muted m-0 pt-2">02-05-2018</p>
                                </div>
                            </div>
                            <div class="card mb-2">
                                <div class="card-body"> Lorem Ipsum is simply dummy text of the printing
                                    <p class="text-muted m-0 pt-2">02-05-2018</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-rewards" role="tabpanel"
                         aria-labelledby="v-pills-messages-tab">
                        <div class="col-xl-12 myprofile_rewards">
                            <div class="media border mb-3 p-3"><img class="mr-3" src="/theme/images/trophy.png" alt="">
                                <div class="media-body">
                                    <h5 class="mt-0">02-05-2018</h5>
                                    <p class="m-0"> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
                                        scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at,
                                        tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec
                                        lacinia congue felis in faucibus.</p>
                                </div>
                            </div>
                            <div class="media border mb-3 p-3"><img class="mr-3" src="/theme/images/trophy.png" alt="">
                                <div class="media-body">
                                    <h5 class="mt-0">02-05-2018</h5>
                                    <p class="m-0"> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
                                        scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at,
                                        tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec
                                        lacinia congue felis in faucibus.</p>
                                </div>
                            </div>
                            <div class="media border mb-3 p-3"><img class="mr-3" src="/theme/images/trophy.png" alt="">
                                <div class="media-body">
                                    <h5 class="mt-0">02-05-2018</h5>
                                    <p class="m-0"> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
                                        scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at,
                                        tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec
                                        lacinia congue felis in faucibus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-cart" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                        <div class="col-xl-12 cart">

                            <table class="table border table-hover tablesaw tablesaw-stack" data-tablesaw-mode="stack"
                                   style="margin-bottom:0;">
                                <thead>
                                <tr>
                                    <th width="400">Product</th>
                                    <th>Available</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th width="70"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cart as $cartItem)
                                    <tr data-id="{{$cartItem->id}}" class="cartItemTr">
                                        <td>
                                            <div class="media">
                                                <a href="@if($cartItem->type == 'deal'){{route('dealItem',[$cartItem->product_id])}}@endif"> <img class="mr-3" src="{{json_decode($cartItem->meta)?json_decode($cartItem->meta,true)['product_img']:''}}" alt=""></a>
                                                <div class="media-body"><a href="">
                                                        <p>{{$cartItem->name}}</p>
                                                    </a></div>
                                            </div>
                                        </td>
                                        <td>In stock</td>
                                        <td>
                                            <div class="quantity">
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-primary btn-number btn-sm border-radius-0 j_cart_count" data-type="minus" data-field="quant[{{$cartItem->type}}][{{$cartItem->product_id}}]"> <i class="fa fa-minus"></i> </button>
                                                    </span>
                                                    <input type="text" name="quant[{{$cartItem->type}}][{{$cartItem->product_id}}]"
                                                           class="form-control input-number form-control-sm border-radius-0 itemQty"
                                                           value="{{$cartItem->quantity}}" min="-1" max="999">
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-primary btn-number btn-sm border-radius-0 j_cart_count" data-type="plus" data-field="quant[{{$cartItem->type}}][{{$cartItem->product_id}}]"> <i class="fa fa-plus"></i> </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>₹ <span class="itemTotal" data-price="{{$cartItem->price}}" >{{ ((float)$cartItem->price) * ((float)$cartItem->quantity) }}</span></td>
                                        <td class="action">
                                            <button class="btn btn-sm btn-danger deleteFromCart" title="Delete"><i
                                                        class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            <table class="table border table-bordered totalTable">
                                    <tbody>
                                    @if($cart->count())
                                    <tr>
                                        <td class="text-right text-large">Sub-Total</td>
                                        <td width="120" class="text-left text-large">₹<span class="allSubTotal">0</span></td>
                                    </tr>
                                    <tr>
                                        <td class="text-right text-large">Shipping</td>
                                        <td class="text-left text-large">Free</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right text-large"><strong>Total</strong></td>
                                        <td class="text-left text-large"><strong>₹<span class="grandTotal">0</span></strong></td>
                                    </tr>
                                    @else
                                    <tr>
                                        <td colspan="4" class="text-center py-5">
                                          <h3>Empty Cart List</h3>
                                          <p>You have no items in your cart list. Start adding!</p>
                                        </td>
                                    </tr>


                                    @endif

                                    </tbody>
                                </table>


                            <div class="row">

                                <div class="col-6  col-md-6 continue_shop"><a href="/">Continue Shopping</a></div>
                                @if($cart->count())
                                    <div class="col-6 col-md-6  text-right checkoutBtnContainer">
                                        <a href="{{route('user.checkOut')}}" class="btn btn-lg  text-uppercase "
                                               style="background-color:#0191ed; color:#fff;font-size:15px; font-weight:600">
                                            Check out
                                        </a>
                                    </div>
                                @endif
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')


    <script type="text/javascript" src="/theme/js/quantity.js"></script>
    <script src='/theme/js/tablesaw.stackonly.js'></script>

    <script>
        $(document).ready(function () {
//add_newaddress_btn
            $('[name=id]').val('0');
            $('.add_newaddress').hide(300);
            $('.add_newaddress_btn .btn').click(function () {
                $('.addDadress .form-control').val('');
                $('[name=id]').val('0');
                $('.add_newaddress').show(300);
                $('.add_newaddress_btn').hide(300);
            });

        })


        $('.editAddrBtn').on('click', function () {
            $('.add_newaddress').show(300);
            $(this).closest('.addessBlock').find('[data-edit]').each(function () {
                $('.addDadress').find('[name=' + $(this).attr('data-edit') + ']').val($(this).html()).focus();
            });

        });

        $('.removeFavorite').on('click', function () {

            $type = $(this).attr('data-type');
            $id = $(this).attr('data-id');

            $.ajax({
                context: $(this),
                url: '{{route('user.removeFavorite',['',''])}}/' + $type + '/' + $id,
                dataType: 'json',
                success: function (data) {
                    toastr.info(data.msg);
                    $(this).closest('.favoriteBlock').slideUp();
                }
            })
        })
    </script>

@endpush