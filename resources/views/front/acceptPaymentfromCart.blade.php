
@extends('front.master')

@section('content')
    <style>
        .summaryblock{
            border: 1px solid #c1c8d2;
            padding: 5px;
            margin: 4px;
        }
    </style>
<!-- Slider top-->
<div class="container-fluid flash_sale_checkout py-4">
    <div class="col-lg-12 pr-4 pl-4 flash_sale_checkout12">

        <div class="row">
            <div class="col-lg-12 ">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12 mb-2">

                        <div class="col-lg-12 order-summer">

                            <h5 class="text-center">ORDER SUMMARY </h5>
                            @php
                                $total = 0;
                                $dealsSent = json_decode($order->order_details);
                            @endphp
                            @foreach($dealsSent->deals as $item)
                                <div class="row summaryblock">
                                    <div class="col-md-4">
                                         <img src="/uploads/{{$item->small_image}}" class="img-fluid ">
                                    </div>
                                    @php
                                        if((!$item->online_sell_price)){
                                            $item->online_sell_price = 0;
                                        }
                                    @endphp
                                    <div class="col-md-8">
                                        <div class="strong col-md-12"><strong>{{$item->title}}</strong></div>
                                        <div class="col-md-12">Price : {{$item->online_sell_price}}</div>
                                        <div class="col-md-12">Quantity : {{$item->quantitySale}}</div>
                                        <div class="col-md-12">Total : {{$item->online_sell_price * $item->quantitySale}}</div>

                                        @php
                                            $total = $total + ($item->online_sell_price * $item->quantitySale);
                                        @endphp
                                    </div>
                                </div>
                            @endforeach

                            <div class="row summaryblock">
                                <h5 class="w-100 text-center" style="font-weight: 900;">Amount Payable :  <i class="fas fa-rupee-sign"></i>{{$total}}</h5>
                            </div>


                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12 col-sm-12 ">
                        <h5 class="pb-3" style="font-weight:600">Delivery Address</h5>
                        @php
                            $addr = json_decode($order->address_details);
                        @endphp
                        @if($addr)
                        <div class="col-xl-12 checkout_delivery_here border mb-3 pt-3 pb-3 addessBlock">
                            <h5><strong><span style="display:none;" data-edit="id">{{$addr->id}}</span> <span data-edit="fullName">{{$addr->fullName}}</span> - <span data-edit="mobile">{{$addr->mobileNumber}}</span></strong></h5>
                            <p><span data-edit="addr1">{{$addr->addr1}}</span><br>
                                <span data-edit="addr2">{{$addr->addr2}}</span>, <span data-edit="city">{{$addr->city}}</span>, <span data-edit="state">{{$addr->state}}</span> - <span class="font-weight-bold"><span data-edit="pincode">{{$addr->pincode}}</span></span>
                                <span style="display:none;" data-edit="landmark">{{$addr->landmark}}</span>
                            </p>
                        </div>
                        @endif

                        @if($order->order_status != 1)
                            <div class="col-lg-12">
                                <form action="{{route('user.viewOrder',[$order->id])}}" method="POST">
                                @csrf
                                <!-- Note that the amount is in paise = 50 INR -->
                                    <script
                                            src="https://checkout.razorpay.com/v1/checkout.js"
                                            data-key="{{config('razorpay.key_id')}}"
                                            data-order_id="{{$order->razorpay_order_id}}"
                                            data-buttontext="Pay with Razorpay"
                                            data-name="Dealdaa Digital Trading and Services Pvt Ltd"
                                            data-description="{{Auth::user()->name}}"
                                            data-image="{{url('/theme/images/footer-logo.png')}}"
                                            data-prefill.name="{{Auth::user()->name}}"
                                            data-prefill.contact="{{Auth::user()->phone}}"
                                            data-theme.color="#3381da"
                                    ></script>
                                    <input type="hidden" value="Hidden Element" name="hidden">
                                </form>
                            </div>
                        @else
                            <div class="col-lg-12 text-success">
                                <i class="fa fa-check-circle"></i> Order Completed. @if($order->order_status == 1) Payment Successful @endif
                            </div>
                        @endif



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')

@endpush