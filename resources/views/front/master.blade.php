<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
    <link href="/theme/css/style.css?v=2" rel="stylesheet">
    <link href="/theme/css/counter.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <link rel="icon" href="/theme/images/favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/theme/css/bootstrap.min.css">
    <link href="/back/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />

    <title>Dealdaa</title>
</head>
<body>
@include('front.header')

@yield('content')

<!--Advertise Section -->
<section class="advertise">
    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="row text-center">
                <div class="col-lg-6 col-md-6 advertise_border">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 p-0">
                            <h3>ADVERTISE WITH US </h3>
                            <p> Deal Daa is a comprehensive Brand
                                Solutions Platform that affords myriad
                                opportunities for  brands to connect with
                                their uniquely defined Target Groups,
                                across specified markets, in a seamless,
                                unobtrusive manner through: </p>
								
								
						<div class="col-lg-12 text-center pt-2"> <a href="">Register Now</a></div>
                        </div>
                        <div class="col-lg-6 col-md-6 " style="" >
                            <h3>BENEFITS OF A ADVERTISER </h3>
                            <ul class="text-left pl-5">
                                <li>Targeted Audience</li>
                                <li>Cost-effective </li>
                                <li>Instent Response</li>
                                <li>Convenience</li>
                            </ul>
                        </div>
                    </div>
                </div>
                {{--<div class="col-lg-6  col-md-6 pl-4">--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-lg-6 col-md-6" >--}}
                        {{--<p>Deal Daa is a free to download mobile app through--}}
                            {{--which you can win gifts for free by participating in--}}
                            {{--lucky draws,without having to pay anything or buy--}}
                            {{--any products & services. </p>--}}
                    {{--</div>--}}
                    <div class="col-lg-6 col-md-6" >
                        <img src="/Thumbnail_horizontal_QRC_Dealdaa.png" class="img-responsive" style="max-width:100%;max-height: 100%; width: 100%;height: 300px; object-fit: contain;">
                    </div>
                    {{--</div>--}}

                {{--</div>--}}
                    {{--<div class="col-lg-12 text-center"> <i class="fab fa-android mr-3" style="color:#231f20"></i> <i class="fab fa-apple" style="color:#231f20;"></i> </div>--}}
                    {{--<div class="row mt-2">--}}
                        {{--<div class="col-lg-3 col-md-4 col-sm-6 text-center pt-1" >--}}
                            {{--<h4 style="font-size:17px">Get The App Link</h4>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-9 col-md-8 col-sm-6">--}}
                            {{--<div class="col-auto p-0">--}}
                                {{--<div class="input-group " >--}}
                                    {{--<input type="text" class="form-control j_getapplinkInput" id="inlineFormInputGroup" placeholder=" Your Mobile Number	" style="border-radius:0">--}}
                                    {{--<div class="input-group-append">--}}
                                        {{--<button class="btn btn-secondary j_getapplink" type="button">Submit</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
</section>


@include('front.footer')

@stack('scripts')

<script src="/back//assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>

<script>

    $('.j_getapplink').on('click',function () {
        var input   =   $('.j_getapplinkInput').val();
        if((input!='')&&(input!=undefined)&&(input!=null)&&($.isNumeric(input))&&(input.length==10))
        {
            $.ajax({
                url: '{{route('getAppLink')}}',
                type:'POST',
                data: {number:input},
                dataType: 'json',
                success: function( resp )
                {
                    toastr["success"](resp.msg);
                },
                error: function( req, status, err )
                {
                    toastr["error"](req.responseJSON.msg);
                }
            });
        }
        else
        {
            toastr["warning"]("Please Enter Valid Phone Number");
        }
    });
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    @if(Session::has('type'))
        toastr["{{Session::get('type')}}"]("{!!Session::get('text')!!}");
    @endif

    @auth
        $('.addtoFavoriteBtn').on('click',function () {

            $type = $(this).attr('data-type');
            $id = $(this).attr('data-id');

            $.ajax({
                url:'{{route('user.addtoFavorite',['',''])}}/'+$type+'/'+$id,
                dataType:'json',
                success:function (data) {
                    toastr.info(data.msg);
                }
            })

        });
    @endauth

    @guest
        $('.addtoFavoriteBtn').on('click',function () {
            $('#loginmodal').modal('show');
        });
    @endguest
    //social share function
    $(document).on('click','.v_sharebtn',function(){

        $url = $(this).closest('.v_shareParent').attr('data-shareUrl');
        console.log($url);

        if($(this).attr('data-type') == 'Facebook'){
            window.open('https://www.facebook.com/sharer/sharer.php?u='+$url, '_blank');
        }
        else if($(this).attr('data-type') == 'Twitter'){
            window.open('http://twitter.com/share?text=Checkout this Deal on DealDaa.com&url='+$url+'&hashtags=DealDaa', '_blank');
        }
        else if($(this).attr('data-type') == 'GooglePlus'){
            window.open('https://plus.google.com/share?url='+$url, '_blank');
        }
        else if($(this).attr('data-type') == 'Whatsapp'){
            window.open('whatsapp://send?text='+$url, '_blank');
        }

    });

    //cart function

    $(document).on('click','.deal_addcart',function () {
        $id = $(this).data('id');
        $type = $(this).attr('data-type');
        $(this).addClass('animated fadeOutUpBig');
        $count = parseInt($('#cartCount').html());
        $('#cartCount').html('' + ($count+1));

        $.ajax({
			context:$(this),
            url:'{{route('user.addCart',['',''])}}'+'/'+$id+'/'+$type,
            dataType:'json',
            success:function (data) {
                toastr.success(data.msg);

            },
			error:function(a,b,c){
				if(a.status == '401'){
					toastr.info("Please Login to continue");
					$(this).removeClass('animated fadeOutUpBig');
					$('#loginmodal').modal('show');

				} 
				else{
					
				}
			}
        });


    });

    $(document).on('click','.j_cart_count',function () {
        $tr = $(this).closest('tr');
        $id = $(this).closest('tr').data('id');
        $count = parseInt($(this).closest('.quantity').find('.input-number').val());
        $.ajax({
            context:$(this),
            url:'{{route('user.countCart',['',''])}}'+'/'+$id+'/'+$count,
            dataType:'json',
            success:function (data) {

                $(this).closest('tr').find('.itemTotal').html( $count*($(this).closest('tr').find('.itemTotal').attr('data-price')) );
                calculateGrandTotal();
                if(data.type=='deleted')
                {
                    $tr.fadeOut();
                    $count = parseInt($('#cartCount').html());
                    $('#cartCount').html('' + ($count-1));
                    toastr.success(data.msg);
                }
                else
                {

                }
            }
        });
    });


    $(document).on('click','.deleteFromCart',function () {
        $tr = $(this).closest('tr');
        $id = $(this).closest('tr').data('id');
        $count = 0;
        $.ajax({
            context:$(this),
            url:'{{route('user.countCart',['',''])}}'+'/'+$id+'/'+$count,
            dataType:'json',
            success:function (data) {

                $(this).closest('tr').find('.itemTotal').html( $count*($(this).closest('tr').find('.itemTotal').attr('data-price')) );
                calculateGrandTotal();
                if(data.type=='deleted')
                {
                    $tr.fadeOut();
                    $count = parseInt($('#cartCount').html());
                    $('#cartCount').html('' + ($count-1));
                    toastr.success(data.msg);
                }
                if(($('#cartCount').html()) == '0'){
                    $('.totalTable').html('<tr> <td colspan="4" class="text-center py-5"> <h3>Empty Cart List</h3> <p>You have no items in your cart list. Start adding!</p> </td> </tr>');
                    $('.checkoutBtnContainer').hide();
                }
            }
        });
    });

    $(function () {
        calculateGrandTotal();
    })
    function calculateGrandTotal() {
        $grandTotal= 0;
        $('.itemTotal').each(function () {
            $grandTotal = $grandTotal + parseFloat($(this).html());
        })
        $('.grandTotal,.allSubTotal').html($grandTotal);
    }

</script>
</body>
</html>