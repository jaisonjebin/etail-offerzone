<!--Flash sale-->
<div class="container-fluid home_flash_sale">
    <div class="col-lg-12">
        <div class="row mb-4">
            <div class="col-xl-12 pl-2 pr-2"><h3>Amazing Flash Sale</h3></div>
            @php
                $i = 0;
            @endphp

            @foreach($flashSales as $flashSale)
                @php
                    if($i < 2){
                        $col = 6;
                    }
                    else{
                        $col = 4;
                    }
                    $i++;
                @endphp
                <div class=" col-lg-{{$col}} col-md-{{$col}} pl-2 p-2">
                    <a href="{{route('flashsaleItem',$flashSale->id)}}">
                        <img src="/uploads/{{$flashSale->small_image}}" class="img-fluid w-100">
                    </a>
                    <div class="col-lg-12 text-right share_icon">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="javascript:void(0)"  data-type="flashSaleItem" data-id="{{$flashSale->id}}" class="btn btn-secondary addtoFavoriteBtn" title="Favorite"> <i class="fas fa-heart"></i></a> </li>
                            <li class="list-inline-item">
                                <div class="dropdown">
                                    <button class="btn btn-secondary "  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-share-alt"></i> </button>
                                    <div  data-shareUrl="{{route('flashsaleItem',$flashSale->id)}}?ref={{getReferalId()}}" class="v_shareParent dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        @include('front.includes.shareDropDownContents')
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            @endforeach

            <div class="col-lg-12 pr-3 text-right  pl-2 pr-2 pt-3"> <a href="{{route('flashsaleItemList')}}"> View All</a> </div>

        </div>

    </div>
</div>
