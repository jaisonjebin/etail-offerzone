<a class="dropdown-item btn v_sharebtn" style="color:#3D5B96;" title="Facebook" data-type="Facebook">
    <i class="fab fa-facebook-f"></i>
</a>
<a class="dropdown-item btn v_sharebtn" style="color:#3399CC;" title="Twitter" data-type="Twitter">
    <i class="fab fa-twitter"></i>
</a>
<a class="dropdown-item btn v_sharebtn" style="color:#DD3F34;" title="Google Plus" data-type="GooglePlus">
    <i class="fab fa-google-plus-g"></i>
</a>

<a class="dropdown-item btn v_sharebtn" style="color: #00b350;" title="Whatsapp" data-type="Whatsapp">
    <i class="fab fa-whatsapp"></i>
</a>