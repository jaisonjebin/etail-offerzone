
<div class="container-fluid home_spicy_deals">
    <div class="col-lg-12">
        <h3>Spicy Deals</h3>
        <!--Spicy Deal Second Line-->
        <div class="row mb-3">
            @foreach($deals as $deal)
                <div class=" col-lg-3 col-md-3 ">
                    <a href="{{route('dealItem',$deal->id)}}">
                        <div class="col-lg-12 border"> <img src="/uploads/{{$deal->small_image}}" class="img-fluid w-100">
                    <div class="col-lg-12 text-right share_icon">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="javascript:void(0)"  data-type="dealItem" data-id="{{$deal->id}}" class="btn btn-secondary addtoFavoriteBtn" title="Favorite"> <i class="fas fa-heart"></i></a> </li>
                            <li class="list-inline-item">
                                <div class="dropdown">
                                    <button class="btn btn-secondary "  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-share-alt"></i> </button>

                                    <div data-shareUrl="{{route('dealItem',$deal->id)}}?ref={{getReferalId()}}" class="v_shareParent dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        @include('front.includes.shareDropDownContents')
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <p>{{$deal->title}}</p>
                    <h5>{{ number_format((($deal->offer_price / ($deal->price??1))*100),0) }}% OFF</h5>
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <p class="pl-0" style="color:#000; font-size:16px; font-weight:600; text-decoration:line-through">Rs {{$deal->price}}</p>
                        </li>
                        <li class="list-inline-item " >
                            <p style="color:#090; font-size:16px; font-weight:600">Rs {{$deal->offer_price}}</p>
                        </li>
                    </ul>
                    <ul class="list-inline pb-0 mb-0">
                        <li class="list-inline-item" style="color:#d61e1e"> <i class="fas fa-clock"></i> </li>
                        <li  class="list-inline-item timer" >
                            <p class="timercount" class="pb-0 mb-0" style="color:#d61e1e" data-endtime="{{ date('M d, Y H:I:s',strtotime($deal->end_time)) }}"></p>
                        </li>
                        <li class="list-inline-item home_spicy_deals_place">
                            <h6>{{$deal->getOwner->address}}</h6>
                        </li>
                    </ul>
                </div>
                    </a>
            </div>
            @endforeach
            <div class="col-lg-12 pr-2 text-right pt-2"> <a href="{{route('deallistDealsCategory','0')}}"> View All</a> </div>
        </div>
    </div>
</div>