<div class="container-fluid home_spicy_deals" style="background-color:#f8f3f3">
    <div class="col-lg-12">
        <h3>Best Offer</h3>
        <div class="row mb-3">
            <div class=" col-lg-3 col-md-3 ">
                <div class="col-lg-12 border"> <img src="/theme/images/flash_sale_slider.jpg" class="img-fluid w-100">
                    <div class="col-lg-12 text-right share_icon">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="#" class="btn btn-secondary " title="Favorite"> <i class="fas fa-heart"></i></a> </li>
                            <li class="list-inline-item">
                                <div class="dropdown">
                                    <button class="btn btn-secondary "  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-share-alt"></i> </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"> <a class="dropdown-item btn" style="color:#3D5B96;" title="Facebook" href="#"><i class="fab fa-facebook-f"></i></a> <a class="dropdown-item btn" style="color:#3399CC;" href="#" title="Twitter"><i class="fab fa-twitter"></i></a> <a class="dropdown-item btn"  style="color:#DD3F34;" title="Google Plus" href="#" ><i class="fab fa-google-plus-g"></i></a> <a class="dropdown-item btn"  style="color:#1884BB;" href="#" title="Linked In"><i class="fab fa-linkedin-in"></i></a> <a class="dropdown-item btn"  style="color:#CC1E2D;" title="Pinterest" href="#" ><i class="fab fa-pinterest-p"></i></a> <a class="dropdown-item btn"  style="color:#FFC90E;" href="#" title="Mail"><i class="fas fa-envelope"></i></a> </div>
                                </div>
                            </li>
                        </ul>
                    </div>        </div>
            </div>
            <div class=" col-lg-3 col-md-3 ">
                <div class="col-lg-12 border"> <img src="/theme/images/discount_slider.jpg" class="img-fluid w-100">
                    <div class="col-lg-12 text-right share_icon">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="#" class="btn btn-secondary " title="Favorite"> <i class="fas fa-heart"></i></a> </li>
                            <li class="list-inline-item">
                                <div class="dropdown">
                                    <button class="btn btn-secondary "  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-share-alt"></i> </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"> <a class="dropdown-item btn" style="color:#3D5B96;" title="Facebook" href="#"><i class="fab fa-facebook-f"></i></a> <a class="dropdown-item btn" style="color:#3399CC;" href="#" title="Twitter"><i class="fab fa-twitter"></i></a> <a class="dropdown-item btn"  style="color:#DD3F34;" title="Google Plus" href="#" ><i class="fab fa-google-plus-g"></i></a> <a class="dropdown-item btn"  style="color:#1884BB;" href="#" title="Linked In"><i class="fab fa-linkedin-in"></i></a> <a class="dropdown-item btn"  style="color:#CC1E2D;" title="Pinterest" href="#" ><i class="fab fa-pinterest-p"></i></a> <a class="dropdown-item btn"  style="color:#FFC90E;" href="#" title="Mail"><i class="fas fa-envelope"></i></a> </div>
                                </div>
                            </li>
                        </ul>
                    </div>        </div>
            </div>
            <div class=" col-lg-3 col-md-3">
                <div class="col-lg-12 border"> <img src="/theme/images/flash sale2.jpg" class="img-fluid w-100">
                    <div class="col-lg-12 text-right share_icon">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="#" class="btn btn-secondary " title="Favorite"> <i class="fas fa-heart"></i></a></li>
                            <li class="list-inline-item">
                                <div class="dropdown">
                                    <button class="btn btn-secondary "  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-share-alt"></i> </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"> <a class="dropdown-item btn" style="color:#3D5B96;" title="Facebook" href="#"><i class="fab fa-facebook-f"></i></a> <a class="dropdown-item btn" style="color:#3399CC;" href="#" title="Twitter"><i class="fab fa-twitter"></i></a> <a class="dropdown-item btn"  style="color:#DD3F34;" title="Google Plus" href="#" ><i class="fab fa-google-plus-g"></i></a> <a class="dropdown-item btn"  style="color:#1884BB;" href="#" title="Linked In"><i class="fab fa-linkedin-in"></i></a> <a class="dropdown-item btn"  style="color:#CC1E2D;" title="Pinterest" href="#" ><i class="fab fa-pinterest-p"></i></a> <a class="dropdown-item btn"  style="color:#FFC90E;" href="#" title="Mail"><i class="fas fa-envelope"></i></a> </div>
                                </div>
                            </li>
                        </ul>
                    </div>        </div>
            </div>
            <div class=" col-lg-3 col-md-3 ">
                <div class="col-lg-12 border"> <img src="/theme/images/flash sale1.jpg" class="img-fluid w-100">
                    <div class="col-lg-12 text-right share_icon">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="#" class="btn btn-secondary " title="Favorite"> <i class="fas fa-heart"></i></a> </li>
                            <li class="list-inline-item">
                                <div class="dropdown">
                                    <button class="btn btn-secondary "  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-share-alt"></i> </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton"> <a class="dropdown-item btn" style="color:#3D5B96;" title="Facebook" href="#"><i class="fab fa-facebook-f"></i></a> <a class="dropdown-item btn" style="color:#3399CC;" href="#" title="Twitter"><i class="fab fa-twitter"></i></a> <a class="dropdown-item btn"  style="color:#DD3F34;" title="Google Plus" href="#" ><i class="fab fa-google-plus-g"></i></a> <a class="dropdown-item btn"  style="color:#1884BB;" href="#" title="Linked In"><i class="fab fa-linkedin-in"></i></a> <a class="dropdown-item btn"  style="color:#CC1E2D;" title="Pinterest" href="#" ><i class="fab fa-pinterest-p"></i></a> <a class="dropdown-item btn"  style="color:#FFC90E;" href="#" title="Mail"><i class="fas fa-envelope"></i></a> </div>
                                </div>
                            </li>
                        </ul>
                    </div>        </div>
            </div>
            <div class="col-lg-12 pr-2 text-right pt-2"> <a href=""> View All</a> </div>
        </div>
    </div>
</div>