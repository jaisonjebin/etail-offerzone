<div class="container-fluid home-winner">
    <h4 class="text-center">LUCKY DRAW WINNERS</h4>
    <div class="col-lg-12 slider_padding">
        <div id="carouselExampleControls4" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active text-center">
                    <div class="row text-center row-margin" >
                        <div class="col-lg-3 col-xl-3 col-md-3">
                            <div class="col-lg-12 winner-bg text-center">
                                <h6>Rashid, Calicut</h6>
                                <img src="/theme/images/winner1.jpg" class="img-fluid">
                                <h3>Samsung Galexy On-next (Gold 64GB) Worth Rs.15000/-</h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 col-md-3">
                            <div class="col-lg-12 winner-bg text-center">
                                <h6>Rashid, Calicut</h6>
                                <img src="/theme/images/winner.png" class="img-fluid">
                                <h3>Samsung Galexy On-next (Gold 64GB) Worth Rs.15000/-</h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 col-md-3">
                            <div class="col-lg-12 winner-bg text-center">
                                <h6>Rashid, Calicut</h6>
                                <img src="/theme/images/winner1.jpg" class="img-fluid">
                                <h3>Samsung Galexy On-next (Gold 64GB) Worth Rs.15000/-</h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 col-md-3">
                            <div class="col-lg-12 winner-bg text-center">
                                <h6>Rashid, Calicut</h6>
                                <img src="/theme/images/winner.png" class="img-fluid">
                                <h3>Samsung Galexy On-next (Gold 64GB) Worth Rs.15000/-</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item text-center">
                    <div class="row text-center row-margin">
                        <div class="col-lg-3 col-xl-3 col-md-3 ">
                            <div class="col-lg-12 winner-bg text-center">
                                <h6>Rashid, Calicut</h6>
                                <img src="/theme/images/winner.png" class="img-fluid">
                                <h3>Samsung Galexy On-next (Gold 64GB) Worth Rs.15000/-</h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 col-md-3">
                            <div class="col-lg-12 winner-bg text-center">
                                <h6>Rashid, Calicut</h6>
                                <img src="/theme/images/winner1.jpg" class="img-fluid">
                                <h3>Samsung Galexy On-next (Gold 64GB) Worth Rs.15000/-</h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 col-md-3">
                            <div class="col-lg-12 winner-bg text-center" >
                                <h6>Rashid, Calicut</h6>
                                <img src="/theme/images/winner.png" class="img-fluid">
                                <h3>Samsung Galexy On-next (Gold 64GB) Worth Rs.15000/-</h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 col-md-3">
                            <div class="col-lg-12 winner-bg text-center">
                                <h6>Rashid, Calicut</h6>
                                <img src="/theme/images/winner1.jpg" class="img-fluid">
                                <h3>Samsung Galexy On-next (Gold 64GB) Worth Rs.15000/-</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls4" role="button" data-slide="prev"> <i class="fa fa-angle-double-left " aria-hidden="true"> </i> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleControls4" role="button" data-slide="next"> <i class="fa fa-angle-double-right" aria-hidden="true"> </i> <span class="sr-only">Next</span> </a> </div>
</div>