
@extends('front.master')

@section('content')

    <!-- Slider top-->
    <div class="container-fluid   p-0" style="overflow:hidden">
        @if($flashSale->banners)
            @php
                $banners = json_decode($flashSale->banners);
            @endphp
            @foreach($banners as $banner)
                <img class="img-fluid w-100" src="/uploads/{{$banner}}" alt="">
                @break
            @endforeach
        @endif
        <div class="col-lg-12 text-center flash_sale_view">

            @if($flashSale->saleStart)
                <div class="col-lg-12">
                    <h4>Congratulations...</h4>
                    <p pb-0>You are here on time and the sale is on now !</p>
                </div>
             @elseif($flashSale->saleEnd)
                <div class="col-lg-12">
                    <h4>Sorry you are late...</h4>
                    <p pb-0>Sale ended !</p>
                </div>
             @else
                <ul class="list-inline">
                    <li class="list-inline-item"> <img src="/theme/images/bell_icon.png" class="img-fluid"  width="80px; " style="margin-top:-30px;"> </li>
                    <li class="list-inline-item">
                        <h4>Be here on time & do not miss</h4>
                        <p pb-0>this flash sale
                            by clicking the <b>'Notify Me'</b> button below</p>
                    </li>
                </ul>
            @endif

        </div>
    </div>
    </div>

    <!--Sponsered by-->

    <div class="container-fluid sponsered_by " style="background-color:#faf7f7; overflow:hidden">

        <div class="row">

            <div class="col-lg-6 col-lg-6">
                <div class="col-lg-12 flash_view_text">
                    <h5 style="font-weight:600"> Product Description</h5>
                    <h6>{{$flashSale->title}} </h6>

                    <p class="pb-0">({{$flashSale->small_description}})</p>
                    <ul class="list-inline mt-0 mb-0">
                        <li class="list-inline-item"><b>Price : </b> &nbsp <span style="text-decoration:line-through !important; color:#f00 ;font-weight:700">&#8377; {{$flashSale->price}} </span> </li>
                        <li class="list-inline-item"><b>&#8377; {{$flashSale->offer_price}}</b> </li>
                        <li class="list-inline-item" style="color:#009f25"><b>You Save :</b> &nbsp <b>&#8377;  {{$flashSale->price - $flashSale->offer_price}}</b> </li>
                    </ul>
                    <p>
                        {!! $flashSale->description !!}
                    </p>
                </div>


                <div class="col-lg-12 mt-5 terms_condition">
                    <h5 style="font-weight:600"> Terms and Condition</h5>
                    <p class="pl-0">
                        {!! $flashSale->termsConditions !!}
                    </p>
                </div>
            </div>
            <div class="col-lg-6 col-lg-6">
                @if($flashSale->sponsers)
                    @php
                        $sponsers = json_decode($flashSale->sponsers);
                    @endphp
                    <div class="col-lg-12">
                        <h3>Sponsered By</h3>
                        @foreach($sponsers as $sponser)
                            <div class=" col-lg-12 p-0"> <img src="/uploads/{{$sponser}}" class="img-fluid w-100"> </div>
                        @endforeach
                    </div>

                @endif
            </div>

        </div>



        <div class="col-lg-12 pt-4 pb-4 text-center ">
			
			@if($flashSale->stock > 0)
				
				<div class="wrapper " style="overflow:hidden">
					{{--@if($flashSale->saleStart)--}}
						{{--<h3>Sale Ends In</h3>--}}
						{{--<time id="count-down" datetime="{{$flashSale->end_time}}"></time>--}}
					{{--@endif--}}
					@if($flashSale->yettoStart)
						<h3>Sale Starts In</h3>
						<time id="count-down" datetime="{{$flashSale->start_time}}"></time>
					@endif
					@if($flashSale->saleEnd)
						<h3>Sale Ended</h3>
					@endif

				</div>
				@if($flashSale->yettoStart)
				<div class="row justify-content-center">
					<div class="col-lg-3 col-md-6 col-sm-4">
						@if(Session::has('type'))
							<label class=" alert alert-{{Session::get('type')}}">
								{!!Session::get('text')!!}
							</label>
						@endif
						
						<form action="{{route('flashSale.notifyme',$flashSale->id)}}" method="post">
						@csrf
						<div class="input-group   m-auto">							
							<input type="text" name="email" class="form-control " id="inlineFormInputGroup" placeholder="Enter Your Email" style="">
							<div class="input-group-append">
								<button class="btn btn-light" type="submit" style="background-color:#0081da; border:none; color:#fff; font-size:15px;" >Notify Me</button>
							</div>
						</div>
						
						</form>
					</div>
				</div>
				@endif

				@if($flashSale->saleStart)
				<div class="row justify-content-center">
					<div class="col-lg-3 col-md-12 col-sm-6  mt-2">
                        <form method="post" action="{{route('user.addtoCart')}}">
                            @csrf
						    <div class="custom-control custom-checkbox  text-left ml-3">
                                <input type="checkbox" class="custom-control-input " id="customControlInline" required>
                                <label class="custom-control-label text-left " for="customControlInline">I agree to Terms and Condition </label>
                            </div>
                            <input type="hidden" class="custom-control-input" name="itemId" value="{{$flashSale->id}}">
							<button class="btn btn-light w-100 buynow " type="sumbit" style="background-color:#0081da; border:none; color:#fff; font-size:15px;" >Buy Now</button>
                        </form>
					</div>
				</div>
				
				
				
    
                <div class="col-xl-12 mt-4 text-center">
                    <div class="circle-wrap ">
                      <div class="circular-bar m-auto">
                        <input type="text" class="dial" data-fgColor="#00bcd4" data-width="150" data-height="150" data-linecap=round  value="{{ 100-((($flashSale->totalstock-$flashSale->sold)*100)/$flashSale->totalstock)}}" >
                        <div class="circular-bar-content">
                          <strong>{{$flashSale->sold}}/{{$flashSale->totalstock}}</strong>
                        </div>
                      </div>
                    </div>
                </div>


				@endif
			@else
				<div class="row justify-content-center">
					<label class=" alert alert-danger">
						Sorry, This product is currently out of stock
					</label>
				</div>
			@endif
			
			
			
			
        </div>
    </div>

    <!-- Subscribe flash sale-->
    @include('front.includes.flashsales')

    <!--Trending Items-->

    @include('front.includes.deals')



@endsection


@push('scripts')

<script>
    // Due to weird things with code pen the following $(document).ready(function(){});
    // code is in here instead being applied in the html.
    // Note: This code should be insterted into the bottom of the html document, you
    // can see where it should be written in the html by looking at the commented out code.
    $(document).ready(function() {
        var current = new Date();

//        ;[ 'Date', 'Hours', 'Minutes' ].forEach(function(val, i) {
//            current['set' + val](current['get' + val]() + (i + 1) * 3);
//        });


        $('#count-down')
//            .attr('datetime', current) // updates the date every time the page loads this is for the example only
            .countdown({
                // wordFormat: "full", // uncomment this for full words
                numberFormat: true
            }, function() {
                // Replace this alert function with your callback function. AKA what you want to happen after the time is up.
                location.reload();

            });
    });

    // Below is the code for the countdown plugin. This code should be saved in a seperate document and linked to the page that it is being used on.

    /* Author : Tyler Benton
     Date : 05/04/12
     jQuery : 1.7.2, works with 2.0.3
     Purpose : light weight countdown timer
     */
    ;(function($) {
        $.fn.countdown = function(options, callback) {
            var obj = this;
            var $obj = $(obj);
            var settings = {
                numberFormat: null
            };
            var interval = '';
            if (options) {
                $.extend(settings, options);
            }
            function init() {
                var newObj = '<ul><li><div class="days"></div></li><li><div class="hours"></div></li><li><div class="minutes"></div></li><li><div class="seconds"></div></li></ul>';
                $obj
                    .append(newObj);
                countdown_process();
            }

            function countdown_process() {
                eventDate = new Date($('#count-down').attr('datetime'));
                eventDate = Date.parse(eventDate) / 1000;
                currentDate = Math.floor($.now() / 1000);
                seconds = eventDate - currentDate;
                $days = $obj.find('.days');
                $hours = $obj.find('.hours');
                $minutes = $obj.find('.minutes');
                $seconds = $obj.find('.seconds');
                days = Math.floor(seconds / (24 * 60 * 60));
                seconds -= days * 24 * 60 * 60;
                hours = Math.floor(seconds / (60 * 60));
                seconds -= hours * 60 * 60;
                minutes = Math.floor(seconds / 60);
                seconds -= minutes * 60;
                if (eventDate <= currentDate) {
                    callback.call(obj);
                    clearInterval(interval);
                    days = 0;
                    seconds = 0;
                    hours = 0;
                    minutes = 0;
                    seconds = 0;
                }
                if (settings.wordFormat === 'full') {
                    days === 1 ? $days.attr('data-interval-text', 'day') : $days.attr('data-interval-text', 'days');
                    hours === 1 ? $hours.attr('data-interval-text', 'hour') : $hours.attr('data-interval-text', 'hours');
                    minutes === 1 ? $minutes.attr('data-interval-text', 'minute') : $minutes.attr('data-interval-text', 'minutes');
                    seconds === 1 ? $seconds.attr('data-interval-text', 'second') : $seconds.attr('data-interval-text', 'seconds');
                } else {
                    days === 1 ? $days.attr('data-interval-text', 'day') : $days.attr('data-interval-text', 'days');
                    hours === 1 ? $hours.attr('data-interval-text', 'hr') : $hours.attr('data-interval-text', 'hrs');
                    minutes === 1 ? $minutes.attr('data-interval-text', 'min') : $minutes.attr('data-interval-text', 'mins');
                    seconds === 1 ? $seconds.attr('data-interval-text', 'sec') : $seconds.attr('data-interval-text', 'secs');
                }

                if (settings.numberFormat) {
                    days = String(days).length >= 2 ? days : '0' + days;
                    hours = String(hours).length >= 2 ? hours : '0' + hours;
                    minutes = String(minutes).length >= 2 ? minutes : '0' + minutes;
                    seconds = String(seconds).length >= 2 ? seconds : '0' + seconds;
                }

                if (!isNaN(eventDate)) {
                    $days.text(days);
                    $hours.text(hours);
                    $minutes.text(minutes);
                    $seconds.text(seconds);
                } else {
                    console.log('Invalid date. Here\'s an example: 16 may 2012 11:59:59');
                    clearInterval(interval);
                }
            }

            init();
            interval = setInterval(countdown_process, 1000);
        };
    })(jQuery); // end plugin

    // $("#your-selector")
    //  .countdown({
    //   date: "6 january 2014 00:00:00",
    //   format: "on",
    //  },function(){
    //   Your call back function
    //  });
</script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js'></script>

<script  src="/theme/js/countown.js"></script>

@endpush
