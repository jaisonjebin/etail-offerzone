
@extends('front.master')

@section('content')
<!-- Slider top-->
<div class="container-fluid flash_sale_checkout py-4">
    <div class="col-lg-12 pr-4 pl-4 flash_sale_checkout12">

        <div class="row">
            <div class="col-lg-12 ">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12 mb-2">

                        <div class="col-lg-12 order-summer">
                            <h5 class="text-center">ORDER SUMMARY </h5>
                            <img src="/uploads/{{$flashSale->small_image}}" class="img-fluid ">
                            <h6><b>{{$flashSale->title}}</b> <br>({{$flashSale->small_description}})</h6>
                            <ul class="list-inline">
                                <li class="list-inline-item">MRP: </li>
                                <li class="list-inline-item text-right float-right"> {{$flashSale->price}} </li>
                            </ul>
                            <ul class="list-inline">
                                <li class="list-inline-item">Discount: </li>
                                <li class="list-inline-item text-right float-right"> {{$flashSale->price - $flashSale->offer_price}} </li>
                            </ul>
                            <ul class="list-inline">
                                <li class="list-inline-item">Delivery Charge : </li>
                                <li class="list-inline-item float-right"> FREE </li>
                            </ul>
                            <ul class="list-inline py-1 mt-2" style="border-top:1px dashed #ccc;border-bottom:1px dashed #ccc;">
                                <li class="list-inline-item">Payeble Amount : </li>
                                <li class="list-inline-item float-right"> {{$flashSale->offer_price}} </li>
                            </ul>
                            <p class="text-center">Your total savings on this flash sale  : {{$flashSale->price -$flashSale->offer_price}}</p>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12 col-sm-12 ">
                        <h5 class="pb-3" style="font-weight:600">Delivery Address</h5>

                        @foreach($dAddress as $addr)
                        <div class="col-xl-12 checkout_delivery_here border mb-3 pt-3 pb-3 addessBlock">
                            <h5><strong><span style="display:none;" data-edit="id">{{$addr->id}}</span> <span data-edit="fullName">{{$addr->fullName}}</span> - <span data-edit="mobile">{{$addr->mobileNumber}}</span></strong></h5>
                            <p><span data-edit="addr1">{{$addr->addr1}}</span><br>
                                <span data-edit="addr2">{{$addr->addr2}}</span>, <span data-edit="city">{{$addr->city}}</span>, <span data-edit="state">{{$addr->state}}</span> - <span class="font-weight-bold"><span data-edit="pincode">{{$addr->pincode}}</span></span>
                                <span style="display:none;" data-edit="landmark">{{$addr->landmark}}</span>
                            </p>
                            <a href="{{route('flashsaleOrder',[$flashSale->id,$addr->id])}}" class="btn btn-primary">Delivery Here</a>
                            <a  class="btn btn-link editAddrBtn">Edit</a>
                            <a  href="{{route('user.deleteDeliveryAddress',$addr->id)}}"  class="btn btn-link delete">Delete</a>
                        </div>
                        @endforeach

                        <div class="col-xl-12 checkout_delivery_here border mb-3 pt-3 pb-3 add_newaddress_btn">
                            <button class="btn btn-secondary w-100 "><i class="fas fa-plus-circle"></i> Add New Address</button>
                        </div>

                        <div class="col-lg-12 border p-4 add_newaddress">
                            <form method="post" action="{{route('user.addDeliveryAddess')}}" class="addDadress">
                                @csrf
                                <input type="hidden" value="0" name="id">
                                <div class="form-group">
                                    <label class="checkout-label" for="inputAddress">Full Name</label>
                                    <input type="text" class="form-control" id="" placeholder="" name="fullName" required>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label class="checkout-label" for="inputEmail4">Mobile Number</label>
                                        <input type="tel" class="form-control" id="" placeholder="" name="mobile" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="checkout-label" for="inputPassword4">Pincode </label>
                                        <input type="tel" class="form-control" id="" placeholder="" required name="pincode">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="checkout-label" for="">House No., Flat, Building, Company ,Appartment</label>
                                    <input type="text" class="form-control" id="" placeholder="" name="addr1" required>
                                </div>
                                <div class="form-group">
                                    <label class="checkout-label" for="inputAddress2">Area, Street, Village, Sector </label>
                                    <input type="text" class="form-control" id="" placeholder="" name="addr2">
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label class="checkout-label" for="inputCity">City</label>
                                        <input type="text" class="form-control" id="inputCity" name="city" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="checkout-label" for="inputState">State</label>
                                        <select id="inputState" class="form-control" required name="state">
                                            <option selected value="">Choose...</option>
                                            <option>Kerala</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="checkout-label" for="inputAddress2">Landmark</label>
                                    <input type="text" class="form-control" id="" placeholder="" name="landmark">
                                </div>
                                <div class="col-lg-12 p-0 text-right">
                                    <button type="submit" class="btn btn-primary">SAVE & DELIVER HERE</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-12 text-center">

                <h5  class="mt-3" style="font-weight:600">SELECT A PAYMENT METHOD </h5>
                <div class="col-lg-12 border p-4 mt-3 "> <img src="/theme/images/Credit-Card-Logos.jpg " width="280" class="img-fluid text-center"><br>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Subscribe flash sale-->
@include('front.includes.flashsales')

<!--Spicy Deals-->
@include('front.includes.deals')

@endsection

@push('scripts')
<script>
    $(document).ready(function() {
//add_newaddress_btn
        $('[name=id]').val('0');
        $('.add_newaddress').hide(300);
        $('.add_newaddress_btn .btn').click(function(){
            $('.addDadress .form-control').val('');
            $('[name=id]').val('0');
            $('.add_newaddress').show(300);
            $('.add_newaddress_btn').hide(300);
        });

    })


    $('.editAddrBtn').on('click',function(){
        $('.add_newaddress').show(300);
        $(this).closest('.addessBlock').find('[data-edit]').each(function(){
            $('.addDadress').find('[name='+$(this).attr('data-edit')+']').val($(this).html()).focus();
        });

    });
</script>
@endpush