<section class="header"><meta name="viewport" content="width=device-width">

    <div class="container-fluid ">
        <div class="row pb-1">
            <div class="col-lg-2 col-md-3 col-sm-2 header_logo pt-2"> <a href="/"><img src="/theme/images/logo.png" class="img-fluid"></a> </div>
            <div class="col-lg-7 col-md-9 col-sm-12 pt-2 header_search">
                <div class="row search">
                    <div class="col-lg-3 col-md-3 pr-0 pl-0 search_category">
                        <div class="form-row align-items-center">
                            <div class="col-auto  w-100">
                                <select class="custom-select mr-sm-2 category" style="border-radius:0" id="inlineFormCustomSelect">
                                    <option selected>All Categories</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 pr-1 pl-0 search_text" >
                        <div class="col-auto p-0">
                            <div class="input-group " >
                                <input type="text" class="form-control " id="inlineFormInputGroup" placeholder="Search in Dealdaa" style="border-radius:0">
                                <div class="input-group-append">
                                    <button class="btn btn-light" type="button"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 pr-0 pl-1 search_location">
                        <div class="col-auto p-0">
                            <div class="input-group mb-2" >
                                <div class="input-group-prepend" >
                                    <div class="input-group-text "><i class="fas fa-map-marker-alt"></i></div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Location" style="border-radius:0">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12 pl-0 ">
                <div class="row">
                    <div class="col-lg-12 col-md-6 social_icons pb-2">
                        <ul class="list-inline mb-0">
                            <li  class="list-inline-item"><a href=""><i class="fab fa-facebook-f" style="color:#4979db"></i></a></li>
                            <li  class="list-inline-item"><a href=""><i class="fab fa-instagram" style="color:#9a4518"></i></a></li>
                            <li  class="list-inline-item"><a href=""><i class="fab fa-twitter" style="color:#33c3ff"></i></a></li>
                            <li  class="list-inline-item"><a href=""><i class="fab fa-google-plus-g" style="color:#ff121c"></i></a></li>
                            <li  class="list-inline-item"><a href=""><i class="fab fa-linkedin-in" style="color:#00a3f6"></i></a></li>
                            <li  class="list-inline-item"><a href=""><i class="fab fa-snapchat-ghost" style="color:#ffbb25"></i></a></li>
                        </ul>
                    </div>

                    <div class="col-lg-12 col-md-6 common_icon">
                        <ul class="list-inline common_icons">
                            <li class="list-inline-item "><a href="{{route('user.profile')}}#v-pills-favorite"><img src="/theme/images/favorite.png" ></a> </li>
                            <li class="list-inline-item header_cart"><span style="position: absolute; margin-left: 23px; padding: 5px;" id="cartCount" class="badge badge-danger rounded-circle">{{isset($cartCount)?$cartCount:0}}</span>
                                <a href="{{route('user.profile')}}#v-pills-cart"><img src="/theme/images/cart.png" ></a></li>
                            <li class="list-inline-item"><a href="{{route('user.wallet')}}"><img src="/theme/images/wallet.png" ></a></li>
                            <li class="list-inline-item search_icon"><a class="search_btn"><img src="/theme/images/search_icon.png" ></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Nav-Bar-->
<section class="menu_bar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-md-12 menu_bar_list">
                <nav class="navbar navbar-expand-lg navbar-light ">
                   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active"> <a class="nav-link" href="{{route('home')}}">HOME</a> </li>
                            <li class="nav-item"> <a href="{{route('flashsaleItemList')}}" class="nav-link">FLASH SALES</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('dealFrontist')}}">DEALS </a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('deallistDealsCategory','deal-of-the-day')}}">DEALS OF THE DAY</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('contact')}}">CONTACT US</a> </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="col-lg-3 col-md-3 pt-2 header_login">

                @auth				
				<div class="dropdown">
					<button class="btn btn-secondary dropdown-toggle"  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}}</button>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
					 <a class="dropdown-item " href="{{route('user.profile')}}"><i class="fas fa-user"></i> My Profile</a>
					  <a class="dropdown-item " href="{{route('user.profile')}}#v-pills-purchase"><i class="fas fa-list"></i> My Purchase</a>
					  <a class="dropdown-item " href="{{route('user.profile')}}#v-pills-favorite" ><i class="fas fa-heart"></i> Favorites</a>
					   <a class="dropdown-item " href="{{route('user.profile')}}#v-pills-team"><i class="fas fa-users"></i> My Team</a>
						<a class="dropdown-item " href="{{route('user.profile')}}#v-pills-notification"><i class="fas fa-bell"></i> Notifications</a>
						<a class="dropdown-item " href="{{route('user.profile')}}#v-pills-rewards"><i class="fas fa-trophy"></i> Rewards</a>
						<a class="dropdown-item " href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> Logout</a>
						 </div>
				  </div>
				  
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                @endauth
                @guest
                <button type="button" class=" btn_main mt-2 " data-toggle="modal" data-target="#loginmodal" style="background-color:transparent; color:#039 ; border:none; outline:none; " >Login/Register</button>
                @endguest
                <!-- Button trigger modal -->
            </div>
        </div>
    </div>
</section>

<!-- [ Modal #1 ] -->
<div class="menu_bar">
    <div class="modal  fade" id="loginmodal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><i class="icon-xs-o-md"></i></button>
                <div class="modal-body p-0 m-auto">
                    <button type="button" class="close clos_login" data-dismiss="modal" aria-label="Close" style="margin-right:-45px; margin-top:-10px; color:#fff;"> <span aria-hidden="true" style="font-size:35px">&times;</span> </button>
                    <div class="row pl-0 p-0">
                        <div class="col-lg-6  login">
                            <h4 class="text-center">LOGIN</h4>
                            <form method="POST" action="{{ route('login')}}">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="" aria-describedby="emailHelp" placeholder="Enter email or mobile">
                                </div>
                                <div class="form-group">
                                    <input type="password"  name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <div class="col-lg-12 text-right p-0"><button type="button" class=" " data-toggle="modal" data-target="#demo-2" data-dismiss="modal" style="background-color:transparent ; border:none; box-shadow:none !important; color:#000;">Forgot Password ?</button></div>
                                <div class="col-lg-12 p-0">
                                    <input type="Submit" class="btn login_btn w-100" value="Submit" style="color:#fff; background-color:#039">
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-6 register">
                            <h4>REGISTER</h4>
                            @if ($errors->any())

                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <script>
                                    document.addEventListener("DOMContentLoaded", function(event) {
                                        $('#loginmodal').modal('show');
                                    });
                                </script>

                            @endif
                            <form action="{{route('user.register')}}" type="post">
                                <div class="form-group">
                                    <input required name="name" value="{{ old('name') }}" ="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <input  name="email" value="{{ old('email') }}" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email or Mobile">
                                </div>
                                <div class="form-group">
                                    <input required name="password" value="{{ old('password') }}"  type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <input required name="password_confirmation" value="{{ old('password_confirmation') }}" type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password">
                                </div>
                                <div class="form-group" >
                                    <input required name="phone" value="{{ old('mobile') }}"  type="tel" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Mobile Number" >
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <a class="btn w-100 sentOtp">Send OTP</a>
                                    </div>
                                    <div class="form-group col-md-7">
                                        <input required name="otp" value="{{ old('otp') }}"  type="tel" class="form-control otp_number" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter OTP" >
                                    </div>
                                </div>
                                <div class="form-check text-left">
                                    <input type="checkbox" required class="form-check-input termsCondtions">
                                    <small id="emailHelp" class="form-text text-muted" >I agree with these <a href="" target="_blank">terms of use</a> </small> </div>
                                    <button class="btn w-100 mt-2 submit disabled">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- [ Modal #2 ] -->
    <div class="modal fade" id="demo-2" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><i class="icon-xs-o-md"></i></button>
                <div class="modal-body p-0 m-auto" >
                    <button type="button" class="close clos_login" data-dismiss="modal" aria-label="Close" > <span aria-hidden="true" style="font-size:35px">&times;</span> </button>
                    <div class="row p-0">
                        <div class="col-lg-6  forgot ">
                            <h4 class="text-center">FORGOT PASSWORD</h4>
                            <h5>Enter Your Mobile Number<br>
                                You Will Get An OTP <br>
                                And Reset Your Password</h5>
                        </div>
                        <div class="col-lg-6 col-md-12 reset_password">
                            <form>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="Enter Mobile Number" placeholder="Enter email or mobile">
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" id="exampleInputPassword1" placeholder="OTP">
                                </div>
                                <div class="col-lg-12 pr-0 mb-2 text-right" style="margin-top:-8px;"> <a href="">Resend</a> </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" id="exampleInputPassword1" placeholder="New Password">
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password">
                                </div>
                                <button type="submit" class="btn w-100  login_btn">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>