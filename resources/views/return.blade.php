<!DOCTYPE html>
<html>
<head>
    <title>dealdaa</title>
</head>
<body><center>
    <h3>Refunds/Cancellations</h3></center>
<p>Thank you very much for shopping at dealdaa.com.
    If in case you are not satisfied with the purchase from our website, please let us help you with a fair return policy.

    Items won in the lucky draw contest or a deal coupon purchased is non-returnable and non-refundable.

    If you want to return and refund an item you purchased in flash sale, please go through the below conditions and return the product accordingly.
</p>
Returns <p>

    You have 15 days to return an item that you purchased from us from the date you received it.
    To be eligible for a return and refund,
    -	Your item must be unused
    -	Your item must be in the same condition as you received it
    -	Your item must be in the original packaging
    -	Your item needs to have the receipt or proof of purchase
</p>
Refunds <p>

    As soon as we receive your item, we will inspect it in detail and notify you that we have received your returned item.
    We will immediately notify you on the status of your refund after inspecting the item.
    If your return is approved, we will initiate a refund to your credit card (or original method of payment).
    You will receive the credit within a certain amount of days, depending on your card issuer's policies.
</p>Shipping <p>

    You will be responsible for paying for your own shipping costs for returning your item. Shipping costs are non-refundable. If you receive a refund, the cost of return shipping will be deducted from your refund.
</p>
Contact Us
<p>
    If you have any questions on how to return your item to us, contact us.
</p>
</body>
</html>